<div class="col-md-3">
   <div class="top-categories">
      <h3>Top Categories</h3>
      <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
         @foreach($categories as $category)
               <div class="dropdown">
                  <a class="dropdown-btn nav-link" href="@if($category->children->isEmpty()) {{ route('productCategoryListingCat.product',$category->id)}} @else # @endif" id="{{$category->id}}" role="tab">{{$category->name}}
                     @if(!$category->children->isEmpty())
                        <i class="fa fa-caret-down"></i>
                        <div class="dropdown-container">
                           @foreach($category->children as $subcats)
                              <a  href="@if($subcats->children->isEmpty()) {{url('productSubSubCategory',$subcats->id)}} @else # @endif" id="{{$subcats->id}}" class="dropdown-btn1" role="tab">{{$subcats->name}}
                                 @if(!$subcats->children->isEmpty())
                                    <i class="fa fa-caret-down"></i>
                                    <div class="dropdown-container1">
                                       @foreach($subcats->children as $sub)
                                          <a href="{{route('productCategoryListingCat.product',$sub->id)}}" id="{{$sub->id}}" class="subcategoryid">{{$sub->name}}
                                       @endforeach
                                    </div>
                                 @endif
                              </a>
                           @endforeach
                        </div>
                     @endif
                  </a>
               </div>
         @endforeach
      </div>
   </div>
</div>