      <section class="support opacityclass" id="support">
         <div class="container">
            <div class="support-area text-center">
               <div class="row">
                  <div class="col-md-4 col-sm-6">
                     <div class="support-single">
                        <span class="fa fa-clock-o"></span>
                        <h5>30 DAYS MONEY BACK</h5>
                        <!-- <P>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, nobis.</P> -->
                     </div>
                  </div>
                  <div class="col-md-4 col-sm-6">
                     <div class="support-single">
                        <span class="fa fa-truck"></span>
                        <h5>FREE SHIPPING</h5>
                        <!-- <P>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, nobis.</P> -->
                     </div>
                  </div>
                  <div class="col-md-4 col-sm-6">
                     <div class="support-single">
                        <span class="fa fa-phone"></span>
                        <h5>SUPPORT 24/7</h5>
                        <!-- <P>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, nobis.</P> -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

 
<!-- footer start -->
    <footer class="opacityclass">
        <div class="footer-top">
            <div class="container">
                <div class="footer-top-area">
                    <div class="row ftr">
                        <div class="col-lg-2 col-md-6 col-sm-6 mb-3">
                            <div class="footer-widget">
                                <h4>Home</h4>
                                <hr>
                                <ul class="footer-nav">
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">Men</a></li>
                                    <li><a href="#">Women</a></li>
                                    <li><a href="#">Kids</a></li>
                                    <li><a href="#">Blog</a></li>
                                    <li><a href="{{route('contactus')}}">Contact</a></li>
                                    <li><a href="{{route('aboutus')}}">About Us</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-6">
                            <div class="footer-widget ftr_ctnus">
                                <div class="footer-widget">
                                    <h4>Knowledge Base</h4>
                                    <hr>
                                    <ul class="footer-nav">
                                        <li><a href="#">Delivery</a></li>
                                        <li><a href="#">Returns</a></li>
                                        <li><a href="#">Services</a></li>
                                        <li><a href="#">Discount</a></li>
                                        <li><a href="#">Special Offer</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-6">
                            <div class="footer-widget">
                                <div class="footer-widget">
                                    <h4>Useful Links</h4>
                                    <hr>
                                    <ul class="footer-nav">
                                        <li><a href="#">Site Map</a></li>
                                        <li><a href="#">Search</a></li>
                                        <li><a href="#">Advanced Search</a></li>
                                        <li><a href="#">Suppliers</a></li>
                                        <li><a href="#">FAQ</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="footer-widget">
                                <div class="footer-widget">
                                    <h4>Contact Us</h4>
                                    <hr>
                                    <div class="contact text-white">
                                        <ul>
                                            <li>
                                                <a href="tel:9876543112"><span class="fa fa-phone-square mr-3"></span>Phone : 9876543112</a>
                                            </li>
                                            <li>
                                                <a href="mailto:info@nilemart.com"><span class="fa fa-envelope-o mr-3"></span>Mail : info@nilemart.com</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="footer-social">
                                    <h4>Keep in Touch</h4>
                                    <hr>
                                        <ul>
                                            <li>
                                            <a href="#"><span class="fa fa-facebook"></span></a> 
                                            </li>
                                            <li>
                                            <a href="#"><span class="fa fa-twitter"></span></a> 
                                            </li>
                                            <li>
                                            <a href="#"><span class="fa fa-google-plus"></span></a> 
                                            </li>
                                            <li>
                                            <a href="#"><span class="fa fa-youtube"></span></a> 
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="footer-widget">
                                <div class="footer-widget">
                                    <h4>Download our App</h4>
                                    <hr>
                                    <ul class="footer-nav">
                                        <li><a href="#"><button class="btn btn-play-store"></button></a></li>
                                        <li><a href="#"><button class="btn btn-app-store"></button></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-sm-12 text-md-center">
                    <h3>Download our App</h3>
                    
                    
                </div> -->
                <div class="col-sm-12">
                    <p>© <span id="year">2020</span>, NileMart. All Rights Reserved.</p>
                  </div>
            </div>
        </div>
    </footer>
    <!-- footer end -->
    <script>
        document.getElementById("year").innerHTML = new Date().getFullYear();
    </script>

    <script type="text/javascript">
        $("#pre-loader").hide();
        $(document).ready(function() {
            $("#pre-loader").fadeOut("fast");
        });
    </script>