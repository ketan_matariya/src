<!doctype html>
<html lang="en">

<head>
    <title>Nilemart | FAQs</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @include('layouts.front.head')

    <style type="text/css">
        #data {
  display: none;
  background: #f1f3f4;
}
.click-me {
  cursor: pointer;
}
    </style>
</head>

<body>

    <div class="main-div">
        @include('layouts.front.header-cart')

    <section class="about-us-main">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h3 class="product-head">FAQs</h3>
                        <hr class="product-head">
                        <br>
                        @foreach($faq as $faqs)
                        <div>
                            <span style="color: #0b3b69; argin: 10px 0; font-weight: 600; font-size: 20px;" class="click-me" id="{{$faqs->id}}">{{$faqs->question}}</span>
                        </div>
                        <br>
                        <br>
                        <div class="fade-me{{$faqs->id}}" id="data">
                            <p>{{$faqs->answer}}</p>
                        </div>
                        <br>
                        @endforeach
                </div>
                <div class="col-md-4 text-center">
                    <div class="about-image">
                        <img src="{{url('css/front/images/about-us-icon.png')}}" alt="about">
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('layouts.front.footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="{{ url('css/front/js/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ url('css/front/js/bootstrap.min.js') }}"></script>
    <script src="{{ url('css/front/js/webslidemenu.js') }}"></script>
    <script src="{{ url('css/front/js/wow.js') }}"></script>
    <script src="{{ url('css/front/js/owl.carousel.min.js') }}"></script>
    <script src="{{ url('css/front/js/jquery.themepunch.plugins.min.js') }}"></script>
    <script src="{{ url('css/front/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ url('css/front/js/function.js') }}"></script>    
    <script type="text/javascript">
        $(document).ready(function() {
            $('.click-me').on('click', function() {
                var id = this.id;
                    $('.fade-me'+id).fadeToggle(1000);    
            });
        });
    </script>
</body>

</html>