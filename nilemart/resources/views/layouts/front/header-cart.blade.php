
<div class="top-orange opacityclass">
   <div class="container-custom d-flex justify-content-between align-items-center" style="height: 30px;">
      <div class="contact w-50 d-flex align-items-center justify-content-start">
         <div class="cntc-mail">
            <a href="tel:9876543112" class="pl-0"><span class="fa fa-phone mr-3 "></span>9876543112</a>
         </div>
         <div class="ml-2 border-left pl-2 cntc-mail">
            <a href="mailto:info@nilemart.com"><span class="fa fa-envelope-o mr-3"></span>info@nilemart.com</a>
         </div>
      </div>
      <div class=" d-flex align-items-center justify-content-end">
         <div class="social-icon-header d-flex align-items-center justify-content-end">
            <ul>
               <li>
                  <a href="#"><span class="fa fa-facebook"></span></a>
               </li>
               <li>
                  <a href="#"><span class="fa fa-twitter"></span></a>
               </li>
               <li>
                  <a href="#"><span class="fa fa-google-plus"></span></a>
               </li>
               <li>
                  <a href="#"><span class="fa fa-youtube"></span></a>
               </li>
            </ul>
         </div>
         <div class="other-links ml-3 ">
            <ul class="nav nav-lnk">
               @if(auth()->check())
               <li class="nav-item myacc">
                  <a class="nav-link text-white" href="{{route('profile')}}"><i class="fa fa-user mr-2" aria-hidden="true"></i> My Account</a>
               </li>
               <li class="nav-item nav-lnk-lgin">
                  <a class="nav-link text-white" href="{{url('logout')}}"><i class="fa fa-sign-out mr-2" aria-hidden="true"></i> Logout</a>
               </li>
               @else
               <li class="nav-item nav-lnk-lgin1">
                  <a class="nav-link text-white" href="{{route('userLogin')}}"><i class="fa fa-sign-in mr-2" aria-hidden="true"></i> Login</a></li>
               @endif
               <!-- <li class="nav-item">
                  <a class="nav-link cart-hover" href="{{route('checkout.product')}}"><i class="fa fa-shopping-cart mr-2" aria-hidden="true"></i> Cart
                     <span class="count">2</span>
                     <div class="cart-hover-dropdown">
                        <div class="order-header d-flex w-100 justify-content-lg-between align-items-center">
                           <span>Order Summary</span>
                           <span>2 item(s)</span>
                        </div>
                        <hr>
                        <ul class="cart-ul">
                           <li>
                              <p>Amul Pasteurised Butter 500 g (Carton)</p>
                              <span>&#8377; 30.00 x 1</span>
                           </li>
                           <li>
                              <p>Mosambi 1 kg</p>
                              <span>&#8377; 30.00 x 1</span>
                           </li>
                        </ul>
                        <hr>
                        <div class="cart-footer d-flex justify-content-between">
                           <div class="price-total">
                              <span>&#8377; 60.00</span>
                              <p>you save &#8377; 20.00</p>
                           </div>
                           <div class="p-to-cart">
                              <button>Proceed to cart</button>
                           </div>
                        </div>
                     </div>
                  </a>
               </li> -->
            </ul>

         </div>
      </div>
   </div>
</div>
<header id="myHeader" class="opacityclass">
   <div class="container-custom opacityclass">
      <div class="top-header opacityclass">
         <div class="logo d-flex align-items-center">
            <!-- <span class="menu-pointer mr-3" onclick="openNav()">&#9776; </span> -->
            <a href="{{route('home')}}">
               <img class="main-logo" src="{{url('css/front/images/final-logo.png')}}" alt="">
               <img class="small-logo" src="{{url('css/front/images/final-logo.png')}}" alt="">
            </a>
            <div class="align-items-center shop-by-div ml-3">
               <p class="mr-3 mb-0 text-dark font-weight-bold">SHOP BY <br> CATEGORY</p>
               <div class="dropdown-menu1 shop-by"><!--open-->
                  <a href="#" class="dropdown-toggle mr-3 drp-mnu-upper" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bars"></i></a>
                  <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">

                     @foreach($categories as $category)

                     <li class="{{(count($category->children)>0) ? 'dropdown-submenu':'dropdown'}}">
                        <a tabindex="-1" href="{{route('productCategoryListingCat.product',$category->id)}}">{{$category->name}}</a>
                        @if(!$category->children->isEmpty())
                        <ul class="dropdown-menu">
                           @foreach($category->children as $subcats)
                            
                           <li class="{{(count($subcats->children)>0) ? 'dropdown-submenu':'dropdown'}}">
                               @if($subcats->children->isEmpty())

                                    <a tabindex="-1" href="{{ route('productCategoryListingCat.product',$subcats->id)}}">{{$subcats->name}}</a>
    
                                    @else
                                       <a tabindex="-1" href="{{ url('productSubSubCategory',$subcats->id)}}">{{$subcats->name}}</a>
                                @endif
                                    
                            @if(!$subcats->children->isEmpty())
                              <ul class="dropdown-menu">
                              @foreach($subcats->children as $sub)
                            
                                 <li class="dropdown"><a tabindex="-1"  href="{{route('productCategoryListingCat.product',$sub->id)}}">{{$sub->name}}</a></li>
                             
                              @endforeach
                           </ul>
                           @endif
                           </li>
                           @endforeach
                        </ul>
                        @endif
                     </li>
                     @endforeach
                  </ul>
               </div>
            </div>
         </div>
         <nav class="navbar mobile-cat">
            <div class="navbar-header">
               <a href="{{route('home')}}">
                  <img class="main-logo" src="{{url('css/front/images/final-logo.png')}}" alt="">
               </a>
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
               </button>
               <div class="collapse navbar-collapse w-100" id="myNavbar">
                  
                  <ul class="nav navbar-nav">
                     <li class="active"><a href="{{route('home')}}">Home</a></li>
                     <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Cat-1<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                           <li><a href="#">sub-cat</a></li>
                           <li><a href="#">sub-cat</a></li>
                           <li><a href="#">sub-cat</a></li>
                        </ul>
                     </li>
                     <li><a href="#">Page 2</a></li>
                     <li><a href="#">Page 3</a></li>
                  </ul>
                  <ul class="nav navbar-nav navbar-right">
                     <li><a href="#">Sign Up</a></li>
                     <li><a href="#">Login</a></li>
                  </ul>
               </div>
            </div>
         </nav>

         <div class="center-search-link">
            <div>
               <div class="search w-100">
                  <div class="input-group">
                     <input type="text" name="query" id="product_name__search" class="form-control disablebutton" placeholder="Search" autocomplete="off">
                     <div class="input-group-append">
                        <span class="input-group-text"><a href=""><span class="fa fa-search " aria-hidden="true"></span></a>
                        </span>
                     </div>
                  </div>
               </div>
               <div id="product__List"></div>
               <div class="menu-part">
                  <div class="wsmenucontainer clearfix">
                     <div class="overlapblackbg">
                     </div>
                     <div class="wsmain ">
                        <nav class="wsmenu">
                           <ul class="mobile-sub mt-3 mb-0">
                              <li class="{{(request()->is('home'))? 'active':''}}">
                                 <a href="{{route('home')}}">Home</a>
                              </li>
                              <li class="{{(request()->is('aboutus'))? 'active': ''}}">
                                 <a href="{{route('aboutus')}}">About us</a>
                              </li>
                              <li class="{{(request()->is('productListing.product'))? 'active':''}}">
                                 <a href="{{route('productListing.product')}}">Shop</a>
                              </li>
                              <li class="{{(request()->is('contactus'))? 'active': ''}}">
                                 <a href="{{route('contactus')}}">Contact us</a>
                              </li>
                           </ul>
                        </nav>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="other-links-1">
            <a class="nav-link cart-hover cart_login" href="{{route('checkout.product')}}">
               
               @php
               if(auth()->check() == "true")
               {
                  $getCntCart=DB::table('cart')->where('customer_id',auth()->user()->id)->count();
                  
               @endphp
               <div class="cart-image">
                  <!-- <img src="{{url('css/front/images/grocery.png')}}" alt="images"> -->
                  <i class="fa fa-shopping-basket" aria-hidden="true"></i>
               </div>
                  <div> My Basket <br> Items <span id="items">{{$getCntCart}}</span></div>
               @php
               }
               else{
                  $getCntCart=0;

                  @endphp
                  
                  <div class="cart-image cart-img-new">
                  <!-- <img src="{{url('css/front/images/grocery.png')}}" alt="images"> -->
                     <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                  </div>
                  <div><a class=" cart-hover clr-bsct" href="{{route('userLogin')}}"> My Basket <br> Items <span id="items">{{$getCntCart}}</span></a></div>
                  @php
               }  
                 
               
               @endphp
              
            </a>
         </div>
         <div id="mySidenav" class="sidenav">
            <ul class="side-nav-list">
               <li>
                  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
               </li>
               <li>
                  <div class="topbar">
                     <img src="{{url('css/front/images/bg.png')}}" alt="">
                     <h2>Hello, Sign in</h2>
                     <button class="ao-buttons">Account</button>
                     <button class="ao-buttons">Orders</button>
                  </div>
               </li>
               <hr>
               <li class="side-nav-item">
                  <a class="side-nav-link" href="">Home</a>
               </li>
               <li class="side-nav-item">
                  <a class="side-nav-link" href="">Shop by Category</a>
               </li>
               <li class="side-nav-item">
                  <a class="side-nav-link" href="">All Offers</a>
               </li>
               <hr>
               <li class="side-nav-item">
                  <a class="side-nav-link" href="">Your Account</a>
               </li>
               <li class="side-nav-item">
                  <a class="side-nav-link" href="">About Us</a>
               </li>
               <hr>
               <li>
                  <div class="contact-us">
                     <h3>Contact Us</h3>
                     <div class="my-3">
                        <span>Call us on :</span>
                        <a href="javascript:void(0)">9876543112</a>
                     </div>
                     <div class="my-3">
                        <span>Mail us on :</span>
                        <a href="javascript:void(0)">info@nilemart.com</a>
                     </div>
                  </div>
               </li>
               <hr>
               <li>
                  <h3>Download Our App</h3>
                  <div class="w-100">
                     <button class="btn mx-0 my-2 btn-play-store"></button>
                     <button class="btn mx-0 my-2 btn-app-store"></button>
                  </div>
               </li>
            </ul>
         </div>
      </div>
   </div>
   <div class="container">
      <div class="search none my-3">
         <div class="input-group">
            <input type="text" class="form-control" placeholder="Search">
            <div class="input-group-append">
               <span class="input-group-text">
                  <a href=""><span class="fa fa-search" aria-hidden="true"></span></a>
               </span>
            </div>
         </div>
      </div>
   </div>
   <div class="bottom-header">
      <!-- <div class="menu-part">
         <div class="container">
            <div class="row">
               <div class="col-sm-12">
                  <div class="wsmenucontainer clearfix">
                     <div class="overlapblackbg">
                     </div>
                     <div class="wsmobileheader clearfix">
                        <a id="wsnavtoggle" class="animated-arrow"><span></span></a>
                        <a href="index.html" class="smallogo"><img src="{{url('css/front/images/final-logo.png')}}" width="54" alt="logo" /></a>
                        <a class="callusicon" href=""><i class="fa fa-shopping-cart"></i></a>
                        <a class="callusicon sign" href="" data-toggle="modal" data-target="#LoginSignup"><img src="{{url('css/front/images/sign-in.svg')}}" alt=""></a>
                     </div>
                     <div class="wsmain ">
                        <nav class="wsmenu clearfix">
                           <div class="respons-menu-logo">
                              <img src="{{url('css/front/images/final-logo.png')}}" width="50">
                           </div>
                           <ul class="mobile-sub wsmenu-list">
                              @foreach($categories as $category)
                              <li>
                                 <a href="#">{{$category->name}} <span class="arrow"></span></a>
                                 <div class="megamenu clearfix">
                                    <ul class="link-list">
                                       <li>
                                          <a href="#">
                                             <i class="fa fa-tag mr-3" aria-hidden="true"></i>
                                             <span>Apple</span>
                                             <span class="arrow text-black"></span>
                                          </a>
                                          <div class="megamenu-one clearfix">
                                             <ul class="link-list">
                                                <li>
                                                   <a href="#">
                                                      <i class="fa fa-tag mr-3" aria-hidden="true"></i>
                                                      <span>Apple</span>
                                                      <span class="arrow text-black"></span>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                       </li>
                                    </ul>
                                 </div>
                              </li>
                              @endforeach
                           </ul>
                        </nav>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div> -->
      <!-- <div class="menu-part">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="wsmenucontainer clearfix">
                     <div class="overlapblackbg">
                     </div>
                     <div class="wsmobileheader clearfix">
                        <a href="{{route('home')}}" class="smallogo"><img src="{{url('css/front/images/final-logo.png')}}" width="54" alt="logo" /></a>
                        <a class="callusicon" href=""><i class="fa fa-shopping-cart"></i></a>
                        <a class="callusicon sign" href=""><i class="fa fa-sign-in" aria-hidden="true"></i></a>
                     </div>

                     <div class="wsmain ">
                        <nav class="wsmenu clearfix">
                           <div class="respons-menu-logo">
                              <img src="{{url('css/front/images/final-logo.png')}}" width="50">
                           </div>
                           <ul class="mobile-sub wsmenu-list">

                              @foreach($categories as $category)
                              <li>
                                 <a href="{{route('productCategoryListingCat.product',$category->id)}}">{{$category->name}}
                                    @if(!$category->children->isEmpty())<span class="arrow"></span>@endif</a>
                                 @if(!$category->children->isEmpty())
                                 <div class="megamenu clearfix">
                                    @foreach($category->children as $subcats)
                                    <ul class="col-sm-3 link-list">
                                       <li><a href="{{route('productCategoryListing.product',$subcats->id)}}"><i class="fa fa-tag"></i>{{$subcats->name}}</a></li>
                                    </ul>
                                    @endforeach
                                 </div>
                                 @endif
                              </li>
                              @endforeach
                           </ul>
                        </nav>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div> -->
   </div>
</header>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script>
   function openNav() {
      document.getElementById("mySidenav").style.width = "300px";
   }

   function closeNav() {
      document.getElementById("mySidenav").style.width = "0";
   }

   $(document).ready(function() {
      $('#product_name__search').keyup(function() {
         var query = $(this).val();
         console.log(query);
         if (query != '') {
            $.ajax({
               url: "{{ route('getProductSearch') }}",
               method: "POST",
               data: {
                  "query": query,
                  "_token": "{{ csrf_token() }}"
               },
               success: function(data) {
                  console.log(data);
                  $('#product__List').fadeIn();
                  $('#product__List').html(data);
               }
            });
         }
      });
      $(document).on('click', '#product__List .dropdown-menu li', function() {
         $('#product_name__search').val($(this).text());
         $('#product__List').fadeOut();
      });
    
   });
</script>