<meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>NileMart</title>
      <meta name="description" content=""/>
      <meta name="keywords" content=""/>
      <!-- Bootstrap -->
      <link rel="shortcut icon" type="img/ico" href="{{url('25x25.png')}}">
      <link rel="stylesheet" href="{{url('css/front/css/bootstrap.min.css')}}">
      <link rel="stylesheet" type="text/css" media="all" href="{{url('css/front/css/webslidemenu.css')}}" />
      <link rel="stylesheet" href="{{url('css/front/css/animate.css')}}">
      <link href="{{url('css/front/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
      <link rel="stylesheet" type="text/css" href="{{url('css/front/css/settings.css?rev=4.0.1')}}" media="screen">
      <link href="{{url('css/front/css/owl.carousel.css')}}" rel="stylesheet">
      <link href="{{url('css/front/css/owl.theme.css')}}" rel="stylesheet">
      <link href="{{url('css/front/css/style.css')}}" rel="stylesheet" type="text/css">
      <link href="{{url('css/front/css/responsive.css')}}" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
      <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet"> -->
      <link href="https://fonts.googleapis.com/css?family=Muli:300,400,600,700" rel="stylesheet">
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.10.0/sweetalert2.min.css"  />

       <style type="text/css">
      #pre-loader {
background-color: transparent;
height: 100%;
width: 100%;
position: fixed;
z-index: 1;
margin-top: 0px;
top: 0px;
left: 0px;
bottom: 0px;
overflow: hidden !important;
right: 0px;
z-index: 999999;
background: rgba(36, 36, 36, 0.72);
}
#pre-loader img {
text-align: center;
left: 0;
position: absolute;
right: 0;
top: 33%;
transform: translateY(-50%);
-webkit-transform: translateY(-50%);
-o-transform: translateY(-50%);
-ms-transform: translateY(-50%);
-moz-transform: translateY(-50%);
z-index: 99;
margin: 0 auto;
}

.rotateImg {
-webkit-animation: rotation 2s infinite linear;
animation: rotation 2s infinite linear;
height: 62px !important;
width: 62px !important;
}
  </style>

                  <div id="pre-loader" style="display:none;">
                    <img src="~/Themes/images/pre-loader/loader-01.png" alt="">
                </div>