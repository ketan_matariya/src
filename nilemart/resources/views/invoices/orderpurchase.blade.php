<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Order Purchase Invoice</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="icon" type="image/png" sizes="25x25" href="{{ asset('25x25.png') }}">
    <style type="text/css">
        table { border-collapse: collapse;}
    </style>
</head>
<body>
    <section class="row">
        <div style="margin-top:3%; text-align: center">
            <img class="logofull" src="{{ 'http://nilemart.co.in/nilemarticon.png' }}" width="auto" height="100px">
        </div>
    </section>

    <section class="row">
        <div style="margin-left: 60%; margin-top: 2%">
            Invoice No. {{$products->invoice_no}} <br />
            Invoice to: {{$products->seller_name}} <br />
            Mobile No.{{$products->mobile}} <br />
            Deliver to: 
            <div style="margin-left: 27%">{{ $products->address }} </div>
            Date : {{ date('M d, Y', strtotime($products['created_at'])) }}
           

        </div>
        <div class="pull-right">
            From: {{config('app.name')}}<br>
        </div>
    </section>

    <section class="row">
        <div class="col-md-12">
            <h3 style="text-align: center;">Purchase Product Details</h3>
            <table class="table table-striped" width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: center;">Product Code</th>
                        <th style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: center;">Name</th>
                        <th style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: center;">Unit</th>
                        <th style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: center;">Quantity</th>
                        <th style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: center;">Price</th>
                        <th style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;">Sale Price</th>
                       
                    </tr>
                </thead>
                <tbody>
                @php
                    $tq=0;
                @endphp

                @foreach($orders as $order)
                                
                    <tr>
                        <td style="width: 18%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">{{$order->product_code}}</td>
                        <td style="width: 18%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">{{$order->name}}</td>
                        <td style="width: 10%; border: 1px solid black; border-collapse: collapse; margin-left:10px;text-align: center;">{{$order->unit_val}}{{$order->unit_key}}</td>
                        <td style="width: 12%; border: 1px solid black; border-collapse: collapse;text-align: center;">{{$order->quantity}}</td>
                        <td style="width: 12%; border: 1px solid black; border-collapse: collapse;text-align:right; margin-right:10px;">{{$order->price}}</td>
                        <td style="width: 12%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$order->sale_price	}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="box">
            <div class="box-body">
                <h3 style="text-align: center;">Order Puchase Information</h3>
                <table class="table  table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;"><strong>Date</strong></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;"><strong>Seller Name</strong></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;"><strong>Order No</strong></td>
                           
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;">{{ date('M d, Y', strtotime($products['created_at'])) }}</td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;">{{ $products->seller_name }}</td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;">{{ $products->order_no }}</td>
                        
                    </tr>
                    </tbody>
                    <tfoot>
                    
                    
                </tfoot>
            </table>
        </div>
    </section>

  

</body>
</html>
                    