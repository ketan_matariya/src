<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Order Invoice</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="icon" type="image/png" sizes="25x25" href="{{ asset('25x25.png') }}">
    <style type="text/css">
        table { border-collapse: collapse;}
    </style>
</head>
<body>
    <section class="row">
        <div style="margin-top:3%; text-align: center">
            <img class="logofull" src="{{ 'http://nilemart.co.in/nilemarticon.png' }}" width="auto" height="100px">
        </div>
    </section>
    <section class="row">
        <div style="margin-left: 60%; margin-top: 2%">
            Invoice to: {{$customer->name}} <br />
            Mobile No.{{$address->phone}} <br />
            Deliver to: <strong>{{ $address->alias }} <br /></strong>
            <div style="margin-left: 27%">{{ $order_address->address }} <br />
            {{ $order_address->landmark }} {{ $address->country_id }}<br />
            {{ $order_address->postcode }} </div>
            <!--<div style="margin-left: 27%">{{ $address->address_1 }} <br />-->
            <!--@if(isset($address->village_id)){{ $address->village_id }}@endif @if(isset($address->tehsil_id)){{ $address->tehsil_id }}@endif <br />-->
            <!--@if(isset($address->city)){{ $address->city }}@endif @if(isset($address->state_code)){{ $address->state_code }}@endif </div>-->

        </div>
        <div class="pull-right">
            From: {{config('app.name')}}<br>
        </div>
    </section>
    <section class="row">
        <div class="col-md-12">
            <h3 style="text-align: center;">Product Details</h3>
            <table class="table table-striped" width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <!--<th style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: center;">HSN Code</th>-->
                        <th style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: center;">Name</th>
                        <th style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: center;">Unit</th>
                        <th style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: center;">Quantity</th>
                        <th style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: center;">Price</th>
                        <th style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;">Shipping Price</th>
                        <!--<th style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: center;">Tax</th>-->
                        <th style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: center;">Total</th>
                        @if($offer_status==1)
                            <th style="width: 18%; border: 1px solid black; border-collapse: collapse;text-align: center;">Offer Product</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                @php
                    $tq=0;
                @endphp

                @foreach($products as $product)
                    @php 
                        $tq=$tq+$product->quantity; 
                        $product_price = str_replace(",","",$product->product_price);
                        $shipping_price = number_format($product->shipping_price, 2);
                        $total = ($product_price*$product->quantity)+$product->shipping_price;
                        $total = number_format($total, 2);
                    @endphp
                    <tr>
                        <!--<td style="width: 14%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">{{$product->hsn_code}}</td>-->
                        <td style="width: 18%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">{{$product->product_name}}</td>
                        <td style="width: 10%; border: 1px solid black; border-collapse: collapse; margin-left:10px;text-align: center;">{{$product->unit}}</td>
                        <td style="width: 12%; border: 1px solid black; border-collapse: collapse;text-align: center;">{{$product->quantity}}</td>
                        <td style="width: 12%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$product_price}}</td>
                        <td style="width: 18%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$shipping_price}}</td>
                        <td style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$total}}</td>
                        @if($offer_status==1)
                            <td style="width: 18%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">{{$product->offer_product}}</td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="box">
            <div class="box-body">
                <h3 style="text-align: center;">Order Information</h3>
                <table class="table  table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;"><strong>Date</strong></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;"><strong>Customer</strong></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;"><strong>Quantity</strong></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;"><strong>Payment</strong></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;"><strong>Status</strong></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;">{{ date('M d, Y', strtotime($order['created_at'])) }}</td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;">{{ $customer->name }}</td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;">{{ $tq }}</td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse; margin-left:10px;"><strong>{{ $order['payment'] }}</strong></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;"">{{ucfirst($statuses)}}</td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">Subtotal:</td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$order->total_products}}</td>
                    </tr>
                    {{-- @if($order['wallet_point']!=0.0 && $order['wallet_point']!="")
                        @php
                            $wallet_amount = $order['wallet_point'] * env('DISCOUNT_POINT');
                            $wallet_amount = number_format($wallet_amount, 2);
                        @endphp
                        <tr>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">Discounts:</td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$wallet_amount}}</td>
                        </tr>
                    @endif
                    <tr>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">Tax:</td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$order->tax}}</td>
                    </tr> --}}
                    <tr>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">Shipping Price:</td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$order->total_shipping}}</td>
                    </tr>
                    @php 
                        $total1 = $order->total + $order->total_shipping +$order->tax ;
                        $total1 = number_format($total1,2);
                        $total1 = str_replace(",","",$total1);
                    @endphp
                    <tr>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse; margin-left:10px;"><strong>Total:</strong></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;"><strong>{{ $total1 }}</strong></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </section>
    
</body>
</html>
                    