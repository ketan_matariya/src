@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if($offers)
            <div class="box">
                <div class="box-body">
                    <div class="box-body">
                        <div class="box-header with-border bg-title border-header">
                            <h4 class="page-title header-color">Manage Offers</h4>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="scroll">
                        <table class="table border-b1px" id="table-id">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Offer</th>
                                    <th>Offer Type</th>
                                    <!-- <th>Company Name</th>
                                    <th>Category Name</th> -->
                                    <th>Expired Date</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                                $i=1;
                            @endphp
                            @foreach ($offers as $offer)
                                <tr>
                                    <!-- <td><a href="{{ route('admin.offers.show', $offer->id) }}">{{ $offer->offer_type }}</a></td> -->
                                    <td>{{ $i++ }}</td>
                                    <td>{{ ucfirst($offer->offer) }}</td>
                                    <td>@if($offer->offer_type=="single_offer") Single Offer @else Combo Offer  @endif</td>
                                    <!-- <td>{{ $offer->company }}</td>
                                    <td>{{ $offer->category }}</td> -->
                                    @php 
                                       $newDate = date("d/m/Y", strtotime($offer->expired_date)); 
                                    @endphp
                                    <td>{{ $newDate }}</td>
                                    <td>@include('layouts.status', ['status' => $offer->status])</td>
                                    <td>
                                        <form action="{{ route('admin.offers.destroy', $offer->id) }}" method="post" class="form-horizontal">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="delete">
                                            <div class="btn-group btn-display">
                                                @if($permission->edit==1)
                                                    <a href="{{ route('admin.offers.edit', $offer->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                @endif
                                                @if($permission->delete==1)
                                                    <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
                                                @endif
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                    {{ $offers->links() }}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        @else
            <div class="box">
                <div class="box-body"><p class="alert alert-warning">No offers found.</p></div>
            </div>
        @endif

    </section>
    <!-- /.content -->
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('#table-id').DataTable();
    } );
</script>
@endsection