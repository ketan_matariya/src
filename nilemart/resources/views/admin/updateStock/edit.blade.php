@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        <!-- @include('layouts.errors-and-messages') -->
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">Update Stock</h4>
                    </div>
                </div>
                <div class="box-body">
                    <form action="{{ route('admin.updateStock.update', $product->id) }}" method="post" id="update_product" class="form" enctype="multipart/form-data">
                            <div class="row">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="put">
                                <div class="col-md-12">
                                
                                    <!-- Tab panes -->
                                    <!--<div class="tab-content" id="tabcontent">-->
                                        <div role="tabpanel" class="tab-pane @if(!request()->has('combination')) active @endif">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="company_id">Company Name<span class="text-danger">*</span></label>
                                                                <select name="company_id" id="company_id" class="form-control " disabled>
                                                                    @if($companies)   
                                                                        <!-- <option>Select Company</option> -->
                                                                        @foreach($companies as $company)
                                                                        <option @if($company->id == $product->company_id) selected="selected" @endif value="{{ $company->id }}">{{ $company->name }}</option>
                                                                        @endforeach
                                                                    @endIf
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="category_id">Parent Category<span class="text-danger">*</span></label>
                                                                <select name="category_id" id="category_id" class="form-control " disabled>
                                                                    @if($categories)   
                                                                        @foreach($categories as $category)
                                                                        <option @if($category->id == $product->category_id) selected="selected" @endif value="{{ $category->id }}">{{ $category->name }}</option>
                                                                        @endforeach
                                                                    @endIf
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="subcategory_id">Sub Category</label>
                                                                <select name="subcategory_id" id="subcategory_id" class="form-control " disabled>
                                                                    @if($subcategories)   
                                                                        @foreach($subcategories as $subcategory)
                                                                        <option @if($subcategory->id == $product->subcategory_id) selected="selected" @endif value="{{ $subcategory->id }}">{{ $subcategory->name }}</option>
                                                                        @endforeach
                                                                    @endIf
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="crop_category">Crop Name<span class="text-danger">*</span></label>
                                                                <select name="crop_category" id="crop_category" class="form-control " disabled>
                                                                    @if($crops)   
                                                                        @foreach($crops as $crop)
                                                                        <option @if($crop->id == $product->crop_category) selected="selected" @endif value="{{ $crop->id }}">{{ $crop->name }}</option>
                                                                        @endforeach
                                                                    @endIf
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="crop_id">Crop Category</label>
                                                                <select name="crop_id" id="crop_id" class="form-control" disabled>
                                                                @if($cropscategories)   
                                                                        @foreach($cropscategories as $cropscategory)
                                                                        <option @if($cropscategory->id == $product->crop_id) selected="selected" @endif value="{{ $cropscategory->id }}">{{ $cropscategory->name }}</option>
                                                                        @endforeach
                                                                    @endIf
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="name">Name <span class="text-danger">*</span></label>
                                                                <input type="text" name="name" id="name" placeholder="Name" class="form-control" value="{!! $product->name !!}" disabled>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="technical_name">Technical Name <span class="text-danger">*</span></label>
                                                                <select name="technical_name" id="technical_name" class="form-control" disabled>
                                                                    @if($companies)  
                                                                        @foreach($technicals as $technical)
                                                                            <option @if($technical->name == $product->technical_name) selected @endif value="{{ $technical->name }}">{{ $technical->name }}</option>
                                                                        @endforeach
                                                                    @endIf
                                                                </select>
                                                                <!-- <input type="text" name="technical_name" id="technical_name" placeholder="Technical Name" class="form-control" value="{!! $product->technical_name !!}"> -->
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="sku">SKU <span class="text-danger">*</span></label>
                                                                <input type="text" name="sku" id="sku" placeholder="xxxxx" class="form-control" value="{!! $product->sku !!}" disabled>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <div class="row">
                                                                <img src="{{ url($product->cover) }}" alt="" class="img-responsive img-thumbnail">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row"></div>
                                                    <div class="form-group">
                                                        @foreach($images as $image)
                                                            <div class="col-md-3">
                                                                <div class="row">
                                                                    <img src="{{ asset('storage/$image->src') }}" alt="" class="img-responsive img-thumbnail"> <br /> <br>
                                                                    <a onclick="return confirm('Are you sure?')" href="{{ route('admin.product.remove.thumb', ['src' => $image->src]) }}" class="btn btn-danger btn-sm btn-block">Remove?</a><br />
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <div class="row"></div>
                                                    <div class="row">
                                                        <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <!-- @include('admin.shared.status-select', ['status' => $product->status]) -->
                                                                <label for="status">Status <span class="text-danger">*</span></label>
                                                                <select name="status" id="status" class="form-control" >
                                                                    <option value="0" @if($product->status==0) selected="selected" @endif>Disable</option>
                                                                    <option value="1" @if($product->status==1) selected="selected" @endif>Enable</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="stock_status">Stock Status <span class="text-danger">*</span></label>
                                                                <select name="stock_status" id="stock_status" class="form-control">
                                                                    <option value="">Select Stock Status</option>
                                                                    <option value="in_stock" @if(isset($product->stock_status) && ($product->stock_status == "in_stock")) selected='true'  @endif>In Stock</option>
                                                                    <option value="out_of_stock" @if(isset($product->stock_status) && ($product->stock_status == 'out_of_stock')) selected='true'  @endif>Out Of Stock</option>
                                                                </select>
                                                                @if ($errors->has('stock_status'))
                                                                    <span class="text-danger" style="color: red">{{ $errors->first('stock_status') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="weight">Weight </label>
                                                                <div class="form-inline">
                                                                    <input type="text" class="form-control col-md-8" id="weight" name="weight" placeholder="0" value="{{ number_format($product->weight, 2) }}" disabled>
                                                                    <label for="mass_unit" class="sr-only">Mass unit</label>
                                                                        @foreach($weight_units as $key => $unit)
                                                                        <input type="text" class="form-control col-md-8" name="mass_unit" id="mass_unit" value="{{ $key }} - ({{ $unit }})" disabled>
                                                                        @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @if(count($product->unit))
                                                        <div class="row"><br>
                                                            <div class="col-sm-6 col-md-10 col-sm-6 col-xs-12">
                                                                <label for="unit">Enter Unit <span class="text-danger">*</span></label>
                                                                <div class="scroll_table1">
                                                                <table class="table table-bordered" id="dynamic_field"> 
                                                                    @foreach($product->unit as $key => $u)
                                                                        <tr id="row{{$key}}"> 
                                                                            <td><label for="unit" class="lable-size">Unit Key <span class="text-danger">*</span></label><input type="text" name="unitkey[]" placeholder="Unit Key" value="{{$u->value}}" class="form-control name_list numeric frm-txtarea" readonly=""/></td>  
                                                                            <td>
                                                                                <label for="unit" class="lable-size">Unit Value<span class="text-danger">*</span></label>
                                                                                <select name="unitoption[]" id="unitoption" class="form-control frm-txtarea1" readonly="">
                                                                                    <option value="ac" <?php echo ($u->key == 'ac') ? 'selected' : ''; ?> >Ac (Acre)</option>
                                                                                    <option value="bbl" <?php echo ($u->key == 'bbl') ? 'selected' : ''; ?>>bbl (Barrel)</option>
                                                                                    <option value="c" <?php echo ($u->key == 'c') ? 'selected' : ''; ?>>c (Cup)</option>
                                                                                    <option value="dp" <?php echo ($u->key == 'dp') ? 'selected' : ''; ?>>dp (Drop)</option>
                                                                                    <option value="ft" <?php echo ($u->key == 'ft') ? 'selected' : ''; ?>>ft (Foot)</option>
                                                                                    <option value="gl" <?php echo ($u->key == 'gl') ? 'selected' : ''; ?>>gl (Gallon)</option>
                                                                                    <option value="gr"  <?php echo ($u->key == 'gr') ? 'selected' : ''; ?>>gr (Gram)</option>
                                                                                    <option value="htr"  <?php echo ($u->key == 'htr') ? 'selected' : ''; ?>>htr (Hectare)</option>
                                                                                    <option value="i"  <?php echo ($u->key == 'i') ? 'selected' : ''; ?>>i (Liter)</option>
                                                                                    <option value="in"  <?php echo ($u->key == 'in') ? 'selected' : ''; ?>>In (Inch)</option>
                                                                                    <option value="kg"  <?php echo ($u->key == 'kg') ? 'selected' : ''; ?>>kg (kilogram)</option>
                                                                                    <option value="km"  <?php echo ($u->key == 'km') ? 'selected' : ''; ?>>km (Kilometer)</option>
                                                                                    <option value="m"  <?php echo ($u->key == 'm') ? 'selected' : ''; ?>>m (Meter)</option>
                                                                                    <option value="mg"  <?php echo ($u->key == 'mg') ? 'selected' : ''; ?>>mg (Milligram)</option>
                                                                                    <option value="ml"  <?php echo ($u->key == 'ml') ? 'selected' : ''; ?>>ml (Milliter)</option>
                                                                                    <option value="pc"  <?php echo ($u->key == 'pc') ? 'selected' : ''; ?>>pc (Piece)</option>
                                                                                    <option value="qt"  <?php echo ($u->key == 'qt') ? 'selected' : ''; ?>>Qt (Quintal)</option>
                                                                                    <option value="scm"  <?php echo ($u->key == 'scm') ? 'selected' : ''; ?>>scm (Square Centimeter)</option>
                                                                                    <option value="sf"  <?php echo ($u->key == 'sf') ? 'selected' : ''; ?>>sf (Square Foot)</option>
                                                                                    <option value="skm"  <?php echo ($u->key == 'skm') ? 'selected' : ''; ?>>skm (Square Kilometer)</option>
                                                                                    <option value="smeter"  <?php echo ($u->key == 'smeter') ? 'selected' : ''; ?>>smeter (Square Meter)</option>
                                                                                    <option value="sqi"  <?php echo ($u->key == 'sqi') ? 'selected' : ''; ?>>sqi (Square Inch)</option>
                                                                                    <option value="sqm"  <?php echo ($u->key == 'sqm') ? 'selected' : ''; ?>>sqm (Square Mile)</option>
                                                                                    <option value="sy"  <?php echo ($u->key == 'sy') ? 'selected' : ''; ?>>sy (Square Yard)</option>
                                                                                    <option value="t"  <?php echo ($u->key == 't') ? 'selected' : ''; ?>>t (Tonne)</option>
                                                                                    <option value="ts"  <?php echo ($u->key == 'ts') ? 'selected' : ''; ?>>ts (Teaspoon)</option>
                                                                                </select>
                                                                            </td>
                                                                            <td><label for="unit" class="lable-size">Price <span class="text-danger">*</span></label><input type="text"  name="prices[]" placeholder="Price" value="@if(isset($u->price)){{str_replace(",","",$u->price)}}@endif" class="form-control name_list numeric price_list"  maxlength="8" readonly=""/></td> 
                                                                            <td><label for="unit" class="lable-size">Sale Price <span class="text-danger">*</span></label><input type="text" name="sale_prices[]" placeholder="Sale Price" value="@if(isset($u->sale_price)){{str_replace(",","",$u->sale_price)}}@endif" class="form-control name_list numeric sale_list" maxlength="8" readonly=""/></td> 
                                                                            </td>  
                                                                            <td><label for="unit" class="lable-size">Quantity <span class="text-danger">*</span></label><input type="text" name="quantities[]" placeholder="Quantity" value="@if(isset($u->quantity)){{$u->quantity}}@endif" class="form-control name_list numeric quantity_list" maxlength="5" readonly=""/></td> 
                                                                            <td><label for="unit" class="lable-size">Update Stock <span class="text-danger">*</span></label><input type="text" name="add_quantities[]" placeholder="Update Stock"  class="form-control name_list numeric upstrock_list" maxlength="5"/></td>  
                                                                        </tr>
                                                                    @endforeach
                                                                </table>
                                                                </div> 
                                                            </div>
                                                        </div>
                                                    @endIf  
                                                    @if($product->additional_info)
                                                        <div class="row">
                                                            <div class="col-sm-6 col-md-10 col-sm-6 col-xs-12">
                                                                <label for="additional_info">Additionl Information <span class="text-danger">*</span></label>
                                                                <table class="table table-bordered" id="additional_info">
                                                                    @foreach($product->additional_info as $key => $aditionInfo)  
                                                                        <tr id="row{{$key}}btn"> 
                                                                            <td><input type="text" name="additional_info[]" placeholder="Enter Key" value="{{$aditionInfo->value}}" class="form-control name_list" disabled/></td>  
                                                                            <td><input type="text" name="additional_info_value[]" placeholder="Enter Value" value="{{$aditionInfo->key}}" class="form-control name_list" disabled/></td>    
                                                                            @if($key == 0)  <td style="width: 9%;"><button type="button" name="add_additional_info" id="add_additional_info" class="btn btn-success" disabled>+</button></td> @endIf 
                                                                            @if($key == 1)  <td style="width: 9%;"><button type="button" name="remove" id="{{$key}}btn" class="btn btn-danger btn_remove_additional_info" disabled>X</button></td> @endIf
                                                                        </tr>  
                                                                    @endforeach
                                                                </table>
                                                            </div>
                                                        </div>
                                                    @endIf
                                                </div>
                                                <!-- <div class="col-md-4">
                                                    <h2>Categories</h2>
                                                    @include('admin.shared.categories', ['categories' => $categories, 'ids' => $product])
                                                </div> -->
                                                
                                            </div>
                                            <div class="row">
                                                <div class="box-footer">
                                                    <div class="btn-group">
                                                        <a href="{{ route('admin.updateStock.index') }}" class="btn btn-default btn-sm">Cancel</a>
                                                        <button type="submit" id="edit-submit" class="btn btn-primary btn-sm">Update</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div role="tabpanel" class="tab-pane @if(request()->has('combination')) active @endif" id="combinations">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    @include('admin.products.create-attributes', compact('attributes'))
                                                </div>
                                                <div class="col-md-8">
                                                    @include('admin.products.attributes', compact('productAttributes'))
                                                </div>
                                            </div>
                                        </div> -->
                                    <!--</div>-->
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection
@section('css')
    <style type="text/css">
        table.dataTable tbody th, table.dataTable tbody td {
        padding: 8px 10px;
        /* width: 100%; */
        }
        label.checkbox-inline {
            padding: 10px 5px;
            display: block;
            margin-bottom: 5px;
        }
        label.checkbox-inline > input[type="checkbox"] {
            margin-left: 10px;
        }
        ul.attribute-lists > li > label:hover {
            background: #3c8dbc;
            color: #fff;
        }
        ul.attribute-lists > li {
            background: #eee;
        }
        ul.attribute-lists > li:hover {
            background: #ccc;
        }
        ul.attribute-lists > li {
            margin-bottom: 15px;
            padding: 15px;
        }
    </style>
@endsection
@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
    <script type="text/javascript">
    
    $(document).ready(function () {
        const checkbox = $('input.attribute');
        $(checkbox).on('change', function () {
            const attributeId = $(this).val();
            if ($(this).is(':checked')) {
                $('#attributeValue' + attributeId).attr('disabled', false);
            } else {
                $('#attributeValue' + attributeId).attr('disabled', true);
            }
            const count = checkbox.filter(':checked').length;
            if (count > 0) {
                $('#productAttributeQuantity').attr('disabled', false);
                $('#productAttributePrice').attr('disabled', false);
                $('#salePrice').attr('disabled', false);
                $('#btnSubmit').attr("disabled", false);
                $('#default').attr('disabled', false);
                $('#combination').attr('disabled', false);
                $('#add_additional_info').attr('disabled', false);
            } else {
                $('#productAttributeQuantity').attr('disabled', true);
                $('#productAttributePrice').attr('disabled', true);
                $('#salePrice').attr('disabled', true);
                $('#default').attr('disabled', true);
                $('#createCombinationBtn').attr('disabled', true);
                $('#combination').attr('disabled', true);
                $('#add_additional_info').attr('disabled', false);
            }
        });
    });

    
    $("#update_product").validate({
        rules: {
            "add_quantities[]" : {
                required: true,
                digits: true,
                maxlength: 5,
            },
            stock_status : {
                required: true,
            }
        },
        messages: {
            "add_quantities[]":{
                required: 'Please Enter stock',
                maxlength: "Please enter no more then 5 digits",
            },
            "stock_status":{
                required: 'Please select stock status',
            }
        },
            
        submitHandler: function (form) {
            $('#edit-submit').attr('disabled', true);
            form.submit();
        },
    });

    $('.numeric').on('input', function (event) {
        this.value = this.value.replace(/[^0-9]/g, '');
    });
    </script>
@endsection