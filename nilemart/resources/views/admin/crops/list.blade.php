@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">

    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if($crops)
            <div class="box">
                <div class="box-body">
                    <h2>Crops</h2>
                    @include('layouts.search', ['route' => route('admin.crops.index')])
                    <table class="table">
                        <thead>
                            <tr>
                                <td class="col-md-1">ID</td>
                                <td class="col-md-2">Name</td>
                                <td class="col-md-2">Email</td>
                                <td class="col-md-2">Mobile</td>
                                <td class="col-md-2">Status</td>
                                <td class="col-md-3">Actions</td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($crops as $crop)
                            <tr>
                                <td>{{ $crop['id'] }}</td>
                                <td>{{ $crop['crop_category'] }}</td>
                                <td>{{ $crop['name'] }}</td>
                               <td>
                                    <form action="{{ route('admin.crops.destroy', $crop['id']) }}" method="post" class="form-horizontal">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="delete">
                                        <div class="btn-group">
                                            <a href="{{ route('admin.crops.show', $crop['id']) }}" class="btn btn-default btn-sm"><i class="fa fa-eye"></i> Show</a>
                                            <a href="{{ route('admin.crops.edit', $crop['id']) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                            <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Delete</button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $crops->links() }}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        @endif

    </section>
    <!-- /.content -->
@endsection