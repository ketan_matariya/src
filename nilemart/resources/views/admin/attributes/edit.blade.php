@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <form action="{{ route('admin.attributes.update', $crop->id) }}" method="post" class="form" enctype="multipart/form-data"> 
                <div class="box-body">
                    <div class="row">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="put">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">Crop name <span class="text-danger">*</span></label>
                                <input type="text" name="name" id="name" placeholder="Crop name" class="form-control" value="{!! $crop->name ?: old('name')  !!}">
                            </div>
                            <div class="form-group">
                                <label for="core_category">Crop Category</label>
                                <select name="core_category" id="core_category" class="form-control select2">
                                    <option>Select Category</option>
                                        @foreach($categories as $category)
                                        <option @if($category->id == $crop->crop_category) selected="selected" @endif value="{{$category->id}}">{{$category->name}}</option>
                                            <!-- <option value="{{ $category->id }}">{{ $category->name }}</option> -->
                                        @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="image">Image <span class="text-danger">*</span></label>
                                <input type="file" name="image" id="image" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.attributes.index') }}" class="btn btn-default">Back</a>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection
