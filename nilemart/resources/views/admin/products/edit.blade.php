@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->

    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">Edit Product</h4>
                    </div>
                </div>
                <div class="box-body">
                    <form action="{{ route('admin.products.update', $product->id) }}" method="post" id="edit_product" class="form" enctype="multipart/form-data">
                        <div class="box-body">
                             <div class="row">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="put">
                                <div class="col-md-12">
                                <div role="tabpanel" class="tab-pane @if(!request()->has('combination')) active @endif">
                                    <div class="row">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="company_id">Company Name <span class="text-danger">*</span></label>
                                                    <select name="company_id" id="company_id" class="form-control " >
                                                        @if($companies)   
                                                            <!-- <option>Select Company</option> -->
                                                            @foreach($companies as $company)
                                                            <option @if($company->id == $product->company_id) selected="selected" @endif value="{{ $company->id }}">{{ $company->name }}</option>
                                                            @endforeach
                                                        @endIf
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="category_id">Category <span class="text-danger">*</span></label>
                                                    <select name="category_id" id="category_id" class="form-control " >
                                                        @if($categories)   
                                                            @foreach($categories as $category)
                                                            <option @if($category->id == $product->category_id) selected="selected" @endif value="{{ $category->id }}">{{ $category->name }}</option>
                                                            @endforeach
                                                        @endIf
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                        </div>
                                        <input type="hidden" name="totalsubcategory" id="totalsubcategory">
                                        <div class="row subcategory" @if($product->subcategory_id!='' || $product->subcategory_id!=null) style="display:block;" @else style="display:none;" @endif>
                                           <div class="col-lg-4">
                                               <div class="form-group">
                                              <label for="subcategory_id">Sub Category <span class="text-danger">*</span></label>
                                                <select name="subcategory_id" id="subcategory_id" class="form-control ">
                                                    @if(!isset($product->subcategory_id))
                                                          <option value="">Select Sub Category</option>
                                                      @endIf
                                                       @if($subcategories)
                                                           @foreach($subcategories as $subcategory)
                                                            <option @if($subcategory->id == $product->subcategory_id) selected="selected" @endif value="{{ $subcategory->id }}">{{ $subcategory->name }}</option>
                                                          @endforeach
                                                        @endIf
                                                    </select>
                                              </div>
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                        </div>
                                       
                                        <input type="hidden" name="totalsubsubcategory" id="totalsubsubcategory">
                                        <div class="row subsubcategory" @if($product->subsubcategory_id!=0 && $product->subsubcategory_id!=null) style="display:block;" @else style="display:none;" @endif>
                                            <div class="col-lg-4">
                                           <div class="form-group">
                                              
                                                   <label for="subsubcategory_id">Sub Sub Category </label>
                                                 
                                                   <select name="subsubcategory_id" id="subsubcategory_id" class="form-control ">
                                                    
                                                    @if($subsubcategories)
                                                           @foreach($subsubcategories as $sbsbcategory)
                                                            <option @if($sbsbcategory->id == $product->subsubcategory_id) selected="selected" @endif value="{{ $sbsbcategory->id }}">{{ $sbsbcategory->name }}</option>
                                                          @endforeach
                                                        @endIf
                                                 </select>
                                              </div>
                                           </div>
                                            <div class="col-lg-4">
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                        </div>
                                        <!--<div class="row">-->
                                        <!--    <div class="col-lg-4">-->
                                        <!--        <div class="form-group">-->
                                        <!--            <label for="crop_category">Crop Name <span class="text-danger">*</span></label>-->
                                        <!--            <select name="crop_category" id="crop_category" class="form-control">-->
                                        <!--                @if($crops)   -->
                                        <!--                    @foreach($crops as $crop)-->
                                        <!--                    <option @if($crop->id == $product->crop_category) selected="selected" @endif value="{{ $crop->id }}">{{ $crop->name }}</option>-->
                                        <!--                    @endforeach-->
                                        <!--                @endIf-->
                                        <!--            </select>-->
                                        <!--        </div>-->
                                        <!--    </div>-->
                                        <!--    <div class="col-lg-4">-->
                                        <!--    </div>-->
                                        <!--    <div class="col-lg-4">-->
                                        <!--    </div>-->
                                        <!--</div>-->

                                        <!--<div class="row">-->
                                        <!--    <div class="col-lg-4">-->
                                        <!--        <div class="form-group">-->
                                        <!--            <label for="crop_id">Crop Category <span class="text-danger">*</span></label>-->
                                        <!--            <select name="crop_id" id="crop_id" class="form-control ">-->
                                        <!--            @if($cropscategories)   -->
                                        <!--                    @foreach($cropscategories as $cropscategory)-->
                                        <!--                    <option @if($cropscategory->id == $product->crop_id) selected="selected" @endif value="{{ $cropscategory->id }}">{{ $cropscategory->name }}</option>-->
                                        <!--                    @endforeach-->
                                        <!--                @endIf-->
                                        <!--            </select>-->
                                        <!--        </div>-->
                                        <!--    </div>-->
                                        <!--    <div class="col-lg-4">-->
                                        <!--    </div>-->
                                        <!--    <div class="col-lg-4">-->
                                        <!--    </div>-->
                                        <!--</div>-->

                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="name">Name <span class="text-danger">*</span></label>
                                                    <input type="text" name="name" id="name" placeholder="Name" class="form-control character" value="{!! $product->name !!}" maxlength="300">
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="name_hi">नाम <span class="text-danger">*</span></label>
                                                    <input type="text" name="name_hi" id="name_hi" placeholder="नाम" class="form-control " value="{!! $product->name_hi !!}" maxlength="300">
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="name_gu">નામ <span class="text-danger">*</span></label>
                                                    <input type="text" name="name_gu" id="name_gu" placeholder="નામ" class="form-control " value="{!! $product->name_gu !!}" maxlength="300">
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="technical_name" id="technical_name" value="Vegetables"/>
                                        <?php /*?>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="technical_name">Technical Name <span class="text-danger">*</span></label>
                                                    <select name="technical_name" id="technical_name" class="form-control">
                                                        @if($companies)  
                                                            @foreach($technicals as $technical)
                                                                <option @if($technical->name == $product->technical_name) selected @endif value="{{ $technical->name }}">{{ $technical->name }}</option>
                                                            @endforeach
                                                        @endIf
                                                    </select>
                                                    <!-- <input type="text" name="technical_name" id="technical_name" placeholder="Technical Name" class="form-control" value="{!! $product->technical_name !!}"> -->
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                        </div>
                                        <?php */?>
                                        <!--<div class="row">-->
                                        <!--    <div class="col-lg-4">-->
                                        <!--        <div class="form-group">-->
                                        <!--            <label for="sku">SKU <span class="text-danger">*</span></label>-->
                                        <!--            <input type="text" name="sku" id="sku" placeholder="xxxxx" class="form-control character" value="{!! $product->sku !!}" maxlength="25">-->
                                        <!--        </div>-->
                                        <!--    </div>-->
                                        <!--    <div class="col-lg-4">-->
                                        <!--    </div>-->
                                        <!--    <div class="col-lg-4">-->
                                        <!--    </div>-->
                                        <!--</div>-->

                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="description">Description </label>
                                                    <textarea class="form-control" name="description" id="description" rows="5" placeholder="Description"  maxlength="600">{!! $product->description  !!}</textarea>
                                                    <!--<textarea class="form-control ckeditor" name="description" id="description" rows="5" placeholder="Description">{!! $product->description  !!}</textarea>-->
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="description_hi">विवरण </label>
                                                    <textarea class="form-control" name="description_hi" id="description_hi" rows="5" placeholder="विवरण"  maxlength="600">{!! $product->description_hi  !!}</textarea>
                                                    <!--<textarea class="form-control ckeditor" name="description_hi" id="description_hi" rows="5" placeholder="description_hi">{!! $product->description_hi  !!}</textarea>-->
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="description_gu">વર્ણન </label>
                                                    <textarea class="form-control" name="description_gu" id="description_gu" rows="5" placeholder="વર્ણન"  maxlength="600">{!! $product->description_gu  !!}</textarea>
                                                    <!--<textarea class="form-control ckeditor" name="description_gu" id="description_gu" rows="5" placeholder="description_gu">{!! $product->description_gu  !!}</textarea>-->
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="shipping_price">Shipping Price </label>
                                                    <input type="text" name="shipping_price" id="shipping_price" placeholder="Shipping Price" class="form-control numeric" value="{{ $product->shipping_price }}" maxlength="5">
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                        </div>

                                        <!--<div class="row">-->
                                        <!--    <div class="col-lg-4">-->
                                        <!--        <div class="form-group">-->
                                        <!--            <label for="hsn_code">HSN Code </label>-->
                                        <!--            <input type="text" name="hsn_code" id="hsn_code" placeholder="HSN Code" class="form-control numeric" value="{{ $product->hsn_code }}" maxlength="8">-->
                                        <!--        </div>-->
                                        <!--    </div>-->
                                        <!--    <div class="col-lg-4">-->
                                        <!--    </div>-->
                                        <!--    <div class="col-lg-4">-->
                                        <!--    </div>-->
                                        <!--</div>-->

                                        @if(isset($product->cover))
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <img src="{{ $product->cover }}" alt="" class="img-responsive img-thumbnail" height="150" width="150">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="row"></div>

                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="cover">Image </label>
                                                    <input type="file" name="cover" id="cover" class="form-control" onchange="GetFileSize()">
                                                    <small class="text-warning">Note :- Upload image with .jpg, .jpeg and .png extensions upto 1MB.</small>
                                                    <label id="image" class="image">Please select cover image</label>
                                                    <label id="image_error" class="image_error">Image size is large.</label>
                                                    <label id="image_type_error"  class="image_type_error">Select image only .jpg, .png, .jpeg file</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    @include('admin.shared.status-select', ['status' => $product->status])
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-4">
                                                @include('admin.shared.attribute-select', [compact('default_weight')]) 
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                        </div>
                                        
                                        <!-- /.box-body -->
                                        <div class="row">
                                            <div class="col-sm-6 col-md-12 col-sm-6 col-xs-12">
                                                <label for="unit">Enter Unit <span class="text-danger">*</span></label>
                                                <div class="scroll">
                                                <table class="table table-bordered" id="dynamic_field">  
                                                @if(count($product->unit))
                                                    @foreach($product->unit as $key => $u)
                                                        <tr id="row{{$key}}"> 
                                                        <input type="hidden" name="unitmainid[]" id="unitmainid[]" value="{{$u->id}}"/>
                                                            <td><label for="unit" class="lable-size">Unit Key <span class="text-danger">*</span></label><input type="text" name="unitkey[]" placeholder="Enter Unit Key" value="{{$u->value}}" class="form-control name_list numeric frm-txtarea"  maxlength="5"/></td>  
                                                            <td>
                                                                <label for="unit" class="lable-size">Unit Value<span class="text-danger frm-txtarea1 ">*</span></label>
                                                                <select name="unitoption[]" id="unitoption" class="form-control frm-txtarea123" >
                                                                    <option value="ac" <?php echo ($u->key == 'ac') ? 'selected' : ''; ?> >Ac (Acre)</option>
                                                                    <option value="bbl" <?php echo ($u->key == 'bbl') ? 'selected' : ''; ?>>bbl (Barrel)</option>
                                                                    <option value="c" <?php echo ($u->key == 'c') ? 'selected' : ''; ?>>c (Cup)</option>
                                                                    <option value="dp" <?php echo ($u->key == 'dp') ? 'selected' : ''; ?>>dp (Drop)</option>
                                                                    <option value="ft" <?php echo ($u->key == 'ft') ? 'selected' : ''; ?>>ft (Foot)</option>
                                                                    <option value="gl" <?php echo ($u->key == 'gl') ? 'selected' : ''; ?>>gl (Gallon)</option>
                                                                    <option value="gr"  <?php echo ($u->key == 'gr') ? 'selected' : ''; ?>>gr (Gram)</option>
                                                                    <option value="htr"  <?php echo ($u->key == 'htr') ? 'selected' : ''; ?>>htr (Hectare)</option>
                                                                    <option value="L"  <?php echo ($u->key == 'L') ? 'selected' : ''; ?>>L (Liter)</option>
                                                                    <option value="in"  <?php echo ($u->key == 'in') ? 'selected' : ''; ?>>In (Inch)</option>
                                                                    <option value="kg"  <?php echo ($u->key == 'kg') ? 'selected' : ''; ?>>kg (kilogram)</option>
                                                                    <option value="km"  <?php echo ($u->key == 'km') ? 'selected' : ''; ?>>km (Kilometer)</option>
                                                                    <option value="m"  <?php echo ($u->key == 'm') ? 'selected' : ''; ?>>m (Meter)</option>
                                                                    <option value="mg"  <?php echo ($u->key == 'mg') ? 'selected' : ''; ?>>mg (Milligram)</option>
                                                                    <option value="ml"  <?php echo ($u->key == 'ml') ? 'selected' : ''; ?>>ml (Milliter)</option>
                                                                    <option value="pc"  <?php echo ($u->key == 'pc') ? 'selected' : ''; ?>>pc (Piece)</option>
                                                                    <option value="qt"  <?php echo ($u->key == 'qt') ? 'selected' : ''; ?>>Qt (Quintal)</option>
                                                                    <option value="scm"  <?php echo ($u->key == 'scm') ? 'selected' : ''; ?>>scm (Square Centimeter)</option>
                                                                    <option value="sf"  <?php echo ($u->key == 'sf') ? 'selected' : ''; ?>>sf (Square Foot)</option>
                                                                    <option value="skm"  <?php echo ($u->key == 'skm') ? 'selected' : ''; ?>>skm (Square Kilometer)</option>
                                                                    <option value="smeter"  <?php echo ($u->key == 'smeter') ? 'selected' : ''; ?>>smeter (Square Meter)</option>
                                                                    <option value="sqi"  <?php echo ($u->key == 'sqi') ? 'selected' : ''; ?>>sqi (Square Inch)</option>
                                                                    <option value="sqm"  <?php echo ($u->key == 'sqm') ? 'selected' : ''; ?>>sqm (Square Mile)</option>
                                                                    <option value="sy"  <?php echo ($u->key == 'sy') ? 'selected' : ''; ?>>sy (Square Yard)</option>
                                                                    <option value="t"  <?php echo ($u->key == 't') ? 'selected' : ''; ?>>t (Tonne)</option>
                                                                    <option value="ts"  <?php echo ($u->key == 'ts') ? 'selected' : ''; ?>>ts (Teaspoon)</option>
                                                                </select>
                                                            </td>
                                                            <td><label for="unit" class="lable-size">Price <span class="text-danger">*</span></label><input type="text" name="prices[]" placeholder="Enter Price" value="@if(isset($u->price)){{str_replace(",","",$u->price)}}@endif" class="form-control name_list numeric price_list1"  maxlength="8"/></td> 
                                                            <td><label for="unit" class="lable-size">Sale Price <span class="text-danger ">*</span></label><input type="text" name="sale_prices[]" placeholder="Enter Sale Price" value="@if(isset($u->sale_price)){{str_replace(",","",$u->sale_price)}}@endif" class="form-control name_list numeric sale_list"  maxlength="8"/></td> 
                                                            <td><label for="unit" class="lable-size">Quantity <span class="text-danger">*</span></label><input type="text" name="quantities[]" placeholder="Enter Quantity" value="@if(isset($u->quantity)){{$u->quantity}}@endif" class="form-control name_list numeric quantity_list"  maxlength="5"/></td>
                                                            <td class="table-align quantity_list"><label for="unit" class="lable-size">Max Quantity <span class="text-danger ">*</span></label><input type="text" name="max_quantities[]" value="@if(isset($u->max_quantities)){{$u->max_quantities}}@endif" placeholder="Enter Max Quantity" class="form-control numeric max_quantity_list"  maxlength="2"/></td>
                                                            <td><label for="unit" class="lable-size">Discount </label><input type="text" name="discount[]" placeholder="Enter Discount" value="@if(isset($u->discount)){{$u->discount}}@endif" class="form-control name_list numeric discount_list"  maxlength="5"/></td>  
                                                            <!--<td><label for="unit" class="lable-size">GST(%) <span class="text-danger">*</span> </label><input type="text" name="gst[]" placeholder="Enter GST" value="@if(isset($u->gst_price)){{$u->gst_price}}@endif" class="form-control name_list numeric"  maxlength="5"/></td>-->
                                                            @if($key == 0) <td  align="center"> <button type="button" name="add" id="add" class="btn btn-success" style="    margin-top: 24px;">+</button></td> @endIf
                                                            @if($key >= 1) <td align="center"><button type="button" name="remove" id="{{$key}},{{$u->id}}" class="btn btn-danger btn_remove btnremveprce" style="margin-top: 24px;">X</button></td> @endIf
                                                        </tr>
                                                    @endforeach

                                                @else
                                                    <tr> 
                                                        <td><label for="unit" class="lable-size">Unit <span class="text-danger">*</span></label><input type="text" name="unitkey[]" placeholder="Enter Key" class="form-control name_list" maxlength="5" /></td>  
                                                        <td>
                                                            <label for="unit" class="lable-size">Unit Value<span class="text-danger">*</span></label>
                                                            <select name="unitoption[]" id="unitoption" class="form-control">
                                                                <option value="ac">Ac (Acre)</option>
                                                                <option value="bbl">bbl (Barrel)</option>
                                                                <option value="c">c (Cup)</option>
                                                                <option value="dp">dp (Drop)</option>
                                                                <option value="ft">ft (Foot)</option>
                                                                <option value="gl">gl (Gallon)</option>
                                                                <option value="gr">gr (Gram)</option>
                                                                <option value="htr">htr (Hectare)</option>
                                                                <option value="i">i (Liter)</option>
                                                                <option value="in">In (Inch)</option>
                                                                <option value="kg">kg (kilogram)</option>
                                                                <option value="km">km (Kilometer)</option>
                                                                <option value="m">m (Meter)</option>
                                                                <option value="mg">mg (Milligram)</option>
                                                                <option value="ml">ml (Milliter)</option>
                                                                <option value="pc">pc (Piece)</option>
                                                                <option value="qt">Qt (Quintal)</option>
                                                                <option value="scm">scm (Square Centimeter)</option>
                                                                <option value="sf">sf (Square Foot)</option>
                                                                <option value="skm">skm (Square Kilometer)</option>
                                                                <option value="smeter">smeter (Square Meter)</option>
                                                                <option value="sqi">sqi (Square Inch)</option>
                                                                <option value="sqm">sqm (Square Mile)</option>
                                                                <option value="sy">sy (Square Yard)</option>
                                                                <option value="t">t (Tonne)</option>
                                                                <option value="ts">ts (Teaspoon)</option>
                                                            </select>
                                                        </td>
                                                        <td><label for="unit" class="lable-size">Price <span class="text-danger">*</span></label><input type="text" name="prices[]" placeholder="Enter Price" class="form-control name_list" maxlength="5" /></td> 
                                                        <td><label for="unit" class="lable-size">Sale Price <span class="text-danger">*</span></label><input type="text" name="sale_prices[]" placeholder="Enter Sale Price" class="form-control name_list" maxlength="5" /></td>  
                                                        <td><label for="unit" class="lable-size">Quantity <span class="text-danger">*</span></label><input type="text" name="quantities[]" placeholder="Enter Sale Price" class="form-control name_list" maxlength="5" /></td>
                                                        <td align="center"><button type="button" name="add" id="add" class="btn btn-success" style="margin-top: 24px;">+</button></td>  
                                                    </tr> 
                                                @endIf  
                                            </table> 
                                            </div> 
                                            </div>
                                        </div>
                                        @if($product->additional_info)
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label for="additional_info">Additionl Information <span class="text-danger">*</span></label>
                                                <table class="table table-bordered" id="additional_info">
                                                @php
                                                    $additional_info = $product->additional_info;

                                                @endphp
                                                @foreach($product->additional_info as $key => $aditionInfo)  
                                                    <tr id="row{{$key}}btn" class="row{{$key}}btn">  
                                                        <td>
                                                            <input type="text" name="additional_info[]" placeholder="Enter Key" id="additional_info_name" value="{{$aditionInfo->value}}" class="form-control name_list  additional_info_txt @if($key==0) additional_info_txt_one @else addition_information_txt @endif " maxlength="50" data-addition-information-txt-id="{{$key}}" />
                                                            <label id="additional_info_error" class="show-hide">
                                                                Please enter additional information
                                                            </label>
                                                            <input type="text" name="additional_info_value[]" placeholder="Enter Value" id="addtional_info_value" value="{{$aditionInfo->key}}" class="form-control name_list additional_info_val @if($key==0) additional_info_val_one @else addition_information_value @endif" maxlength="25" data-addition-information-value-id="{{$key}}"/>
                                                            <label id="additional_info_value_error" class="show-hide">
                                                                Please enter value
                                                            </label>
                                                        </td>    
                                                        @if($key == 0)  <td align="center" ><button type="button" name="add_additional_info" id="add_additional_info" class="btn btn-success">Add</button></td> @endIf 
                                                        @if($key != 0)  <td align="center" ><button type="button" name="remove" id="{{$key}}btn" class="btn btn-danger btn_remove_additional_info">X</button></td> @endIf
                                                    </tr>  
                                                @endforeach

                                                </table>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="additional_info">अतिरिक्त जानकारी <span class="text-danger">*</span></label>
                                                <table class="table table-bordered" id="additional_info_hi">
                                                <?php
                                                    $additional_info_hi=$additional_info; 
                                                    if($product->additional_info_hi==null){
                                                        $additional_info_hi = $additional_info;
                                                    } else{
                                                        $additional_info_hi = $product->additional_info_hi;
                                                    }
                                                ?>
                                                @foreach($additional_info_hi as $key => $aditionInfo)  
                                                    <tr id="row{{$key}}btn" class="row{{$key}}btn"> 
                                                        <td>
                                                            <input type="text" name="additional_info_hi[]" placeholder="Enter Key" id="additional_info_name" value="{{$aditionInfo->value}}" class="form-control name_list additional_info_txt additional_info_txt_{{$key}}_hi @if($key==0) additional_info_txt_hi_one @endif" maxlength="50" data-addition-information-txt-hi-id="{{$key}}"/>
                                                            <label id="additional_info_error" class="show-hide">
                                                                Please enter additional information
                                                            </label>
                                                            <input type="text" name="additional_info_value_hi[]" placeholder="Enter Value" id="addtional_info_value" value="{{$aditionInfo->key}}" class="form-control name_list additional_info_val additional_info_val_{{$key}}_hi @if($key==0) additional_info_val_hi_one @endif" maxlength="25" data-addition-information-value-hi-id="{{$key}}" />
                                                            <label id="additional_info_value_error" class="show-hide">
                                                                Please enter value
                                                            </label>
                                                        </td>
                                                    </tr>  
                                                @endforeach
                                                </table>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="additional_info">વધારાની માહિતી <span class="text-danger">*</span></label>
                                                <table class="table table-bordered" id="additional_info_gu">
                                                <?php
                                                    $additional_info_gu=$additional_info; 
                                                    if($product->additional_info_gu==null){
                                                        $additional_info_gu = $additional_info;
                                                    } else{
                                                        $additional_info_gu = $product->additional_info_gu;
                                                    }
                                                ?>
                                                @foreach($additional_info_gu as $key => $aditionInfo)  
                                                    <tr id="row{{$key}}btn" class="row{{$key}}btn"> 
                                                        <td>
                                                            <input type="text" name="additional_info_gu[]" placeholder="Enter Key" id="additional_info_name" value="{{$aditionInfo->value}}" class="form-control name_list additional_info_txt additional_info_txt_{{$key}}_gu @if($key==0) additional_info_txt_gu_one @endif" maxlength="50" data-addition-information-txt-gu-id="{{$key}}" />
                                                            <label id="additional_info_error" class="show-hide">
                                                                Please enter additional information
                                                            </label>
                                                            <input type="text" name="additional_info_value_gu[]" placeholder="Enter Value" id="addtional_info_value" value="{{$aditionInfo->key}}" class="form-control name_list additional_info_val additional_info_val_{{$key}}_gu @if($key==0) additional_info_val_gu_one @endif" maxlength="25" data-addition-information-value-gu-id="{{$key}}" />
                                                            <label id="additional_info_value_error" class="show-hide">
                                                                Please enter value
                                                            </label>
                                                        </td>
                                                    </tr>  
                                                @endforeach
                                                </table>
                                            </div>
                                        </div>
                                        @else
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label for="additional_info">Additional Information <span class="text-danger">*</span></label>
                                                <table class="table table-bordered" id="additional_info">  
                                                    <tr> 
                                                        <td>
                                                            <input type="text" name="additional_info[]" placeholder="Enter Key" class="form-control name_list character additional_info_txt additional_info_txt_one" id="additional_info_name" maxlength="50" />
                                                            <label id="additional_info_error" class="show-hide" style="display:none;color:red!important">
                                                                Please enter key
                                                            </label>
                                                            <input type="text" name="additional_info_value[]" placeholder="Enter Value" class="form-control name_list character additional_info_val additional_info_val_one" id="addtional_info_value" maxlength="25" /><label id="additional_info_value_error" class="show-hide" style="display:none;color:red!important">
                                                                Please enter additional information value
                                                            </label></td>    
                                                        <td align="center"><button type="button" name="add_additional_info" id="add_additional_info" class="btn btn-success">Add</button></td>  
                                                    </tr>  
                                                </table>  
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="additional_info">अतिरिक्त जानकारी <span class="text-danger">*</span></label>
                                                <table class="table table-bordered" id="additional_info_hi">
                                                    <tr>
                                                        <td>
                                                            <input type="text" name="additional_info_hi[]" placeholder="कुंजी दर्ज" class="form-control name_list additional_info_txt additional_info_txt_hi_one" id="additional_info_name" maxlength="50" />
                                                            <label id="additional_info_error" class="show-hide" style="display:none;color:red!important">
                                                                Please enter key
                                                            </label>
                                                            <input type="text" name="additional_info_value_hi[]" placeholder="मान दर्ज करें" class="form-control name_list additional_info_val additional_info_val_hi_one" id="addtional_info_value" maxlength="25" /><label id="additional_info_value_error" class="show-hide" style="display:none;color:red!important">
                                                                Please enter additional information value
                                                            </label>
                                                        </td>
                                                    </tr>  
                                                </table>  
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="additional_info">વધારાની માહિતી <span class="text-danger">*</span></label>
                                                <table class="table table-bordered" id="additional_info_gu">  
                                                    <tr> 
                                                        <td>
                                                            <input type="text" name="additional_info_gu[]" placeholder="કી દાખલ કરો" class="form-control name_list additional_info_txt additional_info_txt_gu_one" id="additional_info_name" maxlength="50" />
                                                            <label id="additional_info_error" class="show-hide" style="display:none;color:red!important">
                                                                Please enter key
                                                            </label>
                                                            <input type="text" name="additional_info_value_gu[]" placeholder="કિંમત દાખલ કરો" class="form-control name_list additional_info_val additional_info_val_gu_one" id="addtional_info_value" maxlength="25" /><label id="additional_info_value_error" class="show-hide" style="display:none;color:red!important">
                                                                Please enter additional information value
                                                            </label>
                                                        </td>
                                                    </tr>  
                                                </table>  
                                            </div>
                                        </div>
                                        @endIf

                                        <div class="row">
                                            <div class="col-sm-6 col-md-4 col-sm-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="offer_title">Offer Title <span class="text-danger">*</span></label>
                                                    <input type="text" name="offer_title" id="offer_title" placeholder="Offer Title" class="form-control" value="{{ $product->offer_title }}" maxlength="25">
                                                    <label id="offer_title_error" class="show-hide" style="color:red!important; display:none;">
                                                        Please enter offer title
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-4 col-sm-4 col-xs-12">
                                                <div class="form-group">
                                                   <label for="offer_title">प्रस्ताव शीर्षक <span class="text-danger">*</span> </label>
                                                    <input type="text" name="offer_title_hi" id="offer_title_hi" placeholder="प्रस्ताव शीर्षक" class="form-control" value="{{$product->offer_title_hi  }}" maxlength="25" >
                                                    <label id="offer_title_error" class="show-hide" style="color:red!important; display: none;">
                                                        Please enter offer title
                                                    </label>
                                                </div>
                                            </div>
                                           <div class="col-sm-6 col-md-4 col-sm-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="offer_title">ઓફર શીર્ષક <span class="text-danger">*</span> </label>
                                                    <input type="text" name="offer_title_gu" id="offer_title_gu" placeholder="ઓફર શીર્ષક" class="form-control" value="{{$product->offer_title_gu}}" maxlength="25" >
                                                    <label id="offer_title_error" class="show-hide" style="color:red!important; display: none;">
                                                        Please enter offer title
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                               <div class="form-group">
                                                    <label for="offer_product">Offer Product  <span class="text-danger">*</span> </label>
                                                    <input type="text" name="offer_product" id="offer_product" placeholder="Offer Product" class="form-control" value="{{ $product->offer_product }}" maxlength="25">
                                                    <label id="offer_product_error" class="show-hide" style="color:red!important; display:none;">
                                                        Please enter offer product
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-4 col-sm-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="offer_product">उत्पाद प्रस्तुत करते हैं <span class="text-danger">*</span> </label>
                                                    <input type="text" name="offer_product_hi" id="offer_product_hi" placeholder="उत्पाद प्रस्तुत करते हैं" class="form-control" value="{{ $product->offer_product_hi}}" maxlength="25" >
                                                    <label id="offer_product_error" class="show-hide" style="color:red!important; display: none;">
                                                        Please enter offer product
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-4 col-sm-4 col-xs-12">
                                               <div class="form-group">
                                                     <label for="offer_product">ઉત્પાદન ઓફર <span class="text-danger">*</span> </label>
                                                    <input type="text" name="offer_product_gu" id="offer_product_gu" placeholder="ઉત્પાદન ઓફર" class="form-control" value="{{  $product->offer_product_gu }}" maxlength="25" >
                                                    <label id="offer_product_error" class="show-hide" style="color:red!important; display: none;">
                                                        Please enter offer product
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="offer_product_image">Offer Image </label>
                                                    <input type="file" name="offer_product_image" id="offer_product_image" class="form-control" onchange="GetFileSize()">
                                                    <label id="offer_image_error" class="image">Please select offer image</label>
                                                    <label id="image_error" class="image_error">Image size is large.</label>
                                                    <label id="offer_image_type_error"  class="image_type_error">Select image only .jpg, .png, .jpeg file</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="offer_quantity">Offer Quantity  <span class="text-danger">*</span> </label>
                                                    <input type="text" name="offer_quantity" id="offer_quantity" placeholder="Offer Quantity" class="form-control numeric" value="{{ $product->offer_quantity }}" maxlength="5">
                                                    <label id="offer_quantity_error" class="show-hide" style="color:red!important; display: none;">
                                                        Please enter offer quantity
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            @if($product->offer_product_image)
                                                <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <img src="{{ $product->offer_product_image }}" alt="" class="img-responsive img-thumbnail" height="150" width="150">
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        
                                    </div>

                                    <div class="row">
                                        <div class="box-footer">
                                            <div class="btn-group">
                                                <a href="{{ route('admin.products.index') }}" class="btn btn-default ">Cancel</a>
                                                <input type="button" class="btn btn-info" value="Translate" id="translate">
                                                <button type="submit" id="edit-submit" class="btn btn-primary">Update</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection
@section('css')
<style type="text/css">
    table.dataTable tbody th, table.dataTable tbody td {
    padding: 8px 10px;
    /* width: 100%; */
    }
    label.checkbox-inline {
        padding: 10px 5px;
        display: block;
        margin-bottom: 5px;
    }
    label.checkbox-inline > input[type="checkbox"] {
        margin-left: 10px;
    }
    ul.attribute-lists > li > label:hover {
        background: #3c8dbc;
        color: #fff;
    }
    ul.attribute-lists > li {
        background: #eee;
    }
    ul.attribute-lists > li:hover {
        background: #ccc;
    }
    ul.attribute-lists > li {
        margin-bottom: 15px;
        padding: 15px;
    }
    .additional_info_txt {
        margin-bottom: 10px;
    }
    #add_additional_info {
        width: 100%;
    }
</style>
@endsection
@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
    <script type="text/javascript">
    document.getElementById('additional_info_error').style.display = 'none';
    document.getElementById('additional_info_value_error').style.display = 'none';
    document.getElementById('additional_info_error').style.color = 'red';
    document.getElementById('additional_info_value_error').style.color = 'red';
    var i=1; 
    $(document).on('click', '#add', function(){ 
            i++;
            $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="hidden" name="unitmainid[]" id="unitmainid[]" value="'+i+'"/><label for="unit" class="lable-size">Unit <span class="text-danger">*</span></label><input type="text" name="unitkey[]" required placeholder="Enter Unit Key" class="form-control name_list" /></td><td><label for="unit" class="lable-size">Unit Value<span class="text-danger">*</span></label><select name="unitoption[]" id="unitoption" class="form-control select2" required><option value="ac">Ac (Acre)</option><option value="bbl">bbl (Barrel)</option><option value="c">c (Cup)</option><option value="dp">dp (Drop)</option><option value="ft">ft (Foot)</option><option value="gl">gl (Gallon)</option><option value="gr">gr (Gram)</option><option value="htr">htr (Hectare)</option><option value="'+i+'">i (Liter)</option><option value="in">In (Inch)</option><option value="kg">kg (kilogram)</option><option value="km">km (Kilometer)</option><option value="m">m (Meter)</option><option value="mg">mg (Milligram)</option><option value="ml">ml (Milliter)</option><option value="pc">pc (Piece)</option><option value="qt">Qt (Quintal)</option><option value="scm">scm (Square Centimeter)</option><option value="sf">sf (Square Foot)</option><option value="skm">skm (Square Kilometer)</option><option value="smeter">smeter (Square Meter)</option><option value="sqi">sqi (Square Inch)</option><option value="sqm">sqm (Square Mile)</option><option value="sy">sy (Square Yard)</option><option value="t">t (Tonne)</option><option value="ts">ts (Teaspoon)</option></select></td><td class="table-align"><label for="unit" class="lable-size">Price <span class="text-danger">*</span></label><input type="text" name="prices[]" placeholder="Enter Price" class="form-control name_list numeric"  maxlength="8"/></td><td class="table-align"><label for="unit" class="lable-size">Sale Price <span class="text-danger">*</span></label><input type="text" name="sale_prices[]" placeholder="Enter Sale Price" class="form-control numeric"  maxlength="8"/></td></td><td class="table-align"><label for="unit" class="lable-size">Quantity <span class="text-danger">*</span></label><input type="text" name="quantities[]" placeholder="Enter Quantity" class="form-control numeric"  maxlength="5"/></td><td class="table-align"><label for="unit" class="lable-size">Max Quantity <span class="text-danger">*</span></label><input type="text" name="max_quantities[]" value="" placeholder="Enter Max Quantity" class="form-control numeric"  maxlength="2"/></td><td class="table-align"><label for="unit" class="lable-size">Discount </label><input type="text" name="discount[]" placeholder="Enter Discount" class="form-control numeric"  maxlength="5"/></td><td align="center"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove" style="margin-top: 24px;">x</button></td></tr>');
        });
    $(document).on('click', '.btn_remove', function(){  
        var button_id = $(this).attr("id");   
        $('#row'+button_id+'').remove();  
    });

    //Add Additionl information
    $(document).on('click', '#add_additional_info', function(){ 
        i++;    
       /* $('#additional_info').append('<tr id="row'+i+'btn" class="dynamic-added"><td><input type="text" name="additional_info[]" id="additional_info_name_'+i+'" onclick="add_info(this.id)" required placeholder="Enter Key" class="form-control name_list addition_information"  maxlength="50" /><label id="additional_info_error" class="show-hide">Please Enter Additionl Information</label></td><td><input type="text" name="additional_info_value[]" id="additional_info_value_'+i+'" placeholder="Enter Value" class="form-control name_list"  maxlength="20" required/><label id="additional_info_value_error" class="show-hide">Please Enter Value</label></td><td align="center"><button type="button" name="remove" id="'+i+'btn" class="btn btn-danger btn_remove_additional_info">X</button></td></tr>');  
*/
        $('#additional_info').append('<tr id="row'+i+'btn" class="dynamic-added row'+i+'btn"><td><input type="text" name="additional_info[]" id="additional_info_name_'+i+'" onclick="add_info(this.id)" required placeholder="Enter Key" class="form-control name_list addition_information_txt additional_info_txt additional_info_txt_'+i+'"  maxlength="50" data-addition-information-txt-id="'+i+'" /><label id="additional_info_error" class="show-hide">Please Enter Additionl Information</label><input type="text" name="additional_info_value[]" id="additional_info_value_'+i+'" placeholder="Enter Value" class="form-control name_list additional_info_val additional_info_val_'+i+' addition_information_value"  maxlength="20" required data-addition-information-value-id="'+i+'"/><label id="additional_info_value_error" class="show-hide">Please Enter Value</label></td><td align="center"><button type="button" name="remove" id="'+i+'btn" class="btn btn-danger btn_remove_additional_info">X</button></td></tr>'); 

            $('#additional_info_hi').append('<tr id="row'+i+'btn" class="dynamic-added row'+i+'btn"><td><input type="text" name="additional_info_hi[]" id="additional_info_name_'+i+'" onclick="add_info(this.id)" required placeholder="Enter Key" class="form-control name_list  additional_info_txt additional_info_txt_'+i+'_hi"  maxlength="50" data-addition-information-txt-hi-id="'+i+'" /><label id="additional_info_error" class="show-hide">Please Enter Additionl Information</label><input type="text" name="additional_info_value_hi[]" id="additional_info_value_'+i+'" placeholder="Enter Value" class="form-control name_list additional_info_val additional_info_val_'+i+'_hi"  maxlength="20" required data-addition-information-value-hi-id="'+i+'" /><label id="additional_info_value_error" class="show-hide">Please Enter Value</label></td></tr>');

            $('#additional_info_gu').append('<tr id="row'+i+'btn" class="dynamic-added row'+i+'btn"><td><input type="text" name="additional_info_gu[]" id="additional_info_name_'+i+'" onclick="add_info(this.id)" required placeholder="Enter Key" class="form-control name_list additional_info_txt additional_info_txt_'+i+'_gu"  maxlength="50" data-addition-information-txt-gu-id="'+i+'"/><label id="additional_info_error" class="show-hide">Please Enter Additionl Information</label><input type="text" name="additional_info_value_gu[]" id="additional_info_value_'+i+'" placeholder="Enter Value" class="form-control name_list additional_info_val additional_info_val_'+i+'_gu"  maxlength="20" required data-addition-information-value-gu-id="'+i+'" /><label id="additional_info_value_error" class="show-hide">Please Enter Value</label></td></tr>'); 

    });

    $(document).on('click', '.btn_remove_additional_info', function(){  
        var button_id = $(this).attr("id");   
        $('.row'+button_id).remove();  
    });


    $('#crop_category').change(function(){
        $("#crop_id option").remove();
        var id = $(this).val();
        $.ajax({
            url : "{{route('admin.products.getCropCategories')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id
                },
            type: 'post',
            dataType: 'json',
            success: function( result )
            {
                $('#crop_id').append($('<option>', {value:'', text:'Select'}));
                $.each( result, function(k, v) {
                    $('#crop_id').append($('<option>', {value:k, text:v}));
                });
            },
            error: function()
            {
                alert('error...');
            }
        });
    })

   $('#company_id').change(function(){
        $("#category_id option").remove();
        $("#subcategory_id option").remove();
        var id = $(this).val();

    
        $.ajax({
            url : "{{route('admin.products.getCategories')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id
                },
            type: 'post',
            dataType: 'json',
            success: function( result )
            {
                $('#category_id').append($('<option>', {value:'', text:'Select'}));
                $.each( result, function(k, v) {
                    $('#category_id').append($('<option>', {value:k, text:v}));
                });
            },
            error: function()
            {
                alert('error...');
            }
        });
    });
        $('.btnremveprce').click(function(){
         var button_id = $(this).attr("id");   
         var id= button_id.split(",")[1];
         //alert(button_id);
                $.ajax({
            url : "{{route('admin.products.removeProductProduct')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id
                },
            type: 'post',
            dataType: 'json',
            success: function( result )
            {
                $('#row'+button_id.split(",")[0]+'').remove();  
                //$('#crop_id').append($('<option>', {value:'', text:'Select'}));
               // $.each( result, function(k, v) {
               //     $('#crop_id').append($('<option>', {value:k, text:v}));
                //});
            },
            error: function()
            {
                alert('error...');
            }
        });
        // removeProductattribute
    });
    $('#category_id').change(function(){
        $("#subcategory_id option").remove();
        var id = $(this).val();
        $.ajax({
            url : "{{route('admin.products.getSubCategories')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id
                },
            type: 'post',
            dataType: 'json',
            success: function( result )
            {
                if(result!=''){
                    $('#totalsubcategory').val(1);
                      $('.subcategory').show();
                    $('#subcategory_id').append($('<option>', {value:'', text:'Select'}));
                $.each( result, function(k, v) {
                    $('#subcategory_id').append($('<option>', {value:k, text:v}));
                });
                }
                else{
                    $('#totalsubcategory').val(0);
                     $('.subcategory').hide();
                }
               
            },
            error: function()
            {
                alert('error...');
            }
        });
    });
            $('#subcategory_id').change(function(){
                var id=$('#company_id').val();
                var category = $('#subcategory_id').val();
                $("#subsubcategory_id option").remove();
                
                    $.ajax({
                    url : "{{route('admin.products.getSubCategories')}}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": category
                    // "parentid":category
                        },
                    type: 'post',
                    dataType: 'json',
                    success: function( result )
                    {
                        if(result!=''){
                            $('.subsubcategory').show();
                            $('#totalsubsubcategory').val(1);
                            $('#subsubcategory_id').append($('<option>', {value:'', text:'Select Sub Category'}));
                            $.each( result, function(k, v) {
                                $('#subsubcategory_id').append($('<option>', {value:k, text:v}));
                            });
                        }
                        else{
                            $('.subsubcategory').hide();
                            $('#totalsubsubcategory').val(0);
                        }
                    
                    
                    },
                    error: function()
                    {
                        alert('error...');
                    }
                });
            });
    $("#edit_product").validate({
        ignore: [],
                errorClass: 'error text-change',
                successClass: 'validation-valid-label',
                highlight: function (element, errorClass) {
                    $(element).removeClass(errorClass);
                },
                unhighlight: function (element, errorClass) {
                    $(element).removeClass(errorClass);
                },
                validClass: "validation-valid-label",
        rules: {
            name: {
                required: true,
                // lettersonly: true,
                maxlength: 300,
            },
            // sku: {
            //     required: true,
            //     maxlength: 25,
            // },
            // crop_id: {
            //     required: true,
            // },
            // crop_category: {
            //     required: true,
            // },
            category_id: {
                required: true,
            },
            subcategory_id: {
                     required: function () {
                         return $('#totalsubcategory').val()==1;
                     }
                },
                subsubcategory_id: {
                     required: function () {
                         return $('#totalsubsubcategory').val()==1;
                     }
                },
            // hsn_code: {
            //     required: true,
            //     maxlength: 8,
            //     minlength:2,
            // },
            // subcategory_id: {
            //     required: true,
            // },
            company_id : {
                required: true,
            },
            status : {
                required: true,
            }
        },
        messages: {
            "name":{
                required: "Please enter name",
                maxlength:"Please enter no more than 25 character",
            },
            "sku": {
                required:"Please enter sku",
                maxlength:"Please enter no more than 25 character",
            },
            "crop_id":{
                required: "Please select crop sub-category",
            },
            "crop_category":{
                required: "Please select crop",
            },
            "category_id":{
                required: "Please select category",
            },
            "subcategory_id":{
                required: "Please select sub category",
            },
            "subsubcategory_id": {
                     required:  "Please select sub sub category",
                },
            "company_id":{
                required: "Please select company name",
            },
            "status":{
                required: "Please select status",
            },
            "hsn_code":{
                required: "Please enter HSN Code",
                maxlength: "Please enter maximum 8 character",
                minlength: "Please enter minimum 2 character",
            }
        },
            
        submitHandler: function (form) {
            error = 0;
            document.getElementById('image_error').style.display="none";
            document.getElementById('image_type_error').style.display="none";
            document.getElementById('image').style.display="none";
            var logo = $('#cover').val();
            if(logo!=""){
                var fi = document.getElementById('cover');
                if (fi.files.length > 0) {
                    for (var i = 0; i <= fi.files.length - 1; i++) {
                        var fsize = fi.files.item(i).size;
                        var type = fi.files.item(i).type;
                        var size = fsize/1024;   
                        console.log('fsize'+fsize+' type '+type+' size '+size);
                        if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                            document.getElementById('image_type_error').style.display="block";
                            error = 1;
                        } else if(size>1024){
                            document.getElementById('image_error').style.display="block";
                            error = 1;
                        }
                    }
                } 
            }
            addtional_name = $('#additional_info_name').val();
            addtional_value = $('#addtional_info_value').val();
                $("input:text[name='additional_info[]']").each(function( key, value ){
                    if($(this).val().trim()=='' || $(this).val().trim()==0){
                        $(this).css("border","1px solid red");
                        $(this).focus();
                        error = 1;
                    }
                
                });
                $("input:text[name='additional_info_value[]']").each(function( key, value ){
                    if($(this).val().trim()=='' || $(this).val().trim()==0){
                        $(this).css("border","1px solid red");
                        $(this).focus();
                        error = 1;
                    }
                    
                });
                $("input:text[name='unitkey[]']").each(function( key, value ){
                    if($(this).val().trim()=='' || $(this).val().trim()==0){
                        $(this).css("border","1px solid red");
                        $(this).focus();
                        error = 1;
                    }
                    
                });
                $("input:text[name='prices[]']").each(function( key, value ){
                    if($(this).val().trim()=='' || $(this).val().trim()==0){
                        $(this).css("border","1px solid red");
                        $(this).focus();
                        error = 1;
                    }
                    
                });
                $("input:text[name='sale_prices[]']").each(function( key, value ){
                    if($(this).val().trim()=='' || $(this).val().trim()==0){
                        $(this).css("border","1px solid red");
                        $(this).focus();
                        error = 1;
                    }
                    
                });
                $("input:text[name='quantities[]']").each(function( key, value ){
                    if($(this).val().trim()=='' || $(this).val().trim()==0){
                        $(this).css("border","1px solid red");
                        $(this).focus();
                        error = 1;
                    }
                    
                });
                $("input:text[name='max_quantities[]']").each(function( key, value ){
                    if($(this).val().trim()=='' || $(this).val().trim()==0){
                        $(this).css("border","1px solid red");
                        $(this).focus();
                        error = 1;
                    }
                    
                });
                // $("input:text[name='gst[]']").each(function( key, value ){
                //     if($(this).val().trim()=='' || $(this).val().trim()==0){
                //         $(this).css("border","1px solid red");
                //         $(this).focus();
                //         error = 1;
                //     }
                    
                // });
                // console.log(addtional_value);
                if (addtional_name!="") {
                    if (addtional_value=="") {
                        $('#additional_info_value_error').attr('style', 'display: block !important','color:red!important');
                        $('#additional_info_error').attr('style', 'display: none !important');
                        return false;   
                    }
                } else{
                    console.log('addtional_value');
                    if (addtional_value!="") {
                        $('#additional_info_error').attr('style', 'display: block !important','color:red!important');
                        $('#additional_info_value_error').attr('style', 'display: none !important');
                        return false;   
                    }   
                }
                $("input:text[id='addtional_name1[]']").each(function( key, value ){
                    if($(this).val().trim()=='' || $(this).val().trim()==0){
                        $(this).css("border","1px solid red");
                        $(this).focus();
                        error = 1;
                    }
                    
                });
                $("input:text[id='value1[]']").each(function( key, value ){
                    if($(this).val().trim()=='' || $(this).val().trim()==0){
                        $(this).css("border","1px solid red");
                        $(this).focus();
                        error = 1;
                    }
                    
                });
                var offer_title = $('#offer_title').val();
                var offer_product = $('#offer_product').val();
                var offer_quantity = $('#offer_quantity').val();
                var offer_product_image = $('#offer_product_image').val();
                document.getElementById("offer_title_error").style.display = "none";
                document.getElementById("offer_product_error").style.display = "none";
                document.getElementById("offer_quantity_error").style.display = "none";
                document.getElementById("offer_image_error").style.display = "none";
                if (offer_title!="" || offer_product!="" || offer_quantity!="") {
                    if(offer_title=="" && offer_product=="" && offer_quantity=="") {
                        error=1;
                    } else {
                        if(offer_title==""){
                            error=1;
                            document.getElementById("offer_title_error").style.display = "block";
                        }
                        if(offer_product==""){
                            error=1;
                            document.getElementById("offer_product_error").style.display = "block";
                        }
                        if(offer_quantity==""){
                            error=1;
                            document.getElementById("offer_quantity_error").style.display = "block";
                        }
                    }
                }
                if (error == 1)  {
                    return false;
                }
            $('#add-submit').attr('disabled', true);
            form.submit();
        },
    });

    $('.numeric').on('input', function (event) {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    });

    $('.addition_information').on('click', function () {
        console.log('enter');
    });
    
    function GetFileSize() {
        var fi = document.getElementById('cover');
        document.getElementById('image').style.display="none";
        document.getElementById('image_error').style.display="none";
        document.getElementById('image_type_error').style.display="none";
        if (fi.files.length > 0) {
            for (var i = 0; i <= fi.files.length - 1; i++) {
                var fsize = fi.files.item(i).size;
                var type = fi.files.item(i).type;
                var size = fsize/1024;   
                console.log(size);
                console.log(type);
                
                if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                    document.getElementById('image_type_error').style.display="block";
                } else if(size>1024){
                    document.getElementById('image_error').style.display="block";
                }
            }
        }
    }
    function GetFileSizeType() {
        var fi = document.getElementById('cover');
        document.getElementById('image').style.display="none";
        document.getElementById('image_error').style.display="none";
        document.getElementById('image_type_error').style.display="none";
        if (fi.files.length > 0) {
            for (var i = 0; i <= fi.files.length - 1; i++) {
                var fsize = fi.files.item(i).size;
                var type = fi.files.item(i).type;
                var size = fsize/1024;   
                console.log(size);
                console.log(type);
                
                if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                    document.getElementById('image_type_error').style.display="block";
                } else if(size>1024){
                    document.getElementById('image_error').style.display="block";
                }
            }
        }
    }
        
    function add_info(id) {
        autocomplete(document.getElementById(id), info);
    }

        var data = <?php echo json_encode($infos) ?>;
        var count=data.length;
        var info =[];
        for(var i=0;i<count;i++){
            var val = Object.values(data[i]);
            info.push(val[0]);
        }
        console.log(info);
        autocomplete(document.getElementById("additional_info_name"), info);
        function autocomplete(inp, arr) {
            var currentFocus;
            inp.addEventListener("input", function(e) {
            var a, b, i, val = this.value;
            closeAllLists();
            if (!val) { return false;}
            currentFocus = -1;
            a = document.createElement("DIV");
            console.log("en");
            a.setAttribute("id", this.id + "autocomplete-list");
            a.setAttribute("class", "autocomplete-items");
            this.parentNode.appendChild(a);
            for (i = 0; i < arr.length; i++) {
                if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                    b = document.createElement("DIV");
                    b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                    b.innerHTML += arr[i].substr(val.length);
                    b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                    b.addEventListener("click", function(e) {
                        inp.value = this.getElementsByTagName("input")[0].value;
                        console.log('value'+inp.value);
                        closeAllLists();
                    });
                    a.appendChild(b);
                }
            }
        });
        inp.addEventListener("keydown", function(e) {
              var x = document.getElementById(this.id + "autocomplete-list");
              if (x) x = x.getElementsByTagName("div");
              if (e.keyCode == 40) {
                currentFocus++;
                addActive(x);
              } else if (e.keyCode == 38) { 
                currentFocus--;
                addActive(x);
              } else if (e.keyCode == 13) {
                e.preventDefault();
                if (currentFocus > -1) {
                  if (x) x[currentFocus].click();
                }
              }
        });
        function addActive(x) {
            if (!x) return false;
            removeActive(x);
            if (currentFocus >= x.length) currentFocus = 0;
            if (currentFocus < 0) currentFocus = (x.length - 1);
            x[currentFocus].classList.add("autocomplete-active");
        }
        function removeActive(x) {
            for (var i = 0; i < x.length; i++) {
              x[i].classList.remove("autocomplete-active");
            }
        }
        function closeAllLists(elmnt) {
            var x = document.getElementsByClassName("autocomplete-items");
            for (var i = 0; i < x.length; i++) {
              if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
              }
            }
        }
        document.addEventListener("click", function (e) {
              closeAllLists(e.target);
        });


    }
    $('.character').on('input', function (event) {
        this.value = this.value.replace(/[^0-9\.\a-z\A-Z\@ _()$&]/g, '');
    });

    $("#translate").on('click',function(){
        $("#add_submit").css('display','block');
        var name = $("#name").val();
        var description = $('#description').val();
        var description = $('#description').val();
        
        var additional_info_txt_one = $(".additional_info_txt_one").val();
        var additional_info_val_one = $(".additional_info_val_one").val();
        
        var offer_title = $("#offer_title").val();
        var offer_product = $("#offer_product").val();

        var tempArray = {};
        $(".addition_information_txt").each(function(index) {
            var aitid = $(this).attr("data-addition-information-txt-id");
            tempArray['additional_info_txt_'+aitid] = $(this).val();
        });
        $(".addition_information_value").each(function(index) {
            var aitid = $(this).attr("data-addition-information-value-id");
            tempArray['additional_info_val_'+aitid] = $(this).val();
        });
        
        tempArray["name"] = name;
        tempArray["description"] = description;
        tempArray["additional_info_txt_one"] = additional_info_txt_one;
        tempArray["additional_info_val_one"] = additional_info_val_one;
        tempArray["offer_title"] = offer_title;
        tempArray["offer_product"] = offer_product;
        $(".box").css({'opacity':'0.5','pointer-events': 'none'});
        $.ajax({
            url : "{{route('admin.translate')}}",
            data:  tempArray,
            type: 'get',
            dataType: 'json',
            success: function( result )
            {
                var jsonObj = result;
                $("#name_hi").val(jsonObj.name.hi);
                $("#name_gu").val(jsonObj.name.gu);
                $("#description_hi").val(jsonObj.description.hi);
                $("#description_gu").val(jsonObj.description.gu);
                
                $("#offer_title_gu").val(jsonObj.offer_title.gu);
                $("#offer_title_hi").val(jsonObj.offer_title.hi);
                
                $("#offer_product_gu").val(jsonObj.offer_product.gu);
                $("#offer_product_hi").val(jsonObj.offer_product.hi);

                $(".additional_info_txt_hi_one").val(jsonObj.additional_info_txt_one.hi);
                $(".additional_info_txt_gu_one").val(jsonObj.additional_info_txt_one.gu);

                $(".additional_info_val_hi_one").val(jsonObj.additional_info_val_one.hi);
                $(".additional_info_val_gu_one").val(jsonObj.additional_info_val_one.gu);

                for (myObj in jsonObj) {
                    var tempName = String(myObj); 
                    $("."+tempName+"_hi").val(jsonObj[myObj].hi);
                    $("."+tempName+"_gu").val(jsonObj[myObj].gu);
                } 
                $(".box").css({'opacity':'1','pointer-events': 'auto'});
            },
            error: function(error)
            {
                console.log(error);
            }
        });
    });

</script>
@endsection