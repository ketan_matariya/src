@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if(!$list->isEmpty())
            <div class="box">
                <div class="box-body">
                    <div class="box-body">
                        <div class="box-header with-border bg-title border-header">
                            <h4 class="page-title header-color">Manage Tehsil</h4>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table border-b1px" id="table-id">
                            <thead>
                                <tr>
                                    <th class="col-md-4">ID</th>
                                    <th class="col-md-4">Name</th>
                                    <th class="col-md-4">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($list as $tehsil)
                                <tr>
                                    <td>
                                        {{ $tehsil->SDTCode }}
                                    </td>
                                    <td>
                                        {{ $tehsil->name }}
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('admin.country_data.showVillage', $tehsil->SDTCode) }}" class="btn btn-default btn-sm"><i class="fa fa-eye"></i> </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
            
            <!-- /.box -->
            @else
                <div class="box">
                    <p class="alert alert-warning">No tesil created yet. <a href="{{ route('admin.roles.create') }}">Create one!</a></p>
                    
                    <div class="box-footer">
                        <div class="btn-group">
                        </div>
                    </div>
                </div>
            @endif
    </section>
    <!-- /.content -->
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('#table-id').DataTable();
    } );
</script>
@endsection
