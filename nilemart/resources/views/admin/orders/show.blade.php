@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-6">
                        <h2>
                            <a href="#">{{$order->address->alias}}</a> <br />
                            <small>{{$order->email}}</small> <br />
                            <small>{{$order->address->phone}}</small> <br />
                            <small>reference: <strong>{{$order->reference}}</strong></small>
                        </h2>
                    </div>
                    <div class="col-md-3 col-md-offset-3">
                        <h2><a href="@if($order->invoice_path) {{asset($order->invoice_path)}} @else {{route('admin.orders.invoice.generate', $order['id'])}} @endif" class="btn btn-primary btn-block" target="_blank">Download Invoice</a></h2>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <h4> <i class="fa fa-shopping-bag"></i> Order Information</h4>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th class="col-md-2">Date</th>
                            <th class="col-md-2">Customer</th>
                            <th class="col-md-2">Phone</th>
                            <th class="col-md-2">Driver</th>
                            <th class="col-md-2">Payment</th>
                            <th class="col-md-2">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{ date('M d, Y h:i a', strtotime($order['created_at'])) }}</td>
                        <td><a href="{{ route('admin.customers.show', $customer->id) }}">{{ $order->address->alias }}</a></td>
                        <td>{{ $order->address->phone }}</td>
                        <td>@if(isset($order['driver_id'])){{$order['driver']}}@else Driver not assign yet @endif</td>
                        <td><strong>{{ $order['payment'] }}</strong></td>
                        <td>
                            <button type="button" class="btn btn-info btn-block">{{ ucfirst($currentStatus->name) }}</button>
                        </td>
                    </tr>
                    <!-- </tbody>
                    <tbody> -->
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="bg-warning">Subtotal</td>
                        <td class="bg-warning">Rs {{ $order['total_products'] }}</td>
                    </tr>
                    {{-- <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="bg-warning">+ Tax</td>
                        <td class="bg-warning">{{ $order['tax'] }}</td>
                    </tr> --}}
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="bg-warning">+ Shipping</td>
                        <td class="bg-warning">{{ $order['total_shipping'] }}</td>
                    </tr>
                    {{-- @if($order['wallet_point']!=0.0 && $order['wallet_point']!="")
                        @php
                            $wallet_amount = $order['wallet_point'] * env('DISCOUNT_POINT');
                            $wallet_amount = number_format($wallet_amount,2);
                        @endphp
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="bg-warning">- Discount</td>
                            <td class="bg-warning">{{ $wallet_amount }}</td>
                        </tr>
                    @endif --}}
                    <!--<tr>-->
                    <!--    <td></td>-->
                    <!--    <td></td>-->
                    <!--    <td></td>-->
                    <!--    <td class="bg-success text-bold">Order Total</td>-->
                    <!--    <td class="bg-success text-bold">Rs {{ $order['total'] }}</td>-->
                    <!--</tr>-->
                    @php
                        if (isset($wallet_amount)) {
                            $total = $order['total'] + $order['total_shipping'] + $order['tax'] - $wallet_amount;
                        } else{
                            $total = $order['total'] + $order['total_shipping'] + $order['tax'];
                        }
                    @endphp
                    @if($order['total_paid'] != $order['total'])
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="bg-danger text-bold">Total paid</td>
                            <td class="bg-danger text-bold">Rs {{ number_format($total,2) }}</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            @if($order)
                @if(!$items->isEmpty())
                    <div class="box-body">
                        <h4> <i class="fa fa-gift"></i> Items</h4>
                        <table class="table table-striped">
                            <thead>
                            <!--<th class="col-md-2">HSN Code</th>-->
                            <th class="col-md-2">Name</th>
                            <th class="col-md-2">Unit</th>
                            <th class="col-md-2">Quantity</th>
                            <th class="col-md-2">Price</th>
                            <th class="col-md-2">Shipping Price</th>
                            <!--<th class="col-md-1">Tax</th>-->
                            <th class="col-md-2">Total</th>
                            @if($offer_status==1)
                                <th class="col-md-2">Offer Product</th>
                            @endif
                            </thead>
                            <tbody>
                               
                            @foreach($items as $item)
                            @php
                                $item->product_price = str_replace(",","",$item->product_price);
                                $total = ($item->product_price*$item->quantity)+$item->shipping_price;
                                $total = number_format($total ,2);
                                $total = str_replace(",","",$total);
                            @endphp
                                <tr>
                                    <!--<td>{{ $item->hsn_code }}</td>-->
                                    <td>{{ $item->product_name }}</td>
                                    <td>{{ $item->unit }}</td>
                                    <td>{{ $item->quantity }}</td>
                                    <td>{{ str_replace(",","",$item->product_price) }}</td>
                                    <td>{{ number_format($item->shipping_price,2) }}</td>
                                    <td>{{ $total }}</td>
                                    @if($offer_status==1)
                                        <td>{{ $item->offer_product }}</td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4> <i class="fa fa-map-marker"></i> Address</h4>
                            <table class="table table-striped">
                                <thead>
                                    <th class="col-md-2">Address</th>
                                    @if(isset($order->order_address->landmark))
                                    <th class="col-md-2">Land Mark</th>@endif
                                    <!--<th class="col-md-2">Tehsil</th>-->
                                    <!--<th class="col-md-2">City</th>-->
                                    <!--<th class="col-md-2">State</th>-->
                                    <th class="col-md-2">Country</th>
                                    <th class="col-md-2">Zip</th>
                                    <!-- <th class="col-md-2">Province</th> -->
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{ $order->order_address->address }}</td>
                                    @if(isset($order->order_address->landmark))<td>{{ $order->order_address->landmark }}</td>@endif
                                    <!--<td>@if(isset($order->address->village_id)){{ $order->address->village_id }}@endif</td>-->
                                    <!--<td>@if(isset($order->address->tehsil_id)){{ $order->address->tehsil_id }}@endif</td>-->
                                    <!--<td>@if(isset($order->address->city)){{ $order->address->city }}@endif</td>-->
                                    <!-- <td>
                                        @if(isset($order->address->province))
                                            {{ $order->address->province->name }}
                                        @endif
                                    </td> -->
                                    <!--<td>@if(isset($order->address->state_code)){{ $order->address->state_code }}@endif</td>-->
                                    <td>{{ $order->address->country->name }}</td>
                                    <td>{{ $order->order_address->postcode }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            <!-- /.box -->
            <div class="box-footer">
                <div class="btn-group">
                    <a href="{{ route('admin.orders.index') }}" class="btn btn-default">Back</a>
                    @if($order->order_status_id!=1 && $order->order_status_id!=4)
                        @if($permission->edit==1)<a href="{{ route('admin.orders.edit', $order->id) }}" class="btn btn-primary">Edit</a>
                        @endif
                    @endif
                </div>
            </div>
        @endif
            </div>
        </div>

    </section>
    <!-- /.content -->
@endsection