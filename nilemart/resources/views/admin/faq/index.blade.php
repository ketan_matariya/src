@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
<section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <div class="box-body">
                <div class="box-header with-border bg-title border-header">
                    <h4 class="page-title header-color">Manage Faq</h4>
                </div>
            </div>
            <div class="box-body">
                <div class="scroll">
                @if($faq)
                    <table class="table border-b1px" id="table-id">
                        <thead>
                            <tr>
                                <th>Question</th>
                                <th>Answer</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($faq as $data)
                                <tr>
                                    <td>
                                        {{ $data->question }}
                                    </td>
                                    <td>{{ $data->answer }}</td>
                                    <td>
                                        <form action="{{ route('admin.faq.destroy', $data->id) }}" method="get" class="form-horizontal">
                                            @csrf
                                           
                                            <div class="btn-group btn-display">
                                                @if($permission->edit==1)<a href="{{ route('admin.faq.edit', $data->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>@endif
                                                @if($permission->delete==1)<button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>@endif
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                    <p class="alert alert-warning">No attribute yet. <a href="{{ route('admin.attributes.create') }}">Create one</a></p>
                @endif
            </div>
        </div>
        <div class="box-footer">
            <div class="box-body">
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
    <!-- /.content -->
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('#table-id').DataTable();
    } );
</script>
@endsection