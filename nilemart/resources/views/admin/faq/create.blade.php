@extends('layouts.admin.app')
@section('content')
    <!-- Main content -->
    <section class="content">
        <!-- @include('layouts.errors-and-messages') -->
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">@if(isset($faq->id)) Edit @else Add @endif Faq</h4>
                    </div>
                </div>
                <form action="{{ route('admin.faq.store') }}" method="post" class="form" enctype="multipart/form-data" id="add_crop">
                <input type="hidden"  id="id" name="id" value="@if(isset($faq->id)){{ $faq->id }}@endif">
                    <div class="box-body">
                        <div class="row">
                            {{ csrf_field() }}
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="question">Question <span class="text-danger">*</span></label>
                                                    <input type="text" name="question" id="question" placeholder="Question" class="form-control character" value="@if(isset($faq->id)){{ $faq->question }}@else{!! old('question')  !!}@endif" maxlength="">
                                                    @if ($errors->has('question'))
                                                        <span class="text-danger" style="color: red">{{ $errors->first('question') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    
                                        <div class="col-lg-12">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="answer">Answer <span class="text-danger">*</span></label>
                                                    <input type="text" name="answer" id="answer" placeholder="Answer" class="form-control character" value="@if(isset($faq->id)){{ $faq->answer }}@else{!! old('answer')  !!}@endif" maxlength="">
                                                    @if ($errors->has('answer'))
                                                        <span class="text-danger" style="color: red">{{ $errors->first('answer') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ route('admin.faq.index') }}" class="btn btn-default">Cancel</a>
                            <!-- <input type="button" class="btn btn-info" value="Translate" id="translate"> -->
                            <button type="submit" id="submit" class="btn btn-primary button-color">@if(isset($faq->id)) Update @else Add @endif</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection

@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">

    $("#add_crop").validate({
        ignore: [],
        errorClass: 'error',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            question: {
                required: true,
                maxlength: 150,
            }
            answer: {
                required: true,
                maxlength: 500,
            }
        },
        messages: {
            "question":{
                required: "Please enter question",
                maxlength: "Please enter max 150 deigit",
            }

            "answer":{
                required: "Please enter answer",
                maxlength: "Please enter max 500 deigit",
            }
        },
        submitHandler: function(form) {
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
    });

    $('.character').on('input', function (event) {
        this.value = this.value.replace(/[^0-9\.\a-z\A-Z\@ _()$&]/g, '');
    });

</script>
@endsection
