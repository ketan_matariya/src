@extends('layouts.admin.app')

@section('title','View Drivers - Royal Laundry')
@section('content')
    <!-- Main content -->
    <section class="content">
        
    @include('layouts.errors-and-messages')
    <!-- Default box -->
            <div class="box">
                <div class="box-body">
                    <div class="box-body">
                        <div class="box-header with-border bg-title border-header"  >
                            <h4 class="page-title header-color">Manage Drivers Payment</h4>
                        </div>                        	
                    </div>                    
              		<div class="box-body scroll">
							<div>
								 <table class="table border-b1px" id="driver-table">
									<thead>
										<th>Name</th>
										<th>Email</th>
										<th>Contact No.</th>
										<th>Payment</th>
										<th>Action</th>
								</thead>
								<tbody>
									@php $i=1; @endphp 
									@foreach($drivers as $driver)
									<tr>
										<td>{{$driver->name}}</td>
										<td>{{$driver->email}}</td>
										<td>{{$driver->contact}}</td>
										<td>
										    <!--<form action="{{ url('admin/payment/driver/'.$driver->id.'/total/'.$driver->total) }}" method="get" class="form-horizontal">-->
										        <button class="btn btn-primary hide-modal" id="{{$driver->id}}" onclick="driverNotification(this.id)">{{$driver->total}}</button>
										        
										    <!--</form>-->
										</td>
										<td>
	                                        <form action="{{ url('admin/payment/destroy/'.$driver->id) }}" method="get" class="form-horizontal">
	                                            <div class="btn-group">
	                                                 <a href="{{route('admin.payment.show',$driver->id)}}" title="Show" class="btn btn-default btn-sm"><i class="fa fa-eye"></i> </a> 
                                                </div>
	                                        </form>
	                                    </td>
									</tr>
									@endforeach
								</tbody>
							</table>
							</div>
						</div>
	                <!-- /.box-body -->
				</div>
			</div>
			<div class="modal fade" id="exampleModalCenter" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" align="center"><b>Driver Payment</b></h4>
                        </div>
                        <div class="modal-body">
                            <form role="form" method="post" action="{{route('admin.driver.payment')}}">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" id="model_driver_id" value="">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">OTP</label> 
                                        <input type="text" class="form-control" name="otp" placeholder="Enter OTP" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Total Payment</label> 
                                        <input type="text" id="total" class="form-control" name="total" readonly>
                                    </div>
                                </div>
                                <button class="btn btn-primary">Payment</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
			<!-- /.box -->
	    </section>
	    <!-- /.content -->
	@endsection
	@section('js')
    <script>
        $(document).ready(function() {
            $('#driver-table').DataTable({
                "order": [[ 7, "desc" ]],
                "columnDefs": [
                    { "visible": false, "targets": 7 }
                  ]
            });
            
        } );
        
        function driverNotification(id){
            total = $('#'+id).text();
            $('#model_driver_id').val(id);
            $('#total').val(total);
            if(total!=0.00){
                $("#exampleModalCenter").modal('show');
                $.ajax({
                    url : "{{route('admin.driver.payment.otp')}}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": id,
                        "total": total
                    },
                    type: 'get',
                    success: function( result )
                    {
                        console.log(result);
                    },
                    error: function(error)
                    {
                        console.log(error);
                    }
                });
            }else{
                // alert('dd');
                $("#exampleModalCenter").modal('hide');
                
            }
        }
    </script>
	@endsection
	