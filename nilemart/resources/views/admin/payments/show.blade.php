@extends('layouts.admin.app')

@section('title','View Drivers - Royal Laundry')
@section('content')
    <!-- Main content -->
    <section class="content">
        
    @include('layouts.errors-and-messages')
    <!-- Default box -->
            <div class="box">

                <div class="box-body">
                    <div class="box-body">
                        <div class="box-header with-border bg-title border-header"  >
                            <h4 class="page-title header-color">Manage Driver Order</h4>
                        </div>                        	
                    </div>                    
                  		<div class="box-body scroll">
							<div>
								 <table class="table border-b1px" id="driver-table">
									<thead>
									    <th>Order Id</th>
                                        <th>Date</th>
										<th>Customer</th>
										<th>Contact No.</th>
										<th>Total</th>
                                        <th >Status</th>
								</thead>
								<tbody>
									@php $i=1; @endphp 
									@foreach($orders as $data)
									<tr>
										<td>{{$data->id}}</td>
                                        <td>{{date('M d, Y h:i a', strtotime($data->created_at))}}</td>
										<td>{{$data->customer_name}}</td>
										<td>{{$data->mobile}}</td>
										<td>{{str_replace(",","",number_format($data->total_shipping+$data->total,2))}}</td>
										<td><p class="label" style=" color: black; background-color: {{ $data->color }}">{{ ucfirst($data->status) }}</p></td>
									</tr>
									@endforeach
								</tbody>
							</table>
							</div>
						</div>
				                <!-- /.box-body -->
				</div>
			</div>
				            <!-- /.box -->
	    </section>
	    <!-- /.content -->
	@endsection
	@section('js')
    <script>
        $(document).ready(function() {
            $('#driver-table').DataTable({
                "order": [[ 7, "desc" ]],
                "columnDefs": [
                    { "visible": false, "targets": 7 }
                  ]
            });
        } );
    </script>
	@endsection
	