@extends('layouts.admin.app')

@section('content')

    <!-- Main content -->
    <section class="content">
    @if ($message = Session::get('message'))
                            <div class="alert alert-danger alert-block" style="background-color: #00a65a !important; border-color: #00a65a;">
                                <strong>{{ $message }}</strong>
                            </div>
                            @endif
        <div class="box">
            <div class="box-body">
                <div class="box-header with-border bg-title border-header">
                    <h4 class="page-title header-color">Tearm & Condition</h4>
                </div>
            </div>
            <div class="box-body">
                <form action="{{ route('admin.cms.tearm_condtion.update') }}" id="edit_tearm_condition" method="post" class="form">
                    <div class="box-body">
                        {{ csrf_field() }}
                        <input type="text" name="id" id="id" value="{{$data->id}}" hidden>
                        <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Title <span class="text-danger">*</span></label>
                                    <input type="text" name="title" id="title" placeholder="Title" class="form-control character" value="{!! $data->title ?: old('title')  !!}" maxlength="25">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="description">Description <span class="text-danger">*</span></label>
                                    <textarea  name="description" id="description" placeholder="Description" maxlength="500" style="width: 486px; height: 164px;">
                                    	{!! $data->description ?: old('description')  !!}
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    @include('admin.shared.status-select', ['status' => $data->status])
                                </div>  
                            </div>
                            <div class="col-lg-4">
                                
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                        	<a href="{{ route('admin.cms.tearm_condtion.index') }}" class="btn btn-default btn-sm">Cancel</a>
                            <button type="submit" class="btn btn-primary btn-sm">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection


@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
@endsection

