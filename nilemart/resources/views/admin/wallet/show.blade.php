@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if(!$customer_points->isEmpty())
            <div class="box">
                <div class="box-body">
                    <div class="box-body">
                        <div class="box-header with-border bg-title border-header">
                            <h4 class="page-title header-color">{{ $customer->name }} Wallet Summery</h4>
                        </div>
                        <div style="float:right">
                           Total Point : <span class="header-color"><b>  {{ $total_point }} </b></span>
                        </div>

                    </div>
                    @if(!$customer_points->isEmpty())
                    <div class="box-body">
                        <table class="table border-b1px" id="table-id">
                            <thead>
                            <tr>
                                <th class="col-md-2">Point</th>
                                <th class="col-md-2">Amount</th>
                                <th class="col-md-2">Credit or Debit</th>
                                <th class="col-md-2">Description</th>
                                <th class="col-md-2">Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($customer_points as $customer_point)
                                @if($customer_point->amount != 0)
                                    <tr>    
                                        <td><span class="header-color">{{ $customer_point->points }}</span></td>
                                        <td><span class="header-color">{{ $customer_point->amount }}</span></td>
                                        <td>
                                           <span class="header-color"> @if($customer_point->credit_or_debit == 'credit'){{ 'Credit'  }}@else{{ 'Debit'  }}@endif </span>
                                        </td>
                                        <td>
                                            <span class="header-color"> {{ $customer_point->description }} </span>
                                        </td>
                                        <td>
                                           <span class="header-color"> {{ date('d-m-Y H:i:s',strtotime($customer_point->created_at)) }} </span>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif
                    <div class="box-footer">
                        <div class="btn-group">
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="box">
                <div class="box-body"><p class="alert alert-warning">Data Not Found.</p></div>
                <div class="box-footer">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
        @endif

    </section>
    <!-- /.content -->
@endsection

@section('js')

<script type="text/javascript">


    $(document).ready(function() {
        $('#table-id').DataTable({"order": [[ 4, "desc" ]]});
    });

</script>

@endsection