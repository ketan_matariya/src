@extends('layouts.admin.app')

@section('title','View Drivers - Royal Laundry')
@section('content')
    <!-- Main content -->
    <section class="content">
        
    @include('layouts.errors-and-messages')
    <!-- Default box -->
            <div class="box">

                <div class="box-body">
                    <div class="box-body">
                        <div class="box-header with-border bg-title border-header"  >
                            <h4 class="page-title header-color">Manage Drivers</h4>
                        </div>                        	
                    </div>                    
                  		<div class="box-body ">
							<div class="scroll"><!--scroll-->
								 <table class="table border-b1px " id="driver-table">
									<thead>
										<th>Name</th>
										<!--<th>Vehicle Type</th>-->
										<th>Email</th>
										<th>Contact No.</th>
										<th>Profile</th>
										<th>Status</th>
										<th>Action</th>
										<!--<th>Updated At</th>-->
								</thead>
								<tbody>
									@php $i=1; @endphp 
									@foreach($drivers as $driver)
									<tr>
										<td>{{$driver->name}}</td>
										<!--<td>{{$driver->vehiclename}}</td>-->
										<td>{{$driver->email}}</td>
										<td>{{$driver->contact}}</td>
										<td>@if($driver->profile!="") 
											<img src="{{ url('drivers/'.$driver->profile) }}" height="50px" width="50px">@endif
										</td>
										<!-- <td>{{$driver->postalcode}}</td>
										<td>{{$driver->address}}</td> -->
										<td>@include('layouts.status', ['status' => $driver['status']])</td>
										<!-- <td>{{$driver->rating}}</td> -->
										<td>
	                                        <form action="{{ url('admin/driver/destroy/'.$driver->id) }}" method="get" class="form-horizontal">
	                                            <div class="btn-group btn-display">
	                                                <!-- <a href="{{route('admin.driver.usershow',$driver->id)}}" title="Show" class="btn btn-default btn-sm"><i class="fa fa-eye"></i> </a> -->
                                                    @if($permission->edit==1)<a href="{{route('admin.driver.edit',$driver->id)}}" title="Edit" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>@endif
                                                    @if($permission->delete==1)<button onclick="return confirm('Are you sure you want to delete this record?')" title="Delete" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>@endif
	                                            </div>
	                                        </form>
	                                    </td>
	                                    <!--<td>{{ $driver->updated_at }}</td>-->
									</tr>
									@endforeach
								</tbody>
								</table>
							</div>
							</div>
						</div>
				                <!-- /.box-body -->
				</div>
			</div>
				            <!-- /.box -->
	    </section>
	    <!-- /.content -->
	@endsection
	@section('js')
    <script>
        $(document).ready(function() {
            $('#driver-table').DataTable({
                "order": [[ 7, "desc" ]],
                "columnDefs": [
                    { "visible": false, "targets": 7 }
                  ]
            });
        } );
    </script>
	@endsection
	