@extends('layouts.admin.app')

@section('title','Add driver - Royal Laundry')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="box-header with-border bg-title border-header">
                    <h4 class="page-title header-color">@if(isset($drivers->id)) Edit @else  Add @endif Driver</h4>
                </div>
            </div>

            <div class="box-body">
                <form enctype="multipart/form-data" action="{{ route('admin.driver.store') }}" id="create_driver" method="post" class="form">
                    <div class="box-body">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" id="id" value="@if(isset($drivers->id)){{$drivers->id}}@endif">
                         <div class="row">
                            <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Driver Name <span class="text-danger">*</span></label>
                                    <input type="text" name="name" id="name" placeholder="Driver Name" autocomplete="off" class="form-control character" value="@if(old('name')!=''){{old('name')}}@elseif(isset($drivers->name)){{$drivers->name}}@else{{old('name')}}@endif" maxlength="25">
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name_hi"><span class="text-danger">*</span></label>
                                    <input type="text" name="name_hi" id="name_hi" placeholder="Driver Name" autocomplete="off" class="form-control character" value="@if(old('name_hi')!=''){{old('name_hi')}}@elseif(isset($drivers->name_hi)){{$drivers->name_hi}}@else{{old('name_hi')}}@endif" maxlength="25">
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name_gu"><span class="text-danger">*</span></label>
                                    <input type="text" name="name_gu" id="name_gu" placeholder="Driver Name" autocomplete="off" class="form-control character" value="@if(old('name_gu')!=''){{old('name_gu')}}@elseif(isset($drivers->name_gu)){{$drivers->name_gu}}@else{{old('name_gu')}}@endif" maxlength="25">
                                </div>
                            </div>
                        </div>
                        {{-- <div class="row">
                            <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="vehicle"> Vehicle Name <span class="text-danger">*</span></label>
                                    
                                    <select name="vehicletype" id="vehicletype" class="form-control character">
                                        @if(!isset($vehicle->vehicle_model_name))
                                            <option value="">Select Vehicle Name</option>
                                        @endif
                                       
                                        @foreach($vehicles as $vehicle)
                                            <option value="{{$vehicle->id}}" 
                                                @if(old('vehicletype') == $vehicle->id)
                                                    selected="true"
                                                @endif
                                                @if(isset($drivers->vehicle_type)) 
                                                    @if($vehicle->id == $drivers->vehicle_type) selected @endif
                                                @endif
                                            >
                                                {{ $vehicle->vehicle_model_name }}
                                            </option>
                                        @endforeach                          
                                    </select>
                                </div>
                            </div> 
                        </div> --}}
                        <div class="row">
                            <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="email">Email <span class="text-danger">*</span></label>
                                    <!--<div class="input-group">-->
                                        <!--<span class="input-group-addon">@</span>-->
                                        <input type="email" name="email" id="email" placeholder="Email" class="form-control" value="@if(old('email')!=''){{old('email')}}@elseif(isset($drivers->email)){{$drivers->email}}@else{{old('email')}}@endif" maxlength="255" onblur="validateEmail(this);">
                                    <!--</div>-->
                                    @if ($errors->has('email'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                             <div class="col-sm-2 col-md-2 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Country Code <span class="text-danger">*</span></label>
                                    <select name="country_code" class="form-control">    
                                        <option value="+91" @if(isset($drivers->country_code)) @if($drivers->country_code=="+91") selected="true" @endif @endif>+91</option>
                                        <option value="+12" @if(isset($drivers->country_code)) @if($drivers->country_code=="+12") selected="true" @endif @endif>+12</option>
                                    </select>
                                    @if ($errors->has('country_code'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('country_code') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-2 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="mobile">Contact No. <span class="text-danger">*</span></label>
                                    <input type="text" name="contact" id="mobile" placeholder="Contact No." class="form-control numeric" value="@if(old('contact')!=''){{old('contact')}}@elseif(isset($drivers->contact)){{$drivers->contact}}@else{{old('contact')}}@endif" maxlength="10">
                                    @if ($errors->has('contact'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('contact') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        {{-- 
                        <div class="row">                       
                            <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Vehicle Registration number <span class="text-danger">*</span></label>
                                    <input type="text" name="vehicle_reg_no" id="vehicle_reg_no" accept=".doc, .pdf, .jpg, .png" maxlength="8" pattern="[A-Z]{2}[0-9]{2} [A-Z0-9-]{3}" placeholder="Vehicle Registration number" class="form-control toUpperCase" value="@if(old('vehicle_reg_no')!=''){{old('vehicle_reg_no')}}@elseif(isset($drivers->vehicle_reg_no)){{$drivers->vehicle_reg_no}}@else{{old('vehicle_reg_no')}}@endif">
                                    <span style="color: gray">eg. AA00 AAA</span>
                                </div>
                            </div>
                        </div>
                        --}}
                        <div class="row">
                            <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Licence Number <span class="text-danger">*</span></label>
                                    <input type="text" name="licence_no" id="licence_no" placeholder="Licence number" class="form-control character" value="@if(old('licence_no')!=''){{old('licence_no')}}@elseif(isset($drivers->licence_no)){{$drivers->licence_no}}@else{{old('licence_no')}}@endif" maxlength="25">
                                </div>
                            </div>
                        </div>
                        <!--<div class="row">-->
                        <!--    <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">-->
                        <!--        <div class="form-group">-->
                        <!--            <label for="name">Post Code <span class="text-danger">*</span></label>-->
                        <!--            <input type="text" name="postcode" id="postcode" placeholder="Post code" class="form-control" value="@if(old('postcode')!=''){{old('postcode')}}@elseif(isset($drivers->postalcode)){{$drivers->postalcode}}@else{{old('postcode')}}@endif" maxlength="10">-->
                        <!--        </div>-->
                        <!--    </div>-->
                        <!--</div>-->
                             
                        <div class="row">
                            <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Address <span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="address" id="address" rows="5" placeholder="Address"  maxlength="300">@if(old('address')!=''){{old('address')}}@elseif(isset($drivers->address)){{$drivers->address}}@else{{old('address')}}@endif</textarea>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Address <span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="address_hi" id="address_hi" rows="5" placeholder="Address"  maxlength="300">@if(old('address_hi')!=''){{old('address_hi')}}@elseif(isset($drivers->address_hi)){{$drivers->address_hi}}@else{{old('address_hi')}}@endif</textarea>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="address_gu">Address <span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="address_gu" id="address_gu" rows="5" placeholder="Address"  maxlength="300">@if(old('address_gu')!=''){{old('address_gu')}}@elseif(isset($drivers->address_gu)){{$drivers->address_gu}}@else{{old('address_gu')}}@endif</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div id="add_branch" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Select Address</h4>
                                        </div>
                                        <div class="modal-body" id="show_address"></div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="status">Status </label>
                                    <select name="status" id="status" class="form-control">
                                        <option value="2" @if(isset($drivers->status)) @if($drivers->status == 2) selected @endif @endif>Pending</option>
                                        <option value="1" @if(isset($drivers->status)) @if($drivers->status == 1) selected @endif @endif>Active</option>
                                        <option value="0" @if(isset($drivers->status)) @if($drivers->status == 0) selected @endif @endif>Inactive</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="profile">Profile <span class="text-danger">*</span></label>
                                    <td><input type="file" id="profile" name="profile" accept="image/*" class="form-control" onchange="return imgFileValidation()" value="@if(old('profile')!=''){{old('profile')}}@elseif(isset($drivers->profile)){{$drivers->profile}}@else{{old('profile')}}@endif" ></td>
                                    <label id="profilecustom-error" class="custom-errror">Please select profile</label>
                                    @if(isset($drivers->profile))
                                        <img id="imgupdt" src="@if(isset($drivers->profile)) {{ url('drivers/'.$drivers->profile) }}@endif" width="100px" height="100px">
                                    @else
                                        <img id="imgupdt" width="100px">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ url('admin/driver/index') }}" class="btn btn-default">Cancel</a>
                            <input type="button" class="btn btn-info" value="Translate" id="translate">
                            <button type="submit" id="submit" class="btn btn-primary" @if(!isset($drivers->id)) style="display: none;" @endif>Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
<style type="text/css">
    .custom-errror{
        color: red; font-weight: 400; display: none;
    }
</style>
@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">
function validateEmail(emailField){
var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(emailField.value) == false) 
    {
        alert('Invalid Email Address');
        return false;
    }
    return true;
}
function policeCertificate(){
    document.getElementById("police-certificate-error").style.display = "none";
    var selectFile = document.getElementById('police_certificate');
    var imgPath = selectFile.value;
    var imgExtensions = /(\.jpg|\.jpeg|\.png|\.doc|\.pdf)$/i;

    if (!imgExtensions.exec(imgPath)) {
        alert('plz Select only jpeg and png file.');
        selectFile.value = '';
        return false;   
    }
}
$("#translate").on('click',function(){
    $("#submit").css('display','block');
    var name = $("#name").val();
    var address = $("#address").val();
    // alert(account_hold_name);
    var tempArray = {};
    tempArray["name"] = name;
    tempArray["address"] = address;
    console.log(tempArray);
    $(".box").css({'opacity':'0.5','pointer-events': 'none'});
    $.ajax({
        url : "{{route('admin.translate')}}",
        data:  tempArray,
        type: 'get',
        dataType: 'json',
        success: function( result )
        {
            var jsonObj = result;
            $("#name_hi").val(jsonObj.name.hi);
            $("#name_gu").val(jsonObj.name.gu);

            $("#address_hi").val(jsonObj.address.hi);
            $("#address_gu").val(jsonObj.address.gu);

            // $("#account_hold_name_hi").val(jsonObj.account_hold_name.hi);
            // $("#account_hold_name_gu").val(jsonObj.account_hold_name.gu);

            for (myObj in jsonObj) {
                var tempName = String(myObj); 
                $("."+tempName+"_hi").val(jsonObj[myObj].hi);
                $("."+tempName+"_gu").val(jsonObj[myObj].gu);
            } 
            $(".box").css({'opacity':'1','pointer-events': 'auto'});
        },
        error: function(error)
        {
            console.log(error);
        }
    });
});

function imgFileValidation() {
    var selectFile = document.getElementById('profile');//select image
    var imgPath = selectFile.value;
    var imgExtensions = /(\.jpg|\.jpeg|\.png)$/i;
    // document.getElementById("profile-error").style.display = "none"; 
    if (!imgExtensions.exec(imgPath)) {
        alert('plz Select only jpeg and png file.');
        selectFile.value = '';
        return false;   
    }else{//image preview
        if (selectFile.files && selectFile.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imgupdt').attr('src', e.target.result);
        };
        reader.readAsDataURL(selectFile.files[0]);
        }
    }       
}

    var id = $("#id").val();
    $("#create_driver").validate({
    ignore: [],
    errorClass: 'error',
    successClass: 'validation-valid-label',
    highlight: function(element, errorClass) {
        $(element).removeClass(errorClass);
    },
    unhighlight: function(element, errorClass) {
        $(element).removeClass(errorClass);
    },
    validClass: "validation-valid-label",
    rules: {
        // vehicletype: {
        //     required: true,
        //     lettersonly: true,
        //     maxlength: 25,
        // },
        name: {
            required: true,
            // lettersonly: true,
            maxlength: 25,
        },
        contact: {
            required: true,
            digits: true,
            minlength: 7,
            maxlength: 10,
        },
        email: {
            required: true,
            // email: true,
        },
        status : {
            required: true,
        },
        // vehicle_reg_no:{
        //     required: true,
        //     maxlength: 8,
        // },
        licence_no : {
            required: true,
            maxlength: 25,
        },
        // postcode:{
        //     required:true,
        //     maxlength:10,
        // },
        address:{
            required:true,
        },
        // police_certificate:{
        //     required:{
        //         depends: function( element) {
        //                 id = $("#id").val();
        //                 if (id!="") {
                            
        //                     return false;
        //                 } else{
        //                     if ($("#police_certificate").val()=="") {
        //                         return true;
        //                     }
        //                 }
        //             }
        //     }
        // },
        // bank_account_no:{
        //     required:true,
        //     maxlength:25,
        // },
        // bank_name:{
        //     required:true,
        //     maxlength:100,
        // },
        // account_hold_name:{
        //     required:true,
        //     maxlength:100,
        // },
        profile:{
            required:{
                depends: function(element) {
                        id = $("#id").val();
                        if (id!="") {
                            return false;
                        } else{
                            if ($("#profile").val()=="") {
                                return  true;
                            }
                        }
                    }
            }
        }
    },
    messages: {
        "vehicletype":{
            required: "Please select vehicle name.",
        },
        "bank_account_no":{
            required: "Please enter bank account number.",
        },
        "pc_certificate":{
            required: "please select certificate.",
        },
        "name":{
            required: "Please enter driver name.",
            // lettersonly: "Please enter only character.",
            maxlength: "Please enter max 25 character.",
        },
        "bank_name":{
            required: "Please enter bank name.",
        },
        "account_hold_name":{
            required: "Please enter account holder name.",
        },
        "contact":{
            required: "Please enter contact number.",
            digits: "Please enter number only.",
            minlength: "Please enter atleast 7 digit.",
            maxlength: "Please enter no more than 10 digit.",
        },
        "email":{
            required: "Please enter email.",
            // email: "Please enter a valid email address.",
        },
        "password":{
            required: "Please enter password.",
            minlength: "Please enter min 8 character.",
            maxlength: "Please enter max 15 character.",
        },
        "status":{
            required: "Please select status.",
        },
        "vehicle_reg_no":{
            required: "Please enter vehicle registration number.", 
            maxlength: "Please enter max 8 letter.",
        },
        "licence_no":{
            required:"Please enter licence number.",
        },
        "postcode":{
            required:"Please enter postcode.",
            maxlength:"Please enter max 10 character.",
        },
        "address":{
            required:"Please enter address.",
        },
        "profile":{
            required:"Please select profile.",
        },
        "police_certificate":{
            required:"Please select police clearance certificate.",
        },
    },
    submitHandler: function(form) {
        $('button[type="submit"]').attr('disabled', true);
        form.submit();
    },
}); 

     function errorProfile(){
        console.log('dd');
        $("#profile-error").text("Please select profile.");
     }
     $("#profile-error").text("Please select profile.");
    $("#mobile").on('input', function (event){    
        mobile = document.getElementById('mobile').value;
    });
    
    $('.numeric').on('input', function (event) {
        this.value = this.value.replace(/[^0-9]/g, '');
    });

    $('.character').on('input', function (event) {
            this.value = this.value.replace(/[^0-9\.\a-z\A-Z\@ _()$&]/g, '');
    }); 

    $('.toUpperCase').keyup (function () {
        this.value=this.value.toUpperCase();
    });


</script>
@endsection
