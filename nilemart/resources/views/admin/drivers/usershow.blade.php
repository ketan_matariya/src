@extends('layouts.admin.app')

@section('title','Add driver - Royal Laundry')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="box-header with-border bg-title border-header">
                    <h4 class="page-title header-color">Driver</h4>
                </div>
            </div>

            <div class="box-body">
                <form enctype="multipart/form-data" action="{{ route('admin.driver.store') }}" id="create_driver" method="post" class="form">
                    <div class="box-body">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" id="id" value="@if(isset($drivers->id)){{$drivers->id}}@endif">
                         <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="vehicle"> Vehicle Name <span class="text-danger">*</span></label>
                                    
                                    <select name="vehicletype" disabled="" id="vehicletype" class="form-control character">
                                        <option>{{$drivers->vehicle_name}}</option>              
                                    </select>
                                </div>
                            </div>
                        
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Driver Name <span class="text-danger">*</span></label>
                                    <input type="text" readonly="" name="name" id="name" placeholder="Driver Name" class="form-control character" value="@if(old('name')!=''){{old('name')}}@elseif(isset($drivers->name)){{$drivers->name}}@else{{old('name')}}@endif" maxlength="25">
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="email">Email <span class="text-danger">*</span></label>
                                    <!--<div class="input-group">-->
                                        <!--<span class="input-group-addon">@</span>-->
                                        <input type="text" readonly="" name="email" id="email" placeholder="Email" class="form-control" value="@if(old('email')!=''){{old('email')}}@elseif(isset($drivers->email)){{$drivers->email}}@else{{old('email')}}@endif" maxlength="255">
                                    <!--</div>-->
                                    @if ($errors->has('email'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>
                       
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="mobile">Contact No. <span class="text-danger">*</span></label>
                                    <input type="text" readonly="" name="contact" id="mobile" placeholder="Contact No." class="form-control numeric" value="@if(old('contact')!=''){{old('contact')}}@elseif(isset($drivers->contact)){{$drivers->contact}}@else{{old('contact')}}@endif" maxlength="10">
                                    @if ($errors->has('contact'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('contact') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                      
                         <div class="row">                       
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="status">Status </label>
                                    <select name="status" disabled="" id="status" class="form-control">
                                        <option value="2" @if(isset($drivers->status)) @if($drivers->status == 2) selected @endif @endif>Pending</option>
                                        <option value="1" @if(isset($drivers->status)) @if($drivers->status == 1) selected @endif @endif>Enable</option>
                                        <option value="0" @if(isset($drivers->status)) @if($drivers->status == 0) selected @endif @endif>Disable</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Vehicle Registration number <span class="text-danger">*</span></label>
                                    <input type="text" readonly="" name="vehicle_reg_no" id="vehicle_reg_no" placeholder="Vehicle Registration number" class="form-control numeric" value="@if(old('vehicle_reg_no')!=''){{old('vehicle_reg_no')}}@elseif(isset($drivers->vehicle_reg_no)){{$drivers->vehicle_reg_no}}@else{{old('vehicle_reg_no')}}@endif" maxlength="10">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Licence Number <span class="text-danger">*</span></label>
                                    <input type="text" readonly="" name="licence_no" id="licence_no" placeholder="Licence number" class="form-control character" value="@if(old('licence_no')!=''){{old('licence_no')}}@elseif(isset($drivers->licence_no)){{$drivers->licence_no}}@else{{old('licence_no')}}@endif" maxlength="25">
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Police clearance certificate<span class="text-danger">*</span></label>    @if(isset($drivers->police_certificate))
                                           <i class="fa fa-briefcase" aria-hidden="true"></i><a href="{{url('drivers/police-certificate/'.$drivers->police_certificate)}}" download="">  Open Document</a>
                                        @endif   
                                    <input type="file" disabled="" name="police_certificate" onchange="return policeCertificate()" id="police_certificate" class="form-control">
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Bank Account Number<span class="text-danger">*</span></label>
                                    <input type="text" readonly="" name="bank_account_no" id="bank_account_no" placeholder="Bank Account Number" class="form-control character" value="@if(old('bank_account_no')!=''){{old('bank_account_no')}}@elseif(isset($drivers->bank_acc_no)){{$drivers->bank_acc_no}}@else{{old('bank_account_no')}}@endif" maxlength="25">
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Bank Name<span class="text-danger">*</span></label>
                                    <input type="text" readonly="" name="bank_name" id="bank_name" placeholder="Bank Name" class="form-control character" value="@if(old('bank_name')!=''){{old('bank_name')}}@elseif(isset($drivers->bank_name)){{$drivers->bank_name}}@else{{old('bank_name')}}@endif" maxlength="100">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Account Holder Name<span class="text-danger">*</span></label>
                                    <input type="text" readonly="" name="account_hold_name" id="account_hold_name" placeholder="Account Holder Name" class="form-control character" value="@if(old('account_hold_name')!=''){{old('account_hold_name')}}@elseif(isset($drivers->acc_holder_name)){{$drivers->acc_holder_name}}@else{{old('account_hold_name')}}@endif" maxlength="100">
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Post Code <span class="text-danger">*</span></label>
                                    <input type="text" readonly="" name="postcode" id="postcode" placeholder="Post code" class="form-control" value="@if(old('postcode')!=''){{old('postcode')}}@elseif(isset($drivers->postalcode)){{$drivers->postalcode}}@else{{old('postcode')}}@endif" maxlength="10">
                                </div>
                            </div>
                        </div>
                             
                        <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Address <span class="text-danger">*</span></label>
                                    <textarea class="form-control" readonly="" name="address" id="address" rows="5" placeholder="Address"  maxlength="300">@if(old('address')!=''){{old('address')}}@elseif(isset($drivers->address)){{$drivers->address}}@else{{old('address')}}@endif</textarea>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="profile">Profile <span class="text-danger">*</span></label>
                                    <td><input type="file" id="profile" disabled="" name="profile" accept="image/*" class="form-control" onchange="return imgFileValidation()" value="@if(old('profile')!=''){{old('profile')}}@elseif(isset($drivers->profile)){{$drivers->profile}}@else{{old('profile')}}@endif" ></td>
                                    @if(isset($drivers->profile))
                                        <img id="imgupdt" src="@if(isset($drivers->profile)) {{ url('drivers/'.$drivers->profile) }}@endif" width="100px" height="100px">
                                    @else
                                        <img id="imgupdt" width="100px">
                                    @endif
                                </div>
                            </div>
                            
                            <div id="add_branch" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Select Address</h4>
                                        </div>
                                        <div class="modal-body" id="show_address"></div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ url('admin/driver/index') }}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
