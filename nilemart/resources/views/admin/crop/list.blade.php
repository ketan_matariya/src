@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
<section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <div class="box-body">
                <div class="box-header with-border bg-title border-header">
                    <h4 class="page-title header-color">Manage Solution</h4>
                </div>
            </div>
            <div class="box-body">
                @if($crops)
                    <table class="table border-b1px" id="table-id">
                        <thead>
                            <tr>
                                <th>Solution Name</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($crops as $crop)
                                <tr>
                                    <td>
                                        {{ $crop['name'] }}
                                    </td>
                                    <td><img src="{{ $crop['image'] }}" alt="Image not found" class="img-responsive img-thumbnail" height="100" width="100"></td>
                                    <td>
                                        <form action="{{ route('admin.crops.destroy', $crop['id']) }}" method="post" class="form-horizontal">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="delete">
                                            <div class="btn-group">
                                                <a href="{{ route('admin.crops.show', $crop['id']) }}" class="btn btn-default btn-sm"><i class="fa fa-eye"></i> </a>
                                                @if($permission->edit==1)
                                                    <a href="{{ route('admin.crops.edit', $crop['id']) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                @endif
                                                <!--@if($permission->delete==1)-->
                                                <!--    <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>-->
                                                <!--@endif-->
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <p class="alert alert-warning">No attribute yet. <a href="{{ route('admin.attributes.create') }}">Create one</a></p>
                @endif
            </div>
        </div>
        <div class="box-footer">
            <div class="box-body">
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
    <!-- /.content -->
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('#table-id').DataTable();
    } );
</script>
@endsection