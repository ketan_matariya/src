@extends('layouts.admin.app')

@section('content')   
<!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">Edit @if(isset($crop->crop_category)) Sub-Solution @else Solution @endif</h4>
                    </div>
                </div>
                <form action="{{ route('admin.crops.update', $crop->id) }}" method="post" class="form" enctype="multipart/form-data" id="edit_crop"> 
                    <div class="box-body">
                        <div class="row">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="put">
                            <div class="col-md-12">
                                <div class="row">
                                    @if($crop->crop_category)
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-lg-4">
                                                <label for="core_category">Solution Category <span class="text-danger">*</span></label>
                                                <select name="core_category" id="core_category" class="form-control select2">
                                                    <option value="">Select Category</option>
                                                        @foreach($categories as $category)
                                                        <option @if($category->id == $crop->crop_category) selected="selected" @endif value="{{$category->id}}">{{$category->name}}</option>
                                                        @endforeach
                                                </select>
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    @if($crop->crop_category) 
                                                    <label for="name">Sub-Solution Name<span class="text-danger">*</span></label>
                                                    @else
                                                    <label for="name">Solution Name <span class="text-danger">*</span></label>
                                                    @endif
                                                    <input type="text" name="name" id="name" placeholder="Crop name" class="form-control character" value="{!! $crop['name'] ?: old('name')  !!}"  maxlength="25">
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="name_hi"><span class="text-danger">*</span></label>
                                                    <input type="text" name="name_hi" id="name_hi" placeholder="Crop name" class="form-control" value="{!! $crop['name_hi'] ?: old('name_hi')  !!}"  maxlength="25">
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="name_gu"><span class="text-danger">*</span></label>
                                                    <input type="text" name="name_gu" id="name_gu" placeholder="Crop name" class="form-control" value="{!! $crop['name_gu'] ?: old('name_gu')  !!}"  maxlength="25">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="image">Image <span class="text-danger">*</span></label>
                                            <input type="file" name="image" id="image" class="form-control" accept="image/*" onchange="GetFileSize()">
                                            <small class="text-warning">Note :- Upload image with .jpg, .jpeg and .png extensions upto 1 MB.</small>
                                            <label id="image_error" class="image_error">Upload image size is large.</label>
                                            <label id="image1" class="image_error">Please select image</label>
                                            <label id="image_type_error"  class="image_type_error">Upload image with .jpg, .jpeg and .png extensions</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <img src="{{ $crop['image'] }}" alt="Image not found" class="img-responsive img-thumbnail" height="100" width="100">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ route('admin.crops.index') }}" class="btn btn-default">Cancel</a>
                            <input type="button" class="btn btn-info" value="Translate" id="translate">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection



@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">
    $("#edit_crop").validate({
        ignore: [],
        errorClass: 'error',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            name: {
                required: true,
                // lettersonly: true,
                maxlength: 25,
            }
        },
        messages: {
            "name":{
                required: "Please enter name",
                maxlength: "Please enter max 25 character",
            }
        },
        submitHandler: function(form) {
            error=0;
            document.getElementById('image_error').style.display="none";
            document.getElementById('image_type_error').style.display="none";
            document.getElementById('image1').style.display="none";
            var logo = $('#image').val();
            if(logo!=""){
                var fi = document.getElementById('image');
                document.getElementById('image_error').style.display="none";
                document.getElementById('image_type_error').style.display="none";
                if (fi.files.length > 0) {
                    for (var i = 0; i <= fi.files.length - 1; i++) {
                        var fsize = fi.files.item(i).size;
                        var type = fi.files.item(i).type;
                        var size = fsize/1024;  
                        if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                            document.getElementById('image_type_error').style.display="block";
                            error=1;
                        } else if(size>1024){
                            document.getElementById('image_error').style.display="block";
                            error=1;
                        }
                    }
                }
            }
            if (error == 1)  {
                return false;
            }
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
    });
    function GetFileSize() {
        var fi = document.getElementById('image');
        document.getElementById('image1').style.display="none";
        document.getElementById('image_error').style.display="none";
        document.getElementById('image_type_error').style.display="none";
        if (fi.files.length > 0) {
            for (var i = 0; i <= fi.files.length - 1; i++) {

                var fsize = fi.files.item(i).size;
                var type = fi.files.item(i).type;
                var size = fsize/1024;   
                if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                    document.getElementById('image_type_error').style.display="block";
                } else if(size>1024){
                    document.getElementById('image_error').style.display="block";
                }
            }
        }
    }
    
    $('.character').on('input', function (event) {
        this.value = this.value.replace(/[^0-9\.\a-z\A-Z\@ _()$&]/g, '');
    });

    $("#translate").on('click',function(){
        $("#submit").css('display','block');
        var name = $("#name").val();
        $(".box").css({'opacity':'0.5','pointer-events': 'none'});
        $.ajax({
            url : "{{route('admin.translate')}}",
            data: { "name": name,"name2":"name2 value" },
            type: 'get',
            dataType: 'json',
            success: function( result )
            {
                var jsonObj = result;
                $("#name_hi").val(jsonObj.name.hi);
                $("#name_gu").val(jsonObj.name.gu);
                // console.log(result);
                $(".box").css({'opacity':'1','pointer-events': 'auto'});
            },
            error: function(error)
            {
                console.log(error);
            }
        });
    });
</script>
@endsection
