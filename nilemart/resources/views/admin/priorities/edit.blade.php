@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">Edit Priorities</h4>
                    </div>
                </div>
                <form action="{{ route('admin.pro_priority.update', $priority->id) }}" id="edit_permission" method="post" class="form">
                    <div class="box-body">
                        {{ csrf_field() }}
                        <input type="hidden" value="put" name="_method">
                        <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Priority Name  <span class="text text-danger">*</span></label>
                                    <input type="text" name="name" id="name" placeholder="Name" class="form-control" value="{{ old('name') ?: $priority->name }}" disabled>
                                    @if ($errors->has('name'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="display_name">Category <span class="text text-danger">*</span></label>
                                    <select id="category_id" name="category_id"  class="form-control">
                                        @if(!$priority->category_id)
                                            <option value="" selected>Select Category</option>
                                        @endif
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}" @if($category->id==$priority->category_id) selected @endif>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="subcategory_id">Sub Category</label>
                                    <select name="subcategory_id" id="subcategory_id" class="form-control ">
                                         @if(!$priority->subcategory_id)
                                            <option value="" selected>Select Category</option>
                                        @endif
                                        @foreach($subcategories as $category)
                                            <option value="{{ $category->id }}" @if($category->id==$priority->subcategory_id) selected @endif>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="box"> -->
                            <div class="box-body">
                                <h4>Products</h4>
                                @if(!$products->isEmpty())
                                    <table class="table table-striped" id="data">
                                        <thead>
                                          <tr>
                                              <th>No</th>
                                              <th>Name</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i=1; ?>
                                        @if($products)
                                            @foreach($products as $product)
                                                <tr class="sortable" id="{{ $product->id }}">
                                                    <td>{{ $i++ }} </td>
                                                    <td>{{ $product->name }}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                @endif
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    <!-- </div> -->
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <div class="btn-group">
                                <a href="{{ route('admin.pro_priority.index') }}" class="btn btn-default">Cancel</a>
                                <button type="submit" id="update" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection

@section('js')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">

    sort_order=[];
    $(document).ready(function() {
      $("table tbody").sortable({ 
            update: function(event, ui) {  
                $('table tbody tr').each(function() {
                    $(this).children('td:first-child').html($(this).index()+1);
                });
            },
            stop: function(event, ui) {
                $(this).find('.sortable').each(function(i, el){
                    var i_new = (i+1);
                    sort_order[i_new] = $(this).attr('id');
                });
            }
        }).disableSelection();   
    });
    $('#subcategory_id').change(function(){
        $("#data option").remove();
        var id = $(this).val();
        var category_id = $('#category_id').val();
        $.ajax({
            url : "{{ route('admin.priority.getProduct') }}",
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id,
                "category_id": category_id
                },
            type: 'post',
            dataType: 'json',
            success: function( result )
            {
                console.log(result);
                var table = $('#data');
                table.find("tbody tr").remove();
                var i=1;
                $.each( result, function(k, v) {
                    table.append("<tr class='sortable' id="+k+"><td>" + i + "</td><td>" + v + "</td></tr>");
                    sort_order[i]=k;
                    i=i+1;
                });
            },
            error: function()
            {
                alert('error...');
            }
        });
    });

     $('#category_id').change(function(){
        $("#subcategory_id option").remove();
        var id = $(this).val();
        $.ajax({
            url : "{{route('admin.products.getSubCategories')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id
                },
            type: 'post',
            dataType: 'json',
            success: function( result )
            {
                console.log(result);
                $('#subcategory_id').append($('<option>', {value:'', text:'Select'}));
                $.each( result, function(k, v) {
                    $('#subcategory_id').append($('<option>', {value:k, text:v}));
                });
            },
            error: function()
            {
                alert('error...');
            }
        });
        
        
        $("#data option").remove();
        var id = $(this).val();
        $.ajax({
            url : "{{ route('admin.priority.getCategoryProduct') }}",
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id
                },
            type: 'post',
            dataType: 'json',
            success: function( result )
            {
                console.log(result);
                var table = $('#data');
                table.find("tbody tr").remove();
                var i=1;
                $.each( result, function(k, v) {
                    table.append("<tr class='sortable' id="+k+"><td>" + i + "</td><td>" + v + "</td></tr>");
                    sort_order[i]=k;
                    i=i+1;
                });
            },
            error: function()
            {
                alert('error...');
            }
        });
    });

    $('#update').click(function(){
        console.log(sort_order);
        $.ajax({
           type: 'POST',
           url: "{{ route('admin.priority.subcat_sort') }}",
           data: {id:sort_order,'_token':"{{ csrf_token() }}"},
           success: function(data){
            console.log(data);
           }, error : function(error){
                alert("Something went wrong please try again.");
           }
        });
    });
</script>

@endsection

