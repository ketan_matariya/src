@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if(!$priorities->isEmpty())
            <div class="box">
                <div class="box-body">
                    <div class="box-body">
                        <div class="box-header with-border bg-title border-header">
                            <h4 class="page-title header-color">Manage Priorities</h4>
                        </div>
                    </div>
                    @if(!$priorities->isEmpty())
                        <div class="box-body">
                          <div class="scroll">
                            <table class="table border-b1px" id="table-id">
                                <thead>
                                  <tr>
                                      <th class="col-md-3 ">No</th>
                                      <th class="col-md-3">Name</th>
                                      <th class="col-md-3">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                <?php $i=1; ?>
                                @foreach ($priorities as $priority)
                                    <tr class="sortable" id="{{ $priority->id }}">
                                        <td>{{ $priority->priority }} </td>
                                        <td>{{ $priority->name }}</td>
                                        @if($priority->name=="Trending Products")
                                          <td>
                                            @if($permission->edit==1)
                                              <a href="{{ route('admin.pro_priority.edit', $priority->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> </a>
                                            @endif
                                          </td>
                                        @else
                                        <td></td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                          </div>
                        </div>
                    @endif
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
            <!-- /.box -->
        @else
            <div class="box">
                <div class="box-body"><p class="alert alert-warning">No priorities found.</p></div>
                <div class="box-footer">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
        @endif

    </section>
    <!-- /.content -->
@endsection
@section('js')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- <script src='https://cdnjs.cloudflare.com/ajax/libs/Sortable/1.10.1/Sortable.js'></script> -->
<script type="text/javascript">
    $(document).ready(function() {
        var sort_order = [];
      $("table tbody").sortable({ 
          update: function(event, ui) {  
              $('table tbody tr').each(function() {
                  $(this).children('td:first-child').html($(this).index()+1);
              });
          },
          stop: function(event, ui) {
              $(this).find('.sortable').each(function(i, el){
                var i_new = (i+1);
                sort_order[i_new] = $(this).attr('id');
              });
              /* call ajax */
              $.ajax({
                   type: 'POST',
                   url: "{{ route('admin.priority.sort') }}",
                   data: {id:sort_order,'_token':"{{ csrf_token() }}"},
                   success: function(data){
                    // console.log(window.location.href);
                    // window.location.href = window.location.href;
                   }, error : function(error){
                        alert("Something went wrong please try again.");
                   }
               });
          }
      }).disableSelection();    
      
    });
</script>
@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
#app_id{
    width: 20%;
    display: inline-block;
}
</style>
@endpush
