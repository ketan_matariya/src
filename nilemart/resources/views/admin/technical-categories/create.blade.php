@extends('layouts.admin.app')
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">@if(isset($technical)) Edit @else Add @endif Technical Category</h4>
                    </div>
                </div>
                <form action="{{ route('admin.technical.store') }}" method="post" class="form" enctype="multipart/form-data" id="add_technical">
                    <div class="box-body">
                        <div class="row">
                            {{ csrf_field() }}
                            <div class="col-md-12">
                                <input type="hidden" name="id" value="@if(isset($technical->id)) {{$technical->id}} @endif">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="name">Technical Name <span class="text-danger">*</span></label>
                                            <input type="text" name="name" id="name" placeholder="Technical Name" class="form-control character" value="{{ isset($technical->name) ? $technical->name : null }}" maxlength="25">
                                            @if ($errors->has('name'))
                                                <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="name_hi">तकनीकी नाम <span class="text-danger">*</span></label>
                                            <input type="text" name="name_hi" id="name_hi" placeholder="Technical Name" class="form-control character" value="{{ isset($technical->name_hi) ? $technical->name_hi : null }}" maxlength="25">
                                            @if ($errors->has('name_hi'))
                                                <span class="text-danger" style="color: red">{{ $errors->first('name_hi') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="name_hi">તકનીકી નામ <span class="text-danger">*</span></label>
                                            <input type="text" name="name_gu" id="name_gu" placeholder="Technical Name" class="form-control character" value="{{ isset($technical->name_gu) ? $technical->name_gu : null }}" maxlength="25">
                                            @if ($errors->has('name_gu'))
                                                <span class="text-danger" style="color: red">{{ $errors->first('name_gu') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ route('admin.technical.index') }}" class="btn btn-default">Cancel</a>
                            <input type="button" class="btn btn-info" value="Translate" id="translate">
                            <button type="submit" id="submit" style="display: none;" class="btn btn-primary">@if(isset($technical)) Update @else Add @endif</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection


@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">
    $("#add_technical").validate({
        ignore: [],
        errorClass: 'error',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            name: {
                required: true,
                maxlength: 25,
            }
        },
        messages: {
            "name":{
                required: "Please enter technical name",
                maxlength: "Please enter max 25 character",
            }
        },
        submitHandler:  function(form) {
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
    });
    
    $('.character').on('input', function (event) {
        this.value = this.value.replace(/[^0-9\.\a-z\A-Z\@ _()$&]/g, '');
    });

    $("#translate").on('click',function(){
        $("#submit").css('display','block');
        var name = $("#name").val();
        $(".box").css({'opacity':'0.5','pointer-events': 'none'});
        $.ajax({
            url : "{{route('admin.translate')}}",
            data: { "name": name,"name2":"name2 value" },
            type: 'get',
            dataType: 'json',
            success: function( result )
            {
                var jsonObj = result;
                $("#name_hi").val(jsonObj.name.hi);
                $("#name_gu").val(jsonObj.name.gu);
                // console.log(result);
                $(".box").css({'opacity':'1','pointer-events': 'auto'});
            },
            error: function(error)
            {
                console.log(error);
            }
        });
    });
</script>
@endsection
