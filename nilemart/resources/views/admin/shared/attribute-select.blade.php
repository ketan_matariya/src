<div class="form-group">
    <label for="weight">Weight </label>
    <div class="form-inline">
        <input type="text" class="form-control col-md-8 numeric" id="weight" name="weight" placeholder="0.00" value="{{ ($product->weight) }}" maxlength="7">
        <label for="mass_unit" class="sr-only">Mass unit</label>
            @foreach($weight_units as $key => $unit)
            <input type="text" class="form-control col-md-8" name="mass_unit" id="mass_unit" value="{{ $key }} - ({{ $unit }})" disabled>
            @endforeach
    </div>
    <div class="clearfix"></div>
</div>