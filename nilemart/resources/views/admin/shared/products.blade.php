<!--<div class="page-filter" style="margin-bottom: 20px;">-->
<!--    <div class="col-sm-6 col-md-6 col-sm-6 col-xs-12 sorter_margin_bottom">-->
<!--        <div class="sort-product-lable" style="margin-left: -30px;">-->
<!--            <div class="form-group text-left sort-product-lable">-->
<!--                <div class="col-sm-1 lable_product"><label>Show</label></div>-->
<!--                <div class="col-sm-3">-->
<!--                    <select class="form-control w-auto d_inline" name="product_no" id="page_change">-->
<!--                        <option value="10" selected="true" @if(request()->input('product_no')=="10") selected="true" @endif>10</option>-->
<!--                        <option value="25" @if(request()->input('product_no')=="25") selected="true" @endif>25</option>-->
<!--                        <option value="50" @if(request()->input('product_no')=="50") selected="true" @endif>50</option>-->
<!--                        <option value="100" @if(request()->input('product_no')=="100") selected="true" @endif>100</option>-->
<!--                    </select>-->
<!--                </div>-->
<!--                <div class="col-sm-1 lable_product product_margin"><label>Entries</label></div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="col-sm-6 col-md-6 col-sm-6 col-xs-12 sorter_margin_bottom">-->
<!--        <div class="form-group text-right sort-product sortter">-->
<!--             {{-- <label>Sort by:</label>  --}}-->
<!--            <select class="form-control w-auto d_inline"  name="sort" id="sort_change">-->
<!--                <option value="" @if(request()->input('sort')=="") selected="true" @endif>Select Sort by</option>-->
<!--                <option value="product_name" @if(request()->input('sort')=="product_name") selected="true" @endif>Product Name</option>-->
<!--                <option value="selling_price_low_to_high" @if(request()->input('sort')=="selling_price_low_to_high") @endif>Selling Price: Low to High</option>-->
<!--                <option value="selling_price_high_to_low" @if(request()->input('sort')=="selling_price_high_to_low") selected="true" @endif>Selling Price: High to Low</option>-->
<!--                <option value="price_low_to_high" @if(request()->input('sort')=="price_low_to_high") selected="true" @endif>Price: Low to High</option>-->
<!--                <option value="price_high_to_low" @if(request()->input('sort')=="price_high_to_low") selected="true" @endif>Price: High to Low</option>-->
<!--                <option value="active" @if(request()->input('sort')=="active") selected="true" @endif>Active Product</option>-->
<!--            </select>  -->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

@if(!$products->isEmpty())
<div class="scroll">
    <table class="table border-b1px" id="table-id">
        <thead>
        <tr>
            <!--<th class="col-md-1">ID</th>-->
            <!--<th class="col-md-1">Name</th>-->
            <!--<th class="col-md-1">Quantity</th>-->
            <!--<th class="col-md-1">Price</th>-->
            <!--<th class="col-md-1">Sale Price</th>-->
            <!--<th class="col-md-1">Status</th>-->
            <!--<th class="col-md-1">Action</th>-->
            <td>Name</td>
            <td>Technical</td>
            <td>Category</td>
            <td>Company</td>
            <td >Status</td>
            <td>Actions</td>

        </tr>
        </thead>
        <tbody>
        @foreach ($products as $product)
            <tr>
                <td>{{ $product->name }} </td>
                <td>{{ $product->technical_name }}</td>
                <td>{{ $product->category }}</td>
                <td>{{ $product->company }}</td>
                <td>@include('layouts.status', ['status' => $product->status])</td>
                <td>
                    <form action="{{ route('admin.products.destroy', $product->id) }}" method="post" class="form-horizontal">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="delete">
                        <div class="btn-group btn-display">
                            @if($permission->edit==1)<a href="{{ route('admin.products.edit', $product->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> </a>@endif
                            @if($permission->delete==1)<button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> </button>@endif
                        </div>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endif