@extends('layouts.admin.app')
@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if(!$products->isEmpty())
            <div class="box">
                <div class="box-body">
                    <div class="box-body">
                        <div class="box-header with-border bg-title border-header">
                            <h4 class="page-title header-color">Manage Order Purchase</h4>
                        </div>
                    </div>
                    <div class="box-body">
                        @if(!$products->isEmpty())
                        <div class="scroll">
                            <table class="table border-b1px" id="table-id">
                                <thead>
                                <tr>
                              
                                    <td>Order No</td>
                                    <td>Invoice No</td>
                                    <td>Product Old/New</td>
                                    <td>Email</td>
                                    <td>Seller Name</td>
                                    <td>Mobile</td>
                                    <td>Gst No</td>
                                    <td>Status</td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($products as $product)
                                    <tr>
                                        <td>{{ $product->order_no }} </td>
                                        <td>{{ $product->invoice_no }}</td>
                                        <td>{{ ($product->product_oldnew=='0') ? 'Old':'New' }}</td>
                                        <td>{{ $product->email }}</td>
                                        <td>{{ $product->seller_name }}</td>
                                        <td>{{ $product->mobile }}</td>
                                        <td>{{ $product->gstno }}</td>
                                       
                                        <td>
                                            <form action="{{ route('admin.products.destroy', $product->id) }}" method="post" class="form-horizontal">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="delete">
                                                <div class="btn-group btn-display">
                                                    @if($permission->edit==1)<a href="{{ route('admin.orderpurchase.edit', $product->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> </a>@endif
                                                    <?php /* ?>
                                                    @if($permission->delete==1)<button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> </button>@endif
                                                    <?php */?>
                                                    <a href="{{route('admin.orderpurchase.invoice.generate',$product->id)}}" class="btn btn-primary btn-block" target="_blank"><i class="fa fa-download"></i> </a>
                                                </div>
                                            </form>
                                        </td>
                                    </tsr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @endif
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group ">
                    </div>
                </div>
            </div>
            <!-- /.box -->
        @else
            <div class="box">
                <div class="box-body">
                    <p class="alert alert-warning">No products found.</p>
                </div>
                <div class="box-footer">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
        @endif
    </section>
    <!-- /.content -->
@endsection

@section('js')

<script type="text/javascript">
    $("#sort_change").on('change',function(){
        var sort = $("#sort_change").val();
        window.location.href = "{{ request()->url() }}?sort="+sort;
        var url = window.location.href;
        if(url == "{{ request()->url() }}"){
            window.location.href = "{{ request()->url() }}?sort="+sort;
        }
        if(url.includes("{{ request()->url() }}?product_no=")) {
            var product_no = "{{request()->input('product_no')}}";
            window.location.href = "{{ request()->url() }}?product_no="+product_no+"&&sort="+sort;
        }
        if(url.includes("{{ request()->url() }}?sort=")) {
            var product_no = "{{request()->input('product_no')}}";
            window.location.href = "{{ request()->url() }}?sort="+sort+"&&product_no="+product_no;
        }
    });

    $("#page_change").on('change',function(){
        var product_no = $("#page_change").val();
        var url = window.location.href;
        if(url == "{{ request()->url() }}"){
            window.location.href = "{{ request()->url() }}?product_no="+product_no;
            console.log(window.location.href);
        }
        if(url.includes("{{ request()->url() }}?sort=")) {
            var sort = "{{request()->input('sort')}}";
            window.location.href = "{{ request()->url() }}?sort="+sort+"&&product_no="+product_no;
            console.log(window.location.href);
        }
        if(url.includes("{{ request()->url() }}?product_no=")) {
            var sort = "{{request()->input('sort')}}";
            window.location.href = "{{ request()->url() }}?product_no="+product_no+"&&sort="+sort;
            console.log(window.location.href);
        }
    });
    
    $('.table').DataTable();
</script>

@endsection
