
@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    <!--@include('layouts.errors-and-messages')-->
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">Add Order purchase</h4>
                    </div>
                </div>
                <div class="box-body">
                    <form action="{{ route('admin.orderpurchase.store') }}" id="add_product" method="post" class="form" enctype="multipart/form-data">
                        <div class="box-body">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="order_no">Order No <span class="text-danger">*</span></label>
                                        <input maxlength="10" type="text" name="order_no" id="order_no" placeholder="Order No" class="form-control character only_number_allow" value="{{ old('order_no') }}" maxlength="300">
                                    </div>
                                </div>
{{--                            </div>--}}
{{--                            <div class="row">--}}
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="seller_name">Seller Name<span class="text-danger">*</span></label>
                                        <input maxlength="30" type="text" name="seller_name" id="seller_name" placeholder="Seller Name" class="form-control character blank_space_remove" value="{{ old('seller_name') }}" maxlength="300">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="email">Email<span class="text-danger">*</span></label>
                                        <input maxlength="50" type="text" name="email" id="email" placeholder="Email" class="form-control character blank_space_remove" value="{{ old('email') }}" maxlength="300">
                                    </div>
                                </div>
{{--                            </div>--}}
{{--                            <div class="row">--}}
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="address">Address<span class="text-danger">*</span></label>
                                        <textarea maxlength="300" name="address" id="address" placeholder="Address" class="form-control character blank_space_remove">{{ old('address') }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="gstno">Gst No<span class="text-danger">*</span></label>
                                        <input maxlength="15" type="text" name="gstno" id="gstno" placeholder="Gst No" class="form-control character only_space_remove" value="{{ old('gstno') }}" maxlength="300">
                                    </div>
                                </div>
{{--                            </div>--}}
{{--                            <div class="row">--}}
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="mobileno">Mobile No<span class="text-danger">*</span></label>
                                        <input maxlength="10" type="text" name="mobileno" id="mobileno" placeholder="Mobile No" class="form-control character only_number_allow" value="{{ old('mobileno') }}" maxlength="300">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="inoviceno">Invoice No<span class="text-danger">*</span></label>
                                        <input maxlength="10" type="text" name="inoviceno" id="inoviceno" placeholder="Invoice No" class="form-control character only_space_remove" value="{{ old('inoviceno') }}" maxlength="300">
                                    </div>
                                </div>
{{--                            </div>--}}
{{--                            <div class="row">--}}
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="inoviceno">Product New/Old<span class="text-danger">*</span></label>
                                        <select name="product_oldnew" id="product_oldnew" class="form-control">
                                            <option value="">Select Type</option>
{{--                                            <option value="1">New</option>--}}
                                            <option value="0" selected>Old</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <br>
                            <div id="exstproduct" class="exstngprod">
                                <div class="row">
                                    <div class="col-lg-2 productlst">
                                        <div class="form-group">
                                            <label for="product_id">Products Name <span class="text-danger">*</span></label>
                                            <select name="newproduct_id[]" id="newproduct_id" class="form-control">
                                                @if($products)
                                                    <option value="">Select Product</option>
                                                    @foreach($products as $company)
                                                        <option value="{{ $company->id }}">({{ $company->product_code }}) {{ $company->name }}</option>
                                                    @endforeach
                                                @endIf
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 productlst">
                                        <div class="form-group">
                                            <label for="unit">Unit value<span class="text-danger">*</span></label>
                                            <select name="unitoption[]" id="unitoption" class="form-control frm-txtarea123">

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="form-group">
                                            <label for="inoviceno"> Quantity<span class="text-danger">*</span></label>
                                            <input type="text" name="product_availblequantity[]" id="product_availblequantity" placeholder="Available Quantity" class="form-control character" value="{{ old('product_quantity') }}" maxlength="300" disabled>
                                            <input type="hidden" maxlength="5" name="product_avlblquantity[]" id="product_avlblquantity" placeholder="Add Quantity" class="form-control character only_number_allow_and_limit" maxlength="300">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="inoviceno"> New Quantity<span class="text-danger">*</span></label>
                                            <input type="text" name="product_newquantity[]" maxlength="5" id="product_newquantity" placeholder="Add Quantity" class="form-control character only_number_allow_and_limit" value="{{ old('product_quantity') }}" maxlength="300">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="inoviceno"> Price<span class="text-danger">*</span></label>
                                            <input maxlength="10" type="text" name="price[]" id="price" placeholder="Price" class="form-control character only_number_allow" value="{{ old('price') }}" maxlength="300">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="inoviceno"> Sale Price<span class="text-danger">*</span></label>
                                            <input maxlength="10" type="text" name="sale_price[]" id="sale_price" placeholder="Sale Price" class="form-control character only_number_allow" value="{{ old('sale_price') }}" maxlength="300">
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <button type="button" name="addnewdynamic" id="addnewdynamic" class="btn btn-success" style="margin-top: 24px;">+</button>
                                    </div>
                                </div>
                                <?php /*?>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                @if($crops)
                                                    <label for="category_id"> Category <span class="text-danger">*</span></label>
                                                    <select name="category_id" id="category_id" class="form-control" multiple>
                                                        <option value="">Select  Category</option>
                                                        @foreach($categories as $category)
                                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                        @endforeach
                                                    </select>
                                                @endIf
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                        </div>
                                        <div class="col-lg-4">
                                        </div>
                                    </div>
                                    <input type="hidden" name="totalsubcategory" id="totalsubcategory">
                                    <div class="row subcategory">
                                        <div class="col-lg-4">
                                       <div class="form-group">
                                               <label for="subcategory_id">Sub Category <span class="text-danger">*</span></label>
                                              <select name="subcategory_id" id="subcategory_id" class="form-control " multiple>
                                                 <option value="">Select Sub Category</option>
                                             </select>
                                          </div>
                                       </div>
                                        <div class="col-lg-4">
                                        </div>
                                        <div class="col-lg-4">
                                        </div>
                                    </div>
                                    <input type="hidden" name="totalsubsubcategory" id="totalsubsubcategory">
                                    <div class="row subsubcategory">
                                        <div class="col-lg-4">
                                       <div class="form-group">
                                               <label for="subsubcategory_id">Sub Sub Category <span class="text-danger">*</span></label>
                                              <select name="subsubcategory_id" id="subsubcategory_id" class="form-control">
                                              </select>
                                          </div>
                                       </div>
                                        <div class="col-lg-4">
                                        </div>
                                        <div class="col-lg-4">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="inoviceno">Product List<span class="text-danger">*</span></label>
                                                {{-- <select name="product_id" id="product_id" class="form-control">
                                                </select> --}}
                                                <select name="product_id" id="product_id" multiple class="form-control">

                                                  </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="inoviceno">Product Quantity<span class="text-danger">*</span></label>
                                                <input type="text" name="product_quantity" id="product_quantity" placeholder="Product Quantity" class="form-control character" value="{{ old('product_quantity') }}" maxlength="300">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="inoviceno">Product Price<span class="text-danger">*</span></label>
                                                <input type="text" name="price" id="price" placeholder="Product Price" class="form-control character" value="{{ old('price') }}" maxlength="300">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="inoviceno">Product Sale Price<span class="text-danger">*</span></label>
                                                <input type="text" name="sale_price" id="sale_price" placeholder="Product Sale Price" class="form-control character" value="{{ old('sale_price') }}" maxlength="300">
                                            </div>
                                        </div>
                                    </div>
                                    <?php */ ?>
                            </div>
                            <div id="newproduct" style="display: none;">

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="company_id">Company Name <span class="text-danger">*</span></label>
                                            <select name="company_id" id="company_id" class="form-control">
                                                @if($companies)
                                                    <option value="">Select Company</option>
                                                    @foreach($companies as $company)
                                                        <option value="{{ $company->id }}">{{ $company->name }}</option>
                                                    @endforeach
                                                @endIf
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            @if($crops)
                                                <label for="category_id"> Category <span class="text-danger">*</span></label>
                                                <select name="category_id" id="category_id" class="form-control">
                                                    <option value="">Select  Category</option>
                                                    @foreach($categories as $category)
                                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                                            @endIf
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                </div>
                                <input type="hidden" name="totalsubcategory" id="totalsubcategory">
                                <div class="row subcategory">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="subcategory_id">Sub Category <span class="text-danger">*</span></label>
                                            <select name="subcategory_id" id="subcategory_id" class="form-control ">
                                                <option value="">Select Sub Category</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                </div>
                                <input type="hidden" name="totalsubsubcategory" id="totalsubsubcategory">
                                <div class="row subsubcategory">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="subsubcategory_id">Sub Sub Category <span class="text-danger">*</span></label>
                                            <select name="subsubcategory_id" id="subsubcategory_id" class="form-control">

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="name">Name <span class="text-danger">*</span></label>
                                            <input type="text" name="name" id="name" placeholder="Name" class="form-control character" value="{{ old('name') }}" maxlength="300">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="name_hi">नाम <span class="text-danger">*</span></label>
                                            <input type="text" name="name_hi" id="name_hi" placeholder="नाम" class="form-control " value="{{ old('name_hi') }}" maxlength="300">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="name_gu">નામ <span class="text-danger">*</span></label>
                                            <input type="text" name="name_gu" id="name_gu" placeholder="નામ" class="form-control " value="{{ old('name_gu') }}" maxlength="300">
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="technical_name" id="technical_name" value="Vegetables"/>
                            <?php /* ?>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="technical_name">Technical Name <span class="text-danger">*</span></label>
                                                <select name="technical_name" id="technical_name" class="form-control">
                                                    @if($companies)
                                                        <option value="">Select Technical</option>
                                                        @foreach($technicals as $technical)
                                                            <option value="{{ $technical->name }}">{{ $technical->name }}</option>
                                                        @endforeach
                                                    @endIf
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                        </div>
                                        <div class="col-lg-4">
                                        </div>
                                    </div>
                                    <?php */?>
                            <!--<div class="row">-->
                                <!--    <div class="col-lg-4">-->
                                <!--        <div class="form-group">-->
                                <!--            <label for="sku">SKU <span class="text-danger">*</span></label>-->
                            <!--            <input type="text" name="sku" id="sku" placeholder="xxxxx" class="form-control character" value="{{ old('sku') }}" maxlength="25" >-->
                                <!--        </div>-->
                                <!--    </div>-->
                                <!--    <div class="col-lg-4">-->
                                <!--    </div>-->
                                <!--    <div class="col-lg-4">-->
                                <!--    </div>-->
                                <!--</div>-->

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="description">Description </label>
                                            <textarea class="form-control" name="description" id="description" rows="5" placeholder="Description"  maxlength="600">{{ old('description') }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="description_hi">विवरण </label>
                                            <textarea class="form-control" name="description_hi" id="description_hi" rows="5" placeholder="विवरण" maxlength="600">{{ old('description_hi') }}</textarea>
                                            @if ($errors->has('description_hi'))
                                                <span class="text-danger" style="color: red">{{ $errors->first('description_hi') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="description_gu">વર્ણન </label>
                                            <textarea class="form-control" name="description_gu" id="description_gu" rows="5" placeholder="વર્ણન  " maxlength="600">{{ old('description_gu') }}</textarea>
                                            @if ($errors->has('description_gu'))
                                                <span class="text-danger" style="color: red">{{ $errors->first('description_gu') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="cover">Image <span class="text-danger">*</span></label>
                                            <input type="file" name="cover" id="cover" class="form-control" accept="image/*" onchange="GetFileSize()">
                                            <label id="image_error" class="image_error">Image size is large.</label>
                                            <label id="image" class="image">Please select cover image</label>
                                            <label id="image_type_error"  class="image_type_error">Select image only .jpg, .png, .jpeg file</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="status">Status <span class="text-danger">*</span></label>
                                            <select name="status" id="status" class="form-control">
                                                <option value="">Select</option>
                                                <option value="0" @if(old('status')) selected="selected" @endif>Disable</option>
                                                <option value="1" @if(old('status')) selected="selected" @endif>Enable</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="shipping_price">Shipping Price </label>
                                            <input type="text" name="shipping_price" id="shipping_price" placeholder="xxxxx" class="form-control numeric" value="{{ old('shipping_price') }}" maxlength="5" >
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                </div>

                                <!--<div class="row">-->
                                <!--    <div class="col-lg-4">-->
                                <!--       <div class="form-group">-->
                                <!--            <label for="hsn_code">HSN Code </label>-->
                            <!--            <input type="text" name="hsn_code" id="hsn_code" placeholder="HSN Code" class="form-control numeric" value="{{ old('hsn_code') }}" maxlength="8" >-->
                                <!--            <div class="text-danger" id="hsnError"></div>-->
                                <!--        </div>-->
                            <!--        @if ($errors->has('hsn_code'))-->
                            <!--            <span class="text-danger" style="color: red;">{{ $errors->first('hsn_code') }}</span>-->
                                <!--        @endif-->
                                <!--    </div>-->
                                <!--    <div class="col-lg-4">-->
                                <!--    </div>-->
                                <!--    <div class="col-lg-4">-->
                                <!--    </div>-->
                                <!--</div>-->

                                <div class="row">
                                    <div class="col-lg-4">
                                        @include('admin.shared.attribute-select', [compact('default_weight')])
                                    </div>
                                </div>

                                <div class="row"></div>
                            <!-- @include('admin.shared.status-select', ['status' => 2]) -->
                                <div class="row">
                                    <div class="col-sm-6 col-md-12 col-sm-6 col-xs-12">
                                        <label for="unit">Enter Unit <span class="text-danger">*</span></label>
                                        <div class="scroll">
                                            <table class="table table-bordered" id="dynamic_field">
                                                <tr>
                                                    <td class="table-align"><label for="unit" class="lable-size">Unit Key <span class="text-danger ">*</span></label><input type="text" name="unitkey[]" placeholder="Enter Unit Key" class="form-control name_list numeric frm-txtarea12"  maxlength="5"/></td>
                                                    <td class="table-align">
                                                        <label for="unit" class="lable-size">Unit Value<span class="text-danger">*</span></label>
                                                        <select name="unitoption[]" id="unitoption" class="form-control frm-txtarea123">
                                                            <option value="ac">Ac (Acre)</option>
                                                            <option value="bbl">bbl (Barrel)</option>
                                                            <option value="c">c (Cup)</option>
                                                            <option value="dp">dp (Drop)</option>
                                                            <option value="ft">ft (Foot)</option>
                                                            <option value="gl">gl (Gallon)</option>
                                                            <option value="gr">gr (Gram)</option>
                                                            <option value="htr">htr (Hectare)</option>
                                                            <option value="L">L (Liter)</option>
                                                            <option value="in">In (Inch)</option>
                                                            <option value="kg">kg (kilogram)</option>
                                                            <option value="km">km (Kilometer)</option>
                                                            <option value="m">m (Meter)</option>
                                                            <option value="mg">mg (Milligram)</option>
                                                            <option value="ml">ml (Milliter)</option>
                                                            <option value="pc">pc (Piece)</option>
                                                            <option value="qt">Qt (Quintal)</option>
                                                            <option value="scm">scm (Square Centimeter)</option>
                                                            <option value="sf">sf (Square Foot)</option>
                                                            <option value="skm">skm (Square Kilometer)</option>
                                                            <option value="smeter">smeter (Square Meter)</option>
                                                            <option value="sqi">sqi (Square Inch)</option>
                                                            <option value="sqm">sqm (Square Mile)</option>
                                                            <option value="sy">sy (Square Yard)</option>
                                                            <option value="t">t (Tonne)</option>
                                                            <option value="ts">ts (Teaspoon)</option>
                                                        </select>
                                                    </td>
                                                    <td class="table-align"><label for="unit" class="lable-size">Price <span class="text-danger ">*</span></label><input type="text" name="prices[]" placeholder="Enter Price" class="form-control name_list numeric price_list1"  maxlength="8"/></td>
                                                    <td class="table-align"><label for="unit" class="lable-size">Sale Price <span class="text-danger ">*</span></label><input type="text" name="sale_prices[]" placeholder="Enter Sale Price" class="form-control numeric sale_list1"  maxlength="8"/></td>
                                                    <td class="table-align"><label for="unit" class="lable-size">Quantity <span class="text-danger">*</span></label><input type="text" name="quantities[]" placeholder="Enter Quantity" class="form-control numeric quantity_list1"  maxlength="5"/></td>
                                                    <td class="table-align"><label for="unit" class="lable-size">Max Quantity <span class="text-danger ">*</span></label><input type="text" name="max_quantities[]" placeholder="Enter Max Quantity" class="form-control numeric max_quantity_list"  maxlength="2"/></td>
                                                    <td class="table-align"><label for="unit" class="lable-size">Discount</label><input type="text" name="discount[]" placeholder="Enter Discount" class="form-control numeric discount_list"  maxlength="5"/></td>
                                                    <!--<td><label for="unit" class="lable-size">GST(%)<span class="text-danger">*</span> </label>-->
                                                <!--<input type="text" name="gst[]" placeholder="Enter GST" value="@if(isset($u->gst_price)){{$u->gst_price}}@endif" class="form-control name_list numeric"  maxlength="5"/></td>-->
                                                    <td align="center" class="table-align"><button type="button" name="add" id="add" class="btn btn-success" style="margin-top: 24px;">+</button></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <label for="additional_info">Additional Information <span class="text-danger">*</span></label>
                                        <table class="table table-bordered" id="additional_info">
                                            <tr>
                                                <td>
                                                    <input type="text" name="additional_info[]" placeholder="Enter Key" class="form-control name_list character additional_info_txt additional_info_txt_one" id="additional_info_name" maxlength="50" />
                                                    <label id="additional_info_error" class="show-hide" style="display:none;color:red!important">
                                                        Please enter key
                                                    </label>
                                                    <input type="text" name="additional_info_value[]" placeholder="Enter Value" class="form-control name_list character additional_info_val additional_info_val_one" id="addtional_info_value" maxlength="25" /><label id="additional_info_value_error" class="show-hide" style="display:none;color:red!important">
                                                        Please enter additional information value
                                                    </label></td>
                                                <td align="center"><button type="button" name="add_additional_info" id="add_additional_info" class="btn btn-success">Add</button></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-lg-4">
                                        <label for="additional_info">अतिरिक्त जानकारी <span class="text-danger">*</span></label>
                                        <table class="table table-bordered" id="additional_info_hi">
                                            <tr>
                                                <td>
                                                    <input type="text" name="additional_info_hi[]" placeholder="कुंजी दर्ज" class="form-control name_list additional_info_txt additional_info_txt_hi_one" id="additional_info_name" maxlength="50" />
                                                    <label id="additional_info_error" class="show-hide" style="display:none;color:red!important">
                                                        Please enter key
                                                    </label>
                                                    <input type="text" name="additional_info_value_hi[]" placeholder="मान दर्ज करें" class="form-control name_list additional_info_val additional_info_val_hi_one" id="addtional_info_value" maxlength="25" /><label id="additional_info_value_error" class="show-hide" style="display:none;color:red!important">
                                                        Please enter additional information value
                                                    </label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-lg-4">
                                        <label for="additional_info">વધારાની માહિતી <span class="text-danger">*</span></label>
                                        <table class="table table-bordered" id="additional_info_gu">
                                            <tr>
                                                <td>
                                                    <input type="text" name="additional_info_gu[]" placeholder="કી દાખલ કરો" class="form-control name_list additional_info_txt additional_info_txt_gu_one" id="additional_info_name" maxlength="50" />
                                                    <label id="additional_info_error" class="show-hide" style="display:none;color:red!important">
                                                        Please enter key
                                                    </label>
                                                    <input type="text" name="additional_info_value_gu[]" placeholder="કિંમત દાખલ કરો" class="form-control name_list additional_info_val additional_info_val_gu_one" id="addtional_info_value" maxlength="25" /><label id="additional_info_value_error" class="show-hide" style="display:none;color:red!important">
                                                        Please enter additional information value
                                                    </label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6 col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label for="offer_title">Offer Title </label>
                                            <input type="text" name="offer_title" id="offer_title" placeholder="Offer Title" class="form-control" value="{{ old('offer_title') }}" maxlength="25" >
                                            <label id="offer_title_error" class="show-hide" style="color:red!important; display: none;">
                                                Please enter offer title
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label for="offer_title">प्रस्ताव शीर्षक </label>
                                            <input type="text" name="offer_title_hi" id="offer_title_hi" placeholder="प्रस्ताव शीर्षक" class="form-control" value="{{ old('offer_title') }}" maxlength="25" >
                                            <label id="offer_title_error" class="show-hide" style="color:red!important; display: none;">
                                                Please enter offer title
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label for="offer_title">ઓફર શીર્ષક </label>
                                            <input type="text" name="offer_title_gu" id="offer_title_gu" placeholder="ઓફર શીર્ષક" class="form-control" value="{{ old('offer_title') }}" maxlength="25" >
                                            <label id="offer_title_error" class="show-hide" style="color:red!important; display: none;">
                                                Please enter offer title
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label for="offer_product">Offer Product </label>
                                            <input type="text" name="offer_product" id="offer_product" placeholder="Offer Product" class="form-control" value="{{ old('offer_product') }}" maxlength="25" >
                                            <label id="offer_product_error" class="show-hide" style="color:red!important; display: none;">
                                                Please enter offer product
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label for="offer_product">उत्पाद प्रस्तुत करते हैं </label>
                                            <input type="text" name="offer_product_hi" id="offer_product_hi" placeholder="उत्पाद प्रस्तुत करते हैं" class="form-control" value="{{ old('offer_product') }}" maxlength="25" >
                                            <label id="offer_product_error" class="show-hide" style="color:red!important; display: none;">
                                                Please enter offer product
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label for="offer_product">ઉત્પાદન ઓફર </label>
                                            <input type="text" name="offer_product_gu" id="offer_product_gu" placeholder="ઉત્પાદન ઓફર" class="form-control" value="{{ old('offer_product') }}" maxlength="25" >
                                            <label id="offer_product_error" class="show-hide" style="color:red!important; display: none;">
                                                Please enter offer product
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="offer_product_image">Offer Image <span class="text-danger">*</span></label>
                                            <input type="file" name="offer_product_image" id="offer_product_image" class="form-control" accept="image/*" onchange="GetFileSize()">
                                            <label id="image_error" class="image_error">Image size is large.</label>
                                            <label id="offer_image_error" class="show-hide" style="color:red!important; display: none;">Please select offer image</label>
                                            <label id="offer_image_type_error"  class="image_type_error">Select image only .jpg, .png, .jpeg file</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="offer_quantity">Offer Quantity </label>
                                            <input type="text" name="offer_quantity" id="offer_quantity" placeholder="Offer Quantity" class="form-control numeric" value="{{ old('offer_quantity') }}" maxlength="5" >
                                            <label id="offer_quantity_error" class="show-hide" style="color:red!important; display: none;">
                                                Please enter offer quantity
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="btn-group">
                                <a href="{{ route('admin.orderpurchase.index') }}" class="btn btn-default">Cancel</a>
                                <input type="button" class="btn btn-info" value="Translate" id="translate">
                                <button type="submit" style="" id="add_submit" class="btn btn-primary">Add</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
<style type="text/css">
    .additional_info_txt {
        margin-bottom: 10px;
    }
    #add_additional_info {
        width: 100%;
    }
</style>
@section('js')
    <script src="{{ asset('js/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <!-- <script src="{{ asset('js/additional-methods.min.js') }}"></script> -->
    <script>
        function getProductdetail(val,id){
            $.ajax({
                url : "{{route('admin.orderpurchase.getProductsdetail')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": val
                },
                type: 'post',
                dataType: 'json',
                success: function( result )
                {

                    $('#product_availblequantity'+id).val(result.available_quantity);
                    $('#price'+id).val(parseFloat(result.price).toFixed(2));
                    $('#sale_price'+id).val(parseFloat(result.sale_price).toFixed(2));

                    /*$('#crop_id').append($('<option>', {value:'', text:'Select Crop Category'}));
                    $.each( result, function(k, v) {
                        $('#crop_id').append($('<option>', {value:k, text:v}));
                    });*/
                },
                error: function()
                {
                    alert('error...');
                }
            });
            $.ajax({
                url : "{{route('admin.orderpurchase.getProductsattribute')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": val
                },
                type: 'post',
                dataType: 'json',
                success: function( result )
                {
                    //console.log(result);
                    $('#unitoption'+id).empty();
                    $.each( result, function(k, v) {
                        $('#unitoption'+id).append($('<option>', {value:v.id+','+v.value+' '+v.key, text:v.value+' '+v.key}));
                    });
                }
            });
        }
        function getUnitdetail(val,id){
            // alert(val);
            // alert(id);
            $.ajax({
                url : "{{route('admin.orderpurchase.getProductsdetail')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": val
                },
                type: 'post',
                dataType: 'json',
                success: function( result )
                {

                    $('#product_availblequantity'+id).val(result.available_quantity);
                    $('#price'+id).val(parseFloat(result.price).toFixed(2));
                    $('#sale_price'+id).val(parseFloat(result.sale_price).toFixed(2));

                    /*$('#crop_id').append($('<option>', {value:'', text:'Select Crop Category'}));
                    $.each( result, function(k, v) {
                        $('#crop_id').append($('<option>', {value:k, text:v}));
                    });*/
                },
                error: function()
                {
                    alert('error...');
                }
            });
        }
    </script>
    <script type="text/javascript">
        document.getElementById("additional_info_value_error").style.color = "red";
        document.getElementById("additional_info_error").style.color = "red";

        $('#newproduct_id').change(function(){

            $.ajax({
                url : "{{route('admin.orderpurchase.getProductsdetail')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": $(this).val()
                },
                type: 'post',
                dataType: 'json',
                success: function( result )
                {
                    console.log(result);

                    $('#product_availblequantity').val(result.available_quantity);
                    $('#product_avlblquantity').val(result.available_quantity);
                    $('#unitkey').val(result.value);
                    $('#unitoption').val(result.key);
                    $('#price').val(parseFloat(result.price).toFixed(2));
                    $('#sale_price').val(parseFloat(result.sale_price).toFixed(2));
                },
                error: function()
                {
                    alert('error...');
                }
            });

            $.ajax({
                url : "{{route('admin.orderpurchase.getProductsattribute')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": $(this).val()
                },
                type: 'post',
                dataType: 'json',
                success: function( result )
                {
                    //console.log(result);
                    $('#unitoption').empty();
                    $.each( result, function(k, v) {
                        $('#unitoption').append($('<option>', {value:v.id+','+v.value+' '+v.key, text:v.value+' '+v.key}));
                    });
                }
            });
        });

        $('#unitoption').change(function(){
            var id=$(this).attr('id');
            $.ajax({
                url : "{{route('admin.orderpurchase.getProductsdetail')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": $(this).val()
                },
                type: 'post',
                dataType: 'json',
                success: function( result )
                {
                    $('#product_availblequantity').val(result.available_quantity);
                    $('#product_avlblquantity').val(result.available_quantity);
                    //$('#unitkey').val(result.value);
                    // $('#unitoption').val(result.key);
                    $('#price').val(parseFloat(result.price).toFixed(2));
                    $('#sale_price').val(parseFloat(result.sale_price).toFixed(2));
                },
                error: function()
                {
                    alert('error...');
                }
            });
        });
        var i=1;
        $(document).on('click', '#add', function(){
            i++;
            $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td><label for="unit" class="lable-size">Unit <span class="text-danger">*</span></label><input type="text" name="unitkey[]" required placeholder="Enter Unit Key" class="form-control name_list" /></td><td><label for="unit" class="lable-size">Unit Value<span class="text-danger">*</span></label><select name="unitoption[]" id="unitoption" class="form-control select2" required><option value="ac">Ac (Acre)</option><option value="bbl">bbl (Barrel)</option><option value="c">c (Cup)</option><option value="dp">dp (Drop)</option><option value="ft">ft (Foot)</option><option value="gl">gl (Gallon)</option><option value="gr">gr (Gram)</option><option value="htr">htr (Hectare)</option><option value="'+i+'">i (Liter)</option><option value="in">In (Inch)</option><option value="kg">kg (kilogram)</option><option value="km">km (Kilometer)</option><option value="m">m (Meter)</option><option value="mg">mg (Milligram)</option><option value="ml">ml (Milliter)</option><option value="pc">pc (Piece)</option><option value="qt">Qt (Quintal)</option><option value="scm">scm (Square Centimeter)</option><option value="sf">sf (Square Foot)</option><option value="skm">skm (Square Kilometer)</option><option value="smeter">smeter (Square Meter)</option><option value="sqi">sqi (Square Inch)</option><option value="sqm">sqm (Square Mile)</option><option value="sy">sy (Square Yard)</option><option value="t">t (Tonne)</option><option value="ts">ts (Teaspoon)</option></select></td><td class="table-align"><label for="unit" class="lable-size">Price <span class="text-danger">*</span></label><input type="text" name="prices[]" placeholder="Enter Price" class="form-control name_list numeric"  maxlength="7"/></td><td class="table-align"><label for="unit" class="lable-size">Sale Price <span class="text-danger">*</span></label><input type="text" name="sale_prices[]" placeholder="Enter Sale Price" class="form-control numeric"  maxlength="7"/></td></td><td class="table-align"><label for="unit" class="lable-size">Quantity <span class="text-danger">*</span></label><input type="text" name="quantities[]" placeholder="Enter Quantity" class="form-control numeric"  maxlength="5"/></td><td class="table-align"><label for="unit" class="lable-size">Max Quantity <span class="text-danger">*</span></label><input type="text" name="max_quantities[]" placeholder="Enter Max Quantity" class="form-control numeric"  maxlength="2"/></td><td class="table-align"><label for="unit" class="lable-size">Discount </label><input type="text" name="discount[]" placeholder="Enter Discount" class="form-control numeric"  maxlength="5"/></td><td align="center"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove" style="margin-top: 24px;">x</button></td></tr>');
        });
        var j=1;
        $(document).on('click', '#addnewdynamic', function(){
            j++;
            var n=$('#newproduct_id').html();
            //console.log(n);
            var d = $('.productlst').html();

            $('.exstngprod').append('<div class="row" id="row'+j+'"><div class="col-lg-2 productlst"><div class="form-group"><label for="inoviceno">Product Name<span class="text-danger">*</span></label><select name="newproduct_id[]" id="newproduct_id'+j+'" class="form-control newproduct_id select2" onchange="getProductdetail(this.value,'+j+');">'+n+'</select></div></div> <div class="col-lg-2 productlst"><div class="form-group"><label for="unit">Unit<span class="text-danger">*</span></label><select name="unitoption[]" id="unitoption'+j+'" class="form-control frm-txtarea123" aria-invalid="false"  onchange="getUnitdetail(this.value,'+j+');"></select></div></div><div class="col-lg-1"><div class="form-group"><label for="inoviceno"> Quantity<span class="text-danger">*</span><input type="hidden" name="product_avlblquantity[]" id="product_avlblquantity'+j+'" placeholder="Add Quantity" class="form-control character" maxlength="300"><input type="text" name="product_availblequantity[]" id="product_availblequantity'+j+'" placeholder="Available Quantity" maxlength="5" class="form-control character only_number_allow_and_limit" disabled></div></div><div class="col-lg-2"><div class="form-group"><label for="inoviceno">New Quantity<span class="text-danger">*</span></label><input type="text" name="product_newquantity[]" id="product_newquantity'+j+'" placeholder="Add Quantity" class="form-control character only_number_allow_and_limit" maxlength="5"></div></div><div class="col-lg-2"><div class="form-group"><label for="inoviceno"> Price<span class="text-danger">*</span></label><input type="text" name="price[]" id="price'+j+'" placeholder="Price" class="form-control character"  maxlength="7"></div></div><div class="col-lg-2"><div class="form-group"><label for="inoviceno">Product Sale Price<span class="text-danger">*</span></label><input type="text" name="sale_price[]" id="sale_price'+j+'" placeholder="Sale Price" class="form-control character"  maxlength="7"></div></div><div class="col-lg-1"><button type="button" name="remove" id="'+j+'" class="btn btn-danger btn_remove" style="margin-top: 24px;">x</button></div></div>');


            // $(".select2").select2();

            /*   $(".select2").select2({
                       ajax: {
                       url: "{{route('admin.orderpurchase.getProducts')}}",
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                    }
                });*/
        });
        $('.newproduct_id').change(function(){
            var d=$(this).attr('id');
            alert(d);
        });
        $('#product_oldnew').change(function(){
            //alert($(this).val());
            if($(this).val()==1){
                $('#newproduct').show();
                $('#exstproduct').hide();
            }
            else{
                $('#exstproduct').show();
                $('#newproduct').hide();
            }

        });
        $(document).on('click', '.btn_remove', function(){
            var button_id = $(this).attr("id");
            $('#row'+button_id+'').remove();
        });

        $(document).on('click', '#add_additional_info', function(){
            i++;
            $('#additional_info').append('<tr id="row'+i+'btn" class="dynamic-added row'+i+'btn"><td><input type="text" name="additional_info[]" id="additional_info_name_'+i+'" onclick="add_info(this.id)" required placeholder="Enter Key" class="form-control name_list addition_information_txt additional_info_txt additional_info_txt_'+i+'"  maxlength="50" data-addition-information-txt-id="'+i+'" /><label id="additional_info_error" class="show-hide">Please Enter Additionl Information</label><input type="text" name="additional_info_value[]" id="additional_info_value_'+i+'" placeholder="Enter Value" class="form-control name_list additional_info_val additional_info_val_'+i+' addition_information_value"  maxlength="20" required data-addition-information-value-id="'+i+'"/><label id="additional_info_value_error" class="show-hide">Please Enter Value</label></td><td align="center"><button type="button" name="remove" id="'+i+'btn" class="btn btn-danger btn_remove_additional_info">X</button></td></tr>');

            $('#additional_info_hi').append('<tr id="row'+i+'btn" class="dynamic-added row'+i+'btn"><td><input type="text" name="additional_info_hi[]" id="additional_info_name_'+i+'" onclick="add_info(this.id)" required placeholder="Enter Key" class="form-control name_list  additional_info_txt additional_info_txt_'+i+'_hi"  maxlength="50" data-addition-information-txt-hi-id="'+i+'" /><label id="additional_info_error" class="show-hide">Please Enter Additionl Information</label><input type="text" name="additional_info_value_hi[]" id="additional_info_value_'+i+'" placeholder="Enter Value" class="form-control name_list additional_info_val additional_info_val_'+i+'_hi"  maxlength="20" required data-addition-information-value-hi-id="'+i+'" /><label id="additional_info_value_error" class="show-hide">Please Enter Value</label></td></tr>');

            $('#additional_info_gu').append('<tr id="row'+i+'btn" class="dynamic-added row'+i+'btn"><td><input type="text" name="additional_info_gu[]" id="additional_info_name_'+i+'" onclick="add_info(this.id)" required placeholder="Enter Key" class="form-control name_list additional_info_txt additional_info_txt_'+i+'_gu"  maxlength="50" data-addition-information-txt-gu-id="'+i+'"/><label id="additional_info_error" class="show-hide">Please Enter Additionl Information</label><input type="text" name="additional_info_value_gu[]" id="additional_info_value_'+i+'" placeholder="Enter Value" class="form-control name_list additional_info_val additional_info_val_'+i+'_gu"  maxlength="20" required data-addition-information-value-gu-id="'+i+'" /><label id="additional_info_value_error" class="show-hide">Please Enter Value</label></td></tr>');
        });

        $(document).on('click', '.btn_remove_additional_info', function(){
            var button_id = $(this).attr("id");
            console.log(button_id);
            $('.row'+button_id).remove();
        });


        $('#crop_category').change(function(){
            $("#crop_id option").remove();
            var id = $(this).val();
            $.ajax({
                url : "{{route('admin.products.getCropCategories')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                },
                type: 'post',
                dataType: 'json',
                success: function( result )
                {
                    $('#crop_id').append($('<option>', {value:'', text:'Select Crop Category'}));
                    $.each( result, function(k, v) {
                        $('#crop_id').append($('<option>', {value:k, text:v}));
                    });
                },
                error: function()
                {
                    alert('error...');
                }
            });
        })

        $('#company_id').change(function(){
            $("#category_id option").remove();
            $("#subcategory_id option").remove();
            var id = $(this).val();
            console.log('id',id);

            $.ajax({
                url : "{{route('admin.products.getCategories')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                },
                type: 'post',
                dataType: 'json',
                success: function( result )
                {
                    $('#category_id').append($('<option>', {value:'', text:'Select Category'}));
                    $.each( result, function(k, v) {
                        $('#category_id').append($('<option>', {value:k, text:v}));
                    });
                },
                error: function()
                {
                    alert('error...');
                }
            });
        });

        $('#category_id').change(function(){
            $("#subcategory_id option").remove();

            var id = $(this).val();

            $.ajax({
                url : "{{route('admin.products.getSubCategories')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                },
                type: 'post',
                dataType: 'json',
                success: function( result )
                {
                    if(result!=''){
                        $('#totalsubcategory').val(1);
                        $('.subcategory').show();
                        $('#subcategory_id').append($('<option>', {value:'', text:'Select Sub Category'}));
                        $.each( result, function(k, v) {
                            $('#subcategory_id').append($('<option>', {value:k, text:v}));
                        });
                    }
                    else{
                        $('#totalsubcategory').val(0);
                        $('.subcategory').hide();
                    }

                },
                error: function()
                {
                    alert('error...');
                }
            });

            var company_id=$('#company_id').val();
            $.ajax({
                url : "{{route('admin.orderpurchase.getProducts')}}",
                data: {
                    //"subsubcategory_id": subsubcategory_id,
                    "company_id":company_id,
                    "category_id":id,
                    //"subcategory_id":subcategory_id,
                },
                type: 'get',
                dataType: 'json',
                success: function( result )
                {
                    // $('#subsubcategory_id').empty();
                    // console.log(result);
                    $('#product_id').empty();
                    // $('.subsubcategory').show();
                    //   $('#totalsubsubcategory').val(1);
                    //  $('#subsubcategory_id').append($('<option>', {value:'', text:'Select Sub Category'}));
                    $.each( result, function(k, v) {
                        $('#product_id').append($('<option>', {value:k, text:v}));
                    });
                }


            });
        });
        $('#subcategory_id').change(function(){
            var id=$('#company_id').val();
            var category = $('#subcategory_id').val();
            $("#subsubcategory_id option").remove();

            $.ajax({
                url : "{{route('admin.products.getSubCategories')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": category
                    // "parentid":category
                },
                type: 'post',
                dataType: 'json',
                success: function( result )
                {
                    if(result!=''){
                        $('.subsubcategory').show();
                        $('#totalsubsubcategory').val(1);
                        $('#subsubcategory_id').append($('<option>', {value:'', text:'Select Sub Category'}));
                        $.each( result, function(k, v) {
                            $('#subsubcategory_id').append($('<option>', {value:k, text:v}));
                        });
                    }
                    else{
                        $('.subsubcategory').hide();
                        $('#totalsubsubcategory').val(0);
                    }


                },
                error: function()
                {
                    alert('error...');
                }
            });
        });
        $('#subsubcategory_id').change(function(){

            var subsubcategory_id = $(this).val();
            var company_id = $('#company_id').val();
            var category_id = $('#category_id').val();
            var subcategory_id = $('#subcategory_id').val();

            $.ajax({
                url : "{{route('admin.orderpurchase.getProducts')}}",
                data: {
                    "subsubcategory_id": subsubcategory_id,
                    "company_id":company_id,
                    "category_id":category_id,
                    "subcategory_id":subcategory_id,
                },
                type: 'get',
                dataType: 'json',
                success: function( result )
                {
                    $('#product_id').empty();
                    console.log(result);
                    // $('.subsubcategory').show();
                    //   $('#totalsubsubcategory').val(1);
                    //  $('#subsubcategory_id').append($('<option>', {value:'', text:'Select Sub Category'}));
                    $.each( result, function(k, v) {
                        $('#product_id').append($('<option>', {value:k, text:v}));
                    });
                }
            });
        });

        $('#select-meal-type').change(function(){
            var arr = $(this).val();
            console.log(arr)
        })
        $("#add_product").validate({
            ignore: [],
            errorClass: 'error text-change',
            successClass: 'validation-valid-label',
            highlight: function (element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function (element, errorClass) {
                $(element).removeClass(errorClass);
            },
            validClass: "validation-valid-label",
            rules: {
                /*  name: {
                      required: true,
                      // lettersonly: true,
                      maxlength: 300,
                  },*/
                // sku: {
                //     required: true,
                //     maxlength: 25,
                // },
                order_no: {
                    required: true,
                },
                seller_name: {
                    required: true,
                },
                email: {
                    required: true,
                },
                address: {
                    required: true,
                },
                gstno:{
                    required: true,
                },
                mobileno:{
                    required: true,
                },
                inoviceno:{
                    required: true,
                },
                product_oldnew:{
                    required: true,
                },
                company_id:{
                    required: function(){
                        return $('#product_oldnew').val()==1;
                    },
                },
                category_id:{
                    required: function(){
                        return $('#product_oldnew').val()==1;
                    },
                }
                // crop_id: {
                //     required: true,
                // },
                // crop_category: {
                //     required: true,
                // },
                /*   category_id: {
                       required: function(){
                           return $('#product_oldnew').val()==0;
                       },
                   },
                   subcategory_id: {
                        required: function () {
                            if($('#product_oldnew').val()==0){
                               return $('#totalsubcategory').val()==1;
                            }
                        }
                   },
                   subsubcategory_id: {
                        required: function () {
                           if($('#product_oldnew').val()==0){
                            return $('#totalsubsubcategory').val()==1;

                           }
                        }
                   },*/

                // hsn_code: {
                //     required: true,
                //     maxlength: 8,
                //     minlength: 2,
                // },
                /* company_id : {
                     required: true,
                 },
                 technical_name : {
                     required: true,
                     maxlength: 25,
                 },
                 status : {
                     required: true,
                 }*/
            },
            messages: {
                "order_no":{
                    required: "Please enter order no",
                },
                "seller_name":{
                    required: "Please enter seller name",
                },
                "gstno":{
                    required: "Please enter gstno",
                },
                "email": {
                    required: "Please enter email",
                },
                "mobileno":{
                    required:  "Please enter mobileno",
                },
                "inoviceno":{
                    required: "Please enter invoiceno",
                },
                "product_oldnew":{
                    required:"Please select producttype",
                },
                "address": {
                    required: "Please enter address",
                },
                "company_id":{
                    required: "Please enter companyid",
                },
                "category_id":{
                    required: "Please enter categoryid",
                }
                /* "name":{
                     required: "Please enter name",
                     maxlength:"Please enter no more than 25 character",
                 },
                 "sku": {
                     required:"Please enter sku",
                     maxlength:"Please enter no more than 25 character",
                 },
                 "cover": {
                     required:"Please select image",
                 }
                 ,
                 "crop_id":{
                     required: "Please select crop sub-category",
                 },
                 "crop_category":{
                     required: "Please select crop",
                 },
                 "category_id":{
                     required: "Please select category",
                 },
                 "subcategory_id":{
                     required: "Please select sub category",
                 },
                 "subsubcategory_id": {
                      required:  "Please select sub sub category",
                 },
                 "company_id":{
                     required: "Please select company name",
                 },
                 "technical_name":{
                     required: 'Please select technical name',
                     maxlength:"Please enter no more than 25 character",
                 },
                 "status":{
                     required: "Please select status",
                 },
                 "hsn_code":{
                     required: "Please enter HSN Code",
                     minlength: "Please enter minimum 2 digit",
                     maxlength: "Please enter maximum 8 digit",
                 }*/
            },



            submitHandler: function (form) {
                // alert(form);

                var product_oldnew=$('#product_oldnew').val();

                if(product_oldnew==1){
                    var error = 0;
                    document.getElementById('image_error').style.display="none";
                    document.getElementById('image_type_error').style.display="none";
                    document.getElementById('image').style.display="none";

                    var logo = $('#cover').val();
                    if(logo==""){
                        document.getElementById('cover').style.display="block";
                        error=1;
                    } else {
                        var fi = document.getElementById('cover');
                        if (fi.files.length > 0) {
                            for (var i = 0; i <= fi.files.length - 1; i++) {
                                var fsize = fi.files.item(i).size;
                                var type = fi.files.item(i).type;
                                var size = fsize/1024;
                                console.log('fsize'+fsize+' type '+type+' size '+size);
                                if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                                    document.getElementById('image_type_error').style.display="block";
                                    error = 1;
                                } else if(size>1024){
                                    document.getElementById('image_error').style.display="block";
                                    error = 1;
                                }
                            }
                        }
                    }
                    addtional_name = $('#additional_info_name').val();
                    addtional_value = $('#addtional_info_value').val();
                    $("input:text[name='additional_info[]']").each(function( key, value ){
                        if($(this).val().trim()=='' || $(this).val().trim()==0){
                            $(this).css("border","1px solid red");
                            $(this).focus();
                            error = 1;
                        }

                    });
                    $("input:text[name='additional_info_value[]']").each(function( key, value ){
                        if($(this).val().trim()=='' || $(this).val().trim()==0){
                            $(this).css("border","1px solid red");
                            $(this).focus();
                            error = 1;
                        }

                    });
                    $("input:text[name='unitkey[]']").each(function( key, value ){
                        if($(this).val().trim()=='' || $(this).val().trim()==0){
                            $(this).css("border","1px solid red");
                            $(this).focus();
                            error = 1;
                        }

                    });
                    $("input:text[name='prices[]']").each(function( key, value ){
                        if($(this).val().trim()=='' || $(this).val().trim()==0){
                            $(this).css("border","1px solid red");
                            $(this).focus();
                            error = 1;
                        }

                    });
                    $("input:text[name='sale_prices[]']").each(function( key, value ){
                        if($(this).val().trim()=='' || $(this).val().trim()==0){
                            $(this).css("border","1px solid red");
                            $(this).focus();
                            error = 1;
                        }

                    });
                    $("input:text[name='quantities[]']").each(function( key, value ){
                        if($(this).val().trim()=='' || $(this).val().trim()==0){
                            $(this).css("border","1px solid red");
                            $(this).focus();
                        }

                    });
                    $("input:text[name='max_quantities[]']").each(function( key, value ){
                        if($(this).val().trim()=='' || $(this).val().trim()==0){
                            $(this).css("border","1px solid red");
                            $(this).focus();
                            error = 1;
                        }

                    });
                    // console.log(addtional_value);
                    if (addtional_name!="") {
                        if (addtional_value=="") {
                            $('#additional_info_value_error').attr('style', 'display: block !important','color:red!important');
                            $('#additional_info_error').attr('style', 'display: none !important');
                            return false;
                        }
                    } else{
                        console.log('addtional_value');
                        if (addtional_value!="") {
                            $('#additional_info_error').attr('style', 'display: block !important','color:red!important');
                            $('#additional_info_value_error').attr('style', 'display: none !important');
                            return false;
                        }
                    }
                    $("input:text[id='addtional_name1[]']").each(function( key, value ){
                        if($(this).val().trim()=='' || $(this).val().trim()==0){
                            $(this).css("border","1px solid red");
                            $(this).focus();
                            error = 1;
                        }

                    });
                    $("input:text[id='value1[]']").each(function( key, value ){
                        if($(this).val().trim()=='' || $(this).val().trim()==0){
                            $(this).css("border","1px solid red");
                            $(this).focus();
                            error = 1;
                        }

                    });
                    var offer_title = $('#offer_title').val();
                    var offer_product = $('#offer_product').val();
                    var offer_quantity = $('#offer_quantity').val();
                    var offer_product_image = $('#offer_product_image').val();
                    document.getElementById("offer_title_error").style.display = "none";
                    document.getElementById("offer_product_error").style.display = "none";
                    document.getElementById("offer_quantity_error").style.display = "none";
                    document.getElementById("offer_image_error").style.display = "none";
                    if (offer_title!="" || offer_product!="" || offer_quantity!="" || offer_product_image!="") {
                        if(offer_title=="" && offer_product=="" && offer_quantity=="" && offer_product_image=="") {

                            error=1;
                            console.log(offer_title);
                        } else {
                            if(offer_title==""){
                                error=1;
                                document.getElementById("offer_title_error").style.display = "block";
                            }
                            if(offer_product==""){
                                error=1;
                                document.getElementById("offer_product_error").style.display = "block";
                            }
                            if(offer_quantity==""){
                                error=1;
                                document.getElementById("offer_quantity_error").style.display = "block";
                            }
                            if(offer_product_image==""){
                                error=1;
                                document.getElementById("offer_image_error").style.display = "block";
                            }
                        }
                    }
                    console.log(error);
                    if (error == 1)  {
                        // alert(error);
                        return false;
                    }else{
                        $('#add_submit').attr('disabled', true);
                        form.submit();
                    }
                }
                else{
                    $('#add_submit').attr('disabled', true);
                    form.submit();
                }
                console.log('add');

            },
        });

        $('.numeric').on('input', function (event) {
            this.value = this.value.replace(/[^0-9\.]/g, '');
        });

        $('.select2-multiple').select2({
            tags: true,
            maximumSelectionLength: 1
        });

        $('#tags').select2({
            tags: true,
            maximumSelectionLength: 1
        });
        var data = <?php echo json_encode($infos) ?>;
        var count=data.length;
        var info =[];
        for(var i=0;i<count;i++){
            var val = Object.values(data[i]);
            info.push(val[0]);
        }
        console.log(info);
        autocomplete(document.getElementById("additional_info_name"), info);
        function autocomplete(inp, arr) {
            var currentFocus;
            inp.addEventListener("input", function(e) {
                var a, b, i, val = this.value;
                closeAllLists();
                if (!val) { return false;}
                currentFocus = -1;
                a = document.createElement("DIV");
                console.log("en");
                a.setAttribute("id", this.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items");
                this.parentNode.appendChild(a);
                for (i = 0; i < arr.length; i++) {
                    if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                        b = document.createElement("DIV");
                        b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                        b.innerHTML += arr[i].substr(val.length);
                        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                        b.addEventListener("click", function(e) {
                            inp.value = this.getElementsByTagName("input")[0].value;
                            console.log('value'+inp.value);
                            closeAllLists();
                        });
                        a.appendChild(b);
                    }
                }
            });
            inp.addEventListener("keydown", function(e) {
                var x = document.getElementById(this.id + "autocomplete-list");
                if (x) x = x.getElementsByTagName("div");
                if (e.keyCode == 40) {
                    currentFocus++;
                    addActive(x);
                } else if (e.keyCode == 38) {
                    currentFocus--;
                    addActive(x);
                } else if (e.keyCode == 13) {
                    e.preventDefault();
                    if (currentFocus > -1) {
                        if (x) x[currentFocus].click();
                    }
                }
            });
            function addActive(x) {
                if (!x) return false;
                removeActive(x);
                if (currentFocus >= x.length) currentFocus = 0;
                if (currentFocus < 0) currentFocus = (x.length - 1);
                x[currentFocus].classList.add("autocomplete-active");
            }
            function removeActive(x) {
                for (var i = 0; i < x.length; i++) {
                    x[i].classList.remove("autocomplete-active");
                }
            }
            function closeAllLists(elmnt) {
                var x = document.getElementsByClassName("autocomplete-items");
                for (var i = 0; i < x.length; i++) {
                    if (elmnt != x[i] && elmnt != inp) {
                        x[i].parentNode.removeChild(x[i]);
                    }
                }
            }
            document.addEventListener("click", function (e) {
                closeAllLists(e.target);
            });


        }

        function add_info(id) {
            autocomplete(document.getElementById(id), info);
        }


        function GetFileSize() {
            var fi = document.getElementById('cover');
            document.getElementById('image').style.display="none";
            document.getElementById('image_error').style.display="none";
            document.getElementById('image_type_error').style.display="none";
            if (fi.files.length > 0) {
                for (var i = 0; i <= fi.files.length - 1; i++) {
                    var fsize = fi.files.item(i).size;
                    var type = fi.files.item(i).type;
                    var size = fsize/1024;
                    console.log(size);
                    console.log(type);

                    if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                        document.getElementById('image_type_error').style.display="block";
                    } else if(size>1024){
                        document.getElementById('image_error').style.display="block";
                    }
                }
            }
        }
        $('.character').on('input', function (event) {
            this.value = this.value.replace(/[^0-9\.\a-z\A-Z\@ _()$&]/g, '');
        });

        $("#translate").on('click',function(){
            $("#add_submit").css('display','block');
            var name = $("#name").val();
            var description = $('#description').val();
            var description = $('#description').val();

            var additional_info_txt_one = $(".additional_info_txt_one").val();
            var additional_info_val_one = $(".additional_info_val_one").val();

            var offer_title = $("#offer_title").val();
            var offer_product = $("#offer_product").val();

            var tempArray = {};
            $(".addition_information_txt").each(function(index) {
                var aitid = $(this).attr("data-addition-information-txt-id");
                tempArray['additional_info_txt_'+aitid] = $(this).val();
            });
            $(".addition_information_value").each(function(index) {
                var aitid = $(this).attr("data-addition-information-value-id");
                tempArray['additional_info_val_'+aitid] = $(this).val();
            });

            tempArray["name"] = name;
            tempArray["description"] = description;
            tempArray["additional_info_txt_one"] = additional_info_txt_one;
            tempArray["additional_info_val_one"] = additional_info_val_one;

            tempArray["offer_title"] = offer_title;
            tempArray["offer_product"] = offer_product;
            // tempArray["additional_info_val_one"] = additional_info_val_one;
            $(".box").css({'opacity':'0.5','pointer-events': 'none'});
            $.ajax({
                url : "{{route('admin.translate')}}",
                data:  tempArray,
                type: 'get',
                dataType: 'json',
                success: function( result )
                {
                    var jsonObj = result;
                    $("#name_hi").val(jsonObj.name.hi);
                    $("#name_gu").val(jsonObj.name.gu);
                    $("#description_hi").val(jsonObj.description.hi);
                    $("#description_gu").val(jsonObj.description.gu);

                    $("#offer_title_gu").val(jsonObj.offer_title.gu);
                    $("#offer_title_hi").val(jsonObj.offer_title.hi);

                    $("#offer_product_gu").val(jsonObj.offer_product.gu);
                    $("#offer_product_hi").val(jsonObj.offer_product.hi);

                    $(".additional_info_txt_hi_one").val(jsonObj.additional_info_txt_one.hi);
                    $(".additional_info_txt_gu_one").val(jsonObj.additional_info_txt_one.gu);

                    $(".additional_info_val_hi_one").val(jsonObj.additional_info_val_one.hi);
                    $(".additional_info_val_gu_one").val(jsonObj.additional_info_val_one.gu);

                    for (myObj in jsonObj) {
                        var tempName = String(myObj);
                        $("."+tempName+"_hi").val(jsonObj[myObj].hi);
                        $("."+tempName+"_gu").val(jsonObj[myObj].gu);
                    }
                    $(".box").css({'opacity':'1','pointer-events': 'auto'});
                },
                error: function(error)
                {
                    console.log(error);
                }
            });
        });

        $("#hsn_code").on('blur',function(e){
            var hsncode = $(this).val();
            $.ajax({
                url : "{{route('admin.products.checkhsncode')}}",
                data:  hsncode,
                type: 'get',
                data : { "hsncode":hsncode },
                success: function( result )
                {
                    if(result=="true"){
                        $("#hsn_code").focus();
                        $("#hsnError").html("HSN code already exists.");
                        e.preventDefault();
                    } else{
                        $("#hsnError").html("");
                    }
                },
                error: function(error)
                {
                    console.log(error);
                }
            });
        });

        $(".blank_space_remove").keypress(function(e) {
            // console.log(e);
            if (e.which === 32 && !this.value.length) {
                e.preventDefault();
            }
            var inputValue = event.charCode;
            if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
                event.preventDefault();
            }
        });



        $(".only_space_remove").keypress(function(e) {
            // console.log(e);
            if (e.which === 32 && !this.value.length) {
                e.preventDefault();
            }
        });


        $('.only_number_allow').keypress(function (event) {
            var keycode = event.which;
            if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
                event.preventDefault();
            }
        });

        $('.only_number_allow_and_limit').keypress(function (event) {
            var keycode = event.which;
            if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
                event.preventDefault();
            }
        });


        $(document).ready(function(){

            // Initialize select2
            $("#newproduct_id").select2({dropdownAutoWidth : true});

        });

    </script>

@endsection
