@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border border-header header-color">
                <h3 class="box-title"><h2>Dashboard</h2></h3>
                @include('layouts.errors-and-messages')
            </div>
            <div class="box-body">
                @if($orders)
                    <div class="box-body">
                        <div class="box-header header-color border-header">
                            <h3>Top 5 Orders</h3>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="scroll">
                        <table class="table border-b1px" id="table-id">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Date</th>
                                    <th class="col-md-3">E-mail</th>
                                    <th >Mobile</th>
                                    <th class="col-md-2">Customer</th>
                                    <th>Total</th>
                                    <th>Status</th>
                                    <th>View</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php $i=1 @endphp
                            @foreach ($orders as $order)
                                <tr>
                                    @php
                                        $wallet_amount = $order['wallet_point'] * env('DISCOUNT_POINT');
                                    @endphp
                                    @php
                                     
                                        if (isset($wallet_amount)) {
                                            $total = $order['total'] + $order['total_shipping'] + $order['tax'] - $wallet_amount;
                                        } else{
                                            $total = $order['total'] + $order['total_shipping'] + $order['tax'];
                                        }
                                    @endphp
                                    <td>{{ $i++ }}</td> 
                                    <td>{{ date('M d, Y', strtotime($order->created_at)) }}</td>
                                    <td>{{$order->email }}</td>
                                    <td>{{$order->mobile }}</td>
                                    <td>{{$order->customer_name}}</td>
                                    <td>
                                        <span class="label @if($order->total != $order->total_paid) label-danger @else label-success @endif"><i class="fa fa-rupee"></i> {{ $total }}</span>
                                    </td>
                                    <td><p class="label" style=" color: black; background-color: {{ $order->color }}">{{ ucfirst($order->status) }}</p></td>
                                    <td>
                                        <!-- <a title="Show order" href="{{ route('admin.orders.show', $order->id) }}">
                                            <p class="text-center" style="color: #fff; background-color:#00c0ef">View & Download</p>
                                        </a> -->
                                        <a title="View order" href="{{ route('admin.orders.show', $order->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-eye" style="color: white; font-size:16px"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                    <div class="box-body">
                        <span class="input-group-btn" style="width: 0">
                            <a href="{{ route('admin.orders.index') }}" class="btn btn-primary btn-sm"> View All</a>
                        </span>
                    </div>
                @else
                    <div class="box">
                        <div class="box-body"><p class="alert alert-warning">No orders found.</p></div>
                    </div>
                @endif
            </div>
            <div class="box-body">
                @if(!$products->isEmpty())
                    <div class="box-body">
                        <div class="box-header border-header header-color">
                            <h3>Top 5 Products</h3>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="scroll">
                        <table class="table border-b1px table-product" id="table-id">
                            <thead>
                            <tr>
                                <th class="col-md-1" >No.</th>
                                <th class="col-md-1">Name</th>
                                <th class="col-md-1">Technical</th>
                                <th class="col-md-1">Category</th>
                                <th class="col-md-1">Company</th>
                                <th class="col-md-1">Status</th>
                                <th class="col-md-1">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=1 @endphp
                            @foreach ($products as $product)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $product->name }} </td>
                                    <td>{{ $product->technical_name }}</td>
                                    <td>Rs {{ $product->category }}</td>
                                    <td>Rs {{ $product->company }}</td>
                                    <td>@include('layouts.status', ['status' => $product->status])</td>
                                    <td>
                                        @php
                                           $permission = App\Helper\Permission::permission('update_stock');
                                        @endphp
                                        @if($permission->edit==1)
                                            <div class="btn-group">
                                                <a href="{{ route('admin.updateStock.edit', $product->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil" style="color: white; font-size:16px"></i></a>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                    <div class="box-body">
                        <span class="input-group-btn" style="width: 0">
                            <a href="{{ route('admin.updateStock.index') }}" class="btn btn-primary btn-sm"> View All</a>
                        </span>
                    </div> 
                @else
                    <div class="box">
                        <div class="box-body"><p class="alert alert-warning">No products found.</p></div>
                    </div>
                @endif
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('#table-id').DataTable({
            "searching": false,
            "paging":   false,
            "ordering": false,
            "info":     false
        });
        $('.table-product').DataTable({
            "searching": false,
            "paging":   false,
            "ordering": false,
            "info":     false
        });
    } );
</script>
@endsection