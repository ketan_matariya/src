<!doctype html>
<html lang="en">

<head>
    <title>Nilemart | Contact Us</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @include('layouts.front.head')
</head>

<body>
<style>
    label.error{
        color: red;
        font-weight: 400;
    }
</style>
<div class="main-div">
    @include('layouts.front.header-cart')
    <div class="container my-4">
        <h3 class="text-orange product-head">Contact Us</h3>
        <hr class="product-head">
    </div>
    <div style="width: 100%"><iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=Plot%20No.%206%20&amp;%207,%20B/h.%20Shah%20Warehouse,%20Near%20Hariyana%20Hotel,%20Piplaj%20Road,%20Shahwadi,%20Ahmedabad+(NileMart)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe></div>
    <section class="contact-page">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                <h3 class="text-orange product-head">NileMart</h3>
            <hr class="product-head">
                    <span>Address</span>
                    <p>Plot No. 6 & 7, B/h. Shah Warehouse,</p>
                    <p>Near Hariyana Hotel, Piplaj Road, Shahwadi,</p>
                    <p>Ahmedabad</p>
                    <span>Phone No.</span>
                    <p>7990459881</p>
                    <span>Email</span>
                    <p> <a href="mailto:nilemart.ahd@gmail.com"><span class="fa fa-envelope-o mr-3"></span>nilemart.ahd@gmail.com</a></p>
                    <h2 class="border-top pt-3 mt-3 text-orange font-weight-bold">Keep in touch</h2>
                    <div class="footer-social">
                        <ul>
                            <li>
                                <a href="#"><span class="fa fa-facebook"></span></a>
                            </li>
                            <li>
                                <a href="#"><span class="fa fa-twitter"></span></a>
                            </li>
                            <li>
                                <a href="#"><span class="fa fa-google-plus"></span></a>
                            </li>
                            <li>
                                <a href="#"><span class="fa fa-youtube"></span></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="login-register-cart">
                        <h3 class="text-orange product-head">Contact Us</h3>
                        <hr class="product-head">
                            <p style="color: green;">
                                @if (\Session::has('success'))
                                    <div class="alert alert-success">
                                        <ul>
                                            <li>{!! \Session::get('success') !!}</li>
                                        </ul>
                                    </div>
                                @endif
                            </p>
                        <form id="create_customer" method="post" action="{{ url('contactMail')}} ">
                            @csrf
                            <div class="form-group">
                                <input type="text" name="name" id="name" placeholder="Name" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" id="email" placeholder="Email" class="form-control">
                            </div>
                            <div class="form-group">
                                <textarea rows="5" name="message"  id="message" placeholder="Message" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="text" maxlength="10" name="mobile" id="mobile" placeholder="Phone Number" class="form-control">
                            </div>
                            <button type="submit" class="btn btn-default cart-login">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('layouts.front.footer')
</div>
 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="{{ url('css/front/js/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ url('css/front/js/bootstrap.min.js') }}"></script>
    <script src="{{ url('css/front/js/webslidemenu.js') }}"></script>
    <script src="{{ url('css/front/js/wow.js') }}"></script>
    <script src="{{ url('css/front/js/owl.carousel.min.js') }}"></script>
    <script src="{{ url('css/front/js/jquery.themepunch.plugins.min.js') }}"></script>
    <script src="{{ url('css/front/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ url('css/front/js/function.js') }}"></script>


<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">
    $("#create_customer").validate({
        ignore: [],
        errorClass: 'error',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            name: {
                required: true,
                maxlength: 50,
            },
            mobile: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10,
            },
            email: {
                required: true,
                email: true,
            },
            message: {
                required: true,
            }
        },
        messages: {
            "name":{
                required: "Please enter name.",
                // lettersonly: "Please enter only character.",
                maxlength: "Please enter max 25 character.",
            },
            "mobile":{
                required: "Please enter mobile.",
                digits: "Please enter number only.",
                minlength: "Please enter atleast 10 digit.",
                maxlength: "Please enter no more than 10 digit.",
            },
            "email":{
                required: "Please enter email.",
                email: "Please enter a valid email address.",
            },
            "message":{
                required: "Please enter message.",
            },
        },
        submitHandler: function(form) {
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
    });
</script>
</body>

</html>