<!doctype html>
<html lang="en">
   <head>
      <title>NileMart | Product Detail</title>
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      @include('layouts.front.head')
   </head>
      <style type="text/css">
/* Style the sidenav links and the dropdown button */
.dropdown a, .dropdown-btn {
  padding: 6px 8px 6px 16px;
  text-decoration: none;
  font-size: 16px;
  color: #fff;
  display: block;
  border: none;
  background: none;
  width: 100%;
  text-align: left;
  cursor: pointer;
  outline: none;
   border-radius: 0;

      border-radius: 0;
    line-height: 30px;
    position: relative;
   
}

/* On mouse-over */
.dropdown a:hover, .dropdown-btn:hover {
  color: #000;
}


/* Dropdown container (hidden by default). Optional: add a lighter background color and some left padding to change the design of the dropdown content */
.dropdown-container {
  display: none;
  background-color: #818181e0;
  padding-left: 8px;
}

.dropdown-container1 {
  display: none;
  background-color: #818181e0;
  padding-left: 8px;
}
/* Optional: Style the caret down icon */
.fa-caret-down {
  float: right;
  padding-right: 8px;
}

   </style>
   <body>
      <div class="main-div">
         @include('layouts.front.header-cart')
         <section class="pro-image-info border-bottom">
            <div class="container">
               <div class="row">
                  <div class="col-md-3">
                     <div class="top-categories">
                        <h3>Top Categories</h3>
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                             @foreach($categories as $category)
                         @if(!$categories->isEmpty())
                       
                        <div class="dropdown">
                            <a class="dropdown-btn nav-link @if($category->id == $products->category_id) active @endif" href="@if($category->children->isEmpty()) {{ route('productCategoryListingCat.product',$category->id)}} @else # @endif" id="{{$category->id}}" role="tab">{{$category->name}}
                           @if(!$category->children->isEmpty() )
                                 <i class="fa fa-caret-down"></i>
                                 <div class="dropdown-container">
                                     @foreach($category->children as $subcats)
                                        <a  href="@if($subcats->children->isEmpty()) {{url('productCategory',$subcats->id)}} @else # @endif" id="{{$subcats->id}}" class="dropdown-btn1" role="tab">{{$subcats->name}}
                                          @if(!$subcats->children->isEmpty())
                                             <i class="fa fa-caret-down"></i>
                                             <div class="dropdown-container1">
                                                @foreach($subcats->children as $sub)
                                                   <a href="#" id="{{$sub->id}}" class="subcategoryid">{{$sub->name}}
                                                @endforeach
                                             </div>
                                          @endif
                                       </a>
                                    @endforeach
                                 </div>
                           @endif
                           </a>
                        </div>
                        @else

                        <a class="nav-link @if($category->id == $products->category_id) active @endif" id="v-pills-home-tab" href="{{route('productCategoryListingCat.product',$category->id)}}" role="tab">{{$category->name}}</a>
                        @endif
                        @endforeach
                        </div>
                     </div>
                  </div>
                  <div class="col-md-9">
                     <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                           <div class="row">
                              <div class="col-lg-6">
                                 <div class="row">
                                    <div class="col-md-12 col-12">
                                       <div class="main-image">
                                          <div class="single-slide">
                                             <img src="{{$products->cover}}">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <input type="hidden" id="user_id" name="user_id" value="@if(!auth()->user()) 0 @else{{auth()->user()->id}}@endif">
                              <input type="hidden" id="dropdwn" name="dropdwn" value="0">
                              <input type="hidden" id="customer_id" name="customer_id" value="@if(!auth()->user()) 0 @else{{auth()->user()->id}}@endif">
                              <input type="hidden" id="product_id" name="product_id" value="{{$products->product_id}}">
                              <input type="hidden" id="pro_price" name="pro_price" value="{{$products->sale_price}}">
                              <input type="hidden" id="attribute_id" name="attribute_id" value="{{$products->id}}">
                              <input type="hidden" id="mrpprice" name="mrpprice" value="{{$products->price}}">
                              <input type="hidden" id="saleprice" name="saleprice" value="{{$products->sale_price}}">
                              <input type="hidden" id="attributeid" name="attributeid" value="{{$products->id}}">
                              <input type="hidden" id="max_quantities" name="max_quantities" value="{{$products->max_quantities}}">
                              <input type="hidden" id="hidproductid" name="hidproductid" value="{{$products->product_id}}">
                              <input type="hidden" id="hidavailable_quantity" name="available_quantity" value="{{$products->available_quantity}}"/>
                              @if(!empty($cart))
                              <input type="hidden" id="hidcartid" name="hidcartid" value="{{$cart->id}}">
                              <input type="hidden" id="hidattributeid" name="hidattributeid" value="{{$cart->attribute_id}}">
                              <input type="hidden" id="hidtotal_price" name="hidtotal_price" value="{{$cart->total_price}}">
                              <input type="hidden" id="hiplus_sine" name="hiplus_sine" value="+">
                              <input type="hidden" id="hidminus_sine" name="hidminus_sine" value="-">
                              @endif
                              <input type="hidden" name="hidattrebute_id" id="hidattrebute_id" value="{{$products->id}}"/>
                              <div class="col-lg-6">
                                 <div class="pro-size-info">
                                    <h1 class="pro-name">{{$products->name}}</h1>
                                 </div>
                                 @php
                                 $mrp = floatval($products->price);
                                 $price = floatval($products->sale_price);
                                 $save = str_replace(",","",$products->price) - str_replace(",","",$products->sale_price);
                                 @endphp
                                   
                                 <hr>
                                 <div class="pricepart">
                                    <input type="hidden" name="" id="mrpP" value="">
                                    <input type="hidden" name="" id="pP" value="">
                                    <p>MRP: <del id="promrp" class="promrp mrpUnit{{$products->product_id}}">{{$products->price}}</del><strong id="proprice" class="proprice price priceUnit{{$products->product_id}}"> {{$products->sale_price}}</strong></p>
                                    <p style="{{(number_format($save,2)!=0.00 ) ? 'display:inline;':'display:none;'}}" id="prodyousave{{$products->product_id}}">You Save : <strong class="text-green" id="prodsave{{$products->product_id}}">&#8377; {{number_format($save,2)}}</strong> &nbsp;&nbsp;&nbsp; Inclusive of all taxes</p>
                                    <p>Unit: <strong id="valueUnit{{$products->product_id}}">{{$products->value}}</strong><strong style="text-transform: uppercase;" id="keyUnit{{$products->product_id}}">{{$products->key}}</strong></p>
                                    <hr>
                                       <p>Availability: <span class="text-green font-weight-bold" id="instock">In Stock </span></p>
                                    <hr>
                                    <p>Inaugural Offer <strong>Free Shipping</strong></p>
                                 </div>
                                 @if($login=='true')
                                 @php
                                 $getCart = DB::table('cart')->where('customer_id',auth()->user()->id)->get();
                                 $getCntCart = $getCart->count();
                                 if(!empty($getCntCart))
                                 {
                                 $getCnt=$getCntCart;
                                 }else {
                                 $getCnt = 0;
                                 } 
                                 @endphp
                                 @endif
                                 <div class="d-flex align-items-end">
                                    <div class="prod-form form-group w-25">
                                       <p class="m-0">Qty</p>
                                       <select class="form-control" id="varntproduct_id" style="text-transform: uppercase;">
                                       @foreach($product as $pro)
                                       <option value="{{$pro->id}},{{$pro->product_id}},{{$pro->max_quantities}},{{$pro->available_quantity}}" >{{$pro->value}} {{$pro->key}}</option>
                                       @endforeach
                                       </select>
                                    </div>
                                    <div class="mx-2 productIncrease" id="pm" style="display: show">
                                       <a class="btn py-0">
                                          @if($login=='true')
                                          <div id="Increas" style="@if($products->available_quantity!=0)  display:none; @else  display:flex; @endif ">
                                             <button class="btnpm" onclick="IncreaseDecreaseValue('hidminus_sine')">
                                             <i class="fa fa-minus" aria-hidden="true"></i>
                                             </button>
                                             <input type="text" class="number-input" id="number" name="number" value="1" readonly>
                                             <button class="btnpm" @if($products->available_quantity!=0) onclick="IncreaseDecreaseValue('hiplus_sine')" @else onclick="errorValue()"  @endif>
                                             <i class="fa fa-plus" aria-hidden="true"></i>
                                             </button>
                                          </div>
                                          @endif
                                       </a>
                                    </div>
                                    <div class="cssbtn-part">
                                       @if($login=='true') 
                                      
                                       <a id="atc" style="display: none" class="cart postaddtocart" onclick="AvailableQuantity()" ><span> Add <i class="fa fa-plus add ml-2"></i></span> </a>
                                        <span class="text-danger font-weight-bold" id="outofstock" style="display: none">Out Of Stock </span>
                                       
                                       @if(!isset($quantity))
                                       <script>
                                          (function(){
                                             var available_quantity = $("#hidavailable_quantity").val();
                                             if(available_quantity!=0)
                                             {
                                              $("#Increas").hide();
                                              $("#atc").show();
                                              $("outofstock").hide();
                                             }else{
                                                $("#Increas").hide();
                                                $("#atc").hide();
                                                $("#outofstock").show();
                                                $("#instock").html('Out Of Stock');
                                             }
                                          })();
                                       </script>
                                       @else
                                       <script>
                                          (function(){
                                              $("#Increas").show();
                                              $("#number").val({{$quantity}});
                                              $("#atc").hide();
                                              var pmrp = $('#mrpprice').val() * {{$quantity}};
                                              var pp = $('#saleprice').val() * {{$quantity}};
                                              $('#promrp').html(pmrp.toFixed(2));
                                              $('#proprice').html(pp.toFixed(2));
                                              var id = $('#varntproduct_id').children("option:selected").val();  
                                              $('#prodsave'+id.split(",")[1]).html('&#8377; '+(pmrp-pp).toFixed(2));
                                             
                                          })();
                                       </script>
                                       @endif
                                       @else
                                       <a id="atc" class="cart" href="{{url('user/login')}}"><span> Add <i class="fa fa-plus add ml-2"></i></span> </a>
                                       @endif
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <section class="pro-detil-additional-info">
                              <div class="row">
                                 <div class="col-sm-12">
                                    <div class="product-des">
                                       <h3>Description</h3>
                                       <div class="pro-title-info">
                                          <!--<h1 class="pro-name">{{$products->name}}</h1>-->
                                          <span class="web-address">{{$products->description}}</span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </section>
                        </div>
                        <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">second tab</div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         @include('layouts.front.footer')
      </div>
      <div class="modal fade login-signup" id="LoginSignup" role="dialog">
         <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <div class="modal-body">
                  <section id="formHolder">
                     <div class="row">
                        <!-- Brand Box -->
                        <div class="col-sm-6 brand">
                           <div class="heading">
                              <img src="{{url('css/front/images/main-logo.svg')}}" alt="">
                           </div>
                           <div class="success-msg">
                              <p>Great! You are one of our members now</p>
                              <a href="#" class="profile">Your Profile</a>
                           </div>
                        </div>
                        <!-- Form Box -->
                        <div class="col-sm-6 form">
                           <!-- Login Form -->
                           <div class="login form-peice switched">
                              <form class="login-form" action="#" method="post">
                                 <div class="form-group">
                                    <label for="loginemail">Email Adderss</label>
                                    <input type="email" name="loginemail" id="loginemail" required>
                                 </div>
                                 <div class="form-group">
                                    <label for="loginPassword">Password</label>
                                    <input type="password" name="loginPassword" id="loginPassword" required>
                                 </div>
                                 <div class="CTA">
                                    <input type="submit" value="Login">
                                    <a href="#" class="switch">I'm New</a>
                                 </div>
                              </form>
                           </div>
                           <!-- End Login Form -->
                           <!-- Signup Form -->
                           <div class="signup form-peice">
                              <form class="signup-form" action="#" method="post">
                                 <div class="form-group">
                                    <label for="name">First Name</label>
                                    <input type="text" name="FirstName" id="FirstName" class="name">
                                    <span class="error"></span>
                                 </div>
                                 <div class="form-group">
                                    <label for="name">Last Name*</label>
                                    <input type="text" name="LastName" id="LastName*" class="name">
                                    <span class="error"></span>
                                 </div>
                                 <div class="form-group">
                                    <label for="email">Email Adderss</label>
                                    <input type="email" name="emailAdress" id="email" class="email">
                                    <span class="error"></span>
                                 </div>
                                 <div class="form-group">
                                    <label for="phone">Phone Number </label>
                                    <input type="text" name="phone" id="phone">
                                 </div>
                                 <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" id="password" class="pass">
                                    <span class="error"></span>
                                 </div>
                                 <div class="form-group">
                                    <label for="passwordCon">Confirm Password</label>
                                    <input type="password" name="passwordCon" id="passwordCon" class="passConfirm">
                                    <span class="error"></span>
                                 </div>
                                 <div class="CTA">
                                    <input type="submit" value="Signup Now" id="submit">
                                    <a href="#" class="switch">I have an account</a>
                                 </div>
                              </form>
                           </div>
                           <!-- End Signup Form -->
                        </div>
                     </div>
                  </section>
               </div>
            </div>
         </div>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
      <script s44rc="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
      <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
      <script src="{{ url('css/front/js/bootstrap.min.js') }}"></script>
      <script src="{{ url('css/front/js/owl.carousel.min.js') }}"></script>
      <script src="{{ url('css/front/js/hammer.min.js') }}"></script>
      <script src="{{ url('css/front/js/xzoom.min.js') }}"></script>
      <script src="{{ url('css/front/js/foundation.min.js') }}"></script>
      <script src="{{ url('css/front/js/zoom.js') }}"></script>
      <script src="{{ url('css/front/js/webslidemenu.js') }}"></script>
      <script src="{{ url('css/front/js/wow.js') }}"></script>
      <script src="{{ url('css/front/js/function.js') }}"></script>
      <script>
         $(document).ready(function() {
             $('.main-image').slick({
                 slidesToShow: 1,
                 slidesToScroll: 1,
                 arrows: false,
                 fade: true,
                 asNavFor: '.small-product-image'
             });
         
             $('.small-product-image').slick({
                 slidesToShow: 3,
                 slidesToScroll: 1,
                 asNavFor: '.main-image',
                 focusOnSelect: true,
                 vertical: true,
                 arrows: true,
                 prevArrow: '<button><i class="fa fa-arrow-up" aria-hidden="true"></i></button>',
                 nextArrow: '<button><i class="fa fa-arrow-down" aria-hidden="true"></i></button>',
             });
         
             $('#radioBtn a').on('click', function() {
                 var sel = $(this).data('title');
                 var tog = $(this).data('toggle');
                 $('#' + tog).prop('value', sel);
         
                 $('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').removeClass('active').addClass('notActive');
                 $('a[data-toggle="' + tog + '"][data-title="' + sel + '"]').removeClass('notActive').addClass('active');
             });
         });
      </script>
      <script type="text/javascript">
      
         function IncreaseDecreaseValue(div_id)
         {
            var value = parseInt(document.getElementById('number').value, 10);
             var mrp = $('#mrpP').val();
             @if(null !==Auth::user())
             {
                 var userid="{{Auth::user()->id}}";
                 var customer_id = "{{Auth::user()->id}}";
             }
             @endif
             var mrpprice = $('#pP').val();
             var dropdwn = $('#dropdwn').val();
             var  plus_minus = $("#"+div_id).val();
             
             var  cartid = $("#hidcartid").val();
             var  productid = $("#hidproductid").val();
             var  total_price = $("#mrpprice").val();
             var id = $('#varntproduct_id').children("option:selected").val();  
             var attributeid = id.split(",")[0];
             var quantity = $("#number").val();
             value = isNaN(value) ? 0 : value;
             if(div_id=='hidminus_sine')
             {
                if(value == 1)
                {
                   
                  $.ajax({
                        url: "{{url('api/v1/addCart')}}",
                data: {
                   user_id: userid,
                   customer_id: customer_id,
                   product_id: productid,
                   quantity: 0,
                   total_price: $('#saleprice').val(),
                   attribute_id: $('#attributeid').val(),
                 },
               type: 'POST',
               dataType: 'json',
               success: function(result) { 
                 
                   if(result!="")
                   {
                     showCart(userid,customer_id);
                    $("#atc").show();
                    $("#Increas").hide();
                     
                   }
               },
               error: function(error){
                 console.log("error");
               }
             });
                    
                }
                else{
                 value--;
                 plus_minus ="-";
                }
             }
             else
             {
                
                 value++;
                 plus_minus ="+";
             }
             document.getElementById('number').value = value;
             if(dropdwn=='0'){
                 var pmrp = $('#mrpprice').val() * value;
                 var pp = $('#saleprice').val() * value;
             }
             else{
                 var pmrp = mrp * value;
                 var pp = mrpprice * value;
             }
            var max_quantities=  $("#max_quantities").val();
            var available_quantity = $("#hidavailable_quantity").val();
            if(value<=available_quantity)
            {
            if(value<=max_quantities)
            {
                $('#promrp').html(pmrp.toFixed(2));
                $('#proprice').html(pp.toFixed(2));
                $('#prodsave'+id.split(",")[1]).html('&#8377; '+(pmrp-pp).toFixed(2));
                Updatequantity(cartid,value,productid,userid,userid,attributeid,total_price,plus_minus);
            }
            else
            {
                $("#number").val(max_quantities);
                toastr.options = {
                          "preventDuplicates": true,
                           "preventOpenDuplicates": true
                     };
                toastr.error ("<span style='font-size:15px;font-weight:bold'>You cannot add more items. </span>");
                
            }
          }else
          {
                $("#number").val(available_quantity);
                toastr.options = {
                          "preventDuplicates": true,
                           "preventOpenDuplicates": true
                     };
                toastr.error ("<span style='font-size:15px;font-weight:bold'>Product Not Available </span>");
                
          }

         }
         
          
         function AvailableQuantity()
         {
             var attribute_id = $("#attribute_id").val();
             $.ajax({
            url:"{{url('SelectUnitPrice')}}/"+attribute_id,
            method: 'get',
               success: function(data) {
                                     
                if(data[0].available_quantity>0)
                {
                 
                 var user_id = $("#user_id").val();
                 var customer_id = $("#customer_id").val();
                 var product_id = $('#product_id').val();
                 var quantity = $("#number").val();
                 var total_price = $("#pro_price").val();
                 var attribute_id = $("#attribute_id").val();
                 var varntproduct_id = $('#varntproduct_id').val();
         getCartData(user_id,customer_id,product_id,quantity,total_price,attribute_id);
         
         function getCartData(user_id,customer_id,product_id,quantity,total_price,attribute_id){
             
             $.ajax({
               url: "{{url('api/v1/addCart')}}",
               data: {
                 user_id: user_id,
                 customer_id: customer_id,
                 product_id: product_id,
                 quantity: quantity,
                 total_price: $('#saleprice').val(),
                 attribute_id: $('#attributeid').val(),
               },
               type: 'POST',
               dataType: 'json',
               success: function(result) { 
                
                   if(result!="")
                   {
                     $("#atc").hide();
                     $("#Increas").show();
                     showCart(user_id,customer_id);
                   }
               },
               error: function(error){
                 console.log("error");
               }
             });
         } 
                }else{
                 $("#atc").show();
                 alert("Product Not Available")
                }
              }
           });
         }
         
         
         function deleteCart(customer_id,product_id,attribute_id){
             $.ajax({
                 url: "{{url('api/v1/deleteProductCart')}}",
                 data: {
                   customer_id: customer_id,
                   product_id: product_id,
                   attribute_id: attribute_id,
                   user_id: customer_id,
                 },
                 type: 'POST',
                 success: function(result) {
                   console.log("addtocart",result);
                 },
                 error: function(error){
                   console.log("error");
                 }
               });
         }
         
         function Updatequantity(cartid,quantity,product_id,customer_id,user_id,attribute_id,total_price,plus_minus){
            var max_quantities=  $("#max_quantities").val();
         if(quantity<=max_quantities)
         {
           $.ajax({
               url: "{{url('api/v1/addCart')}}",
               data: {
                 user_id: user_id,
                 customer_id: customer_id,
                 product_id: product_id,
                 quantity: quantity,
                 total_price: total_price,
                 attribute_id: attribute_id,
                 cart_id:cartid,
               },
               type: 'POST',
               success: function(result) {
                 console.log("addtocart",result);
               },
               error: function(error){
                 console.log("error");
               }
             });
         }
         else
         {
            $("#number").val(max_quantities);
            toastr.options = {
                          "preventDuplicates": true,
                           "preventOpenDuplicates": true
                     };
         toastr.error ("<span style='font-size:15px;font-weight:bold'>You cannot add more items. </span>");
         }
         }
         
         function errorValue()
         {
            toastr.options = {
                          "preventDuplicates": true,
                           "preventOpenDuplicates": true
                     };
             toastr.error ("<span style='font-size:15px;font-weight:bold'>Product is a out of stock </span>");
         }
         
      </script>
      <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
      <script type="text/javascript">
         $(".sec-content").hide();
         $(".add").click(function() {
             $("#pm").show();
             $("#atc").hide();
             
         });
      </script>
      <script type="text/javascript">
         $(".cartadderror").click(function(){
            toastr.options = {
                          "preventDuplicates": true,
                           "preventOpenDuplicates": true
                     };
             toastr.error ("<span style='font-size:15px;font-weight:bold'>Product is a out of stock </span>");
         });
         
         function showCart(user_id,customer_id){
                 $.ajax({
                   url: "{{url('api/v1/showCart')}}",
                   data: {
                     // _token: CSRF_TOKEN,
                     user_id: user_id,
                     customer_id: customer_id,
                   },
                   type: 'GET',
                   dataType: 'json',
                   success: function(result) {
                       if(result!="")
                       {
                           var count=(result.data.carts === 'undefined')?0:result.data.carts.length;  
                           $("#items").html(count);     
                       }
                   },
                   error: function(error){
                     console.log("error");
                   }
                 });
             } 
         var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
         
         $(".postaddtocart").click(function(){
             var user_id = $("#user_id").val();
             var customer_id = $("#customer_id").val();
             var product_id = $('#product_id').val();
             var quantity = $("#number").val();
             var total_price = $("#pro_price").val();
             var attribute_id = $("#attribute_id").val();
             var varntproduct_id = $('#varntproduct_id').val();
             
             getCartData(user_id,customer_id,product_id,quantity,total_price,attribute_id);
             function getCartData(user_id,customer_id,product_id,quantity,total_price,attribute_id){
                 $.ajax({
                   url: "{{url('api/v1/addCart')}}",
                   data: {
                     user_id: user_id,
                     customer_id: customer_id,
                     product_id: product_id,
                     quantity: quantity,
                     total_price: $('#saleprice').val(),
                     attribute_id: $('#attributeid').val(),
                   },
                   type: 'POST',
                   dataType: 'json',
                   success: function(result) {
                       if(result!="")
                       {
                         $("#atc").hide();
                         $("#Increas").show();
                         showCart(user_id,customer_id);
                         toastr.options = {
                          "preventDuplicates": true,
                           "preventOpenDuplicates": true
                         };
                         toastr.success("<span style='font-size:15px;font-weight:bold'>"+result.message+"</span>");
                       }
                   },
                   error: function(error){
                     console.log("error");
                   }
                 });
             } 
         });
      </script>
      <script type="text/javascript">
         $(document).ready(function(){
             var CSRF_TOKEN = $('meta[name="csrf-token"]');
             var CSRF_TOKEN = $('meta[name="csrf-token"]');
             $("select#varntproduct_id").on('change',function(){
             var id = $(this).children("option:selected").val();  
             var customer_id = $("#customer_id").val();
             var quantity = $("#number").val();
             var product_id = id.split(",")[1];
             var attribute_id = id.split(",")[0];
             var max_quantities = id.split(",")[2];
             var available_quantityes = id.split(",")[3];
             $("#hidproductid").val(product_id);
             $("#hidattributeid").val(attribute_id);
             $("#hidattrebute_id").val(attribute_id);
             $("#max_quantities").val(max_quantities);
             $("#hidavailable_quantity").val(available_quantityes);
             $.ajax({
                 type:'get',
                 url: "{{url('productCheckcart')}}",
                 data: {
                         customer_id:customer_id,
                         product_id: product_id,
                         quantity:quantity,
                         attribute_id:attribute_id,
                 },
                 success: function(result) {
                     if(result!="")
                     {
                        if(result == 1)
                        { 
                           if(available_quantityes==0)
                           {
                              $("#atc").hide();
                              $("#outofstock").show();
                              $("#instock").html('Out Of Stock');
                           }else{
                              $("#atc").show();
                              $("#outofstock").hide();
                              $("#instock").html('In Stock');
                              $("#number").val(1);
                              $("#Increas").hide();
                           }

                        }else
                        {
                           if(result.attribute_id ==  attribute_id || result.product_id != product_id)
                           {
                             $("#Increas").show();
                             $("#number").val(result.quantity);
                             $("#atc").hide();
                           }
                        }
                     }
                 }
             });

               $('#dropdwn').val('1');
               $.ajax({
                   url:"{{url('SelectUnitPrice')}}/"+id.split(",")[0],
                   method: 'get',
                   success: function(data) {
                     $('.priceUnit'+id.split(",")[1]).empty();
                       if(data[0].discount!=undefined){
                         $('#proddiscount'+id.split(",")[1]).css('display','inline');
                         $('#proddiscount'+id.split(",")[1]).empty();
                         $('#proddiscount'+id.split(",")[1]).html(data[0].discount);
                       }
                     var quantity=$('#number').val(); 
                     var totalsale = parseFloat(data[0].sale_price) * quantity;
                     $('.priceUnit'+id.split(",")[1]).html(totalsale.toFixed(2));
                     $('#saleprice').val(data[0].sale_price);
                     $('#attributeid').val(id.split(',')[0]);
                     var totalprice=parseFloat(data[0].price) * quantity;
                    $('.mrpUnit'+id.split(",")[1]).html(totalprice.toFixed(2));
                     $('#valueUnit'+id.split(",")[1]).html(data[0].value);
                     $('#valueUnitList'+id.split(",")[1]).html(data[0].value);
                     //var saveprice=parseFloat(data[0].price) - parseFloat(data[0].sale_price);
                     var saveprice=parseFloat( totalprice - totalsale);
                     if(saveprice.toFixed(2)!=0.0){
                       $('#prodyousave'+id.split(",")[1]).show();
                       $('#prodsave'+id.split(",")[1]).html('&#8377; '+saveprice.toFixed(2));
                     }
                     $('#keyUnit'+id.split(",")[1]).html(data[0].key);
                     $('#keyUnitList'+id.split(",")[1]).html(data[0].key);
                     $('#mrpP').val(data[0].price);
                     $('#pP').val(data[0].sale_price);
                  }
               });
           });
         });
      </script>
            <script>
/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
  this.classList.toggle("active");
  var dropdownContent = this.nextElementSibling;
  if (dropdownContent.style.display === "block") {
  dropdownContent.style.display = "none";
  } else {
  dropdownContent.style.display = "block";
  }
  });
}
</script>

<script>
/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
var dropdown = document.getElementsByClassName("dropdown-btn1");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
  this.classList.toggle("active");
  var dropdownContent = this.nextElementSibling;
  if (dropdownContent.style.display === "block") {
  dropdownContent.style.display = "none";
  } else {
  dropdownContent.style.display = "block";
  }
  });
}
</script>
   </body>
</html>