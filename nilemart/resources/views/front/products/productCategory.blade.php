<!doctype html>
<html lang="en">

<head>
   <title>NileMart | Category</title>
   @include('layouts.front.head')
</head>

<body>
   <div class="main-div">
      @include('layouts.front.header-cart')

      <!-- <section class="slider-part inner-slider">
          <img src="{{url('css/front/images/product-category-inner.jpg')}}" alt="">
      </section> -->

      <!-- <section>
         <div class="container">
            <div class="row">
               <div class="col-sm-12">
                  <div class="page-breadcrumb">
                     <h2>Categories</h2>
                     <ul class="breadcrumb">
                        <li><a href="{{route('home')}}"><span class="fa fa-home"></span> Home</a></li>
                        <li class="active">Categories</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section> -->
      <div class="container">
         <div class="row">
            <div class="col-md-3">
               <div class="top-categories">
                  <h3>Top Categories</h3>
                  <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                     <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Fruits</a>
                     <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Grains</a>
                     <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Vegetable stores</a>
                  </div>
               </div>
            </div>
            <div class="col-md-9">
            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
               <section class="fine-jewellery">
                  <div class="container">
                     <h3 class="product-head">Category</h3>
                     <hr class="product-head">
                     <div class="single-product mb-3">
                        <div class="row">
                           @if(!$categories->isEmpty())
                           @foreach($categories as $cat)
                           <div class="col-md-4 col-6">
                              <li>
                                 <figure>
                                    <a class="product-img" href="{{route('productSubCategory.product',$cat->id)}}"><img src="{{$cat->cover}}" class="img-fluid" alt="polo shirt img"></a>
                                    <a class="add-card-btn justify-content-center" href="{{route('productSubCategory.product',$cat->id)}}">View More</a>
                                    <figcaption>
                                       <h4 class="product-title"><a href="#">{{$cat->name}}</a></h4>
                                    </figcaption>
                                 </figure>
                              </li>
                           </div>
                           @endforeach
                           @else
                           <div class="col-12">
                              <div class="empty-message">
                                 <img src="{{url('css/front/images/no-product.gif')}}" alt="empty product">
                                 <p>Category not available.</p>
                              </div>
                           </div>
                           @endif
                        </div>
                     </div>
                  </div>
               </section>
            </div>
            <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab"></div>
            </div>
         </div>
      </div>

      <!-- footer start -->
      @include('layouts.front.footer')
   </div>


   <div class="modal fade login-signup" id="LoginSignup" role="dialog">
      <div class="modal-dialog modal-lg">
         <!-- Modal content-->
         <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body">
               <section id="formHolder">
                  <div class="row">
                     <!-- Brand Box -->
                     <div class="col-sm-6 brand">
                        <div class="heading">
                           <img src="{{url('css/front/images/main-logo.svg')}}" alt="">
                        </div>

                        <div class="success-msg">
                           <p>Great! You are one of our members now</p>
                           <a href="#" class="profile">Your Profile</a>
                        </div>
                     </div>


                     <!-- Form Box -->
                     <div class="col-sm-6 form">

                        <!-- Login Form -->
                        <div class="login form-peice switched">
                           <form class="login-form" action="#" method="post">
                              <div class="form-group">
                                 <label for="loginemail">Email Adderss</label>
                                 <input type="email" name="loginemail" id="loginemail" required>
                              </div>

                              <div class="form-group">
                                 <label for="loginPassword">Password</label>
                                 <input type="password" name="loginPassword" id="loginPassword" required>
                              </div>

                              <div class="CTA">
                                 <input type="submit" value="Login">
                                 <a href="#" class="switch">I'm New</a>
                              </div>
                           </form>
                        </div><!-- End Login Form -->


                        <!-- Signup Form -->
                        <div class="signup form-peice">
                           <form class="signup-form" action="#" method="post">

                              <div class="form-group">
                                 <label for="name">Full Name</label>
                                 <input type="text" name="username" id="name" class="name">
                                 <span class="error"></span>
                              </div>

                              <div class="form-group">
                                 <label for="email">Email Adderss</label>
                                 <input type="email" name="emailAdress" id="email" class="email">
                                 <span class="error"></span>
                              </div>

                              <div class="form-group">
                                 <label for="phone">Phone Number - <small>Optional</small></label>
                                 <input type="text" name="phone" id="phone">
                              </div>

                              <div class="form-group">
                                 <label for="password">Password</label>
                                 <input type="password" name="password" id="password" class="pass">
                                 <span class="error"></span>
                              </div>

                              <div class="form-group">
                                 <label for="passwordCon">Confirm Password</label>
                                 <input type="password" name="passwordCon" id="passwordCon" class="passConfirm">
                                 <span class="error"></span>
                              </div>

                              <div class="CTA">
                                 <input type="submit" value="Signup Now" id="submit">
                                 <a href="#" class="switch">I have an account</a>
                              </div>
                           </form>
                        </div><!-- End Signup Form -->
                     </div>
                  </div>
               </section>
            </div>
         </div>
      </div>
   </div>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
   <script src="{{ url('css/front/js/jquery-1.12.4.min.js') }}"></script>
   <script src="{{ url('css/front/js/bootstrap.min.js') }}"></script>
   <script src="{{ url('css/front/js/webslidemenu.js') }}"></script>
   <script src="{{ url('css/front/js/wow.js') }}"></script>
   <script src="{{ url('css/front/js/owl.carousel.min.js') }}"></script>
   <script src="{{ url('css/front/js/jquery.themepunch.plugins.min.js') }}"></script>
   <script src="{{ url('css/front/js/jquery.themepunch.revolution.min.js') }}"></script>
   <script src="{{ url('css/front/js/function.js') }}"></script>


</body>

</html>