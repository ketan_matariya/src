<!doctype html>
<html lang="en">

<head>
   <title>NileMart | Product List</title>
   <meta name="csrf-token" content="{{ csrf_token() }}" />
   @include('layouts.front.head')
</head>

<body>
   <div class="main-div">
      @include('layouts.front.header-cart')
      <div class="container">
         <div class="row">
            <div class="col-md-3">
               <div class="top-categories">
                  <h3>Top Categories</h3>
                 
                  <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                     @foreach($categories as $category)
                        @if($getproducts!=null)
                        <a class="nav-link @if($getproducts->category_id == $category->id) active @endif" id="v-pills-home-tab" href="{{route('productCategoryListingCat.product',$category->id)}}" role="tab">{{$category->name}} </a>
                        @else
                        <a class="nav-link @if($cat == $category->id) active @endif" id="v-pills-home-tab" href="{{route('productCategoryListingCat.product',$category->id)}}" role="tab">{{$category->name}} </a>
                        @endif
                      @endforeach
                  </div>
               </div>
            </div>
            <div class="col-md-9">
               <div class="tab-pane active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                  <section class="product-listing">
                     <div class="container">
                        <h3 class="text-orange product-head">{{($subcategory!=null) ? 'Sub Category':'Products'}}</h3>
                        <hr class="product-head">
                        <div class="single-product">
                           <div class="row">
                            
                             @if($subcategory!=null)
                             @foreach($subcategory as $subcategry)
                             <div class="col-md-4 col-6">
                              <li>
                                 <figure>
                                    <a class="product-img" href="{{route('productCategoryListingCat.product',$subcategry->id)}}"><img src="{{$subcategry->cover}}" class="img-fluid" alt="{{$subcategry->name}}"></a>
                                    <!-- <a class="add-card-btn" href="http://127.0.0.1:8000/productDetail/38">Add to Cart <i class="fa fa-plus-circle" aria-hidden="true"></i></a> -->
                                    <figcaption>
                                       <h4 class="product-title">
                                          <a href="#">{{$subcategry->name}}</a>
                                          {{-- <span class="product-price font-weight-bold"> ₹ 356.00</span> --}}
                                       </h4>
                                       <div class="prod-form form-group w-100">
                                          
                                       {{-- <select class="form-control" id="product_id" name="product_id">
                                                                     <option class="pid" value="38,38">1 kg</option>
                                                                  </select> --}}
                                       </div>
                                       <a class="add-card-btn justify-content-center" href="{{route('productCategoryListingCat.product',$subcategry->id)}}">View More</a>
                                      <?php /* ?> <div class="add-to-cart-div mt-3">
                                          <div class="qtn-class">
                                             <span>Qty </span>
                                             <input type="number" name="" id="" value="1">
                                          </div>
                                          <div class="add-button">
                                             <button><i class="fa fa-plus mr-2" aria-hidden="true"></i>Add</button>
                                          </div>
                                       </div>
                                       <?php */?>
                                    </figcaption>
                                   
                                 </figure>

                                 <!-- product badge -->
                                 {{-- <span class="badge sale" href="#"><img src="http://127.0.0.1:8000/css/front/images/sale.png" alt=""></span> --}}
                              </li>
                           </div>
                           @endforeach
                            @else
                             
                           @if(!$products->isEmpty())
                  @foreach($products as $product)
                  @php
                  $proDB = DB::table('product_attribute')->leftjoin('products','products.id','product_attribute.product_id')->select('product_attribute.*')->where('product_id',$product->product_id)->get();
                  $proNameDB = DB::table('product_attribute')->leftjoin('products','products.id','product_attribute.product_id')->select('product_attribute.*')->where('product_id',$product->product_id)->first();
                  @endphp

                              <div class="col-md-4 col-6">
                                 <li>
                                    <figure>
                                       <a class="product-img" href="{{route('productDetail.product',$product->product_id)}}"><img src="{{ $product->cover }}" class="img-fluid" alt="{{$product->name}}"></a>
                                       <!-- <a class="add-card-btn" href="{{route('productDetail.product',$product->product_id)}}">Add to Cart <i class="fa fa-plus-circle" aria-hidden="true"></i></a> -->
                                       <figcaption>
                                          <h4 class="product-title">
                                             <a href="#">{{ $product->name }}</a>
                                             <span class="product-price font-weight-bold" id="saleprice{{$product->id}}"> &#8377; {{$product->sale_price}}</span>
                                             <input type="hidden" name="getproductid" id="getproductid" value="{{$product->id}}"/>
                                             <input type="hidden" id="pro_price{{$product->id}}" name="pro_price" value="{{$product->sale_price}}">
                                             <input type="hidden" id="attributeid{{$product->id}}" name="attributeid" value="{{$product->id}}">
                                             <input type="hidden" id="user_id" name="user_id" value="@if(!auth()->user()) 0 @else{{auth()->user()->id}}@endif">
                                             <input type="hidden" id="customer_id" name="customer_id" value="@if(!auth()->user()) 0 @else{{auth()->user()->id}}@endif">
                                          </h4>
                                          
                                          <div class="prod-form form-group w-100">
                                             
                                          <select class="form-control newproductid" id="product_id{{$product->id}}" name="product_id{{$product->id}}">
                                    @foreach($proDB as $pDB)
                                    <option class="pid" value="{{$pDB->id}},{{$product->id}}">{{$pDB->value}} {{$pDB->key}}</option>
                                    @endforeach
                                 </select>
                                          </div>
                                          <div class="add-to-cart-div mt-3">
                                             <div class="qtn-class">
                                                <span>Qty </span>
                                                <input type="number" name="quantity" id="quantity{{$product->id}}" value="1" min="1">
                                             </div>
                                             <div class="add-button">
                                              
                                                <button class="postaddtocart" id="{{$product->id}}"><i class="fa fa-plus mr-2" aria-hidden="true"></i>Add</button>
                                             </div>
                                          </div>
                                       </figcaption>
                                       <!-- <span class="product-price text-grey">M.R.P. <del> &#8377; {{$product->price}}</del></span> -->
                                       <!-- <span class="product-price font-weight-bold">Sale Price &#8377; {{$product->sale_price}}</span> -->
                                       <!-- <div class="pro-quantity pro-main justify-content-end mt-2">
                              <a href="#" class="qty qty-minus " onclick="decreaseValue()"><i class="fa fa-minus" aria-hidden="true"></i></a>
                              <input type="numeric" id="number" value="1" readonly />
                              <a href="#" class="qty qty-plus" onclick="increaseValue()"><i class="fa fa-plus" aria-hidden="true"></i></a>
                           </div> -->
                                    </figure>

                                    <!-- product badge -->
                                    <span class="badge sale" href="#"><img src="{{url('css/front/images/sale.png')}}" alt=""></span>
                                 </li>
                              </div>
                              @endforeach
                              @else
                              <div class="col-12">
                                 <div class="empty-message">
                                    <img src="{{url('css/front/images/no-product.gif')}}" alt="empty product">
                                    <p>Product not available.</p>
                                 </div>
                              </div>
                              @endif

                              @endif
                           </div>
                        </div>

                  </section>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- news latter section start -->

   <!-- news latter section end -->

   @include('layouts.front.footer')
   </div>




   <div class="modal fade login-signup" id="LoginSignup" role="dialog">
      <div class="modal-dialog modal-lg">
         <!-- Modal content-->
         <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body">
               <section id="formHolder">
                  <div class="row">
                     <!-- Brand Box -->
                     <div class="col-sm-6 brand">
                        <div class="heading">
                           <img src="{{url('css/front/images/main-logo.svg')}}" alt="">
                        </div>

                        <div class="success-msg">
                           <p>Great! You are one of our members now</p>
                           <a href="#" class="profile">Your Profile</a>
                        </div>
                     </div>


                     <!-- Form Box -->
                     <div class="col-sm-6 form">

                        <!-- Login Form -->
                        <div class="login form-peice switched">
                           <form class="login-form" action="#" method="post">
                              <div class="form-group">
                                 <label for="loginemail">Email Adderss</label>
                                 <input type="email" name="loginemail" id="loginemail" required>
                              </div>

                              <div class="form-group">
                                 <label for="loginPassword">Password</label>
                                 <input type="password" name="loginPassword" id="loginPassword" required>
                              </div>

                              <div class="CTA">
                                 <input type="submit" value="Login">
                                 <a href="#" class="switch">I'm New</a>
                              </div>
                           </form>
                        </div><!-- End Login Form -->


                        <!-- Signup Form -->
                        <div class="signup form-peice">
                           <form class="signup-form" action="#" method="post">

                              <div class="form-group">
                                 <label for="name">First Name</label>
                                 <input type="text" name="FirstName" id="FirstName" class="name">
                                 <span class="error"></span>
                              </div>

                              <div class="form-group">
                                 <label for="name">Last Name*</label>
                                 <input type="text" name="LastName" id="LastName*" class="name">
                                 <span class="error"></span>
                              </div>

                              <div class="form-group">
                                 <label for="email">Email Adderss</label>
                                 <input type="email" name="emailAdress" id="email" class="email">
                                 <span class="error"></span>
                              </div>

                              <div class="form-group">
                                 <label for="phone">Phone Number </label>
                                 <input type="text" name="phone" id="phone">
                              </div>

                              <div class="form-group">
                                 <label for="password">Password</label>
                                 <input type="password" name="password" id="password" class="pass">
                                 <span class="error"></span>
                              </div>

                              <div class="form-group">
                                 <label for="passwordCon">Confirm Password</label>
                                 <input type="password" name="passwordCon" id="passwordCon" class="passConfirm">
                                 <span class="error"></span>
                              </div>

                              <div class="CTA">
                                 <input type="submit" value="Signup Now" id="submit">
                                 <a href="#" class="switch">I have an account</a>
                              </div>
                           </form>
                        </div><!-- End Signup Form -->
                     </div>
                  </div>

               </section>
            </div>
         </div>
      </div>
   </div>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
   <script src="{{ url('css/front/js/jquery-1.12.4.min.js') }}"></script>
   <script src="{{ url('css/front/js/bootstrap.min.js') }}"></script>
   <script src="{{ url('css/front/js/webslidemenu.js') }}"></script>
   <script src="{{ url('css/front/js/wow.js') }}"></script>
   <script src="{{ url('css/front/js/owl.carousel.min.js') }}"></script>
   <script src="{{ url('css/front/js/jquery.themepunch.plugins.min.js') }}"></script>
   <script src="{{ url('css/front/js/jquery.themepunch.revolution.min.js') }}"></script>
   <script src="{{ url('css/front/js/function.js') }}"></script>
   <script type="text/javascript">
     // $("select#product_id").change(function() {
      $(".newproductid").change(function() {
         var id=$(this).attr('id');
       
       var newid = $('#'+id).children("option:selected").val();
       console.log(newid);
        var productid=$('#getproductid').val();
         var attrbuteid=id.split(",")[1];
         $('#attributeid'+id.split(",")[0]).val(attrbuteid);
         
         $.ajax({
            url: '{{url('SelectUnitPrice')}}/' + id.split(",")[0],
            method: 'get',
            success: function(data) {
               $('#priceUnit' + id.split(",")[1]).empty();
               $('#saleprice'  +id.split(",")[1]).html('&#8377;' + data[0].sale_price);
               $('#priceUnit' + id.split(",")[1]).html('&#8377;' + data[0].sale_price);
            }
         });
      });
   </script>
      <script type="text/javascript">
         var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
         $(".postaddtocart").click(function(){
            var product_id = $(this).attr('id');
           
             var user_id = $("#user_id").val();
             var customer_id = $("#customer_id").val();
            
             var quantity = $("#quantity"+product_id).val();

            
             var total_price = $("#pro_price"+product_id).val();
             var attribute_id = $("#attributeid"+product_id).val();
             var varntproduct_id = $('select#product_id').val();
            // var quantity=$('#number').val();  
           
             getCartData(user_id,customer_id,product_id,quantity,total_price,attribute_id);
             function getCartData(user_id,customer_id,product_id,quantity,total_price,attribute_id){
                 console.log({customer_id,product_id,quantity,total_price,attribute_id});
     
                 
                 $.ajax({
                   url: "{{url('api/v1/addCart')}}",
                   data: {
                     // _token: CSRF_TOKEN,
                     user_id: user_id,
                     customer_id: customer_id,
                     product_id: product_id,
                     quantity: quantity,
                   // total_price: total_price,
                     total_price: total_price.replace(",",""),
                     attribute_id: attribute_id,
                   },
                   type: 'POST',
                   // dataType: 'json',
                   success: function(result) {
     
                      
                     console.log("addtocart",result);
                   },
                   error: function(error){
                     console.log("error");
                   }
                 });
             }
         });
     </script>
</body>

</html>