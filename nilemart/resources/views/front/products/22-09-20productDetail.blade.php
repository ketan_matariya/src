<!doctype html>
<html lang="en">

<head>
    <title>NileMart | Product Detail</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @include('layouts.front.head')
</head>

<body>
    <div class="main-div">
        @include('layouts.front.header-cart')
        <!-- <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-breadcrumb">
                            <h2>Product Detail</h2>
                            <ul class="breadcrumb">
                                <li><a href="{{route('home')}}"><span class="fa fa-home"></span> Home</a></li>
                                <li class="active">Product Detail</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->

        <section class="pro-image-info border-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="top-categories">
                            <h3>Top Categories</h3>
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                @foreach($categories as $category)
                                <a class="nav-link @if($category->name == 'Fresh Fruitsa') active @endif" id="v-pills-home-tab" href="{{route('productCategoryListingCat.product',$category->id)}}" role="tab">{{$category->name}}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <?php /*?>
                                             <div class="col-md-2 col-4 pr-0">
                                                <div class="small-product-image">
                                                    <div>
                                                        <div class="item">
                                                            <a><img width="80" src="{{$products->cover}}"></a>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="item">
                                                            <a><img width="80" src="{{$products->cover}}"></a>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="item">
                                                            <a><img width="80" src="{{$products->cover}}"></a>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="item">
                                                            <a><img width="80" src="{{$products->cover}}"></a>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="item">
                                                            <a><img width="80" src="{{$products->cover}}"></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           
                                            <div class="col-md-12 col-12">
                                                <div class="main-image">
                                                    <div class="single-slide">
                                                        <img src="{{$products->cover}}">
                                                    </div>
                                                    <div class="single-slide">
                                                        <img src="{{$products->cover}}">
                                                    </div>
                                                    <div class="single-slide">
                                                        <img src="{{$products->cover}}">
                                                    </div>
                                                    <div class="single-slide">
                                                        <img src="{{$products->cover}}">
                                                    </div>
                                                    <div class="single-slide">
                                                        <img src="{{$products->cover}}">
                                                    </div>
                                                </div>
                                                 <?php */?>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" id="user_id" name="user_id" value="@if(!auth()->user()) 0 @else{{auth()->user()->id}}@endif">
                                    <input type="hidden" id="dropdwn" name="dropdwn" value="0">
                    <input type="hidden" id="customer_id" name="customer_id" value="@if(!auth()->user()) 0 @else{{auth()->user()->id}}@endif">
                  <?php /*?>  <input type="hidden" id="product_id" name="product_id" value="{{$products->product_id}}">
                    <input type="hidden" id="pro_price" name="pro_price" value="{{$products->sale_price}}">
                    <input type="hidden" id="attribute_id" name="attribute_id" value="{{$products->id}}">
                   <input type="hidden" id="mrpprice" name="mrpprice" value="{{$products->price}}">
                   <input type="hidden" id="saleprice" name="saleprice" value="{{$products->sale_price}}">
                   <input type="hidden" id="attributeid" name="attributeid" value="{{$products->id}}"><?php */?> 

                    <div class="col-lg-6">
                                        <div class="pro-size-info">
                                            <?php /*?>   <h1 class="pro-name">{{$products->name}}</h1><?php */?> 

                                            <span class="web-address">Fruit</span>
                                        </div>
                                        @php
                                        $mrp = $products->price;
                                        $price = $products->sale_price;
                                        $save = $mrp - $price;
                                        @endphp
                                        <hr>
                                        <div class="pricepart">
                                            <input type="hidden" name="" id="mrpP" value="">
                                            <input type="hidden" name="" id="pP" value="">
                                            <p>MRP: <del id="promrp" class="promrp mrpUnit{{$products->product_id}}">{{$products->price}}</del><strong id="proprice" class="proprice price priceUnit{{$products->product_id}}"> {{$products->sale_price}}</strong></p>
                                            <p>Discount: <strong> {{$products->discount}}%</strong> </p>
                                            <p>You Save : <strong class="text-green">&#8377; {{number_format($save,2)}}</strong> &nbsp;&nbsp;&nbsp; Inclusive of all taxes</p>
                                            <p>Unit: <strong id="valueUnit{{$products->product_id}}">{{$products->value}}</strong><strong style="text-transform: uppercase;" id="keyUnit{{$products->product_id}}">{{$products->key}}</strong></p>
                                            <hr>
                                            <p>Availiblity: <span class="text-green font-weight-bold">In Stock</span></p>
                                            <hr>
                                            <p>Inaugural Offer <strong>Free Shipping</strong></p>
                                        </div>

                                        <div class="d-flex align-items-end">
                                            <div class="prod-form form-group w-25">
                                                <p class="m-0">Qty</p>
                                                <select class="form-control" id="varntproduct_id" style="text-transform: uppercase;">
                                                    @foreach($product as $pro)
                                                    <option value="{{$pro->id}},{{$pro->product_id}}">{{$pro->value}} {{$pro->key}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="mx-2" id="pm">
                                                <a class="btn py-0">
                                                    <div style="display: flex;"><button class="btnpm" onclick="decreaseValue()">
                                                            <i class="fa fa-minus" aria-hidden="true"></i></button>
                                                        <input type="text" class="number-input" id="number" name="number" value="1">
                                                        <button class="btnpm" onclick="increaseValue()">
                                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                                        </button>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="cssbtn-part" style="display: flex;">
                                                <a id="atc" class="cart postaddtocart"><span> Add <i class="fa fa-plus add ml-2"></i></span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <section class="pro-detil-additional-info">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="product-des">
                                                <h3>Description</h3>
                                                <div class="pro-title-info">
                                                    <h1 class="pro-name">{{$products->name}}</h1>
                                                    <span class="web-address">{{$products->description}}</span>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="product-des">
                                                <h3>Additional Information</h3>
                                                <div class="pro-title-info">
                                                    <h1 class="pro-name">Weight:</h1>
                                                    <span class="web-address">{{$products->weight}}</span>
                                                </div>
                                                <div class="pro-title-info">
                                                    <h1 class="pro-name">Unit value:</h1>
                                                    <span style="text-transform: uppercase;" class="web-address" id="keyUnitList{{$products->product_id}}">{{$products->key}}</span>
                                                </div>
                                                <div class="pro-title-info">
                                                    <h1 class="pro-name">Technical Name:</h1>
                                                    <span class="web-address">{{$products->technical_name}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </section>
                            </div>
                            <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">second tab</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <!-- news latter section start -->

        <!-- news latter section end -->
        @include('layouts.front.footer')
    </div>




    <div class="modal fade login-signup" id="LoginSignup" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal-body">
                    <section id="formHolder">
                        <div class="row">
                            <!-- Brand Box -->
                            <div class="col-sm-6 brand">
                                <div class="heading">
                                    <img src="{{url('css/front/images/main-logo.svg')}}" alt="">
                                </div>

                                <div class="success-msg">
                                    <p>Great! You are one of our members now</p>
                                    <a href="#" class="profile">Your Profile</a>
                                </div>
                            </div>


                            <!-- Form Box -->
                            <div class="col-sm-6 form">

                                <!-- Login Form -->
                                <div class="login form-peice switched">
                                    <form class="login-form" action="#" method="post">
                                        <div class="form-group">
                                            <label for="loginemail">Email Adderss</label>
                                            <input type="email" name="loginemail" id="loginemail" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="loginPassword">Password</label>
                                            <input type="password" name="loginPassword" id="loginPassword" required>
                                        </div>

                                        <div class="CTA">
                                            <input type="submit" value="Login">
                                            <a href="#" class="switch">I'm New</a>
                                        </div>
                                    </form>
                                </div><!-- End Login Form -->


                                <!-- Signup Form -->
                                <div class="signup form-peice">
                                    <form class="signup-form" action="#" method="post">

                                        <div class="form-group">
                                            <label for="name">First Name</label>
                                            <input type="text" name="FirstName" id="FirstName" class="name">
                                            <span class="error"></span>
                                        </div>

                                        <div class="form-group">
                                            <label for="name">Last Name*</label>
                                            <input type="text" name="LastName" id="LastName*" class="name">
                                            <span class="error"></span>
                                        </div>

                                        <div class="form-group">
                                            <label for="email">Email Adderss</label>
                                            <input type="email" name="emailAdress" id="email" class="email">
                                            <span class="error"></span>
                                        </div>

                                        <div class="form-group">
                                            <label for="phone">Phone Number </label>
                                            <input type="text" name="phone" id="phone">
                                        </div>

                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input type="password" name="password" id="password" class="pass">
                                            <span class="error"></span>
                                        </div>

                                        <div class="form-group">
                                            <label for="passwordCon">Confirm Password</label>
                                            <input type="password" name="passwordCon" id="passwordCon" class="passConfirm">
                                            <span class="error"></span>
                                        </div>

                                        <div class="CTA">
                                            <input type="submit" value="Signup Now" id="submit">
                                            <a href="#" class="switch">I have an account</a>
                                        </div>
                                    </form>
                                </div><!-- End Signup Form -->
                            </div>
                        </div>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
    <script s44rc="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <!-- <script src="{{ url('css/front/js/jquery-1.12.4.min.js') }}"></script> -->
    <script src="{{ url('css/front/js/bootstrap.min.js') }}"></script>
    <script src="{{ url('css/front/js/owl.carousel.min.js') }}"></script>
    <script src="{{ url('css/front/js/hammer.min.js') }}"></script>
    <script src="{{ url('css/front/js/xzoom.min.js') }}"></script>
    <script src="{{ url('css/front/js/foundation.min.js') }}"></script>
    <script src="{{ url('css/front/js/zoom.js') }}"></script>
    <script src="{{ url('css/front/js/webslidemenu.js') }}"></script>
    <script src="{{ url('css/front/js/wow.js') }}"></script>
    <script src="{{ url('css/front/js/function.js') }}"></script>

    <!-- Radio btn -->
    <!-- <style type="text/css">
        .btnpm {
            border: none;
            background: #0b3b69;
            color: white;
            font-size: 12px;
            height: 30px;
            width: 30px;
            justify-content: center;
            align-items: center;
            display: flex;
            outline: none !important;
            border-radius: 76px;
        }
    </style> -->
    <script>
        $(document).ready(function() {
            $('.main-image').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.small-product-image'
            });
            $('.small-product-image').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                asNavFor: '.main-image',
                focusOnSelect: true,
                vertical: true,
                arrows: true,
                prevArrow: '<button><i class="fa fa-arrow-up" aria-hidden="true"></i></button>',
                nextArrow: '<button><i class="fa fa-arrow-down" aria-hidden="true"></i></button>',
            });

            $('#radioBtn a').on('click', function() {
                var sel = $(this).data('title');
                var tog = $(this).data('toggle');
                $('#' + tog).prop('value', sel);

                $('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').removeClass('active').addClass('notActive');
                $('a[data-toggle="' + tog + '"][data-title="' + sel + '"]').removeClass('notActive').addClass('active');
            });
        });
    </script>





    <script type="text/javascript">
        function increaseValue() {
            var value = parseInt(document.getElementById('number').value, 10);
            var mrp = $('#mrpP').val();
            var mrpprice = $('#pP').val();
            var dropdwn = $('#dropdwn').val();
            console.log({mrp,mrpprice});

            value = isNaN(value) ? 0 : value;
            value++;
            document.getElementById('number').value = value;

            if(dropdwn=='0'){
                var pmrp = $('#mrpprice').val() * value;
                var pp = $('#saleprice').val() * value;
            }
            else{
                var pmrp = mrp * value;
                var pp = mrpprice * value;
            }
           
        
            console.log(pmrp);
            console.log(pp);
            $('#promrp').html(pmrp.toFixed(2));
            $('#proprice').html(pp.toFixed(2));
           // $('#saleprice').val(pp.toFixed(2));
        }

        function decreaseValue() {
            var value = parseInt(document.getElementById('number').value, 10);
            var mrp = $('#mrpP').val();
            var mrpprice = $('#pP').val();
            var dropdwn = $('#dropdwn').val();
            console.log({mrp,mrpprice});
            value = isNaN(value) ? 0 : value;
            value < 1 ? value = 1 : '';
            value--;
            document.getElementById('number').value = value;
            //var pmrp = mrp * value;
            //var pmrp = $('#mrpprice').val() * value;
           // var pp = mrpprice * value;
          //var pp = $('#saleprice').val() * value;
          if(dropdwn=='0'){
                var pmrp = $('#mrpprice').val() * value;
                var pp = $('#saleprice').val() * value;
            }
            else{
                var pmrp = mrp * value;
                var pp = mrpprice * value;
            }
            console.log({pmrp,pp});
            $('#promrp').html(pmrp.toFixed(2));
            $('#proprice').html(pp.toFixed(2));
        }
    </script>

    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script type="text/javascript">
        $(".sec-content").hide();
        //$('#varntproduct_id').val().length
        //alert($('#varntproduct_id > option').length);
        $(".add").click(function() {
            $("#pm").show();
            $("#atc").hide();
        });
    </script>

    <script type="text/javascript">
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $(".postaddtocart").click(function(){
        var user_id = $("#user_id").val();
        var customer_id = $("#customer_id").val();
        var product_id = $('#product_id').val();
        var quantity = $("#number").val();
        var total_price = $("#pro_price").val();
        var attribute_id = $("#attribute_id").val();
        var varntproduct_id = $('#varntproduct_id').val();
        var quantity=$('#number').val();  
        console.log(product_id);
        getCartData(user_id,customer_id,product_id,quantity,total_price,attribute_id);
        function getCartData(user_id,customer_id,product_id,quantity,total_price,attribute_id){
            console.log({customer_id,product_id,quantity,total_price,attribute_id});

            
            $.ajax({
              url: "{{url('api/v1/addCart')}}",
              data: {
                // _token: CSRF_TOKEN,
                user_id: user_id,
                customer_id: customer_id,
                product_id: product_id,
                quantity: quantity,
              // total_price: total_price,
                total_price: $('#saleprice').val(),
                attribute_id: $('#attributeid').val(),
              },
              type: 'POST',
              // dataType: 'json',
              success: function(result) {
                console.log("addtocart",result);
              },
              error: function(error){
                console.log("error");
              }
            });
        } 
    });
</script>
<script type="text/javascript">
  $("select#varntproduct_id").change(function(){
      var id = $(this).children("option:selected").val();  
      var quantity=$('#number').val();  

      $('#dropdwn').val('1');
      //alert(id);
      $.ajax({
          //url: 'http://localhost:8000/SelectUnitPrice/'+id.split(",")[0],
          url:"{{url('SelectUnitPrice')}}/"+id.split(",")[0],
          method: 'get',
          success: function(data) {
            // console.log(data[0].key);
            $('.priceUnit'+id.split(",")[1]).empty();
            $('.priceUnit'+id.split(",")[1]).html(data[0].sale_price);
            //$('#mrpprice').val(data[0].sale_price);
            $('#saleprice').val(data[0].sale_price);
            $('#attributeid').val(id.split(',')[0]);
            
            $('.mrpUnit'+id.split(",")[1]).html(data[0].price);
            $('#valueUnit'+id.split(",")[1]).html(data[0].value);
            $('#valueUnitList'+id.split(",")[1]).html(data[0].value);

            $('#keyUnit'+id.split(",")[1]).html(data[0].key);
            $('#keyUnitList'+id.split(",")[1]).html(data[0].key);
            $('#mrpP').val(data[0].price);
            $('#pP').val(data[0].sale_price);
         }
      });
  });
</script>


</body>

</html>