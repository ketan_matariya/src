<!doctype html>
<html lang="en">

<head>
   <title>NileMart | Sub-Category</title>
   @include('layouts.front.head')
</head>
   <style type="text/css">
/* Style the sidenav links and the dropdown button */
.dropdown a, .dropdown-btn {
  padding: 6px 8px 6px 16px;
  text-decoration: none;
  font-size: 16px;
  color: #fff;
  display: block;
  border: none;
  background: none;
  width: 100%;
  text-align: left;
  cursor: pointer;
  outline: none;
   border-radius: 0;

      border-radius: 0;
    line-height: 30px;
    position: relative;
   
}

/* On mouse-over */
.dropdown a:hover, .dropdown-btn:hover {
  color: #000;
}


/* Dropdown container (hidden by default). Optional: add a lighter background color and some left padding to change the design of the dropdown content */
.dropdown-container {
  display: none;
  background-color: #818181e0;
  padding-left: 8px;
}

.dropdown-container1 {
  display: none;
  background-color: #818181e0;
  padding-left: 8px;
}
/* Optional: Style the caret down icon */
.fa-caret-down {
  float: right;
  padding-right: 8px;
}

   </style>
<body>
   <div class="main-div">
      @include('layouts.front.header-cart')

      <!-- <section class="slider-part inner-slider">
          <img src="{{url('css/front/images/product-category-inner.jpg')}}" alt="">
      </section> -->

      <!-- <section>
         <div class="container">
            <div class="row">
               <div class="col-sm-12">
                  <div class="page-breadcrumb">
                     <h2>Sub Categories</h2>
                     <ul class="breadcrumb">
                        <li><a href="index.html"><span class="fa fa-home"></span> Home</a></li>
                        <li class="active"><a href="{{route('productCategory.product')}}">Categories</a> </li>
                        <li class="active">Sub Categories</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section> -->
      <div class="container">
         <div class="row">
            <div class="col-md-3">
               <div class="top-categories">
                  <h3>Top Categories</h3>
                  <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        @foreach($categories as $category)
                        @if(!$categories->isEmpty())
                           <div class="dropdown">
                              <a class="dropdown-btn nav-link @if($getproducts->category_id == $category->id) active @endif" id="{{$category->id}}" role="tab">{{$category->name}}
                                 @if(!$category->children->isEmpty())
                                 <i class="fa fa-caret-down"></i>
                                 <div class="dropdown-container">
                                     @foreach($category->children as $subcats)
                                       <a href="#" id="{{$subcats->id}}" class="dropdown-btn1" role="tab">{{$subcats->name}}
                                          @if(!$subcats->children->isEmpty())
                                             <i class="fa fa-caret-down"></i>
                                             <div class="dropdown-container1">
                                                @foreach($subcats->children as $sub)
                                                   <a href="#" id="{{$sub->id}}" class="subcategoryid">{{$sub->name}}
                                                @endforeach
                                             </div>
                                          @endif
                                       </a>
                                    @endforeach
                                 </div>
                           @endif
                              </a>
                           </div>
                            @else
                        <a class="nav-link @if($cat == $category->id) active @endif" id="v-pills-home-tab" href="{{route('productCategoryListingCat.product',$category->id)}}" role="tab">{{$category->name}} </a>
                        @endif
                        @endforeach
                       
                  </div>
               </div>
            </div>

            <div class="col-md-9">
               <div class="tab-pane active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                  <section class="fine-jewellery">
                     <div class="container">
                        <div class="single-product mb-3">
                           <div class="row">
                              @if(!$subCategories->isEmpty())
                              @foreach($subCategories as $subcat)
                              <div class="col-md-4 col-sm-6">
                                 <li>
                                    <figure>
                                       <a class="product-img" href="{{url('productCategory',$subcat->id)}}"><img src="{{$subcat->cover}}" class="img-fluid" alt="polo shirt img"></a>
                                       <a class="add-card-btn" href="{{url('productCategory',$subcat->id)}}">View More</a>
                                       <figcaption>
                                          <h4 class="product-title"><a href="#">{{$subcat->name}}</a></h4>
                                       </figcaption>
                                    </figure>
                                 </li>
                              </div>
                              @endforeach
                              @else
                              <div class="col-12">
                                 <div class="empty-message">
                                    <img src="{{url('css/front/images/no-product.gif')}}" alt="empty product">
                                    <p>Subcategory not available.</p>
                                 </div>
                              </div>
                              @endif
                           </div>
                        </div>
                     </div>
                  </section>
               </div>
            </div>
         </div>
      </div>
   </div>



   <!-- footer start -->
   @include('layouts.front.footer')
   </div>


   <div class="modal fade login-signup" id="LoginSignup" role="dialog">
      <div class="modal-dialog modal-lg">
         <!-- Modal content-->
         <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body">
               <section id="formHolder">
                  <div class="row">
                     <!-- Brand Box -->
                     <div class="col-sm-6 brand">
                        <div class="heading">
                           <img src="{{url('css/front/images/main-logo.svg')}}" alt="">
                        </div>

                        <div class="success-msg">
                           <p>Great! You are one of our members now</p>
                           <a href="#" class="profile">Your Profile</a>
                        </div>
                     </div>


                     <!-- Form Box -->
                     <div class="col-sm-6 form">

                        <!-- Login Form -->
                        <div class="login form-peice switched">
                           <form class="login-form" action="#" method="post">
                              <div class="form-group">
                                 <label for="loginemail">Email Adderss</label>
                                 <input type="email" name="loginemail" id="loginemail" required>
                              </div>

                              <div class="form-group">
                                 <label for="loginPassword">Password</label>
                                 <input type="password" name="loginPassword" id="loginPassword" required>
                              </div>

                              <div class="CTA">
                                 <input type="submit" value="Login">
                                 <a href="#" class="switch">I'm New</a>
                              </div>
                           </form>
                        </div><!-- End Login Form -->


                        <!-- Signup Form -->
                        <div class="signup form-peice">
                           <form class="signup-form" action="#" method="post">

                              <div class="form-group">
                                 <label for="name">Full Name</label>
                                 <input type="text" name="username" id="name" class="name">
                                 <span class="error"></span>
                              </div>

                              <div class="form-group">
                                 <label for="email">Email Adderss</label>
                                 <input type="email" name="emailAdress" id="email" class="email">
                                 <span class="error"></span>
                              </div>

                              <div class="form-group">
                                 <label for="phone">Phone Number - <small>Optional</small></label>
                                 <input type="text" name="phone" id="phone">
                              </div>

                              <div class="form-group">
                                 <label for="password">Password</label>
                                 <input type="password" name="password" id="password" class="pass">
                                 <span class="error"></span>
                              </div>

                              <div class="form-group">
                                 <label for="passwordCon">Confirm Password</label>
                                 <input type="password" name="passwordCon" id="passwordCon" class="passConfirm">
                                 <span class="error"></span>
                              </div>

                              <div class="CTA">
                                 <input type="submit" value="Signup Now" id="submit">
                                 <a href="#" class="switch">I have an account</a>
                              </div>
                           </form>
                        </div><!-- End Signup Form -->
                     </div>
                  </div>

               </section>
            </div>
         </div>
      </div>
   </div>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

   <script src="{{ url('css/front/js/jquery-1.12.4.min.js') }}"></script>

   <script src="{{ url('css/front/js/bootstrap.min.js') }}"></script>
   <script src="{{ url('css/front/js/webslidemenu.js') }}"></script>
   <script src="{{ url('css/front/js/wow.js') }}"></script>

   <script src="{{ url('css/front/js/owl.carousel.min.js') }}"></script>
   <script src="{{ url('css/front/js/jquery.themepunch.plugins.min.js') }}"></script>
   <script src="{{ url('css/front/js/jquery.themepunch.revolution.min.js') }}"></script>
   <script src="{{ url('css/front/js/function.js') }}"></script>
      <script>
/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
  this.classList.toggle("active");
  var dropdownContent = this.nextElementSibling;
  if (dropdownContent.style.display === "block") {
  dropdownContent.style.display = "none";
  } else {
  dropdownContent.style.display = "block";
  }
  });
}
</script>

<script>
/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
var dropdown = document.getElementsByClassName("dropdown-btn1");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
  this.classList.toggle("active");
  var dropdownContent = this.nextElementSibling;
  if (dropdownContent.style.display === "block") {
  dropdownContent.style.display = "none";
  } else {
  dropdownContent.style.display = "block";
  }
  });
}
</script>

</body>

</html>