<!doctype html>
<html lang="en">
   <head>
      <title>NileMart | Product List</title>
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      @include('layouts.front.head')
   </head>
   <style type="text/css">
/* Style the sidenav links and the dropdown button */
.dropdown a, .dropdown-btn {
  padding: 6px 8px 6px 16px;
  text-decoration: none;
  font-size: 16px;
  color: #fff;
  display: block;
  border: none;
  background: none;
  width: 100%;
  text-align: left;
  cursor: pointer;
  outline: none;
   border-radius: 0;

      border-radius: 0;
    line-height: 30px;
    position: relative;
   
}

/* On mouse-over */
.dropdown a:hover, .dropdown-btn:hover {
  color: #000;
}


/* Dropdown container (hidden by default). Optional: add a lighter background color and some left padding to change the design of the dropdown content */
.dropdown-container {
  display: none;
  background-color: #818181e0;
  padding-left: 8px;
}

.dropdown-container1 {
  display: none;
  background-color: #818181e0;
  padding-left: 8px;
}
/* Optional: Style the caret down icon */
.fa-caret-down {
  float: right;
  padding-right: 8px;
}

   </style>
   <body>
      <div class="main-div">
         @include('layouts.front.header-cart')
         <div class="container">
            <div class="row">
                 @include('layouts.front.sidebar-cart')

               <div class="col-md-9">
                  <div class="tab-pane active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                     <section class="product-listing">
                        <div class="container">
                           <h3 class="text-orange product-head">{{($subcategory!=null) ? 'Sub Category':'Products'}}</h3>
                           <hr class="product-head">
                           <div class="single-product">
                              <div class="row">
                                 @if($subcategory!=null)
                                @foreach($categori->children as $subcategry)
                                 <div class="col-md-4 col-6">
                                    <li>
                                       @if($subcategry->children->isEmpty())

                                          <figure>
                                             <a class="product-img" href="{{ route('productCategoryListingCat.product',$subcategry->id) }}"><img src="{{$subcategry->cover}}" class="img-fluid" alt="{{$subcategry->name}}"></a>
                                             <!-- <a class="add-card-btn" href="http://127.0.0.1:8000/productDetail/38">Add to Cart <i class="fa fa-plus-circle" aria-hidden="true"></i></a> -->
                                             <figcaption>
                                                <h4 class="product-title">
                                                   <a href="#">{{$subcategry->name}}</a>
                                                </h4>
                                                <div class="prod-form form-group w-100">
                                                </div>
                                                <a class="add-card-btn justify-content-center" href="{{ route('productCategoryListingCat.product',$subcategry->id) }} ">View More</a>
                                             </figcaption>
                                          </figure>

                                       @else
                                          <figure>
                                             <a class="product-img" href="{{ url('productSubSubCategory',$subcategry->id) }}"><img src="{{$subcategry->cover}}" class="img-fluid" alt="{{$subcategry->name}}"></a>
                                             <!-- <a class="add-card-btn" href="http://127.0.0.1:8000/productDetail/38">Add to Cart <i class="fa fa-plus-circle" aria-hidden="true"></i></a> -->
                                             <figcaption>
                                                <h4 class="product-title">
                                                   <a href="#">{{$subcategry->name}}</a>
                                                </h4>
                                                <div class="prod-form form-group w-100">
                                                </div>
                                                <a class="add-card-btn justify-content-center" href="{{ url('productSubSubCategory',$subcategry->id) }} ">View More</a>
                                             </figcaption>
                                          </figure>
                                       @endif

                                       <!-- product badge -->
                                    </li>
                                 </div>
                                 @endforeach
                                 @else

                                 @if(!empty($products))
      @foreach($products as $product)
      @php
      $proDB = DB::table('product_attribute')->leftjoin('products','products.id','product_attribute.product_id')->select('product_attribute.*')->orderBy('product_attribute.id', 'ASC')->where('product_id',$product->id)->get();
      $proNameDB = DB::table('product_attribute')->leftjoin('products','products.id','product_attribute.product_id')->select('product_attribute.*')->where('product_id',$product->id)->first();
      @endphp
      <div class="col-md-4 col-6 ">
      <li>
      <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
      <figure>
      <a class="product-img" href="{{route('productDetail.product',$product->id)}}">
      <img src="{{ $product->cover }}" class="img-fluid" alt="{{$product->name}}"></a>
      <figcaption>
      <h4 class="product-title">
      <a href="#">{{ $product->name }} </a>
      <span id="priceUnit{{$product->id}}" class="product-price font-weight-bold">&#8377;{{$product->sale_price}}</span>
      <input type="hidden" name="getproductid" id="getproductid" value="{{$product->id}}"/>
      <input type="hidden" id="pro_price{{$product->id}}" name="pro_price" value="{{$product->sale_price}}">
      <input type="hidden" id="attributeid{{$product->id}}" name="attributeid" value="{{$product->attribute_id}}">
      <input type="hidden" id="user_id" name="user_id" value="@if(!auth()->user()) 0 @else{{auth()->user()->id}}@endif">
      <input type="hidden" id="customer_id" name="customer_id" value="@if(!auth()->user()) 0 @else{{auth()->user()->id}}@endif">
      <input type="hidden" id="pro_sellprice{{$product->id}}" name="pro_sellprice" value="{{$product->sale_price}}">
      <input type="hidden" id="max_quantities{{$product->id}}" name="max_quantities" value="{{$product->max_quantities}}">
      <input type="hidden" id="hidavailable_quantity{{$product->id}}" name="available_quantity" value="{{$product->available_quantity}}"/>
      <input type="hidden" id="dropdwn" name="dropdwn" value="0">
      <input type="hidden" id="productquantity{{$product->id}}" name="" value="{{$product->quantity}}">
      
      </h4>
      <div class="prod-form form-group w-100">
      <select class="form-control newvariantprdct" id="product_id{{$product->id}}" name="product_id{{$product->id}}">
      @foreach($proDB as $pDB)
      <option class="pid" value="{{$pDB->id}},{{$product->id}},{{$pDB->max_quantities}},{{$pDB->available_quantity}}" >{{$pDB->value}} {{$pDB->key}}</option>
      @endforeach
      </select>
      </div>
      <div class="add-to-cart-div mt-3">
      <div class="qtn-class">
      @if($login=='true')
      <div id="Increas{{$product->id}}" class="Increas{{$product->id}}" style="display: none">
      <button  id="{{$product->id}}" pluse_minus="hidminus_sine"  class="btnpm" >
      <i class="fa fa-minus" aria-hidden="true"></i>
      </button>
      <input type="text" class="number-input" id="quantity{{$product->id}}" name="quantity" value="1" readonly>
      <input type="hidden" class="number-input" id="productid" name="productid" value="{{$product->id}}" readonly>
      <button id="{{$product->id}}" class="btnpm" pluse_minus="hiplus_sine">
      <i class="fa fa-plus" aria-hidden="true"></i>
      </button>
      </div>
      @endif
      </div>
      @if($product->quantity==0)
      <script>
         $(document).ready(function(){
            (function(){
                $("#Increas{{$product->id}}").hide();
                $("#addButton{{$product->id}}").show();
                $(".outeof").show();
            })();
         });
      </script>
      @else
      <script>
         $(document).ready(function(){
            (function(){
                $("#Increas{{$product->id}}").show();
                $("#addButton{{$product->id}}").hide();
                var pro_sellprice = $("#pro_sellprice{{$product->id}}").val();
                document.getElementById('quantity{{$product->id}}').value = '{{$product->quantity}}';
                newsale =parseFloat(pro_sellprice)*parseInt('{{$product->quantity}}');
                $('#priceUnit{{$product->id}}').html('&#8377; ' +  newsale.toFixed(2));
                $(".outeof").show();
            })();
         });
      </script>
      @endif
      <div class="add-button">
      @if($login=='true')
      @if($product->available_quantity!=0)
      <button  style="display: none" class="postaddtocart" productid="{{$product->id}}" id="addButton{{$product->id}}" ><i class="fa fa-plus mr-2" aria-hidden="true"></i>Add</button>
      <span class="text-danger font-weight-bold" style="display: none"  id="outofstock{{$product->id}}">Out Of Stock </span>
      @else
      <span class="text-danger font-weight-bold outeof" style="display: none"  id="outofstock{{$product->id}}">Out Of Stock </span>
      @endif
      @if(isset($quantity))
      @foreach($quantity as $cart)
      <script>
         (function(){
            $("#Increas{{$cart->product_id}}").show();
            $("#addButton{{$cart->product_id}}").hide();
            $("#quantity{{$cart->product_id}}").val({{$cart->quantity}});
         })();
      </script>
      @endforeach
      @else
      <script>
         (function(){
             $("#Increas{{$product->id}}").show();
             $("#addButton{{$product->id}}").hide();
         })();
      </script>
      @endif
      @else
      <a id="atc" class="buttonCart " href="{{url('user/login')}}"><span> Add <i class="fa fa-plus add ml-2"></i></span> </a>
      @endif
      </div>
      </div>
      </figcaption>
      </figure>
      <!-- product badge -->
      <span class="badge sale" href="#">
      <img src="{{url('css/front/images/sale.png')}}" alt="">
      </span>
      </li>
      </div>
      @endforeach
      @else


                                 <div class="col-12">
                                    <div class="empty-message">
                                       <img src="{{url('css/front/images/no-product.gif')}}" alt="empty product">
                                       <p>Product not available.</p>
                                    </div>
                                 </div>
                                 @endif
                                 @endif
                              </div>
                           </div>
                     </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- news latter section start -->
         <!-- news latter section end -->
         @include('layouts.front.footer')
      </div>
      <div class="modal fade login-signup" id="LoginSignup" role="dialog">
         <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <div class="modal-body">
                  <section id="formHolder">
                     <div class="row">
                        <!-- Brand Box -->
                        <div class="col-sm-6 brand">
                           <div class="heading">
                              <img src="{{url('css/front/images/main-logo.svg')}}" alt="">
                           </div>
                           <div class="success-msg">
                              <p>Great! You are one of our members now</p>
                              <a href="#" class="profile">Your Profile</a>
                           </div>
                        </div>
                        <!-- Form Box -->
                        <div class="col-sm-6 form">
                           <!-- Login Form -->
                           <div class="login form-peice switched">
                              <form class="login-form" action="#" method="post">
                                 <div class="form-group">
                                    <label for="loginemail">Email Adderss</label>
                                    <input type="email" name="loginemail" id="loginemail" required>
                                 </div>
                                 <div class="form-group">
                                    <label for="loginPassword">Password</label>
                                    <input type="password" name="loginPassword" id="loginPassword" required>
                                 </div>
                                 <div class="CTA">
                                    <input type="submit" value="Login">
                                    <a href="#" class="switch">I'm New</a>
                                 </div>
                              </form>
                           </div>
                           <!-- End Login Form -->
                           <!-- Signup Form -->
                           <div class="signup form-peice">
                              <form class="signup-form" action="#" method="post">
                                 <div class="form-group">
                                    <label for="name">First Name</label>
                                    <input type="text" name="FirstName" id="FirstName" class="name">
                                    <span class="error"></span>
                                 </div>
                                 <div class="form-group">
                                    <label for="name">Last Name*</label>
                                    <input type="text" name="LastName" id="LastName*" class="name">
                                    <span class="error"></span>
                                 </div>
                                 <div class="form-group">
                                    <label for="email">Email Adderss</label>
                                    <input type="email" name="emailAdress" id="email" class="email">
                                    <span class="error"></span>
                                 </div>
                                 <div class="form-group">
                                    <label for="phone">Phone Number </label>
                                    <input type="text" name="phone" id="phone">
                                 </div>
                                 <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" id="password" class="pass">
                                    <span class="error"></span>
                                 </div>
                                 <div class="form-group">
                                    <label for="passwordCon">Confirm Password</label>
                                    <input type="password" name="passwordCon" id="passwordCon" class="passConfirm">
                                    <span class="error"></span>
                                 </div>
                                 <div class="CTA">
                                    <input type="submit" value="Signup Now" id="submit">
                                    <a href="#" class="switch">I have an account</a>
                                 </div>
                              </form>
                           </div>
                           <!-- End Signup Form -->
                        </div>
                     </div>
                  </section>
               </div>
            </div>
         </div>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
      <script src="{{ url('css/front/js/jquery-1.12.4.min.js') }}"></script>
      <script src="{{ url('css/front/js/bootstrap.min.js') }}"></script>
      <script src="{{ url('css/front/js/webslidemenu.js') }}"></script>
      <script src="{{ url('css/front/js/wow.js') }}"></script>
      <script src="{{ url('css/front/js/owl.carousel.min.js') }}"></script>
      <script src="{{ url('css/front/js/jquery.themepunch.plugins.min.js') }}"></script>
      <script src="{{ url('css/front/js/jquery.themepunch.revolution.min.js') }}"></script>
      <script src="{{ url('css/front/js/function.js') }}"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
      <script type="text/javascript">
         $(".btnpm").click(function(){
            var product_id = $(this).attr('id');
            var div_id = $(this).attr('pluse_minus');
            var sale_price = $("#pro_price"+product_id).val();
            var attributeid = $("#attributeid"+product_id).val();
            var pro_sellprice = $("#pro_sellprice"+product_id).val();
            var quantity = document.getElementById('quantity'+product_id).value;
            var sale_price = $("#pro_sellprice"+product_id).val();
            var total_price = quantity*sale_price;
            
            var cartid = $("#cartId"+product_id).val();
            @if(null !==Auth::user())
                   {
                       var userid="{{Auth::user()->id}}";
                       var customer_id = "{{Auth::user()->id}}";
                   }
             @endif
             var value = parseInt(document.getElementById('quantity'+product_id).value, 10);
             value = isNaN(value) ? 0 : value;
             if(div_id=='hidminus_sine')
             {

               if(value == 1)
               {
                  debugger;
               $.ajax({
               url: "{{url('api/v1/addCart')}}",
                data: {
                   user_id: userid,
                   customer_id: customer_id,
                   product_id: product_id,
                   quantity: 0,
                   total_price: total_price,
                   attribute_id: attributeid,
               },
               type: 'POST',
               dataType: 'json',
               success: function(result) {
                  debugger;
                   if(result!="")
                   {
                     showCart(userid,customer_id);
                    $("#addButton"+product_id).show();
                    $("#Increas"+product_id).hide();
                     
                   }
               },
               error: function(error){
                 console.log("error");
               }
             });

               }else{
                  value--;
                  plus_minus ="-";
               }
               
             }else
             {

               value++;
               plus_minus ="+";
             }
             var max_quantities=  $("#max_quantities"+product_id).val();
            var available_quantity = $("#hidavailable_quantity"+product_id).val();
            if(value<=available_quantity)
            {
            if(value<=max_quantities)
            {
              
                  document.getElementById('quantity'+product_id).value = value;
                  newsale = pro_sellprice*value;
                  $('#priceUnit' + product_id).html('&#8377; ' +  newsale.toFixed(2));
                  var max_quantities=  $("#max_quantities"+product_id).val();
                  Updatequantity(cartid,value,product_id,userid,userid,attributeid,total_price,plus_minus);
            }
            else
            {
                document.getElementById('quantity'+product_id).value = max_quantities;
                toastr.options = {
                          "preventDuplicates": true,
                           "preventOpenDuplicates": true
                     };
                toastr.error ("<span style='font-size:15px;font-weight:bold'>You cannot add more items. </span>");
              
            }
          }else
          {
                document.getElementById('quantity'+product_id).value = available_quantity;
                toastr.options = {
                          "preventDuplicates": true,
                           "preventOpenDuplicates": true
                     };
                toastr.error ("<span style='font-size:15px;font-weight:bold'>Product Not Available </span>");
               
          }

         });


      function Updatequantity(cartid,quantity,product_id,customer_id,user_id,attribute_id,total_price,plus_minus){
                  var max_quantities=  $("#max_quantities"+product_id).val();

               if(quantity<=max_quantities)
               {
                 $.ajax({
                     url: "{{url('api/v1/addCart')}}",
                     data: {
                       user_id: user_id,
                       customer_id: customer_id,
                       product_id: product_id,
                       quantity: quantity,
                       total_price: total_price,
                       attribute_id: attribute_id,
                       cart_id:cartid,
                     },
                     type: 'POST',
                     success: function(result) {
                       console.log("addtocart",result);
                     },
                     error: function(error){
                       console.log("error");
                     }
                   });
               }
               else
               {

                  $("#quantity"+product_id).val(max_quantities);
                  toastr.options = {
                          "preventDuplicates": true,
                           "preventOpenDuplicates": true
                     };
                 toastr.error ("<span style='font-size:15px;font-weight:bold'>You cannot add more items. </span>");
               }
               }

$(document).ready(function(){
      var CSRF_TOKEN = $('meta[name="csrf-token"]');
      
    $(".newvariantprdct").on("change",function(){

       var id = $(this).children("option:selected").val();
       
       var product_id = id.split(",")[1];
       var attribute_id = id.split(",")[0];
       var max_quantities = id.split(",")[2];
       var available_quantity = id.split(",")[3];
       var customer_id = $("#customer_id").val();
       $("#attributeid"+product_id).val(attribute_id);
       $("#max_quantities"+product_id).val(max_quantities);
       $("#hidavailable_quantity"+product_id).val(available_quantity);
      $.ajax({
              type: "get",
              url: "{{url('productCheckcart')}}",
              data:{
                     customer_id:customer_id,
                     product_id: product_id,
                     attribute_id:attribute_id,
              },
              success: function(result){
                if(result!=""){
                   if(result == 1)
                   {
                      if(available_quantity==0)
                      {
                        $("#Increas"+product_id).hide();
                       $("#addButton"+product_id).hide();
                       $("#quantity"+product_id).val(1);
                       $("#outofstock"+product_id).show();
                      }else{
                           $("#Increas"+product_id).hide();
                           $("#addButton"+product_id).show();
                           $("#quantity"+product_id).val(1);
                           $("#outofstock"+product_id).hide();
                      }

                   }else{
                     if(result.attribute_id ==  attribute_id )
                           {
                             $("#Increas"+product_id).show();
                             $("#quantity"+product_id).val(result.quantity);
                             $("#addButton"+product_id).hide();
                             $("#outofstock"+product_id).hide();
                           }
                   }
                }
              }
      });
         $.ajax({
                url: '{{url('SelectUnitPrice')}}/' + id.split(",")[0],
                method: 'get',
                success: function(data) {
                   $('#priceUnit'+product_id).html('&#8377;' + data[0].sale_price);
                   $('#pro_sellprice'+product_id).val(data[0].sale_price);
                   var quantity = $("#quantity"+product_id).val()
                   var pro_sellprice = $("#pro_sellprice"+product_id).val();
                   document.getElementById('quantity'+product_id).value = quantity;
                   newsale = parseFloat(pro_sellprice)*parseInt(quantity);
                   $('#priceUnit'+product_id).html('&#8377; ' +  newsale.toFixed(2));
                   var quantity = $("#quantity"+product_id).val();
                }
             });
    });
   });


      </script>
      <script type="text/javascript">
         var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

         $(".postaddtocart").click(function(){

            var product_id = $(this).attr('productid');
            console.log(product_id);
           
             var user_id = $("#user_id").val();
             var customer_id = $("#customer_id").val();
             var quantity = $("#quantity"+product_id).val();
             var price=$('#pro_sellprice'+product_id).val();
             var total_price = $('#pro_sellprice'+product_id).val();
             var attribute_id = $("#attributeid"+product_id).val();
             var varntproduct_id = $('select#product_id'+product_id).val();
            getCartData(user_id,customer_id,product_id,quantity,total_price,attribute_id);
             function getCartData(user_id,customer_id,product_id,quantity,total_price,attribute_id){
                 console.log({customer_id,product_id,quantity,total_price,attribute_id});
                 var total_price = total_price;
               var newtotal_price = total_price.replace(",","");

                 $.ajax({

                   url: "{{url('api/v1/addCart')}}",
                   data: {
                     user_id: user_id,
                     customer_id: customer_id,
                     product_id: product_id,
                     quantity: quantity,
                     total_price: newtotal_price,
                     attribute_id: varntproduct_id.split(",")[0],
                   },
                   type: 'POST',
                   success: function(result) {
                      if(result!="")
                      {
                         $("#Increas"+product_id).show();
                        $("#addButton"+product_id).hide();
                        showCart(user_id,customer_id);
                         var msg = JSON.parse(result);
                         toastr.options = {
                          "preventDuplicates": true,
                           "preventOpenDuplicates": true
                     };
                         toastr.success("<span style='font-size:15px;font-weight:bold'>"+msg.message+"</span>");
                      }
                     console.log("addtocart",result);
                   },
                   error: function(error){
                     console.log("error");
                   }
                 });
             }
         });

         function showCart(user_id,customer_id){
                 $.ajax({
                   url: "{{url('api/v1/showCart')}}",
                   data: {
                      user_id: user_id,
                      customer_id: customer_id,
                   },
                   type: 'GET',
                   dataType: 'json',
                   success: function(result) {
                       if(result!="")
                       {
                           var count=(result.data.carts === 'undefined')?0:result.data.carts.length;
                           $("#items").html(count);
                       }
                   },
                   error: function(error){
                     console.log("error");
                   }
                 });
             }


      </script>
      <script>
/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
  this.classList.toggle("active");
  var dropdownContent = this.nextElementSibling;
  if (dropdownContent.style.display === "block") {
  dropdownContent.style.display = "none";
  } else {
  dropdownContent.style.display = "block";
  }
  });
}
</script>

<script>
/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
var dropdown = document.getElementsByClassName("dropdown-btn1");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
  this.classList.toggle("active");
  var dropdownContent = this.nextElementSibling;
  if (dropdownContent.style.display === "block") {
  dropdownContent.style.display = "none";
  } else {
  dropdownContent.style.display = "block";
  }
  });
}
</script>
   </body>
</html>
