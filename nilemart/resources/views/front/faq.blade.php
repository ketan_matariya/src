<!doctype html>
<html lang="en">
  <head>
    <title>Nilemart | FAQs</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @include('layouts.front.head')
    <style type="text/css">
      #data {
        display: none;
        background: #f1f3f4;
      }
      .click-me {
        cursor: pointer;
      }
    </style>
  </head>
  <body>
    <div class="main-div">
      @include('layouts.front.header-cart')
      <section class="about-us-main">
        <div class="container">
          <div class="row">
            <div class="col-md-8">
              <h3 class="product-head">FAQs</h3>
              <hr class="product-head">
              <br>
              <div class="accordion">
                @foreach($faq as $faqs)
                  <div class="accordion-item items-FAQ">
                    <a class="click-me" id="{{$faqs->id}}">{{$faqs->question}}<i class="fa fa-caret-down mrgn-lft"></i></a>
                    <div class="content fade-me{{$faqs->id}}" id="data">
                      <p>{{$faqs->answer}}</p>
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
            <div class="col-md-4 text-center">
              <div class="about-image">
                <img src="{{url('css/front/images/about-us-icon.png')}}" alt="about">
              </div>
            </div>
          </div>
        </div>
      </section>
      @include('layouts.front.footer')
      <script type="text/javascript">
        $(document).ready(function() {
          $('.click-me').on('click', function() {
            var id = this.id;
            $('.fade-me'+id).fadeToggle(1000);    
          });
        });
      </script>
      <script type="text/javascript">
        const items = document.querySelectorAll(".accordion a");
        function toggleAccordion(){
          this.classList.toggle('active');
          this.nextElementSibling.classList.toggle('active');
        }
        items.forEach(item => item.addEventListener('click', toggleAccordion));
      </script>
    </div>
  </body>
</html>