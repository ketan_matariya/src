<!doctype html>
<html lang="en">

<head>
    <title>Nilemart | Privacy Policy</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @include('layouts.front.head')
</head>

<body>
    <div class="main-div">
        @include('layouts.front.header-cart')

    <section class="about-us-main">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h3 class="product-head">Privacy Policy</h3>
                    <hr class="product-head">
                     <span style="color: #0b3b69; margin: 10px 0; font-weight: 600; font-size: 16px">{{$privacypolicy->title}}</span>
                    <br>
                    <br>
                    <div style="margin-left: 30px;">
                        <p>{{$privacypolicy->description}}</p>
                    </div>
                </div>
                <div class="col-md-4 text-center">
                    <div class="about-image">
                        <img src="{{url('css/front/images/about-us-icon.png')}}" alt="about">
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('layouts.front.footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="{{ url('css/front/js/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ url('css/front/js/bootstrap.min.js') }}"></script>
    <script src="{{ url('css/front/js/webslidemenu.js') }}"></script>
    <script src="{{ url('css/front/js/wow.js') }}"></script>
    <script src="{{ url('css/front/js/owl.carousel.min.js') }}"></script>
    <script src="{{ url('css/front/js/jquery.themepunch.plugins.min.js') }}"></script>
    <script src="{{ url('css/front/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ url('css/front/js/function.js') }}"></script> 
</body>

</html>