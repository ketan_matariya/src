<!doctype html>
<html lang="en">

<head>
    <title>NileMart | MyOrder</title>
    @include('layouts.front.head')
</head>

<body>
    @include('layouts.front.header-cart')

    <section class="my-order-main">
        <div class="container">
            <h3 class="text-orange product-head">My orders</h3>
            <hr class="product-head">
            <div class="cart-listing mt-3 border-0 p-0">
                
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="cart-inner">
                            <div class="cart-heading">
                                <div>
                                    <span>My Orders History</span>
                                </div>
                                
                            </div>
                            <hr>
                           
                            @if(!empty($orders))
                            <div class="cart-listing">
                                <table class="table border-b1px">
                                    <thead>
                                        <tr>
                                            <th>Order Id</th>
                                            <th>Date</th>
                                            <th>Total</th>
                                            <th>Status</th>
                                            <th>View</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($orders as $order)
                                        <tr>
                                           <td>{{$order->id}}</td>
                                           <td>{{date('M d, Y h:i a', strtotime($order->created_at))}}</td>
                                           @php
                                              $wallet_amount = $order['wallet_point'] * env('DISCOUNT_POINT');
                                           @endphp
                                           @php
                                     
                                              if (isset($wallet_amount)) {
                                                     $total = $order['total'] + $order['total_shipping'] + $order['tax'] - $wallet_amount;
                                              } else{
                                                    $total = $order['total'] + $order['total_shipping'] + $order['tax'];
                                             }
                                           @endphp
                                           <td>
                                            <span class="label @if($order->total != $order->total_paid) label-danger @else label-success @endif">Rs {{ $total }}</span>
                                           </td>
                                           <td>
                                            <p class="label" style=" color: white; background-color: {{ $order->color }}">{{ ucfirst($order->status) }}</p>
                                           </td>
                                           <td>
                                            <a title="Show order" href="{{ route('show', $order->id) }}" class="btn btn-primary btn-lg"><i class="fa fa-eye"></i></a>
                                           </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @else
                              <div class="col-lg-12 col-md-12">
                                <div class="empty-message">
                                    <img src="http://127.0.0.1:8000/css/front/images/no-product.gif" alt="empty product">
                                    <p>Orders History not available.</p>
                                 </div>
                              </div>
                            @endif
                        </div>
                    </div>
                    {{-- <div class="col-md-12 col-lg-4 checkout-cart">
                        <div class="coupon-code">
                            <h3 class="font-weight-bold mb-3">Payment Details</h3>
                            <div class="cart-right-total">
                                <table>
                                    <tr>
                                        <td>MRP Total</td>
                                        <td class="text-right" nowrap>&#8377; 708.76</td>
                                    </tr>
                                    <tr>
                                        <td>Discount </td>
                                        <td class="text-right" nowrap>&#8377; 20.00</td>
                                    </tr>
                                    <tr style="border-top:1px solid #ccc;">
                                        <td class="v-a-top">
                                            <h6>Total Amount</h6>
                                        </td>
                                        <td class="text-right" nowrap>
                                            <div class="prise">
                                                <span>&#8377; 708.76</span>
                                                <p>You save &#8377; 20.00</p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                          
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </section>
    {{-- <section class="suggested-product">
        <div class="container">
            <h3 class="text-orange product-head">Suggested Product</h3>
            <hr class="product-head">
            <div class="single-product">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <li>
                            <figure>
                                <a class="product-img" href="#"><img src="{{url('css/front/images/men-1.jpg')}}" class="img-fluid" alt="polo shirt img"></a>
                                <a class="add-card-btn" href="#">View More</a>
                                <figcaption>
                                    <h4 class="product-title"><a href="#">Blazer</a></h4>
                                    <span class="product-price">&#8377; 45.50</span><span class="product-price"><del>$65.50</del></span>
                                </figcaption>
                            </figure>
                        </li>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <li>
                            <figure>
                                <a class="product-img" href="#"><img src="{{url('css/front/images/men-2.jpg')}}" class="img-fluid" alt="polo shirt img"></a>
                                <a class="add-card-btn" href="#">View More</a>
                                <figcaption>
                                    <h4 class="product-title"><a href="#">Shoes</a></h4>
                                    <span class="product-price">&#8377; 45.50</span><span class="product-price"><del>$65.50</del></span>
                                </figcaption>
                            </figure>
                            <!-- product badge -->
                            <!--<span class="badge sale" href="#"><img src="{{url('css/front/images/sale.png')}}" alt=""></span>-->
                        </li>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <li>
                            <figure>
                                <a class="product-img" href="#"><img src="{{url('css/front/images/men-3.jpg')}}" class="img-fluid" alt="polo shirt img"></a>
                                <a class="add-card-btn" href="#">View More</a>
                                <figcaption>
                                    <h4 class="product-title"><a href="#">Tshirts</a></h4>
                                    <span class="product-price">&#8377; 45.50</span><span class="product-price"><del>$65.50</del></span>
                                </figcaption>
                            </figure>
                        </li>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <li>
                            <figure>
                                <a class="product-img" href="#"><img src="{{url('css/front/images/men-4.jpg')}}" class="img-fluid" alt="polo shirt img"></a>
                                <a class="add-card-btn" href="#">View More</a>
                                <figcaption>
                                    <h4 class="product-title"><a href="#">Goggles</a></h4>
                                    <span class="product-price">&#8377; 45.50</span><span class="product-price"><del>$65.50</del></span>
                                </figcaption>
                            </figure>
                            <span class="badge sale" href="#"><img src="{{url('css/front/images/sale.png')}}" alt=""></span>
                        </li>
                    </div>
                </div>
            </div>
        </div>
    </section>  --}}

    @include('layouts.front.footer')
</body>

</html>