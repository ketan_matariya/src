<!doctype html>
<html lang="en">
   <head>
      <title>NileMart | MyOrder</title>
      @include('layouts.front.head')
   </head>
   <body>
      @include('layouts.front.header-cart')
      <section class="my-order-main">
         <div class="container">
            <h3 class="text-orange product-head">Order Information</h3>
            <hr class="product-head">
            <div class="cart-listing mt-3 border-0 p-0">
               <div class="row">
                  <div class="col-md-12 col-lg-12">
                     <div class="cart-inner">
                        <div class="cart-listing">
                           <div class="box">
                              <div class="box-body">
                                 <div class="box-body"> 
                                     
                                    <h4> <i class="fa fa-shopping-bag"></i> Order Information</h4>
                                    <table class="table table-striped">
                                       <thead>
                                          <tr>
                                             <th>Date</th>
                                             <th>Customer</th>
                                             <th>Phone</th>
                                             <th></th>
                                             <th>Payment</th>
                                             <th>Status</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <tr>
                                             <td>{{ date('M d, Y h:i a', strtotime($order['created_at'])) }}</td>
                                             <td>{{ $order->address->alias }}</td>
                                             <td>{{ $order->address->phone }}</td>
                                              <td></td>
                                             <td><strong>{{ $order['payment'] }}</strong></td>
                                             <td>
                                                <button type="button" class="btn btn-info btn-block">{{ ucfirst($currentStatus->name) }}</button>
                                             </td>
                                          </tr>
                                          <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="bg-warning">Subtotal</td>
                        <td class="bg-warning">Rs {{ $order['total_products'] }}</td>
                    </tr>
                    {{-- <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="bg-warning">+ Tax</td>
                        <td class="bg-warning">{{ $order['tax'] }}</td>
                    </tr> --}}
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="bg-warning">+ Shipping</td>
                        <td class="bg-warning">{{ $order['total_shipping'] }}</td>
                    </tr>
                     @php
                        if (isset($wallet_amount)) {
                            $total = $order['total'] + $order['total_shipping'] + $order['tax'] - $wallet_amount;
                        } else{
                            $total = $order['total'] + $order['total_shipping'] + $order['tax'];
                        }
                    @endphp
                    @if($order['total_paid'] != $order['total'])
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="bg-danger text-bold">Total paid</td>
                            <td class="bg-danger text-bold">Rs {{ number_format($total,2) }}</td>
                        </tr>
                    @endif
                                       </tbody>
                                    </table>
                                 </div>
                              @if($order)
                                @if(!$items->isEmpty())
                                 <div class="box-body">
                                    <h4> <i class="fa fa-gift"></i> Items</h4>
                                    <table class="table table-striped">
                                       <thead>
                                          <th>Name</th>
                                          <th>Unit</th>
                                          <th>Quantity</th>
                                          <th>Price</th>
                                          <th>Shipping Price</th>
                                          <th>Total</th>
                                       </thead>
                                       <tbody>
                                        @foreach($items as $item)
                                        @php
                                            $item->product_price = str_replace(",","",$item->product_price);
                                            $total = ($item->product_price*$item->quantity)+$item->shipping_price;
                                            $total = number_format($total ,2);
                                            $total = str_replace(",","",$total);
                                        @endphp
                                          <tr>
                                             <td>{{ $item->product_name }}</td>
                                             <td>{{ $item->unit }}</td>
                                             <td>{{ $item->quantity }}</td>
                                             <td>{{ str_replace(",","",$item->product_price) }}</td>
                                             <td>{{ number_format($item->shipping_price,2) }}</td>
                                             <td>{{ $total }}</td>
                                          </tr>
                                        @endforeach
                                       </tbody>
                                    </table>
                                 </div>
                                @endif
                                 <div class="box-body">
                                    <div class="row">
                                       <div class="col-md-12">
                                          <h4> <i class="fa fa-map-marker"></i> Address</h4>
                                          <table class="table table-striped">
                                             <thead>
                                                <th>Address</th>
                                                @if(isset($order->order_address->landmark))<th>Land Mark</th>@endif
                                                <th>Country</th>
                                                <th></th>
                                                <th>Zip</th>
                                                
                                             </thead>
                                             <tbody>
                                                <tr>
                                                   <td>{{ $order->order_address->address }}</td>
                                                   @if(isset($order->order_address->landmark))<td>{{ $order->order_address->landmark }}</td>@endif
                                                   <td>{{ $order->address->country->name }}</td>
                                                   <td></td>
                                                   <td>{{ $order->order_address->postcode }}</td>
                                                   
                                                </tr>
                                                
                                             
                                             </tbody>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- /.box -->
                               
                              @endif
                              </div>
                           </div>
                        </div>
                        
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      @include('layouts.front.footer')
   </body>
</html>