<!doctype html>
<html lang="en">
   <head>
      <title>NileMart | Checkout</title>
      @include('layouts.front.head')


   </head>
   <body>

       <div class="main-div">
         @include('layouts.front.header-cart')
         <section >
         <!--    <div class="container">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="page-breadcrumb">
                        <h2>Checkout</h2>
                        <ul class="breadcrumb">
                           <li><a href="{{route('home')}}"><span class="fa fa-home"></span> Home</a></li>
                           <li class="active">Checkout</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </section> -->
         <section id="closeamodalss" class="checkout-cart opacityclass">
            <div class="container">
               <div class="row">
                  <div class="col-sm-12">
                     <div id="myDiv">
                        <div class="tabs-data">
                           @if(Auth::check())
                           <div class="d-lg-flex justify-content-between">
                              <div>
                                 <h3 class="product-head text-orange">Checkout<span id="productcount">({{$cartitems->count()}})</span></h3>
                                 <hr class="product-head mb-3">
                              </div>
                              <ul class="nav nav-tabs">
                                 <li class="active" id="tab1">
                                    <a data-toggle="tab" href="#CartPreview">
                                    <span class="icn-checkout"><i class="fa fa-shopping-cart mr-3"></i>Cart Preview</span>
                                    </a>
                                 </li>
                                 <li id="tab2">
                                    <a data-toggle="tab" href="#DeliveryDetails" id="deliverydetail">
                                    <span class="icn-checkout"><i class="fa fa-user mr-3" id="deliveryicon"></i>Delivery Details</span>
                                    </a>
                                 </li>
                                 <li id="tab3">
                                    <a data-toggle="tab" href="#PaymentMethod" id="paymentCheck">
                                    <span class="icn-checkout"><i class="fa fa-credit-card mr-3" aria-hidden="true" id="paymenticon"></i>Payment Method</span>
                                    </a>
                                 </li>
                              </ul>
                           </div>
                           @endif
 
                           <div class="tab-content">
                              <div id="CartPreview" class="tab-pane active">
                                 <div class="row" id="product_message">
                                    <div class="col-md-12 col-lg-8 hideproduct">
                                       @if(Auth::check())
                                       <div class="cart-inner">
                                          <div class="cart-heading">
                                             <h3>Groceries Basket <span>({{$cartitems->count()}} items)</span></h3>
                                             @if($cartitems->count()>0) 
                                             <span> <span id="maintotalprice"> &#8377;  {{number_format((float) $totalamount->total, 2, '.', '')}}</span></span>
                                             @endif
                                          </div>
                                          @if($cartitems->count()>0)
                                          @foreach($cartitems->get() as $cart)
                                          <hr>
                                          <div class="cart-listing">
                                             <div class="w-100 justify-content-end ">
                                                <a class="text-danger float-right" href="javascript:void(0)" onclick="deleteProduct({{$cart->id}},{{$cart->product_id}},{{$cart->attribute_id}},{{$cart->total_price}})"><i class="fa fa-trash fa-2x" aria-hidden="true"></i></a>
                                             </div>
                                             <div class="d-flex justify-content-start align-items-center mb-2">
                                                <div class="pro-image">
                                                   <a class="product-img" href="{{route('productDetail.product',$cart->products->id)}}">
                                                   <img src="{{$cart->products->cover}}" class="img-fluid" alt="polo shirt img">
                                                   </a>
                                                </div>
                                                <div class="pro-description">
                                                   <a class="product-img" href="{{route('productDetail.product',$cart->products->id)}}">
                                                      <h4 id="
                                                      ">
                                                         {{$cart->products->name}}
                                                      </h4>
                                                   </a>
                                                   <div class="pro-prise">
                                                      @php
                                                      $mrp = floatval($cart->productsattribute->price);
                                                      $price = floatval($cart->productsattribute->sale_price);
                                                      $price_qty =floatval($cart->productsattribute->price)*$cart->quantity;
                                                      $save = floatval($mrp) - floatval($price_qty);
                                                      $sale_price = $cart->productsattribute->sale_price*$cart->quantity;
                                                      @endphp
                                                      <!--product price--->
                                                      <span>Unit : <strong><span id="unite">{{$cart->productsattribute->value}} {{$cart->productsattribute->key}}</span></strong><br></span>
                                                      <span>Unit Price : <strong><span id="unite">&#8377; <del>{{$cart->productsattribute->price}} </del>&nbsp;&nbsp;  &#8377;  {{$cart->productsattribute->sale_price}}</span></strong><br></span>
                                                      <span>Price :<del> <strong>&#8377; <span id="price{{$cart->id}}">{{number_format((float)$price_qty, 2, '.', '')}}</span></strong></del><br></span>
                                                      <input type="hidden" name="hidprice{{$cart->id}}" id="hidprice{{$cart->id}}" value="{{$cart->productsattribute->price}}"/>
                                                      <span>Sale Price : <strong>&#8377; <span id="saleprce{{$cart->id}}"> {{number_format((float)$sale_price, 2, '.', '')}}</span></strong><br></span>
                                                      <input type="hidden" name="hidsaleprce{{$cart->id}}" id="hidsaleprce{{$cart->id}}" value="{{$cart->productsattribute->sale_price}}"/>
                                                      {{-- @if($cart->productsattribute->discount!=0.0)
                                                      <span>Discount : {{$cart->productsattribute->discount}}% <br> </span>
                                                      <span>You save : &#8377; {{$save}}<br> </span>
                                                      @endif --}}
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pro-quantity w-100 justify-content-end">
                                                <a href="javascript:void(0)" class="qty qty-minus " onclick="decreaseValue({{$cart->id}},{{$cart->product_id}},{{$cart->attribute_id}},{{$cart->total_price}},'-')">-</a>
                                                <input type="numeric" id="number{{$cart->id}}" value="{{$cart->quantity}}" readonly />
                                                <a href="javascript:void(0)" class="qty qty-plus" onclick="increaseValue({{$cart->id}},{{$cart->product_id}},{{$cart->attribute_id}},{{$cart->total_price}},'+')">+</a>
                                             </div>
                                          </div>
                                          @endforeach
                                          @endif
                                          <?php /*?>
                                          <div class="cart-listing">
                                             <div class="d-flex justify-content-start align-items-center mb-2">
                                                <div class="pro-image">
                                                   <img src="{{url('css/front/images/women-1.jpg')}}" class="img-fluid" alt="polo shirt img">
                                                </div>
                                                <div class="pro-description">
                                                   <h4>Fancy Bags</h4>
                                                   <div class="pro-prise">
                                                      <span>Sale Price : <strong>&#8377; 354.38</strong></span>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pro-quantity w-100 justify-content-end">
                                                <a href="javascript:void(0)" class="qty qty-minus " onclick="decreaseValue()">-</a>
                                                <input type="numeric" id="number" value="1" readonly />
                                                <a href="javascript:void(0)" class="qty qty-plus" onclick="increaseValue()">+</a>
                                             </div>
                                          </div>
                                          <hr>
                                          <div class="cart-listing">
                                             <div class="d-flex justify-content-start align-items-center mb-2">
                                                <div class="pro-image">
                                                   <img src="{{url('css/front/images/women-1.jpg')}}" class="img-fluid" alt="polo shirt img">
                                                </div>
                                                <div class="pro-description">
                                                   <h4>Fancy Bags</h4>
                                                   <div class="pro-prise">
                                                      <span>Sale Price : <strong>&#8377; 354.38</strong></span>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pro-quantity w-100 justify-content-end">
                                                <a href="javascript:void(0)" class="qty qty-minus " onclick="decreaseValue()">-</a>
                                                <input type="numeric" id="number" value="1" readonly />
                                                <a href="javascript:void(0)" class="qty qty-plus" onclick="increaseValue()">+</a>
                                             </div>
                                          </div>
                                          <?php */?>
                                       </div>
                                       @endif
                                    </div>
                                    @if(Auth::check())
                                    <div class="col-md-12 col-lg-4 hideproduct">
                                       <div class="coupon-code">
                                          <h3 class="font-weight-bold mb-3">Payment Details</h3>
                                          <div class="cart-right-total">
                                             <table>
                                                <tr>
                                                   <td>MRP Total</td>
                                                   @if($cartitems->count()>0)
                                                   <td id="mrptotalamnt" class="text-right" nowrap> {{ $totalamount->total}}</td>
                                                   @endif
                                                </tr>
                                                <tr>
                                                   <td>Shipping Total</td>
                                                   @if($cartitems->count()>0)
                                                   <td id="shoppingamnt" class="text-right" nowrap>&#8377; {{ $totalshiping->totalshipping}}</td>
                                                   @endif
                                                </tr>
                                                @if($cartitems->count()>0)
                                                <?php /* ?>
                                                <tr>
                                                   <td>Discount </td>
                                                   <td class="text-right" nowrap>&#8377; 20.00</td>
                                                </tr>
                                                <?php */?>
                                                <tr style="border-top:1px solid #ccc;">
                                                   <td class="v-a-top">
                                                      <h6>Total Amount </h6>
                                                   </td>
                                                   <td class="text-right" nowrap>
                                                      <div class="prise">
                                                         <span>&#8377; <span id="totalpriceamunt">{{$totalshipamount->totalship}} </span></span>
                                                         {{-- 
                                                         <p id="newsavingprice" style="display:none;">You save &#8377; 20.00</p>
                                                         --}}
                                                         <p id="newsavingprice" style="display:none;"></p>
                                                      </div>
                                                   </td>
                                                </tr>
                                                @endif
                                             </table>
                                          </div>
                                          <div class="place-order">
                                             <button class="btn btn-primary subscribe" type="button"> Place Order</button>
                                          </div>
                                       </div>
                                    </div>
                                    @endif
                                 </div>
                              </div>

                              @if(Auth::check())
                              <div id="DeliveryDetails" class="tab-pane fade">
                                 <div class="tab-login-signin delivery-details">
                                    <div class="row">
                                       <div class="col-md-12 col-lg-8">
                                          <div class="order-address">
                                             
                                             @if($getcntaddress>0)
                                             <h3>Select Delivery Address</h3>
                                              
                                             <!-- <hr> -->
                                             @if($getcntaddress>0)
                                             
                                              @if(isset($activAddress))
                                             <span id="listUseradrs">
                                               
                                                <div class="radio">
                                                   <input id="radio{{ $activAddress->id}}" class=
                                                   "radioselect" name="radio[]" id="radio[]" type="radio" {{($activAddress->default==1) ? "checked='true'":''}} onchange="defaultAddres({{ $activAddress->id}});">
                                                   <label for="radio{{ $activAddress->id}}" class="radio-label"></label>
                                                </div>
                                               
                                                <div class="pay-detail">
                                                   @if($activAddress->default==1)
                                                   <input type="hidden" name="hiname" id="hiname" value="{{$activAddress->name}}"/>
                                                   <input type="hidden" name="hidfulladrss" id="hidfulladrss" value=" {{ $activAddress->address}}"/>
                                                   <input type="hidden" name="hidlandmark" id="hidlandmark" value="{{ $activAddress->landmark}}"/>
                                                   <input type="hidden" name="hidstreet" id="hidstreet" value="{{ $activAddress->area}}"/>
                                                   <input type="hidden" name="hidcity" id="hidcity" value="{{ $activAddress->city}}"/>
                                                   <input type="hidden" name="hidzip" id="hidzips" value="{{ $activAddress->postcode}}"/>
                                                   <input type="hidden" name="hidaddressid" id="hidaddressid" value="{{ $activAddress->id}}"/>
                                                   <input type="hidden" name="hidmail" id="hidmail" value="{{ $activAddress->email}}"/>
                                                   <span id="actname"><h5 id="actusername">{{ $activAddress->name}}</h5></span>
                                                   <p>
                                                      <span id="actaddress">{{ $activAddress->address}}</span>
                                                      <span id="actlandmark"> {{ $activAddress->landmark}}</span>
                                                      <span id="actarea">{{ $activAddress->area}} </span>
                                                      <span id="actcity"> {{ $activAddress->city}} </span>
                                                      <span id="actpostcode"> {{ $activAddress->postcode}}<br></span>
                                                      <span id="actemail"> {{ $activAddress->email}} </span>
                                                      <span id="actmobile">{{ $activAddress->mobile}}  </span>
                                                   </p>
                                                   
                                                   <span id="defultadrs{{ $activAddress->id}}" class="newabc" style="{{($activAddress->default==1) ? 'display:inline;':'display:none;'}}">
                                                   <button class="def-add mb-3">Default Address</button>
                                                   </span>
                                                   @endif
                                                </div>
                                             </span>
                                             @endif

                                             {{-- @foreach($address->get() as $address)
                                             
                                             <span id="listUseradrs">
                                               
                                                <div class="radio">
                                                   <input id="radio{{ $address->id}}" class=
                                                   "radioselect" name="radio[]" id="radio[]" type="radio" {{($address->default==1) ? "checked='true'":''}} onchange="defaultAddres({{ $address->id}});">
                                                   <label for="radio{{ $address->id}}" class="radio-label"></label>
                                                </div>
                                               
                                                <div class="pay-detail">
                                                   @if($address->default==1)
                                                   <input type="hidden" name="hiname" id="hiname" value="{{$address->name}}"/>
                                                   <input type="hidden" name="hidfulladrss" id="hidfulladrss" value=" {{ $address->address}}"/>
                                                   <input type="hidden" name="hidlandmark" id="hidlandmark" value="{{ $address->landmark}}"/>
                                                   <input type="hidden" name="hidstreet" id="hidstreet" value="{{ $address->area}}"/>
                                                   <input type="hidden" name="hidcity" id="hidcity" value="{{ $address->city}}"/>
                                                   <input type="hidden" name="hidzip" id="hidzips" value="{{ $address->postcode}}"/>
                                                   <input type="hidden" name="hidaddressid" id="hidaddressid" value="{{ $address->id}}"/>
                                                   <input type="hidden" name="hidmail" id="hidmail" value="{{ $address->email}}"/>
                                                   <h5>{{ $address->name}}</h5>
                                                   <p>
                                                      {{ $address->address}}
                                                      {{ $address->landmark}}
                                                      {{ $address->area}}
                                                      {{ $address->city}}
                                                      {{ $address->postcode}}<br>
                                                      {{ $address->email}}
                                                      {{ $address->mobile}}  
                                                   </p>
                                                   
                                                   <span id="defultadrs{{ $address->id}}" class="newabc" style="{{($address->default==1) ? 'display:inline;':'display:none;'}}">
                                                   <button class="def-add mb-3">Default Address</button>
                                                   </span>
                                                   @endif
                                                </div>
                                             </span>
                                             @endforeach --}}

                                             @endif
                                             @endif
                                             @if($getcntaddress==0)
                                             No Address Found
                                             @endif
                                             <div class="change-add">
                                                <button onclick="openModal()" class="m-3">Change/Add Address</button>
                                             </div>
                                          </div>
                                          <div class="cart-inner">
                                             <div class="cart-heading">
                                                <div>
                                                   <span>Order Details </span>
                                                   
                                                </div>
                                                @if($cartitems->count()>0)
                                                <span>&#8377; <span id="maintotal">{{number_format((float)$cart->total_price, 2, '.', '')}}</span></span>
                                                @endif
                                             </div>
                                             @if(Auth::check())
                                             @if($cartitems->count()>0)
                                             @foreach($cartitems->get() as $cart)
                                             <hr>
                                             <div class="cart-listing">
                                                <div class="d-flex justify-content-start align-items-center mb-2">
                                                   <div class="pro-image">
                                                      <a class="product-img" href="{{route('productDetail.product',$cart->products->id)}}">
                                                      <img src="{{$cart->products->cover}}" class="img-fluid" alt="polo shirt img">
                                                      </a>
                                                   </div>
                                                   <div class="pro-description">
                                                      <a class="product-img" href="{{route('productDetail.product',$cart->products->id)}}">
                                                         <h4>
                                                            {{$cart->products->name}}
                                                         </h4>
                                                      </a>
                                                      <div class="pro-prise">
                                                         @php
                                                         $mrp = floatval($cart->productsattribute->price);
                                                         $price_qty =floatval($cart->productsattribute->price)*$cart->quantity;
                                                         $sale_price = $cart->productsattribute->sale_price*$cart->quantity;
                                                         $save = floatval($mrp) - floatval($price_qty);
                                                         @endphp
                                                         @if($cart->productsattribute->discount!=0.0)
                                                         <span>Discount : {{$cart->productsattribute->discount}}% <br> </span>
                                                         <span>You save : &#8377; {{$save}}<br> </span>
                                                         @endif
                                                         <span>Price : <strong>&#8377; <del> {{number_format((float)$price_qty, 2, '.', '')}}</del></strong><br></span>
                                                         <span>Sale Price : <strong>&#8377; <span id="saleprces">{{number_format((float)$sale_price, 2, '.', '')}}  </span></strong><br></span>
                                                         <span>Unit : <strong><span id="unite">{{$cart->productsattribute->value}} {{$cart->productsattribute->key}}</span></strong><br></span>
                                                         <span>Qty : <strong><span id="qty">{{$cart->quantity}}</span></strong><br></span>
                                                         <input type="hidden" name="hidsaleprce" id="hidsaleprce{{$cart->id}}" value="{{$cart->productsattribute->sale_price}}"/>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="pro-quantity w-100 justify-content-end" style="display: none;">
                                                   <a href="javascript:void(0)" class="qty qty-minus " onclick="decreaseValue({{$cart->id}},{{$cart->product_id}},{{$cart->attribute_id}},{{$cart->total_price}},'-')">-</a>
                                                   <input type="numeric" name="number11{{$cart->id}}" id="number11{{$cart->id}}" value="{{$cart->quantity}}" readonly/>
                                                   <a href="javascript:void(0)" class="qty qty-plus" onclick="increaseValue({{$cart->id}},{{$cart->product_id}},{{$cart->attribute_id}},{{$cart->total_price}},'+')">+</a>
                                                </div>
                                             </div>
                                             @endforeach
                                             @endif
                                             @endif
                                          </div>
                                       </div>
                                       <div class="col-md-12 col-lg-4">
                                          <div class="imp-notice">
                                             <h3>IMPORTANT NOTICE</h3>
                                             <p>We will try to deliver your order at the earliest. However, due to the current surge in orders to may be in delay. Our team will be touch with you regarding your order.</p>
                                          </div>
                                          <div class="coupon-code">
                                             <h3 class="font-weight-bold mb-3">Payment Details</h3>
                                             <div class="cart-right-total">
                                                <table>
                                                   <tr>
                                                      <td>MRP Total</td>
                                                      @if($cartitems->count()>0)
                                                      <td class="text-right" id="newmrptotal" nowrap>&#8377; {{number_format(floatval($totalamount->total),2)}}</td>
                                                      @endif
                                                   </tr>
                                                   <tr>
                                                      <td>Shipping Total</td>
                                                      @if($cartitems->count()>0)
                                                      <td class="text-right" id="newshipping" nowrap>&#8377; {{ $totalshiping->totalshipping}}</td>
                                                      @endif
                                                   </tr>
                                                   @if($cartitems->count()>0)
                                                   <tr style="border-top:1px solid #ccc;">
                                                      <td class="v-a-top">
                                                         <h6>Total Amount </h6>
                                                      </td>
                                                      <td class="text-right" nowrap>
                                                         <div class="prise">
                                                            <span>&#8377; <span id="totalpriceamunts">{{$totalshipamount->totalship}}</span></span>
                                                            {{-- 
                                                            <p>You save &#8377; {{$totalshipamount->totalship}}</p>
                                                            --}}
                                                            <p id="newsavingprice11" style="display:none;"></p>
                                                         </div>
                                                      </td>
                                                   </tr>
                                                   @endif
                                                </table>
                                             </div>
                                             <div class="place-order">
                                                <button class="btn btn-primary subscribe" type="button" id="ordernow"> Make Payment</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              @endif
                              <div id="PaymentMethod" class="tab-pane fade pt-3">
                                 <div class="payment-method">
                                    <h2>Chose your payment method</h2>
                                 
                                    <div class="col-md-8">
                                       <form method="post">
                                          @csrf
                                          <div class="row">
                                             <div class="col-sm-12">
                                                
                                                <div class="form-group">
                                                   <label>Select Payment Method</label>
                                                   <select name="paymentmethod" id="paymentmethod" class="form-control">
                                                      <option value="COD">Cash on Delivery</option>
                                                   </select>
                                                </div>
                                             </div>
                                             
                                             <div class="col-md-12 justify-content-end">
                                               
                                                <button type="button" class="btn btn-default cart-login">Checkout</button><br>
                                                <span id="success_message" style="display: none;color:green"></span>
                                             
                                             </div>
                                          </div>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
           
           
         </section>
          <!-- address modal start --> 
            @if(Auth::check())
            <div id="addressModal" class="address-modal">
{{--               @if($getcntaddress>0)--}}
               <ul class="side-nav-list">
                  <li class="text-right">
                     <a href="javascript:void(0)" class="closebtn" onclick="closeModal()"></a>
                  </li>
                  <button class="w-100 mb-3 grey-modal-button"  onclick="openAddModal()">Add Address</button>
                  @if(!empty($latsaddress))
                     @foreach($latsaddress->get() as $newaddress)
                     <li>
                        <div class="order-address">
                           <div class="radio">
                              <input id="newradio{{ $newaddress->id}}" name="newradio[]" id="newradio[]" type="radio" {{($newaddress->default==1) ? "checked='true'":''}} onchange="defaultAddres({{ $newaddress->id}});" >
                              <label for="newradio{{ $newaddress->id}}" class="radio-label"></label>
                           </div>
                           <div class="pay-detail">
                                 <span class="float-right removeRt" style="{{($newaddress->default == 0) ? 'display:inline;':'display:none;'}}" id="deletAddress{{ $newaddress->id}}"><span onclick="deletAddress({{ $newaddress}})"><i class="fa fa-trash fa-lg" style="color:#0b3b69"></i></span></span>
                              
                              <span class="float-right pr-3" ><span onclick="openEditModal({{ $newaddress}})" ><i class="fa fa-edit fa-lg" style="color:#0b3b69"></i></span></span>
                              
                              <h5>{{ $newaddress->name}}</h5>
                              <p>
                                 {{ $newaddress->address}}
                                 {{ $newaddress->landmark}}
                                 {{ $newaddress->area}}
                                 {{ $newaddress->city}}
                                 {{ $newaddress->postcode}}<br>
                                 {{ $newaddress->email}}
                                 {{ $newaddress->mobile}}
                              </p>
                              <span id="defultadrs1{{ $newaddress->id}}" class="newabc1" style="{{($newaddress->default==1) ? 'display:inline;':'display:none;'}}">
                                 <button class="def-add mb-3">Default Address</button>
                              </span>

                           </div>
                        </div>
                     </li>
                     @endforeach
                  @endif
                  
               </ul>
{{--               @else--}}
               <button class="w-100 mb-3 grey-modal-button" onclick="openAddModal()">Add Address</button>
{{--               @endif--}}
            </div>
            @endif
            <!-- address modal end here -->
          <!-- add address modal start here -->
            <div id="addAddressModal" class="address-modal">
               <ul class="side-nav-list">
                  <li class="text-right">
                     <a href="javascript:void(0)" class="closebtn" onclick="closeAddModal()">&times;Close</a>
                  </li>
                  <li>
                     <div class="order-address">
                        <h3 id="addaddress">Add Address</h3>
                        <hr>
                        <form type="post" name="frmAddadrss" id="frmAddadrss">
                           <div class="form-group">
                              <input type="hidden" name="addrsid" id="addrsid"/>
                              <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Name" maxlength="30">
                              <span id="fnameerror" style="color:red;display:none;">Please enter name</span>
                           </div>
                           
                           <div class="form-group">
                              <input type="text" class="form-control" name="address" id="address" placeholder="Address" maxlength="300">
                              <span id="addresserror" style="color:red;display:none;">Please enter address</span>
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" name="landmark" id="landmark" placeholder="Landmark" maxlength="100">
                              <span id="landmarkerror" style="color:red;display:none;">Please enter landmark</span>
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" name="area" id="area" placeholder="Area" maxlength="50">
                              <span id="areaerror" style="color:red;display:none;">Please enter area</span>
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" name="city" id="city" placeholder="City" maxlength="50">
                              <span id="cityerror" style="color:red;display:none;">Please enter city</span>
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" name="postcode" id="postcode" placeholder="Post code" maxlength="6">
                              <span id="postcodeerror" style="color:red;display:none;">Please enter postcode</span>
                           </div>
                           <div class="form-group">
                              <input type="email" class="form-control"  name="email" id="email" placeholder="Email" maxlength="50">
                              <span id="emailerror" style="color:red;display:none;">Please enter email</span>
                              <span id="emailvalid" style="color:red;display:none;">please enter valid email</span>
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Phone No" maxlength="10" minlength="7">
                              <span id="mobileerror" style="color:red;display:none;">Please enter mobile</span>
                              <span id="mobilvaliedeerror" style="color:red;display:none;">Please enter valid mobile</span>
                           </div>
                           <div class="change-add">
                              <button class="text-center" type="button" id="frmAddSubmit">Add</button>
                              <button class="text-center" type="button" id="frmAddUpdate" style="display:none;">Update</button>
                           </div>
                        </form>
                     </div>
                  </li>
               </ul>
            </div>
            <!-- add address modal end here -->
         @include('layouts.front.footer')
      </div>
      @if(Auth::check())
      <script type="text/javascript">
         
         var value = $("#productcount").html();
         if(value == "(0)"){
            $(".subscribe").hide();
            $(".hideproduct").hide();
            $("#product_message").append(" <div class='col-md-12 col-lg-12 hideproduct'><h1><lable>No product in cart </lable><h1></div>");

              
        $('#deliverydetail').click(function(){
            $("#deliveryicon").css('color','#8d8d8e');
            $("#tab2").removeClass('active'); 
            var oldUrl = $(this).attr("href"); // Get current url
            var newUrl = oldUrl.replace("#"); // Create new url
            $(this).attr("href", newUrl); // Set herf value
        });

        $('#paymentCheck').click(function(){
            $(this).removeClass('active');  
            $("#paymenticon").css('color','#8d8d8e');
            var oldUrl = $(this).attr("href"); // Get current url
            var newUrl = oldUrl.replace("#"); // Create new url
            $(this).attr("href", newUrl); // Set herf value
        });
   
         }
         else{
            console.log();
         }

      </script>
      <script>
         function openModal() {
            document.getElementById("addressModal").style.width = "300px";
            $(".opacityclass").css("opacity", "0.");
            
            $('.disablebutton').attr("disabled", true);
         }


         var modal = document.getElementById("addressModal");

         $('body').click(function(){
            if (!modal.contains(event.target)){
               if($("#addressModal").width() > 200){
                  closeModal();
               }
            }
         });

         function closeModal() {
             document.getElementById("addressModal").style.width = "0";
             $(".opacityclass").css({ 'background-color' : '', 'opacity' : '' });
             $('.disablebutton').attr("disabled", false);
         }


         
      </script>
      <script>
         function openAddModal() {
            $(".opacityclass").css("opacity", "0.8");
            $(".opacityclass").css("background-color", "#9fa3a7");
            $('.disablebutton').attr("disabled", true);

             document.getElementById("addAddressModal").style.width = "300px";
             document.getElementById('frmAddSubmit').style.display='inline';
             document.getElementById('frmAddUpdate').style.display='none';
             document.getElementById('addrsid').value="";
             document.getElementById('first_name').value="";
             document.getElementById('address').value="";
             document.getElementById('landmark').value="";
             document.getElementById('area').value="";
             document.getElementById('city').value="";
             document.getElementById('postcode').value="";
             document.getElementById('email').value="{{ Auth::user()->email }}";
             document.getElementById('mobile').value="{{ Auth::user()->mobile }}";
             $("#addaddress").text('Add Address');
         }
         function openEditModal(address) {
            
              $(".opacityclass").css("opacity", "0.5");
              $(".opacityclass").css("background-color", "#9fa3a7");
              $('.disablebutton').attr("disabled", true);

             document.getElementById("addAddressModal").style.width = "300px";
             document.getElementById('frmAddSubmit').style.display='none';
             document.getElementById('frmAddUpdate').style.display='inline';
             document.getElementById('addrsid').value=address.id;
             document.getElementById('first_name').value=address.name;
             document.getElementById('address').value=address.address;
             document.getElementById('landmark').value=address.landmark;
             document.getElementById('area').value=address.area;
             document.getElementById('city').value=address.city;
             document.getElementById('postcode').value=address.postcode;
             document.getElementById('email').value=address.email;
             document.getElementById('mobile').value=address.mobile;
             $("#addaddress").text('Edit Address');
         }
         function closeAddModal() {
             document.getElementById("addAddressModal").style.width = "0";
             $(".opacityclass").css({ 'background-color' : '', 'opacity' : '' });
             $('.disablebutton').attr("disabled", false);
         }

         function deletAddress(address)
         {
           var addrsid = address.id;
           var user_id = '{{Auth::user()->id}}';
           var r=confirm('Are you want to delete this address?');
             if(r==true){
                 $.ajax({
                     type : "GET",
                     url : "{{url('api/v1/delete-address')}}",
                     data : {
                        address_id : addrsid,
                        user_id : user_id, 
                     },
                     success: function(result) {
                        result = JSON.parse(result)
                        if(result.message=="Success")
                        {
                           toastr.error("<span style='font-size:15px;font-weight:bold'>"+result.data+"</span>");
                        }
                        location.href="{{url('/checkout')}}";
                     }
                 })
             }
         }
      </script>
      <script type="text/javascript">
         function increaseValue(cartid,productid,attributeid,total_price,plus_minus) {
             var value = parseInt(document.getElementById('number'+cartid).value, 10);
             value = isNaN(value) ? 0 : value;
             value++;
             document.getElementById('number'+cartid).value = (value==0) ? 1:value;
             document.getElementById('number11'+cartid).value = (value==0) ? 1:value;
             var userid="{{Auth::user()->id}}";
           
             var saleprce = $('#hidsaleprce'+cartid).val();
          
             var newsaleprce=saleprce*value;
             $('#saleprce'+cartid).empty();
             $('#saleprce'+cartid).html(newsaleprce.toFixed(2));
         
             var price = $("#hidprice"+cartid).val();
             var newprice = price*value;
             $("#price"+cartid).empty();
             $("#price"+cartid).html(newprice.toFixed(2));
             Updatequantity(cartid,value,productid,userid,userid,attributeid,total_price,plus_minus);
             showCart(userid);
         }
         function deleteCart(customer_id,product_id,attribute_id,total_price){
             $.ajax({
                 url: "{{url('api/v1/deleteProductCart')}}",
                 data: {
                   customer_id: customer_id,
                   product_id: product_id,
                   attribute_id: attribute_id,
                   user_id: customer_id,
                   total_price: total_price,
                   delete_price: total_price,
                 },
                 type: 'POST',
                 success: function(result) {
                   console.log("addtocart",result);
                 },
                 error: function(error){
                   console.log("error");
                 }
               });
         }
         function deleteProduct(cartid,productid,attributeid,total_price)
         {
             var r=confirm('Are you want to delete this item from cart?');
             var userid="{{Auth::user()->id}}";
             if(r==true){
                 deleteCart(userid,productid,attributeid,total_price);
                 location.href="{{url('/checkout')}}";
             }
         }
         function decreaseValue(cartid,productid,attributeid,total_price,plus_minus) {
             var value = parseInt(document.getElementById('number'+cartid).value, 10);
             if(value == 1)
                 {
                     deleteProduct(cartid,productid,attributeid,total_price)
                 }else{
             var userid="{{Auth::user()->id}}";
             
             
                 var value = parseInt(document.getElementById('number'+cartid).value, 10);
                 value = isNaN(value) ? 0 : value;
                 value < 1 ? value = 1 : '';
                 value--;
                 document.getElementById('number'+cartid).value = (value==0) ? 1:value;
                 document.getElementById('number11'+cartid).value = (value==0) ? 1:value;
                 var userid="{{Auth::user()->id}}";
                 if(value!=0){   
                     var saleprce = $('#hidsaleprce'+cartid).val();
                     var newsaleprce=saleprce*value;
                     $('#saleprce'+cartid).empty();
                     $('#saleprce'+cartid).html(newsaleprce.toFixed(2));
                     
                     var price = $("#hidprice"+cartid).val();
                     var newprice = price*value;
                     $("#price"+cartid).empty();
                     $("#price"+cartid).html(newprice.toFixed(2));
         
                     Updatequantity(cartid,value,productid,userid,userid,attributeid,total_price,plus_minus);
                     showCart(userid);
             
                 }
                 
             }
            
             
         }
         
         function Updatequantity(cartid,quantity,product_id,customer_id,user_id,attribute_id,total_price,plus_minus){
             $.ajax({
                 url: "{{url('api/v1/updateCart')}}",
                 data: {
                  user_id: user_id,
                   customer_id: customer_id,
                   product_id: product_id,
                   quantity: quantity,
                   total_price: total_price,
                   attribute_id: attribute_id,
                   cart_id:cartid,
                   plus_minus:plus_minus,
                 },
                 type: 'POST',
                 success: function(result) {
                   console.log("addtocart",result);
                
                 },
                 error: function(error){
                   console.log("error");
                 }
               });
         }
         function showCart(customer_id){
             
             $.ajax({
                 url: "{{url('api/v1/showCart')}}",
                 data: {
                     customer_id:customer_id,
                     user_id:customer_id
                 },
                 type: 'GET',
                 success: function(result) {
         
                   $('#maintotal').empty();
                   $('#maintotal').html(JSON.parse(result).data.total_order_price);
                   $('#totalpriceamunt').empty();
                   $('#totalpriceamunt').html(JSON.parse(result).data.total);
                   $('#totalpriceamunts').empty();
                   $('#totalpriceamunts').html(JSON.parse(result).data.total);
                   $('#maintotal12').html(JSON.parse(result).data.total_order_price);
                   $('#mrptotalamnt').empty();
                   $('#mrptotalamnt').html('&#8377; '+ JSON.parse(result).data.total_order_price);
         
                   $('#maintotalprice').empty();
                   $('#maintotalprice').html('&#8377; '+ JSON.parse(result).data.total_order_price);
         
                   $('#shoppingamnt').empty();
                   $('#shoppingamnt').html('&#8377; '+ JSON.parse(result).data.shipping_price);
                   $('#newmrptotal').empty();
                   $('#newmrptotal').html('&#8377; '+ JSON.parse(result).data.total_order_price);
                   $('#newshipping').empty();
                     $('#newshipping').html('&#8377; '+ JSON.parse(result).data.shipping_price);
                     if(JSON.parse(result).data.saving_price!='0.00' || JSON.parse(result).data.saving_price!='0.0'){
                         $('#newsavingprice').empty();
                         $('#newsavingprice').html('You save &#8377;'+ JSON.parse(result).data.saving_price);
                         $('#newsavingprice').show();
                         $('#newsavingprice11').empty();
                         $('#newsavingprice11').html('You save &#8377;'+ JSON.parse(result).data.saving_price);
                         $('#newsavingprice11').show();
                     }
                     else{
                         $('#newsavingprice').hide();
                         $('#newsavingprice11').hide();
                     }
                 },
                 error: function(error){
                   console.log("error");
                 }
         
             });
         }
         function showAddress(customer_id){
             $.ajax({
                 url: "{{url('api/v1/user-address-list')}}",
                 data: {
                     user_id:customer_id,
                     type:'1'
                 },
                 type: 'GET',
                 success: function(result) {
                     console.log(result[0].status);
                    
                 },
                 error: function(error){
                   console.log("error");
                 }
             });
         }
         function editAddress(id,customer_id,address,postcode,name,mobile,email,landmark,area,city){
             
             $.ajax({
                 url: "{{url('api/v1/add-address')}}",
                 data: {
                     id:id,
                     user_id:customer_id,
                     address:address,
                     type:'1',
                     postcode:postcode,
                     name:name,
                     mobile:mobile,
                     email:email,
                     landmark:landmark,
                     area:area,
                     city:city,
                     isedit:1,
                     lat:null,
                     long:null
                 },
                 type: 'POST',
                 // dataType: 'json',
                 success: function(result) {
                  location.href="{{url('/checkout?isRedirect=true')}}";
                  $(".opacityclass").css({ 'background-color' : '', 'opacity' : '' });
                     return false;
                 },
                 error: function(error){
                   console.log("error");
                 }
             });
         }
         
         function addAddress(customer_id,address,postcode,name,mobile,email,landmark,area,city){
             $.ajax({
                 url: "{{url('api/v1/add-address')}}",
                 data: {
                     user_id:customer_id,
                     address:address,
                     type:'1',
                     postcode:postcode,
                     name:name,
                     mobile:mobile,
                     email:email,
                     landmark:landmark,
                     area:area,
                     city:city,
                     isedit:0,
                     lat:null,
                     long:null
                 },
                 type: 'POST',
                 // dataType: 'json',
                 success: function(result) {
                    result = JSON.parse(result)
                  if(result.status==0)
                  {
                     toastr.error("<span style='font-size:15px;font-weight:bold'>"+result.message+"</span>");
                  }else{
                     location.href="{{url('/checkout')}}";
                     $(".opacityclass").css({ 'background-color' : '', 'opacity' : '' });
                  }
                     return false;
                 },
                 error: function(error){
                 }
             });
         }

         function defaultAddres(address_id,e){
           // debugger;
          $.ajax({
               url: "{{url('defaultAddres')}}",
                 data: {
                     user_id : "{{Auth::user()->id}}",
                     address_id : address_id
                 },
                 type: 'GET',
                 success: function(result) {
                    // debugger;
                    $("#actusername").html(result.name);
                    $("#actaddress").html(result.address);
                    $("#actlandmark").html(result.landmark);
                    $("#actarea").html(result.area);
                    $("#actcity").html(result.city)
                    $("#actemail").html(result.email);
                    $("#actmobile").html(result.mobile);
                    // console.log(result);
                     return false;
                 },
            });
            
           var d = $('.newabc:visible').attr('id');
           var dn = $('.newabc1:visible').attr('id');
             $.ajax({
                 url: "{{url('api/v1/user-default-address')}}",
                 data: {
                     user_id:"{{Auth::user()->id}}",
                     address_id:address_id
                 },
                 type: 'GET',
                 success: function(result) {
                     $('#'+d).hide();
                     $('#'+dn).hide();
                     $('#radio'+address_id).prop('checked','true');
                     $('#newradio'+address_id).prop('checked','true');
                     
                     $('#defultadrs'+address_id).show();
                     $('#defultadrs1'+address_id).show();

                     $('.removeRt').show();
                     $('#deletAddress'+address_id).hide();
                     // $('#deletAddress'+address_id+1).show();
                    console.log(result);
                     return false;
                 },
                 error: function(error){
                   console.log("error");
                 }
             });
         }

         function checkout(address_id){
             $.ajax({
                 url: "{{url('api/v1/order')}}",
                 data: {
                     user_id:"{{Auth::user()->id}}",
                     address_id:address_id
                 },
             });
         }
         
         
         var userid="{{Auth::user()->id}}";
         showCart(userid);
         showAddress(userid);
         
         
      </script>
      <script type="text/javascript">
         $(document).ready(function(event){
           
            var vars = [], hash;
                    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                    for(var i = 0; i < hashes.length; i++)
                    {
                        hash = hashes[i].split('=');
                        vars.push(hash[0]);
                        vars[hash[0]] = hash[1];
                    }
                    var isRedirect = vars["isRedirect"];
                    if(isRedirect)
                    {
                        $('.nav-tabs a[href="#DeliveryDetails"]').parent("li").removeAttr('class');
                        $('#tab1').removeAttr('class');
                        $('#tab2').addClass('active');
                        $('#CartPreview').removeClass('tab-pane active');
                        $('#CartPreview').addClass('tab-pane');
                        $('#DeliveryDetails').removeAttr('class');
                        $('#DeliveryDetails').attr('class','tab-pane active');
                    }

             $('#frmAddSubmit').click(function(){
                 var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if($('#first_name').val()=='' || $('#address').val()=='' && $('#landmark').val()=='' && $('#area').val()=='' && $('#city').val()=='' && $('#postcode').val()=='' && $('#email').val()=='' && $('#mobile').val()==''){
                     $('#fnameerror').show();
                     $('#addresserror').show();
                     $('#landmarkerror').hide();
                     $('#areaerror').show();
                     $('#cityerror').show();
                     $('#postcodeerror').show();
                     $('#emailerror').show();
                     $('#mobileerror').show();
                   
                     //return false;
                }
               
                else if($('#address').val()==''){
                     $('#addresserror').show();
                     return false;
                }
                else if($('#area').val()==''){
                     $('#areaerror').show();
                     return false;
                }
                else if($('#city').val()==''){
                     $('#cityerror').show();
                     return false;
                }
                else if($('#postcode').val()==''){
                     $('#postcodeerror').show();
                     return false;
                }
               
                else if(!regex.test($('#email').val()))
                {
                    $("#emailvalid").show();
                    $('#emailerror').hide();
                    return false;
                }
              
                else if($('#mobile').val().length!=10){
                     $('#mobilvaliedeerror').show();
                     $('#mobileerror').hide();
                     return false;
                }
           
               
             
                else{
                     $('#fnameerror').hide();
                     $('#addresserror').hide();
                     $('#landmarkerror').hide();
                     $('#areaerror').hide();
                     $('#cityerror').hide();
                     $('#postcodeerror').hide();
                     $('#emailerror').hide();
                     $('#mobileerror').hide();
                     $("#emailvalid").hide();
                    
                     var userid="{{Auth::user()->id}}";
                 var address=$('#address').val();
                 var postcode=$('#postcode').val();
                 var name = $('#first_name').val();
                 var mobile=$('#mobile').val();
                 var email=$('#email').val();
                 var landmark=$('#landmark').val();
                 var area=$('#area').val();
                 var city=$('#city').val();
                
                 addAddress(userid,address,postcode,name,mobile,email,landmark,area,city);
                 //location.href="{{url('/checkout')}}";
                 return false;
                }
               
             });
             $('#frmAddUpdate').click(function(){
                 var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                 if($('#first_name').val()=='' || $('#address').val()=='' && $('#area').val()=='' && $('#city').val()=='' && $('#postcode').val()=='' && $('#email').val()=='' && $('#mobile').val()==''){
                     $('#fnameerror').show();
                     $('#addresserror').show();
                     $('#areaerror').show();
                     $('#cityerror').show();
                     $('#postcodeerror').show();
                     $('#emailerror').show();
                     $('#mobileerror').show();
                   
                     return false;
                }
                else if($('#address').val()==''){
                     $('#addresserror').show();
                     return false;
                }
                else if($('#area').val()==''){
                     $('#areaerror').show();
                     return false;
                }
                else if($('#city').val()==''){
                     $('#cityerror').show();
                     return false;
                }
                else if($('#postcode').val()==''){
                     $('#postcodeerror').show();
                     return false;
                }
                else if($('#email').val()==''){
                     $('#emailerror').show();
                     return false;
                }
                else if(!regex.test($('#email').val()))
                {
                    $("#emailvalid").show();
                    $('#emailerror').hide();
                    return false;
                }
                else if($('#mobile').val()==''){
                     $('#mobileerror').show();
                     return false;
                }
                else if($('#mobile').val().length!=10){
                     $('#mobilvaliedeerror').show();
                     $('#mobileerror').hide();
                     return false;
                }
                else
                {
         
         
                 $('#fnameerror').hide();
                 $('#addresserror').hide();
                 $('#landmarkerror').hide();
                 $('#areaerror').hide();
                 $('#cityerror').hide();
                 $('#postcodeerror').hide();
                 $('#emailerror').hide();
                 $('#mobileerror').hide();
                 $("#emailvalid").hide();
                
                 var userid="{{Auth::user()->id}}";
                 var id=$('#addrsid').val();
                 var address=$('#address').val();
                 var postcode=$('#postcode').val();
                 var name = $('#first_name').val();
                 var mobile=$('#mobile').val();
                 var email=$('#email').val();
                 var landmark=$('#landmark').val();
                 var area=$('#area').val();
                 var city=$('#city').val();
                
                
                 editAddress(id,userid,address,postcode,name,mobile,email,landmark,area,city);
                 // location.href="{{ url('/checkout') }}";
                 return false;
                }
                 
                });
            
             $('.subscribe').click(function(){
               
                 $('.nav-tabs a[href="#DeliveryDetails"]').parent("li").removeAttr('class');
                 $('#tab1').removeAttr('class');
                 $('#tab2').addClass('active');
                 $('#CartPreview').removeClass('tab-pane active');
                 $('#CartPreview').addClass('tab-pane');
                 $('#DeliveryDetails').removeAttr('class');
                 $('#DeliveryDetails').attr('class','tab-pane active');
                 //window.location.href = window.location.href +"?isRedirect=true";
                

             });
             
             $('#ordernow').click(function(){
            
                  if({{$getcntaddress}}==0){
                     alert('Please Add Address');
               }else
               {
                 $('.nav-tabs a[href="#PaymentMethod"]').parent("li").removeAttr('class');
                 $('#tab1').removeAttr('class');
                 $('#tab2').removeAttr('class');
                 $('#tab3').addClass('active');
                 $('#CartPreview').removeClass('tab-pane active');
                 $('#CartPreview').addClass('tab-pane');
                 $('#DeliveryDetails').removeAttr('class');
                 $('#DeliveryDetails').addClass('tab-pane');
                 $('#PaymentMethod').removeAttr('class');
                 $('#PaymentMethod').attr('class','tab-pane active');
               }
             });
            
             $('.cart-login').click(function(){
                 if({{$getcntaddress}}==0)
                 {
                     alert('Please Add Address');
                 }else
                 {
                  
                             $.ajax({
                                 url: "{{url('api/v1/order')}}",
                                 data: {
                                     alias:$('#hiname').val(),
                                     mobile:'{{ Auth::user()->mobile }}',
                                     mail:$('#hidmail').val(),
                                     zip:$('#hidzips').val(),
                                     street: $('#hidfulladrss').val()+$('#hidlandmark').val()+$('#hidstreet').val()+$('#hidcity').val()+$('#hidzips').val(),
                                     full_address: "",
                                     payment:$('#paymentmethod').val(),
                                     customer_id : '{{Auth::user()->id}}',
                                     total_gst:'0',
                                     user_id:'{{Auth::user()->id}}',
                                     address_id:$('#hidaddressid').val() 
                                 },
                                 type: 'POST',
                                 dataType: 'json',
                                 success: function(result) {
                                     
                                     if(result!="")
                                     {
                                         if(result.message =="Success")
                                         {
                                             
                                             toastr.success("<span style='font-size:15px;font-weight:bold'>"+result.data+"</span>");
                                             location.href="{{url('/')}}";
                                         }
                                         
                                         console.log(result);
                                     }
                                     return false;
                                 },
                                 error: function(error){
                                 }
                     });
                     return false;
                 }
             });
         });
         
         
         $('#postcode').on('input', function (event) {
           this.value = this.value.replace(/[^0-9]/g, '');
         });
         $('#mobile').on('input', function (event) {
           this.value = this.value.replace(/[^0-9]/g, '');
         });
         
           
      </script>
      @endif
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
      <script src="{{ url('css/front/js/jquery-1.12.4.min.js') }}"></script>
      <script src="{{ url('css/front/js/bootstrap.min.js') }}"></script>
      <script src="{{ url('css/front/js/webslidemenu.js') }}"></script>
      <script src="{{ url('css/front/js/wow.js') }}"></script>
      <script src="{{ url('css/front/js/owl.carousel.min.js') }}"></script>
      <script src="{{ url('css/front/js/jquery.themepunch.plugins.min.js') }}"></script>
      <script src="{{ url('css/front/js/jquery.themepunch.revolution.min.js') }}"></script>
      <script src="{{ url('css/front/js/function.js') }}"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
   </body>
</html>