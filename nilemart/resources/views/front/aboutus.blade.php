<!doctype html>
<html lang="en">

<head>
    <title>Nilemart | About Us</title>
    @include('layouts.front.head')
</head>

<body>
    <div class="main-div">
        @include('layouts.front.header-cart')
    <section class="about-us-main">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h3 class="product-head">About Us</h3>
                    <hr class="product-head">
                    <p>India's most convenient online grocery channel</p>
                    <p>Nilemart Fresh and Smart makes your grocery shopping even simpler. No more hassles of sweating it out in crowded markets, grocery shops & supermarkets - now shop from the comfort of your home; office or on the move.</p>
                    <p>We offer you convenience of shopping everything that you need for your home - be it fresh fruits & vegetables, rice, dals, oil, packaged food, dairy item, frozen, pet food, household cleaning items & personal care products from a single virtual store</p>
                </div>
                <div class="col-md-4 text-center">
                    <div class="about-image">
                        <img src="{{url('css/front/images/about-us-icon.png')}}" alt="about">
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('layouts.front.footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="{{ url('css/front/js/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ url('css/front/js/bootstrap.min.js') }}"></script>
    <script src="{{ url('css/front/js/webslidemenu.js') }}"></script>
    <script src="{{ url('css/front/js/wow.js') }}"></script>
    <script src="{{ url('css/front/js/owl.carousel.min.js') }}"></script>
    <script src="{{ url('css/front/js/jquery.themepunch.plugins.min.js') }}"></script>
    <script src="{{ url('css/front/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ url('css/front/js/function.js') }}"></script>    
</body>

</html>