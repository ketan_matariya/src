<!doctype html>
<html lang="en">
   <head>
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      @include('layouts.front.head')
   </head>
   <body>
      <div class="main-div">
      @include('layouts.front.header-cart')
      <!-- banner section start -->
      <section class="banner-section">
         <div id="carouselExampleCaptions" class="carousel slide" autoplay="true" data-ride="carousel">
            <div class="carousel-inner">
               @if(!$offers->isEmpty())
               @foreach($offers as $offer)
               @if ($loop->first)
               <div class="carousel-item active">
                  @else
                  <div class="carousel-item">
                     @endif
                     <img src="{{ $offer->cover }}" class="d-block w-100" alt="banner image">
                  </div>
                  @endforeach
                  @else
                  <div class="col-12">
                     <div class="empty-message">
                        <img src="{{url('css/front/images/no-product.gif')}}" alt="empty product">
                        <p>Offer not available.</p>
                     </div>
                  </div>
                  @endif
               </div>
               <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
               <span class="carousel-control-prev-icon" aria-hidden="true"></span>
               <span class="sr-only">Previous</span>
               </a>
               <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
               <span class="carousel-control-next-icon" aria-hidden="true"></span>
               <span class="sr-only">Next</span>
               </a>
            </div>
      </section>
      
      <!-- banner section end -->
      <!-- offer section start -->
      <section class="offer-section">
      <div class="container">
      <h3 class="text-orange product-head">Offers on daily essentials</h3>
      <hr class="product-head ">
      <div class="row space-margin">
      @foreach($offerProduct as $product)
      <div class="col-lg-4 col-md-4 col-4 padingnewspacing">
      <a class="product-img" href="{{route('productDetail.product',$product->id)}}">
      <div class="offer-single-product">
      <img src="{{ $product->cover }}" class="img-fluid" alt="{{$product->name}}">
      <h4 class="product-title">{{ $product->name }}</h4>
      <!--<span class="special-ofr">UP TO {{$product->discount}}% OFF</span>-->
      </div>
      </a>
      </div>
      @endforeach
      @if(count($categories)>0)
      @foreach($categories as $cat)
      <div class="col-lg-4 col-md-4 col-4 padingnewspacing">
      <div class="offer-single-product">
      <a class="product-img" href="{{route('productCategoryListingCat.product',$cat->id)}}">
      <img src="{{$cat->cover}}" class="img-fluid" alt="{{$cat->name}}">
      <h4 class="product-title">{{$cat->name}}</h4>
      </a>
      </div>
      </div>
      @endforeach 
      @endif
      </div>
      </div>
      </section>
      <!-- category section start -->
      <section class="fine-jewellery product">
      <div class="container">
      <h3 class="text-orange product-head">Category</h3>
      <hr class="product-head">
      <div class="single-product mb-3">
      <div class="row">
      @if(!$categories->isEmpty())
      @foreach($categories as $cat)
      <div class="col-lg-4 col-md-4 col-4 padingnewspacing ">
      <li>
      <figure>
      @if($cat->children->isEmpty())
      <a class="product-img" href="{{route('productCategoryListingCat.product',$cat->id)}}"><img src="{{$cat->cover}}" class="img-fluid" alt="polo shirt img"></a>
      <a class="add-card-btn justify-content-center" href="{{route('productCategoryListingCat.product',$cat->id)}}">View More</a>

      @else
      <a class="product-img" href="
         {{route('productSubCategory.product',$cat->id)}}"><img src="{{$cat->cover}}" class="img-fluid" alt="polo shirt img"></a>
      <a class="add-card-btn justify-content-center" href="{{route('productSubCategory.product',$cat->id)}}">View More</a>

      @endif
    
      <figcaption>
      <h4 class="product-title"><a href="#">{{$cat->name}}</a></h4>
      </figcaption>
      </figure>
      </li>
      </div>
      @endforeach
      @else
      <p>category not availble.</p>
      @endif
      </div>
      </div>
      </div>
      </section>
      <!-- category section end -->
      <!-- product section start -->
      <section class="product">
      <div class="container">
      <h3 class="text-orange product-head">Products </h3>
      <hr class="product-head">
      <div class="single-product">
      <div class="row">
         
      @if(!empty($products))
      
      @foreach($products as $product)
      @php
      $proDB = DB::table('product_attribute')->leftjoin('products','products.id','product_attribute.product_id')->select('product_attribute.*')->orderBy('product_attribute.id', 'ASC')->where('product_id',$product->id)->get();
      $proNameDB = DB::table('product_attribute')->leftjoin('products','products.id','product_attribute.product_id')->select('product_attribute.*')->where('product_id',$product->id)->first();
      @endphp
      <div class="col-lg-3 col-md-4 col-6 padingnewspacing">
      <li>
      <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
      <figure>
      <a class="product-img" href="{{route('productDetail.product',$product->id)}}">
      <img src="{{ $product->cover }}" class="img-fluid" alt="{{$product->name}}"></a>
      <figcaption>
      <h4 class="product-title">
         
      <a href="#">{{ $product->name }} </a>
      <span id="priceUnit{{$product->id}}" class="product-price font-weight-bold">&#8377;{{$product->sale_price}}</span>
      <input type="hidden" name="getproductid" id="getproductid" value="{{$product->id}}"/>
      <input type="hidden" id="pro_price{{$product->id}}" name="pro_price" value="{{$product->sale_price}}">
      <input type="hidden" id="attributeid{{$product->id}}" name="attributeid" value="{{$product->attribute_id}}">
      <input type="hidden" id="user_id" name="user_id" value="@if(!auth()->user()) 0 @else{{auth()->user()->id}}@endif">
      <input type="hidden" id="customer_id" name="customer_id" value="@if(!auth()->user()) 0 @else{{auth()->user()->id}}@endif">
      <input type="hidden" id="pro_sellprice{{$product->id}}" name="pro_sellprice" value="{{$product->sale_price}}">
      <input type="hidden" id="max_quantities{{$product->id}}" name="max_quantities" value="{{$product->max_quantities}}">
      <input type="hidden" id="hidavailable_quantity{{$product->id}}" name="available_quantity" value="{{$product->available_quantity}}"/>
      <input type="hidden" id="dropdwn" name="dropdwn" value="0">

      <input type="hidden" id="productquantity{{$product->id}}" name="" value="{{$product->quantity}}">
     
      </h4>
      <div class="prod-form form-group w-100">
      <select class="form-control newvariantprdct" id="product_id{{$product->id}}" name="product_id{{$product->id}}">
      @foreach($proDB as $pDB)
        <option class="pid" value="{{$pDB->id}},{{$product->id}},{{$pDB->max_quantities}},{{$pDB->available_quantity}}" >{{$pDB->value}} {{$pDB->key}}</option>
      @endforeach
      </select>
      </div>
      <div class="add-to-cart-div mt-3">
      <div class="qtn-class">
         @if($login=='true')
            <div id="Increas{{$product->id}}" class="Increas{{$product->id}}" style="display: none">
               <button  id="{{$product->id}}" pluse_minus="hidminus_sine"  class="btnpm" >
                  <i class="fa fa-minus" aria-hidden="true"></i>
               </button>
                  <input type="text" class="number-input" id="quantity{{$product->id}}" name="quantity" value="1" readonly>
                  <input type="hidden" class="number-input" id="productid" name="productid" value="{{$product->id}}" readonly>
               <button id="{{$product->id}}" class="btnpm" pluse_minus="hiplus_sine">
                  <i class="fa fa-plus" aria-hidden="true"></i>
               </button>
            </div>
         @endif 
      </div>
      @if($product->quantity==0)
      <script>
         $(document).ready(function(){
            (function(){
                $("#Increas{{$product->id}}").hide();
                $("#addButton{{$product->id}}").show();
                $(".outeof").show();
            })();
         });
       </script>
       @else
       <script>
         $(document).ready(function(){
            (function(){
                $("#Increas{{$product->id}}").show();
                $("#addButton{{$product->id}}").hide();
                var pro_sellprice = $("#pro_sellprice{{$product->id}}").val();
                document.getElementById('quantity{{$product->id}}').value = '{{$product->quantity}}';
                newsale =parseFloat(pro_sellprice)*parseInt('{{$product->quantity}}');
                $('#priceUnit{{$product->id}}').html('&#8377; ' +  newsale.toFixed(2));
                $(".outeof").show();
            })();
         });
       </script>
       @endif
      <div class="add-button">
         @if($login=='true')
           @if($product->available_quantity!=0)
           
                  <button  style="display: none" class="postaddtocart" productid="{{$product->id}}" id="addButton{{$product->id}}" ><i class="fa fa-plus mr-2" aria-hidden="true"></i>Add</button>
                  <span class="text-danger font-weight-bold" style="display: none"  id="outofstock{{$product->id}}">Out Of Stock </span>
                  @else
                  <span class="text-danger font-weight-bold outeof" style="display: none"  id="outofstock{{$product->id}}">Out Of Stock </span>
                
            @endif
            @if(isset($quantity))
             
            @foreach($quantity as $cart)
            <script>
               (function(){
                  $("#Increas{{$cart->product_id}}").show();
                  $("#addButton{{$cart->product_id}}").hide();
                  $("#quantity{{$cart->product_id}}").val({{$cart->quantity}});
               })();
            </script>
            <script>
               $("#pre-loader").hide();
               $('#addButton{{$cart->product_id}}').click(function(){
                  $("#pre-loader").show();
               });
            </script>
            @endforeach
            @else
            <script>
               (function(){
                   $("#Increas{{$product->id}}").show();
                   $("#addButton{{$product->id}}").hide();
               })();
              
            </script>
            @endif
            @else
               <a id="atc" class="buttonCart " href="{{url('user/login')}}"><span> Add <i class="fa fa-plus add ml-2"></i></span> </a>
            @endif
      </div>
      </div>
      </figcaption>
      </figure>
      <!-- product badge -->
      <span class="badge sale" href="#">
      <!--<img src="{{url('css/front/images/sale.png')}}" alt="">-->
      </span>
      </li>
      </div>
      @endforeach
      @else
      <div class="col-12">
      <div class="empty-message">
      <img src="{{url('css/front/images/no-product.gif')}}" alt="empty product">
      <p>Product not available.</p>
      </div>
      </div>
      @endif 
      </div>
      </div>
      </div>
      </section>
      <!-- product section end -->
      @include('layouts.front.footer')
      </div>
      <!-- login modal content start -->
      <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-body login-bg">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <div class="row m-0">
                     <div class="col-md-5">
                     </div>
                     <div class="col-md-7">
                        <div class="login-right">
                           <h4 class="text-orange login-title font-weight-bold">Login</h4>
                           <form class="login-form" action="">
                              <div class="form-group">
                                 <input type="email" placeholder="Email address" class="form-control" id="exampleInputEmail1">
                              </div>
                              <div class="form-group">
                                 <input type="password" placeholder="Password" class="form-control" id="exampleInputPassword1">
                              </div>
                              <button type="submit" class="btn btn-primary my-1">Submit</button>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- login modal content end -->
      <!-- register modal content start -->
      <div class="modal fade" id="register-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-body login-bg">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <div class="row m-0">
                     <div class="col-md-5">
                     </div>
                     <div class="col-md-7">
                        <div class="login-right">
                           <h4 class="text-orange login-title font-weight-bold">Register</h4>
                           <form class="login-form" action="">
                              <div class="form-group">
                                 <input type="email" placeholder="Email address" class="form-control" id="exampleInputEmail1">
                                 <span class="error"></span>
                              </div>
                              <div class="form-group">
                                 <input type="text" name="phone" class="form-control" placeholder="Phone Number" id="phone">
                              </div>
                              <div class="form-group">
                                 <input type="password" placeholder="Password" class="form-control" id="exampleInputPassword1">
                                 <span class="error"></span>
                              </div>
                              <div class="form-group">
                                 <input type="password" class="form-control" placeholder="Confirm Password" name="passwordCon" id="passwordCon" class="passConfirm">
                                 <span class="error"></span>
                              </div>
                              <button type="submit" class="btn btn-primary my-1">Submit</button>
                              <!-- <div class="register-now">
                                 Don't have an account?<a href="javascript:void(0)" > Register now!</a>
                                 </div> -->
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- register modal content end -->
      <div class="modal fade video-model" id="myModal" role="dialog">
         <div class="modal-dialog modal-xl">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">sol</h4>
               </div>
               <div class="modal-body">
                  <video id="vpopup-autoplay" loop autoplay controls width="100%" height="auto" preload="none">
                     <source src="video/showcase-ring-jewellery-video.mp4">
                  </video>
               </div>
            </div>
         </div>
      </div>
      <div class="modal fade login-signup" id="LoginSignup" role="dialog">
         <div class="modal-dialog modal-lg" style="overflow: hidden;">
            <!-- Modal content-->
            <div class="modal-content">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <div class="modal-body">
                  <section id="formHolder">
                     <div class="row">
                        <!-- Brand Box -->
                        <div class="col-sm-6 brand">
                           <div class="heading">
                              <img src="{{url('css/front/images/main-logo.svg')}}" alt="">
                           </div>
                           <div class="success-msg">
                              <p>Great! You are one of our members now</p>
                              <a href="#" class="profile">Your Profile</a>
                           </div>
                        </div>
                        <!-- Form Box -->
                        <div class="col-sm-6 form">
                           <!-- Login Form -->
                           <div class="login form-peice switched">
                              <form class="login-form" action="#" method="post">
                                 <div class="form-group">
                                    <label for="loginemail">Email Adderss</label>
                                    <input type="email" name="loginemail" id="loginemail" required>
                                 </div>
                                 <div class="form-group">
                                    <label for="loginPassword">Password</label>
                                    <input type="password" name="loginPassword" id="loginPassword" required>
                                 </div>
                                 <div class="CTA">
                                    <input type="submit" value="Login">
                                    <a href="#" class="switch">I'm New</a>
                                 </div>
                              </form>
                           </div>
                           <!-- End Login Form -->
                           <!-- Signup Form -->
                           <div class="signup form-peice">
                              <form class="signup-form" action="#" method="post">
                                 <div class="form-group">
                                    <label for="name">First Name</label>
                                    <input type="text" name="FirstName" id="FirstName" class="name">
                                    <span class="error"></span>
                                 </div>
                                 <div class="form-group">
                                    <label for="name">Last Name*</label>
                                    <input type="text" name="LastName" id="LastName*" class="name">
                                    <span class="error"></span>
                                 </div>
                                 <div class="form-group">
                                    <label for="email">Email Adderss</label>
                                    <input type="email" name="emailAdress" id="email" class="email">
                                    <span class="error"></span>
                                 </div>
                                 <div class="form-group">
                                    <label for="phone">Phone Number </label>
                                    <input type="text" name="phone" id="phone">
                                 </div>
                                 <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" id="password" class="pass">
                                    <span class="error"></span>
                                 </div>
                                 <div class="form-group">
                                    <label for="passwordCon">Confirm Password</label>
                                    <input type="password" name="passwordCon" id="passwordCon" class="passConfirm">
                                    <span class="error"></span>
                                 </div>
                                 <div class="CTA">
                                    <input type="submit" value="Signup Now" id="submit">
                                    <a href="#" class="switch">I have an account</a>
                                 </div>
                              </form>
                           </div>
                           <!-- End Signup Form -->
                        </div>
                     </div>
                  </section>
               </div>
            </div>
         </div>
      </div>
      <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
      <script src="{{ url('css/front/js/jquery-1.12.4.min.js') }}"></script>
      <script src="{{ url('css/front/js/bootstrap.min.js') }}"></script>
      <script src="{{ url('css/front/js/webslidemenu.js') }}"></script>
      <script src="{{ url('css/front/js/wow.js') }}"></script>
      <script src="{{ url('css/front/js/owl.carousel.min.js') }}"></script>
      <script src="{{ url('css/front/js/jquery.themepunch.plugins.min.js') }}"></script>
      <script src="{{ url('css/front/js/jquery.themepunch.revolution.min.js') }}"></script>
      <script src="{{ url('css/front/js/function.js') }}"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
      <script type="text/javascript">
   $(".btnpm").click(function(){
      var product_id = $(this).attr('id');
      var div_id = $(this).attr('pluse_minus');
      var sale_price = $("#pro_price"+product_id).val();
      var attributeid = $("#attributeid"+product_id).val();
      var pro_sellprice = $("#pro_sellprice"+product_id).val();
      var quantity = document.getElementById('quantity'+product_id).value;
      var sale_price = $("#pro_sellprice"+product_id).val();
      var total_price = quantity*sale_price;
      var cartid = $("#cartId"+product_id).val();
      @if(null !==Auth::user())
             {
                 var userid="{{Auth::user()->id}}";
                 var customer_id = "{{Auth::user()->id}}";
             }
       @endif
       var value = parseInt(document.getElementById('quantity'+product_id).value, 10);
       
       value = isNaN(value) ? 0 : value;
       if(div_id=='hidminus_sine')
       {
         if(value == 1)
         {
           
            $.ajax({
               url: "{{url('api/v1/addCart')}}",
                data: {
                   user_id: userid,
                   customer_id: customer_id,
                   product_id: product_id,
                   quantity: 0,
                   total_price: total_price,
                   attribute_id: attributeid,
               },
               type: 'POST',
               dataType: 'json',
               success: function(result) { 
                 
                   if(result!="")
                   {
                     showCart(userid,customer_id);
                    $("#addButton"+product_id).show();
                    $("#Increas"+product_id).hide();
                     
                   }
               },
               error: function(error){
                 console.log("error");
               }
             });
             
         }else{
            value--;
            plus_minus ="-";
         }
         
       }else
       {
          
         value++;
         plus_minus ="+";
       }
       var max_quantities=  $("#max_quantities"+product_id).val();
            var available_quantity = $("#hidavailable_quantity"+product_id).val();
            if(value<=available_quantity)
            {
            if(value<=max_quantities)
            {
                  document.getElementById('quantity'+product_id).value = value;
                  newsale = pro_sellprice*value;
                  $('#priceUnit' + product_id).html('&#8377; ' +  newsale.toFixed(2));
                  var max_quantities=  $("#max_quantities"+product_id).val();
                  Updatequantity(cartid,value,product_id,userid,userid,attributeid,total_price,plus_minus);
            }
            else
            {
                document.getElementById('quantity'+product_id).value = max_quantities;
                toastr.options = {
                          "preventDuplicates": true,
                           "preventOpenDuplicates": true
                     };
                toastr.error ("<span style='font-size:15px;font-weight:bold'>You cannot add more items. </span>");
            }
          }else
          {
                document.getElementById('quantity'+product_id).value = available_quantity;
                toastr.options = {
                          "preventDuplicates": true,
                           "preventOpenDuplicates": true
                     };
                toastr.error ("<span style='font-size:15px;font-weight:bold'>Product Not Available </span>");
          }

      

   });


     function Updatequantity(cartid,quantity,product_id,customer_id,user_id,attribute_id,total_price,plus_minus){
            var max_quantities=  $("#max_quantities"+product_id).val();
           
         if(quantity<=max_quantities)
         {
           $.ajax({
               url: "{{url('api/v1/addCart')}}",
               data: {
                 user_id: user_id,
                 customer_id: customer_id,
                 product_id: product_id,
                 quantity: quantity,
                 total_price: total_price,
                 attribute_id: attribute_id,
                 cart_id:cartid,
               },
               type: 'POST',
               success: function(result) {
                 console.log("addtocart",result);
               },
               error: function(error){
                 console.log("error");
               }
             });
         }
         else
         {
            
            $("#quantity"+product_id).val(max_quantities);
            toastr.options = {
                          "preventDuplicates": true,
                           "preventOpenDuplicates": true
                     };
           toastr.error ("<span style='font-size:15px;font-weight:bold'>You cannot add more items. </span>");
         }
         }
   $(document).ready(function(){
      var CSRF_TOKEN = $('meta[name="csrf-token"]');
    $(".newvariantprdct").on("change",function(){
       
       var id = $(this).children("option:selected").val();
       var product_id = id.split(",")[1];
       var attribute_id = id.split(",")[0];
       var max_quantities = id.split(",")[2];
       var available_quantity = id.split(",")[3];
       var customer_id = $("#customer_id").val();
       $("#attributeid"+product_id).val(attribute_id);
       $("#max_quantities"+product_id).val(max_quantities);
       $("#hidavailable_quantity"+product_id).val(available_quantity);
      $.ajax({
              type: "get",
              url: "{{url('productCheckcart')}}",
              data:{
                     customer_id:customer_id,
                     product_id: product_id,
                     attribute_id:attribute_id,
              },
              success: function(result){
                if(result!=""){
                   if(result == 1)
                   {
                      if(available_quantity==0)
                      {
                        $("#Increas"+product_id).hide();
                       $("#addButton"+product_id).hide();
                       $("#quantity"+product_id).val(1);
                       $("#outofstock"+product_id).show();
                      }else{
                           $("#Increas"+product_id).hide();
                           $("#addButton"+product_id).show();
                           $("#quantity"+product_id).val(1);
                           $("#outofstock"+product_id).hide();
                      }
                     
                   }else{
                     if(result.attribute_id ==  attribute_id )
                           {
                             $("#Increas"+product_id).show();
                             $("#quantity"+product_id).val(result.quantity);
                             $("#addButton"+product_id).hide();
                             $("#outofstock"+product_id).hide();
                           }
                   }
                }
              }
      });
         $.ajax({
                url: 'SelectUnitPrice/' + id.split(",")[0],
                method: 'get',
                success: function(data) {
                   $('#priceUnit'+product_id).html('&#8377;' + data[0].sale_price);
                   $('#pro_sellprice'+product_id).val(data[0].sale_price);
                   var quantity = $("#quantity"+product_id).val()
                   var pro_sellprice = $("#pro_sellprice"+product_id).val();
                   document.getElementById('quantity'+product_id).value = quantity;
                   newsale = parseFloat(pro_sellprice)*parseInt(quantity);
                   $('#priceUnit'+product_id).html('&#8377; ' +  newsale.toFixed(2));
                   var quantity = $("#quantity"+product_id).val();
                }
             });
    });
   });
   
      </script>
      <!-- main slider js -->
      <script>
         var revapi;
         $(document).ready(function() {
            setTimeout(function() {}, 0);
         
            revapi = jQuery('.fullwidthbanner').revolution({
         
               delay: 7000,
               startwidth: 1920,
               startheight: 776,
               hideThumbs: 10,
         
               thumbWidth: 100,
               thumbHeight: 50,
               thumbAmount: 5,
         
               navigationType: "both",
               navigationArrows: "solo",
               navigationStyle: "round",
         
         
               touchenabled: "on",
               onHoverStop: "on",
         
               navigationHAlign: "center",
               navigationVAlign: "bottom",
               navigationHOffset: 0,
               navigationVOffset: 170,
         
               soloArrowLeftHalign: "left",
               soloArrowLeftValign: "center",
               soloArrowLeftHOffset: 20,
               soloArrowLeftVOffset: 0,
         
               soloArrowRightHalign: "right",
               soloArrowRightValign: "center",
               soloArrowRightHOffset: 20,
               soloArrowRightVOffset: 0,
         
               shadow: 0,
               fullWidth: "on",
               fullScreen: "off",
         
               stopLoop: "off",
               stopAfterLoops: -1,
               stopAtSlide: -1,
         
         
               shuffle: "off",
         
               autoHeight: "on",
               forceFullWidth: "off",
               sliderLayout: 'fullwidth',
         
               hideThumbsOnMobile: "off",
               hideBulletsOnMobile: "off",
               hideArrowsOnMobile: "off",
               hideThumbsUnderResolution: 0,
         
               hideSliderAtLimit: 0,
               hideCaptionAtLimit: 768,
               hideAllCaptionAtLilmit: 0,
               startWithSlide: 0,
               videoJsPath: "http://www.themepunch.com/codecanyon/revolution_wp/plugins/revslider/rs-plugin/videojs/",
               fullScreenOffsetContainer: ""
               // autoplay: 'true'
            });
         
            var vid1 = null;
         
            vid1 = document.getElementById("v-autoplay");
            vid1.autoplay = true;
            vid1.load();
         
            setTimeout(function() {
               vid1 = document.getElementById("v-autoplay");
               vid1.autoplay = true;
               vid1.load();
            }, 1000);
         
            revapi.bind('revolution.slide.onchange', function(e, data) {
               if (data.slideIndex == 2) {
                  vid1 = document.getElementById("v-autoplay");
                  vid1.autoplay = true;
                  vid1.load();
               }
            });
         
            var vid2 = null;
            $('#myModal').on('show.bs.modal', function() {
               vid2 = document.getElementById("vpopup-autoplay");
               vid2.autoplay = true;
               vid2.load();
            });
         
         });
         //ready
      </script>
      <script type="text/javascript">
         var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

         $(".postaddtocart").click(function(){
            $(".loading").show();
            var product_id = $(this).attr('productid');
             var user_id = $("#user_id").val();
             var customer_id = $("#customer_id").val();
             var quantity = $("#quantity"+product_id).val();
             var price=$('#pro_sellprice'+product_id).val();
             var total_price = $('#pro_sellprice'+product_id).val();
             var attribute_id = $("#attributeid"+product_id).val();
             var varntproduct_id = $('select#product_id'+product_id).val();
            getCartData(user_id,customer_id,product_id,quantity,total_price,attribute_id);
             function getCartData(user_id,customer_id,product_id,quantity,total_price,attribute_id){
                 var total_price = total_price;
               var newtotal_price = total_price.replace(",","");
               
                 $.ajax({
                    
                   url: "{{url('api/v1/addCart')}}",
                   data: {
                     user_id: user_id,
                     customer_id: customer_id,
                     product_id: product_id,
                     quantity: quantity,
                     total_price: newtotal_price,
                     attribute_id: varntproduct_id.split(",")[0],
                   },
                   type: 'POST',
                   success: function(result) {
                     $(".loading").hide();
                      if(result!="")
                      {
                         $("#Increas"+product_id).show();
                        $("#addButton"+product_id).hide();
                        showCart(user_id,customer_id);
                         var msg = JSON.parse(result);
                         toastr.options = {
                          "preventDuplicates": true,
                           "preventOpenDuplicates": true
                          };
                         toastr.success("<span style='font-size:15px;font-weight:bold'>"+msg.message+"</span>");
                      }
                   },
                   error: function(error){
                     $(".loading").hide();
                   }
                 });
             }
         });

         function showCart(user_id,customer_id){
                 $.ajax({
                   url: "{{url('api/v1/showCart')}}",
                   data: {
                      user_id: user_id,
                      customer_id: customer_id,
                   },
                   type: 'GET',
                   dataType: 'json',
                   success: function(result) {
                       if(result!="")
                       {
                           var count=(result.data.carts === 'undefined')?0:result.data.carts.length;  
                           $("#items").html(count);     
                       }
                   },
                   error: function(error){
                     console.log("error");
                   }
                 });
             } 

             
      </script>
      <!-- main slider js -->
   </body>
</html>