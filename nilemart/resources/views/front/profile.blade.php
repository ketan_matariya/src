<?php
    $id = Auth::user()->id;
?>
<!doctype html>
<html lang="en">

<head>
    <title>NileMart | Profile</title>
    @include('layouts.front.head')
</head>
<style>
body {font-family: Arial, Helvetica, sans-serif;}

/* The Modal (background) */
.modal1 {
    display: none;
    position: fixed;
    z-index: 1111;
    padding-top: 150px;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    overflow: auto;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0,0.4);
}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 20%;
    position: absolute;
    right: 0;
    top: 0;
    z-index: 7;
    height: 100%;
}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}
@media only screen and (max-width: 768px) {
    .modal-content {
        width: 50% !important;
    }    
}
</style>
<body>
    <div class="main-div">
        @include('layouts.front.header-cart')

        <section class="profile-main opacityclass">
            <div class="container">
                <h3 class="text-orange product-head">My Account</h3>
                <hr class="product-head">
                <div class="row">
                    <div class="col-md-6">
                        <div class="left-profile">
                            <div class="profile-pic">
                                <img src="{{url('css/front/images/default-user.jpg')}}" class="img-fluid" alt="profile">
                            </div>
                            <div class="name-info">
                                <h4>@if(!auth()->user())User name @else{{auth()->user()->name}}@endif</h4>
                                <p>@if(!auth()->user())  @else{{auth()->user()->email}}@endif</p>
                                <p>@if(!auth()->user())  @else{{auth()->user()->mobile}}@endif</p>
                            </div>
                            <div class="row mt-3">
                                <div class="col-6">
                                    <button class="profile-left-button disablebutton"><i class="fa fa-credit-card mr-3" aria-hidden="true"></i> Payment Method</button>
                                </div>
                                <div class="col-6">
                                  <a href="{{route('orders')}}"><button class="profile-left-button disablebutton" id="orderhistory"><i class="fa fa-history mr-3 " aria-hidden="true"></i>Order History</button></a>
                                </div>
                                <div class="col-12">
                                    <button class="profile-left-button disablebutton" onclick="openEditModal()" id="{{$address->id??''}}"><i class="fa fa-map-marker mr-3"  aria-hidden="true"></i>Delivary Address</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="user-info">
                            <h3 class="text-orange">Account Information<i class="fa fa-edit disablebutton" style="font-size:36px; color: #000; margin-left: 90px;" id="editProfile"></i></h3>
                            <hr>
                            <div class="row">
                                <div class="col-6">
                                    <span>Full Name</span>
                                    <p>@if(!auth()->user())User name @else{{auth()->user()->name}}@endif</p>
                                </div>
                                <div class="col-6">
                                    <span>Phone No.</span>
                                    <p>@if(!auth()->user()) @else{{auth()->user()->mobile}}@endif</p>
                                </div>
                                <div class="col-6">
                                    <span>Email</span>
                                    <p>@if(!auth()->user())  @else{{auth()->user()->email}}@endif</p>
                                </div>
                                <div class="col-6">
                                    <span>Address</span>
                                    <p>@if(isset($address)) @if(!empty($address)) {{$address->address}} <br>{{$address->landmark}} <br> {{$address->area}} </br> {{$address->city}} @endif @endif
                                    </p>    
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mt-4">
                        <div class="user-info">
                            <ul>
                                <li>
                                    <a href="">My List</a>
                                </li>
                                <li>
                                    <a href="">Legal Information</a>
                                </li>
                                <li>
                                    <a href="{{route('tearmcondition')}}">Tearm & Condition</a>
                                </li>
                                <li>
                                    <a href="{{route('privacypolicy')}}">Privacy Policy</a>
                                </li>
                                <li>
                                    <a href="{{route('faq')}}">FAQs</a>
                                </li>
                                <li>
                                    <a href="{{route('contactus')}}">Contact us</a>
                                </li>
                                <li>
                                    <a href="{{url('logout')}}">Log Out</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div id="myModal" class="modal1">


                        <!-- Modal content -->
                        <div class="modal-content">
                          <li class="text-right">
                                  <a href="javascript:void(0)" class="closebtn" onclick="closeAddModal()">×</a>
                            </li>
                            <form class="editprofile-form" action="{{url('editProfile',$id)}}" method="post">
                              <h1> Edit Profile </h1>
                              <p>
                              </p>
                              <p>
                                
                              </p>
                                @csrf
                                <div class="form-group">
                                    <label for="fullname" class="clr-txt">Full Name</label>
                                    <input type="text" name="fullname" id="fullname" class="txt_area" value="@if(!auth()->user())User name @else{{auth()->user()->name}}@endif" required>
                                </div>

                                <div class="form-group">
                                    <label for="useremail" class="clr-txt">Email Adderss</label>
                                    <input type="email" name="useremail" id="useremail" class="txt_area1" value="@if(!auth()->user())  @else{{auth()->user()->email}}@endif" required>
                                </div>

                                <div class="form-group">
                                    <label for="useremail" class="clr-txt">Address</label>
                                    <input type="text" name="address" id="fullname" class="txt_area2">
                                </div>

                                <div class="form-group">
                                    <label for="phonenumber" class="clr-txt" >Phone No</label>
                                    <input type="text" class="form-control" name="phonenumber" id="phonenumber" placeholder="Phone No" maxlength="10" minlength="7" value="{{Auth::user()->mobile}}" disabled="true">
                                </div>

                                <div class="edit">
                                    <input type="submit" value="Submit">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- <section class="product-listing">
            <div class="container">
                <h3 class="text-orange product-head">View More Product</h3>
                <hr class="product-head">
                <div class="single-product">
                    <div class="row">
                        @if(!$products->isEmpty())
                        @foreach($products as $product)
                        <div class="col-lg-3 col-md-4 col-6">
                            <li>
                                <figure>
                                    <a class="product-img" href="{{route('productDetail.product',$product->id)}}"><img src="{{ $product->cover }}" class="img-fluid" alt="{{$product->name}}"></a>
                                    <a class="add-card-btn" href="{{route('productDetail.product',$product->id)}}">View More</a>
                                    <figcaption>
                                        <h4 class="product-title"><a href="{{route('productDetail.product',$product->id)}}">{{ $product->name }}</a></h4>
                                    </figcaption>
                                </figure>
                                <span class="badge sale" href="#"><img src="{{url('css/front/images/sale.png')}}" alt=""></span>
                            </li>
                        </div>
                        @endforeach
                        @else
                        <div class="col-12">
                            <div class="empty-message">
                                <img src="{{url('css/front/images/no-product.gif')}}" alt="empty product">
                                <p>Product not available.</p>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </section> -->
        @include('layouts.front.footer')
    </div>

 <!-- address modal end here -->
            <!-- add address modal start here -->
            <div id="editAddressModal" class="address-modal">
               <ul class="side-nav-list">
                  <li class="text-right">
                     <a href="javascript:void(0)" class="closebtn" onclick="closeAddModal()">&times;</a>
                  </li>
                  
                  <button class="w-100 mb-3 grey-modal-button" id="addaddressbutton" onclick="openAddModal()">Add Address</button>
             
                  <li>
                     <div class="order-address">
                        <h3 id="editaddress">Edit Address</h3>
                        <hr>
                        <form type="post" action="{{url('editAddress',$address->id??'')}}" class="editAddress-form">
                            @csrf
                           <div class="form-group">
                            <input type="hidden" name="addrid" id="addrid" value="{{$address->id??''}}"/>
                             <input type="hidden" name="customerid" id="customerid" value="{{$address->user_id??''}}"/>
                              <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Name" maxlength="30" value="{{$address->name??''}}">
                              <span id="fnameerror" style="color:red;display:none;">Please enter name</span>
                           </div>
                           
                           <div class="form-group">
                              <input type="text" class="form-control" name="address" id="address" placeholder="Address" maxlength="300" value="{{$address->address??''}}">
                              <span id="addresserror" style="color:red;display:none;">Please enter address</span>
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" name="landmark" id="landmark" placeholder="Landmark" maxlength="100" value="{{$address->landmark??''}}">
                              <span id="landmarkerror" style="color:red;display:none;">Please enter landmark</span>
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" name="area" id="area" placeholder="Area" maxlength="50" value="{{$address->area??''}}">
                              <span id="areaerror" style="color:red;display:none;">Please enter area</span>
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" name="city" id="city" placeholder="City" maxlength="50" value="{{$address->city??''}}">
                              <span id="cityerror" style="color:red;display:none;">Please enter city</span>
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" name="postcode" id="postcode" placeholder="Post code" maxlength="6" value="{{$address->postcode??''}}">
                              <span id="postcodeerror" style="color:red;display:none;">Please enter postcode</span>
                           </div>
                           <div class="form-group">
                              <input type="email" class="form-control"  name="email" id="email" placeholder="Email" maxlength="50" value="{{Auth::user()->email}}" disabled="true">
                              <span id="emailerror" style="color:red;display:none;">Please enter email</span>
                              <span id="emailvalid" style="color:red;display:none;">please enter valid email</span>
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Phone No" maxlength="10" minlength="7" value="{{Auth::user()->mobile}}" disabled="true">
                              <span id="mobileerror" style="color:red;display:none;">Please enter mobile</span>
                              <span id="mobilvaliedeerror" style="color:red;display:none;">Please enter valid mobile</span>
                           </div>
                           <div class="change-add" id="updateaddress">
                             <button class="text-center" type="button" id="frmAddUpdate" style="display:none; margin-right: 60px;">Update</button>
                           </div>
                        </form>
                     </div>
                  </li>
               </ul>
            </div>
            <!-- add address modal end here -->

 <!-- add address modal start here -->
            <div id="addAddressModal" class="address-modal">
               <ul class="side-nav-list">
                  <li class="text-right">
                     <a href="javascript:void(0)" class="closebtn" onclick="closeAddModal()">&times;</a>
                  </li>
                  <li>
                     <div class="order-address">
                        <h3 id="addaddress">Add Address</h3>
                        <hr>
                        <form type="post" name="frmAddadrss" id="frmAddadrss">
                           <div class="form-group">
                              <input type="hidden" name="addrsid" id="addrsid"/>
                              <input type="text" class="form-control" name="first_name" id="addfirst_name" placeholder="Name" maxlength="30">
                              <span id="addfnameerror" style="color:red;display:none;">Please enter name</span>
                            
                           </div>
                           
                           <div class="form-group">
                              <input type="text" class="form-control" name="address" id="addaddaddress" placeholder="Address" maxlength="300">
                              <span id="addaddresserror" style="color:red;display:none;">Please enter address</span>
                             
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" name="landmark" id="addlandmark" placeholder="Landmark" maxlength="100">
                              <span id="addlandmarkerror" style="color:red;display:none;">Please enter landmark</span>
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" name="area" id="addarea" placeholder="Area" maxlength="50">

                              <span id="addareaerror" style="color:red;display:none;">Please enter area</span>
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" name="city" id="addcity" placeholder="City" maxlength="50">
                               <span id="addcityerror" style="color:red;display:none;">Please enter city</span>
                            
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" name="postcode" id="addpostcode" placeholder="Post code" maxlength="6">
                             <span id="addpostcodeerror" style="color:red;display:none;">Please enter postcode</span>
                           </div>
                           <div class="form-group">
                              <input type="email" class="form-control"  name="email" id="addemail" placeholder="Email" maxlength="50" value="{{Auth::user()->email}}">
                              <span id="addemailerror" style="color:red;display:none;">Please enter email</span>
                              <span id="addemailvalid" style="color:red;display:none;">please enter valid email</span>
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" name="mobile" id="addmobile" placeholder="Phone No" maxlength="10" minlength="7" value="{{Auth::user()->mobile}}">
                              <span id="addmobileerror" style="color:red;display:none;">Please enter mobile</span>
                              <span id="addmobilvaliedeerror" style="color:red;display:none;">Please enter valid mobile</span>
                           </div>
                           <div class="change-add">
                              <button class="text-center" type="button" id="frmAddSubmit">Add</button>
                             
                           </div>
                        </form>
                     </div>
                  </li>
               </ul>
            </div>
            <!-- add address modal end here -->

    <div class="modal fade login-signup" id="LoginSignup" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal-body">
                    <section id="formHolder">
                        <div class="row">
                            <!-- Brand Box -->
                            <div class="col-sm-6 brand">
                                <div class="heading">
                                    <img src="{{url('css/front/images/main-logo.svg')}}" alt="">
                                </div>

                                <div class="success-msg">
                                    <p>Great! You are one of our members now</p>
                                    <a href="#" class="profile">Your Profile</a>
                                </div>
                            </div>


                            <!-- Form Box -->
                            <div class="col-sm-6 form">

                                <!-- Login Form -->
                                <div class="login form-peice switched">
                                    <form class="login-form" action="#" method="post">
                                        <div class="form-group">
                                            <label for="loginemail">Email Adderss</label>
                                            <input type="email" name="loginemail" id="loginemail" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="loginPassword">Password</label>
                                            <input type="password" name="loginPassword" id="loginPassword" required>
                                        </div>

                                        <div class="CTA">
                                            <input type="submit" value="Login">
                                            <a href="#" class="switch">I'm New</a>
                                        </div>
                                    </form>
                                </div><!-- End Login Form -->


                                <!-- Signup Form -->
                                <div class="signup form-peice">
                                    <form class="signup-form" action="#" method="post">

                                        <div class="form-group">
                                            <label for="name">First Name</label>
                                            <input type="text" name="FirstName" id="FirstName" class="name">
                                            <span class="error"></span>
                                        </div>

                                        <div class="form-group">
                                            <label for="name">Last Name*</label>
                                            <input type="text" name="LastName" id="LastName*" class="name">
                                            <span class="error"></span>
                                        </div>

                                        <div class="form-group">
                                            <label for="email">Email Adderss</label>
                                            <input type="email" name="emailAdress" id="email" class="email">
                                            <span class="error"></span>
                                        </div>

                                        <div class="form-group">
                                            <label for="phone">Phone Number </label>
                                            <input type="text" name="phone" id="phone">
                                        </div>

                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input type="password" name="password" id="password" class="pass">
                                            <span class="error"></span>
                                        </div>

                                        <div class="form-group">
                                            <label for="passwordCon">Confirm Password</label>
                                            <input type="password" name="passwordCon" id="passwordCon" class="passConfirm">
                                            <span class="error"></span>
                                        </div>

                                        <div class="CTA">
                                            <input type="submit" value="Signup Now" id="submit">
                                            <a href="#" class="switch">I have an account</a>
                                        </div>
                                    </form>
                                </div><!-- End Signup Form -->
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="{{ url('css/front/js/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ url('css/front/js/bootstrap.min.js') }}"></script>
    <script src="{{ url('css/front/js/webslidemenu.js') }}"></script>
    <script src="{{ url('css/front/js/wow.js') }}"></script>
    <script src="{{ url('css/front/js/owl.carousel.min.js') }}"></script>
    <script src="{{ url('css/front/js/jquery.themepunch.plugins.min.js') }}"></script>
    <script src="{{ url('css/front/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ url('css/front/js/function.js') }}"></script>


</body>
<script type="text/javascript">
    $("#editProfile").click(function(){
        $('#myModal').show();
    });
</script>
<script type="text/javascript">

     function openAddModal() {

        $(".opacityclass").css("opacity", "0.5");
        $(".opacityclass").css("background-color", "#9fa3a7");
        $('.disablebutton').attr("disabled", true);

             document.getElementById("addAddressModal").style.width = "300px";
             document.getElementById('frmAddSubmit').style.display='inline';
             document.getElementById('frmAddUpdate').style.display='none';
             document.getElementById('addrsid').value="";
             document.getElementById('first_name').value="";
             document.getElementById('address').value="";
             document.getElementById('landmark').value="";
             document.getElementById('area').value="";
             document.getElementById('city').value="";
             document.getElementById('postcode').value="";
             document.getElementById('email').value="";
             document.getElementById('mobile').value="";
             $("#addaddress").text('Add Address');
         }

    function openEditModal(address) {
          
        $(".opacityclass").css("opacity", "0.5");
        $(".opacityclass").css("background-color", "#9fa3a7");
        $('.disablebutton').attr("disabled", true);

        document.getElementById("editAddressModal").style.width = "300px";
        document.getElementById('frmAddUpdate').style.display='inline';
        document.getElementById('addrid').value=address.id;
        document.getElementById('first_name').value=address.name;
        document.getElementById('address').value=address.address;
        document.getElementById('landmark').value=address.landmark;
        document.getElementById('area').value=address.area;
        document.getElementById('city').value=address.city;
        document.getElementById('postcode').value=address.postcode;
        document.getElementById('email').value=address.email;
        document.getElementById('mobile').value=address.mobile;
    }

    function closeAddModal() {
         
        document.getElementById("editAddressModal").style.width = "0";
        $(".opacityclass").css({ 'background-color' : '', 'opacity' : '' });
    }

      $('#frmAddSubmit').click(function(){
                 var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if($('#addfirst_name').val()==''){
                     $('#addfnameerror').show();
                   
                     //return false;
                }
               
                else if($('#addaddaddress').val()==''){
                     $('#addaddresserror').show();
                     return false;
                }

                else if($('#addlandmark').val()==''){
                     $('#addlandmarkerror').show();
                     return false;
                }

                else if($('#addarea').val()==''){
                     $('#addareaerror').show();
                     return false;
                }
                else if($('#addcity').val()==''){
                     $('#addcityerror').show();
                     return false;
                }
                else if($('#addpostcode').val()==''){
                     $('#addpostcodeerror').show();
                     return false;
                }

                else if($('#addemail').val()==''){
                     $('#addemailerror').show();
                     return false;
                }
                else if(!regex.test($('#addemail').val()))
                {
                    $("#addemailvalid").show();
                    $('#addemailerror').hide();
                    return false;
                }
                else if($('#addmobile').val()==''){
                     $('#addmobileerror').show();
                     return false;
                }
                else if($('#addmobile').val().length!=10){
                     $('#addmobilvaliedeerror').show();
                     $('#addmobileerror').hide();
                     return false;
                }
           
             
                else{
                     $('#addfnameerror').hide();
                     $('#addaddresserror').hide();
                     $('#addlandmarkerror').hide();
                     $('#addareaerror').hide();
                     $('#addcityerror').hide();
                     $('#addpostcodeerror').hide();
                     $('#emailerror').hide();
                     $('#mobileerror').hide();
                     $("#emailvalid").hide();
                     $('#mobilvaliedeerror').hide();
                    
                    
                     var userid="{{Auth::user()->id}}";
                 var address=$('#addaddaddress').val();
                 var postcode=$('#addpostcode').val();
                 var name = $('#addfirst_name').val();
                 var mobile=$('#addmobile').val();
                 var email=$('#addemail').val();
                 var landmark=$('#addlandmark').val();
                 var area=$('#addarea').val();
                 var city=$('#addcity').val();
                
                 addAddress(userid,address,postcode,name,mobile,email,landmark,area,city);
                
                 return false;
                }
               
             });

           
function addAddress(customer_id,address,postcode,name,mobile,email,landmark,area,city){

             $.ajax({
                 url: "{{url('api/v1/add-address')}}",
                 data: {
                     user_id:customer_id,
                     address:address,
                     type:'1',
                     postcode:postcode,
                     name:name,
                     mobile:mobile,
                     email:email,
                     landmark:landmark,
                     area:area,
                     city:city,
                     isedit:0,
                     lat:null,
                     long:null
                 },
                 type: 'POST',
                 // dataType: 'json',
                 success: function(result) {
                    result = JSON.parse(result)
                    console.log(result);
                  if(result.status==0)
                  {
                     toastr.error("<span style='font-size:15px;font-weight:bold'>"+result.message+"</span>");
                  }else{
                    location.href="{{url('/profile?isRedirect=true')}}";
                    $(".opacityclass").css({ 'background-color' : '', 'opacity' : '' });
                  
                  }
                     return false;
                 },
                 error: function(error){
                 }
             });
         }


     $('#frmAddUpdate').click(function(){

        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                 if($('#first_name').val()=='' || $('#address').val()=='' && $('#area').val()=='' && $('#city').val()=='' && $('#postcode').val()=='' && $('#email').val()=='' && $('#mobile').val()==''){
                     $('#fnameerror').show();
                     $('#addresserror').show();
                     $('#areaerror').show();
                     $('#cityerror').show();
                     $('#postcodeerror').show();
                    
                     return false;
                }
                else if($('#address').val()==''){
                     $('#addresserror').show();
                     return false;
                }
                else if($('#area').val()==''){
                     $('#areaerror').show();
                     return false;
                }
                else if($('#city').val()==''){
                     $('#cityerror').show();
                     return false;
                }
                else if($('#postcode').val()==''){
                     $('#postcodeerror').show();
                     return false;
                }
               
        else
        {
         
        $('#fnameerror').hide();
        $('#addresserror').hide();
        $('#landmarkerror').hide();
        $('#areaerror').hide();
        $('#cityerror').hide();
                 $('#postcodeerror').hide();
                


                 var userid="{{Auth::user()->id}}";
                 var id=$('#addrid').val();
                 var address=$('#address').val();
                 var postcode=$('#postcode').val();
                 var name = $('#first_name').val();
                 var mobile=$('#mobile').val();
                 var email=$('#email').val();
                 var landmark=$('#landmark').val();
                 var area=$('#area').val();
                 var city=$('#city').val();
                 var customer_id=$('#customerid').val();

                $.ajax({
                 url: "{{url('api/v1/add-address')}}",
                 data: {
                     id:id,
                     user_id:customer_id,
                     address:address,
                     type:'1',
                     postcode:postcode,
                     name:name,
                     mobile:mobile,
                     email:email,
                     landmark:landmark,
                     area:area,
                     city:city,
                     isedit:1,
                     lat:null,
                     long:null
                 },
                 type: 'POST',
                 // dataType: 'json',
                 success: function(result) {
                  location.href="{{url('/profile?isRedirect=true')}}";
                    $(".opacityclass").css({ 'background-color' : '', 'opacity' : '' });
                    return false;
                 },
                 error: function(error){
                   console.log("error");
                 }

             });
              }
                
                });
</script>
</html>