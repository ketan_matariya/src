<!doctype html>
<html lang="en">

<head>
    <title>NileMart | Sign in/ Sign up</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @include('layouts.front.head')
</head>

<body>
    @include('layouts.front.header-cart')
    <section class="login-register">
        <div class="container">
            <div class="col-md-5">
                <div class="login-banner">
                    <img src="{{url('css/front/images/login-side-banner.png')}}" class="img-fluid" alt="login banner">
                </div>
            </div>
            <div class="col-md-7">
                
                <div class="login-card otp-card" style="{{($result!='' || $errors->first('otp')!='' ||  Session::get('message')!='')?'display:block;':'display:none;'}}">
                    @if ($message = Session::get('message'))
                    <div class="alert alert-danger alert-block">
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    <h3>Verify</h3>
                    @if($result!='')
                    <p>We have sent 6 digit OTP on +91-{{$result}}</p>
                    @endif
                    <form action="{{ route('registerloginstore') }}" method="post">
                        {{ csrf_field() }}
                        <input type="text" name="id" id="id" value="{{$result}}" hidden>
                    <div class="label-no">
                        <label for="">No.</label>
                        
                        <input type="text" maxlength="6"  name="otp" id="otp" placeholder="Enter OTP Number">
                        @if($errors->has('otp'))
                        <br>
                        <span style="color: red;">{{ $errors->first('otp') }}</span>
                        @endif
                    </div>
                    <div class="w-100 text-center mt-3">
                        <button type="submit" id="submitOTP" class="btn btn-default cart-login"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                    </div>
                    </form>
                    <form action="{{url('resendotp')}}" method="post">
                        @csrf
                        <input type="text" name="id" id="id" value="{{$result}}" hidden>

                        <button type="submit" id="submitOTP" class="btn btn-default cart-login"> <span style="font-size: 15px; padding: 5px;"> Resend OTP</span> <i class="fa fa-send-o" style="font-size:20px" aria-hidden="true"></i></button>
                    </form>
                </div>
            </div>
        </div>

    </section>
    @include('layouts.front.footer')

<script>
    $('#numeric').on('input', function (event) {
        this.value = this.value.replace(/[^0-9]/g, '');
    });
    $('#otp').on('input', function (event) {
        this.value = this.value.replace(/[^0-9]/g, '');
    });
</script>

</body>

</html>