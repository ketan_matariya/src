<!doctype html>
<html lang="en">

<head>
    <title>NileMart | Sign in/ Sign up</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @include('layouts.front.head')
</head>

<body>
    @include('layouts.front.header-cart')

    <!-- <section class="login-register">
        <div class="container">
            <div class="tab-login-signin">
                <div class="row">
                    <div class="col-md-6 border-right">
                        <div class="login-register-cart">
                            <h2>Login</h2>
                            @if ($message = Session::get('message'))
                            <div class="alert alert-danger alert-block">
                                <strong>{{ $message }}</strong>
                            </div>
                            @endif
                            <form method="post" action="{{route('loginstore')}}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input type="email" placeholder="Email address" class="form-control" name="email" value="{{ old('email') }}">
                                    @if($errors->has('email'))
                                    <span style="color: red;">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input type="password" placeholder="Password" class="form-control" name="password">
                                    @if($errors->has('password'))
                                    <span style="color: red;">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-default cart-login">Login</button>
                            </form>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="login-register-cart">
                            <h2>Register</h2>
                            <form method="post" action="{{url('/register')}}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input type="text" placeholder="First Name" class="form-control" name="name" value="{{ old('name') }}">
                                    @if($errors->has('name'))
                                    <span style="color: red;">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input type="text" placeholder="Last Name" class="form-control" name="last_name" value="{{ old('last_name') }}">
                                    @if($errors->has('last_name'))
                                    <span style="color: red;">{{ $errors->first('last_name') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input type="email" placeholder="Email Adderss" class="form-control" name="email" value="{{ old('email') }}">
                                    @if($errors->has('email'))
                                    <span style="color: red;">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input type="text" placeholder="Phone Number" class="form-control" name="mobile" value="{{ old('mobile') }}">
                                    @if($errors->has('mobile'))
                                    <span style="color: red;">{{ $errors->first('mobile') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input type="password" placeholder="Password" class="form-control" name="password" value="{{ old('password') }}">
                                    @if($errors->has('password'))
                                    <span style="color: red;">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input type="password" placeholder="Confirm Password" class="form-control" name="confirmpassword" value="">
                                    @if($errors->has('confirmpassword'))
                                    <span style="color: red;">{{ $errors->first('confirmpassword') }}</span>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-default cart-login">Register</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->

    <section class="login-register">
        <div class="container">
            <div class="col-md-5">
                <div class="login-banner">
                    <img src="{{url('css/front/images/login-side-banner12.png')}}" class="img-fluid" alt="login banner">
                </div>
            </div>
            <div class="col-md-7">
                <div class="login-card sign-up-card mb-3" id="sendotp2" style="{{(Session::get('result')!=''  || $errors->first('otp')!='' ||  Session::get('message')!='')?'display:none;':'display:block;'}}">
                    <h3>Sign in</h3>
                    <p>Sign in to access your Orders, Offers and Wishlist.</p>
                    
                    <!-- <ul class="social-ul">
                        <li>
                            <button class="btn fb"><i class="fa fa-facebook mr-2" aria-hidden="true"></i> Facebook</button>
                        </li>
                        <li>
                            <button class="btn tw"><i class="fa fa-twitter mr-2" aria-hidden="true"></i> Twitter</button>
                        </li>
                        <li>
                            <button class="btn gm"><i class="fa fa-envelope mr-2" aria-hidden="true"></i> Gmail</button>
                        </li>
                    </ul> -->

                    <form action="{{ route('userStorRegistration') }}" method="post">
                        {{ csrf_field() }}
                    <div class="label-no">
                        <label for="">+91</label>
                        <input type="text" id="numeric" maxlength="10" name="mobile" id="mobile" placeholder="Mobile Number">
                        @if($errors->has('mobile'))
                        <br>
                        <span style="color: red;">{{ $errors->first('mobile') }}</span>
                        @endif
                    </div>
                    <div class="w-100 text-center mt-3">
                        <button type="submit" id="submit" class="btn btn-default cart-login set-lgin"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                    </div>
                    </form>
                </div>
                <div class="login-card otp-card" style="{{(Session::get('result')!='' || $errors->first('otp')!='' ||  Session::get('message')!='')?'display:block;':'display:none;'}}">
                    @if ($message = Session::get('message'))
                    <div class="alert alert-danger alert-block">
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    <h3>Verify</h3>
                    @if(Session::get('result')!='')
                    <p>We have sent 6 digit OTP on +91-{{Session::get('result')}}</p>
                    @endif
                    <form action="{{ route('loginstore') }}" method="post">
                        {{ csrf_field() }}
                        <input type="text" name="id" id="id" value="{{Session::get('result')}}" hidden>
                    <div class="label-no">
                        <label for="">No.</label>
                        
                        <input type="text" maxlength="6" name="otp" id="otp" placeholder="Enter OTP Number">
                        @if($errors->has('otp'))
                        <br>
                        <span style="color: red;">{{ $errors->first('otp') }}</span>
                        @endif
                    </div>
                    <div class="w-100 text-center mt-3">
                        <button type="submit" id="submitOTP" class="btn btn-default cart-login"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                    </div>
                    </form>
                    <form action="{{url('resendotp')}}" method="post">
                        @csrf
                        <input type="text" name="id" id="id" value="{{Session::get('result')}}" hidden>

                        <button type="submit" id="submitOTP" class="btn btn-default cart-login resend-btn"> <i class="fa fa-send-o otp-icon" style="font-size:27px" aria-hidden="true"></i></button>
                    </form>
                </div>
            </div>
        </div>

    </section>
    @include('layouts.front.footer')

<script>
    $('#numeric').on('input', function (event) {
        this.value = this.value.replace(/[^0-9]/g, '');
    });
    $('#otp').on('input', function (event) {
        this.value = this.value.replace(/[^0-9]/g, '');
    });
</script>

</body>

</html>