<!doctype html>
<html lang="en">

<head>
    <title>NileMart | Sign in/ Sign up</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @include('layouts.front.head')
</head>

<body>

    @include('layouts.front.header-cart')

    <section class="login-register">
        <div class="container">
            <div class="col-md-5">
                <div class="login-banner">
                    <img src="{{url('css/front/images/login-side-banner.png')}}" class="img-fluid" alt="login banner">
                </div>
            </div>
            <div class="col-md-7">
                <div class="login-card sign-up-card mb-3" id="sendotp2" style="{{(Session::get('result')!=''  || $errors->first('otp')!='' ||  Session::get('message')!='')?'display:none;':'display:block;'}}">
                    <h3>Sign Up</h3>
                    <p>Sign Up to access your Orders, Offers and Wishlist.</p>
                    <form action="{{ url('/register',$mobileno)}}" method="post">
                        {{ csrf_field() }}
                    <div class="label-no">
                        <label for="">Name</label>&nbsp;
                        <input type="text" id="customer_id" maxlength="20" name="customer_id"  placeholder="Name">
                        @if($errors->has('name'))
                        <br>
                        <span style="color: red;">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                    <div class="label-no">
                        <label for="">Email</label>&nbsp;
                        <input type="text" id="customer_email" maxlength="50" name="customer_email" placeholder="Email">
                        @if($errors->has('email'))
                        <br>
                        <span style="color: red;">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                    <div class="label-no">
                        <label for="">+91</label>
                        <input type="text" maxlength="10" name="mobile" id="mobile" placeholder="Mobile Number" value="{{$mobileno}}" disabled>
                        @if($errors->has('mobile'))
                        <br>
                        <span style="color: red;">{{ $errors->first('mobile') }}</span>
                        @endif
                    </div>
                    <div class="w-100 text-center mt-3">
                        <button type="submit" id="submit" class="btn btn-default cart-login"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

    </section>
    @include('layouts.front.footer')

<script>
    $('#numeric').on('input', function (event) {
        this.value = this.value.replace(/[^0-9]/g, '');
    });
</script>

</body>

</html>