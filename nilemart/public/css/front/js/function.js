// new js start
// top header hide js start
var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
        var currentScrollPos = window.pageYOffset;
        if (prevScrollpos > currentScrollPos) {
            document.getElementById("top-header").style.top = "0";
        } else {
            document.getElementById("top-header").style.top = "-50px";
        }
        prevScrollpos = currentScrollPos;
    }
    // top header hide js end
    // new js end
    //Animation
jQuery(window).load(function($) {
    wow = new WOW({
        animateClass: 'animated',
        offset: 50,
    });
    wow.init();
});
//Animation


document.getElementById("year").innerHTML = new Date().getFullYear();

// sticky Header
window.onscroll = function() { myFunction() };
var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
    if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
    } else {
        header.classList.remove("sticky");
    }
}


// LOGIN SIGNUP
/*global $, document, window, setTimeout, navigator, console, location*/
$(document).ready(function() {
    'use strict';
    var usernameError = true,
        emailError = true,
        passwordError = true,
        passConfirm = true;

    // Detect browser for css purpose
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $('.form form label').addClass('fontSwitch');
    }

    // Label effect
    $('input').focus(function() {
        $(this).siblings('label').addClass('active');
    });

    // Form validation
    $('input').blur(function() {

        // User Name
        if ($(this).hasClass('name')) {
            if ($(this).val().length === 0) {
                $(this).siblings('span.error').text('Please type your full name').fadeIn().parent('.form-group').addClass('hasError');
                usernameError = true;
            } else if ($(this).val().length > 1 && $(this).val().length <= 6) {
                $(this).siblings('span.error').text('Please type at least 6 characters').fadeIn().parent('.form-group').addClass('hasError');
                usernameError = true;
            } else {
                $(this).siblings('.error').text('').fadeOut().parent('.form-group').removeClass('hasError');
                usernameError = false;
            }
        }
        // Email
        if ($(this).hasClass('email')) {
            if ($(this).val().length == '') {
                $(this).siblings('span.error').text('Please type your email address').fadeIn().parent('.form-group').addClass('hasError');
                emailError = true;
            } else {
                $(this).siblings('.error').text('').fadeOut().parent('.form-group').removeClass('hasError');
                emailError = false;
            }
        }

        // PassWord
        if ($(this).hasClass('pass')) {
            if ($(this).val().length < 8) {
                $(this).siblings('span.error').text('Please type at least 8 charcters').fadeIn().parent('.form-group').addClass('hasError');
                passwordError = true;
            } else {
                $(this).siblings('.error').text('').fadeOut().parent('.form-group').removeClass('hasError');
                passwordError = false;
            }
        }

        // PassWord confirmation
        if ($('.pass').val() !== $('.passConfirm').val()) {
            $('.passConfirm').siblings('.error').text('Passwords don\'t match').fadeIn().parent('.form-group').addClass('hasError');
            passConfirm = false;
        } else {
            $('.passConfirm').siblings('.error').text('').fadeOut().parent('.form-group').removeClass('hasError');
            passConfirm = false;
        }

        // label effect
        if ($(this).val().length > 0) {
            $(this).siblings('label').addClass('active');
        } else {
            $(this).siblings('label').removeClass('active');
        }
    });


    // form switch
    $('a.switch').click(function(e) {
        $(this).toggleClass('active');
        e.preventDefault();

        if ($('a.switch').hasClass('active')) {
            $(this).parents('.form-peice').addClass('switched').siblings('.form-peice').removeClass('switched');
        } else {
            $(this).parents('.form-peice').removeClass('switched').siblings('.form-peice').addClass('switched');
        }
    });


    // Form submit
    $('form.signup-form').submit(function(event) {
        event.preventDefault();

        if (usernameError == true || emailError == true || passwordError == true || passConfirm == true) {
            $('.name, .email, .pass, .passConfirm').blur();
        } else {
            $('.signup, .login').addClass('switched');

            setTimeout(function() { $('.signup, .login').hide(); }, 700);
            setTimeout(function() { $('.brand').addClass('active'); }, 300);
            setTimeout(function() { $('.heading').addClass('active'); }, 600);
            setTimeout(function() { $('.success-msg p').addClass('active'); }, 900);
            setTimeout(function() { $('.success-msg a').addClass('active'); }, 1050);
            setTimeout(function() { $('.form').hide(); }, 700);
        }
    });

    //  Footer accordion
    $(document).ready(function() {
        $('.collapse.in').prev('.panel-heading').addClass('active');
        $('#accordion, #bs-collapse')
            .on('show.bs.collapse', function(a) {
                $(a.target).prev('.panel-heading').addClass('active');
            })
            .on('hide.bs.collapse', function(a) {
                $(a.target).prev('.panel-heading').removeClass('active');
            });
    });

    // Header searchbox-icon
    $(document).ready(function() {
        var submitIcon = $('.searchbox-icon');
        var inputBox = $('.searchbox-input');
        var searchBox = $('.searchbox');
        var isOpen = false;
        submitIcon.click(function() {
            if (isOpen == false) {
                searchBox.addClass('searchbox-open');
                inputBox.focus();
                isOpen = true;
            } else {
                searchBox.removeClass('searchbox-open');
                inputBox.focusout();
                isOpen = false;
            }
        });
        submitIcon.mouseup(function() {
            return false;
        });
        searchBox.mouseup(function() {
            return false;
        });
        $(document).mouseup(function() {
            if (isOpen == true) {
                $('.searchbox-icon').css('display', 'block');
            }
        });
    });

    // Reload page
    $('a.profile').on('click', function() {
        location.reload(true);
    });


    // Form js




});



$(document).ready(function($) {
    $("#ProductDetailSld").owlCarousel({
        autoPlay: 2000,
        items: 4,
        loop: true,
        autoPlay: true,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 3],
        itemsTablet: [767, 2],
        itemsMobile: [479, 1],
        dots: false,
        pagination: false,
        margin: 10,
        nav: true,

    });
});
$(document).ready(function($) {
    $("#ConnectWithUs").owlCarousel({
        autoPlay: 2000,
        items: 5,
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: [979, 3],
        itemsTablet: [767, 2],
        itemsMobile: [479, 1],
        dots: false,
        pagination: false,
        margin: 0,
        nav: true,
        navigation: true,
        navigationText: ['<span><i class="fa fa-angle-left"></i></span>', '<span><i class="fa fa-angle-right"></i></span>'],
    });
});

$(document).ready(function($) {
    $("#productdetail").owlCarousel({
        autoPlay: false,
        items: 5,
        itemsDesktop: [1199, 5],
        itemsDesktopSmall: [979, 5],
        itemsTablet: [767, 5],
        itemsMobile: [479, 5],
        dots: false,
        pagination: false,
        nav: true,
        navigation: true,
        navigationText: ['<span><i class="fa fa-angle-left"></i></span>', '<span><i class="fa fa-angle-right"></i></span>'],
    });
});