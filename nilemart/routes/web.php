<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/**
 * Admin routes
 */
Route::namespace('Admin')->group(function () {
    Route::get('admin/login', 'LoginController@showLoginForm')->name('admin.login');
    Route::post('admin/login', 'LoginController@login')->name('admin.login');
    Route::get('admin/logout', 'LoginController@logout')->name('admin.logout');
});
Route::group(['prefix' => 'admin', 'middleware' => ['employee'], 'as' => 'admin.'], function () {
    Route::namespace('Admin')->group(function () {
        // Route::group(['middleware' => ['role:admin|superadmin|clerk, guard:employee']], function () {
        Route::get('/', 'DashboardController@index')->name('dashboard');
        Route::namespace('Products')->group(function () {
            Route::resource('products', 'ProductController');
            Route::get('remove-image-product', 'ProductController@removeImage')->name('product.remove.image');
            Route::get('remove-image-thumb', 'ProductController@removeThumbnail')->name('product.remove.thumb');
            Route::post('getCategories', 'ProductController@getCategories')->name('products.getCategories');
            Route::post('getSubSubCategories', 'ProductController@getSubSubCategories')->name('products.getSubSubCategories');
            Route::post('removeProductProduct', 'ProductController@removeProductattribute')->name('products.removeProductProduct');
            Route::post('getCropCategories', 'ProductController@getCropCategories')->name('products.getCropCategories');
            Route::post('getSubCategories', 'ProductController@getSubCategories')->name('products.getSubCategories');
            Route::get('checkhsncode', 'ProductController@checkHsnCode')->name('products.checkhsncode');
        });
        Route::namespace('Customers')->group(function () {
            Route::resource('customers', 'CustomerController');
            Route::resource('customers.addresses', 'CustomerAddressController');
        });
        // Route::namespace('Crop')->group(function () {
        //     Route::resource('crops', 'CropController');
        // });
        Route::namespace('Crops')->group(function () {
            Route::resource('crops', 'CropController');
            Route::get('crops/create/{id?}', 'CropController@create')->name('crops.create');
        });

        Route::namespace('CustomerNotifications')->group(function () {
            Route::post('getValue', 'NotificationController@getValue')->name('notifications.getValue');
            Route::resource('notifications', 'NotificationController');
        });

        Route::namespace('TechnicalCategoties')->group(function () {
            Route::resource('technical', 'TechnicalController');
        });

        Route::namespace('UpdateStocks')->group(function () {
            Route::resource('updateStock', 'UpdateStockController');
        });


        Route::namespace('Postcodes')->group(function () {
            Route::resource('postcode', 'PostcodeController');
        });

        Route::namespace('CountriesData')->group(function () {
            Route::resource('country_data', 'CountryDataController');
            Route::get('country_data/show-tehsil/{id?}', 'CountryDataController@showTehsil')->name('country_data.showTehsil');
            Route::get('country_data/show-village/{id?}', 'CountryDataController@showVillage')->name('country_data.showVillage');
            Route::get('country_data/destroy-village/{id?}', 'CountryDataController@destroyVillage')->name('country_data.destroyVillage');
            Route::post('getCity', 'CountryDataController@getCity')->name('country_data.getCity');
            Route::post('getTehsil', 'CountryDataController@getTehsil')->name('country_data.getTehsil');
            Route::get('createVillage', 'CountryDataController@createVillage')->name('country_data.createVillage');
            Route::get('createCity', 'CountryDataController@createCity')->name('country_data.createCity');
        });

        Route::namespace('Priorities')->group(function () {
            Route::resource('pro_priority', 'PriorityController');
            // Route::post('update', 'PriorityController@update');
            Route::post('pro_priority/sort', 'PriorityController@cat_sort')->name('priority.sort');
            Route::post('pro_priority/subcat_sort', 'PriorityController@subcat_sort')->name('priority.subcat_sort');
            Route::post('pro_priority/getProduct', 'PriorityController@getProduct')->name('priority.getProduct');
            Route::post('pro_priority/getCategoryProduct', 'PriorityController@getCategoryProduct')->name('priority.getCategoryProduct');
        });

        Route::namespace('Offers')->group(function () {
            Route::resource('offers', 'OffersController');
            Route::get('offers_product', 'OffersController@getProduct')->name('offers.product.attr');
            Route::post('offers/{id}', 'OffersController@update')->name('offers.update');
            Route::get('remove-image-offer', 'OffersController@removeImage')->name('offers.remove.image');
        });
        Route::namespace('Categories')->group(function () {
            Route::resource('categories', 'CategoryController');
            Route::get('showsubcatgry/{mainid}/{id}', 'CategoryController@showsubcatgry')->name('categories.showsubcatgry');
            
            Route::post('categories/storesub', 'CategoryController@storesub')->name('categories.storesub');
            Route::get('createsub/{mainid}/{id}', 'CategoryController@createsub')->name('categories.createsub');
            Route::get('editsub/{mainid}/{subid}/{id}', 'CategoryController@editsub')->name('categories.editsub');
            Route::post('categories/updatesub', 'CategoryController@updatesub')->name('categories.updatesub');
            Route::get('remove-image-category', 'CategoryController@removeImage')->name('category.remove.image');
            Route::get('categories/destroysub/{mainid}/{subid}/{id}', 'CategoryController@destroysub')->name('driver.destroysub');
        });
        Route::namespace('Orders')->group(function () {
            Route::get('orders/printorder', 'OrderController@printInvoice')->name('orders.print_invoice');
            Route::resource('orders', 'OrderController');
            Route::resource('order-statuses', 'OrderStatusController');
            Route::get('orders/{id}/invoice', 'OrderController@generateInvoice')->name('orders.invoice.generate');
            Route::post('orders/print_order', 'OrderController@orderPrint')->name('orders.print_order');
        });


        //Driver
        Route::namespace('Drivers')->group(function () {
            // Route::resource('driver', 'DriverController');
            Route::get('driver/index', 'DriverController@show')->name('driver.index');
            Route::get('driver/usershow/{id}', 'DriverController@usershow')->name('driver.usershow');
            Route::get('driver/create/', 'DriverController@create')->name('driver.create');
            Route::post('driver/store', 'DriverController@store')->name('driver.store');
            Route::get('driver/edit/{id}', 'DriverController@edit')->name('driver.edit');
            Route::get('driver/destroy/{id}', 'DriverController@destroy')->name('driver.destroy');
        });

        //Payment
        Route::namespace('Payments')->group(function () {
            Route::resource('payment', 'PaymentController');
            Route::get('payment/driver/{id}/total/{total}', 'PaymentController@driverNotification');
            Route::get('payment/drivers/payment/otp', 'PaymentController@driverNotifications')->name('driver.payment.otp');
            Route::post('payment/drivers/payment', 'PaymentController@paymentSubmit')->name('driver.payment');
        });

        Route::resource('addresses', 'Addresses\AddressController');
        Route::resource('countries', 'Countries\CountryController');
        Route::resource('countries.provinces', 'Provinces\ProvinceController');
        Route::resource('countries.provinces.cities', 'Cities\CityController');
        Route::resource('couriers', 'Couriers\CourierController');
        Route::resource('attributes', 'Attributes\AttributeController');
        Route::resource('attributes.values', 'Attributes\AttributeValueController');
        Route::resource('brands', 'Brands\BrandController');

        Route::get('list-customer', 'Wallet\WalletController@index')->name('wallet.index');
        Route::get('wallet-show/{id}', 'Wallet\WalletController@walletSummery')->name('wallet.show');

        // });
        //Changed the employees to companies
        // Route::group(['middleware' => ['role:admin|superadmin, guard:employee']], function () {
        Route::resource('companies', 'CompaniesController');
        Route::get('companies/{id}/profile', 'CompaniesController@getProfile')->name('companies.profile');
        Route::get('remove-logo', 'CompaniesController@removeLogo')->name('company.remove.logo');
        Route::get('changeStatus', 'CompaniesController@changeStatus')->name('companies.status');
        Route::put('companies/{id}/profile', 'CompaniesController@updateProfile')->name('companies.profile.update');
        Route::resource('roles', 'Roles\RoleController');
        Route::resource('permissions', 'Permissions\PermissionController');
 
        Route::get('cms/tearm_condtion/index','CMS\TearmCondtionController@index')->name('cms.tearm_condtion.index');
        Route::post('cms/tearm_condtion/update', 'CMS\TearmCondtionController@update')->name('cms.tearm_condtion.update');

        Route::get('cms/privacy_policy/index','CMS\PrivacyPolicyController@index')->name('cms.privacy_policy.index');
        Route::post('cms/privacy_policy/update', 'CMS\PrivacyPolicyController@update')->name('cms.privacy_policy.update');
        // });

        //Faq
        Route::get('faq/index', 'Faq\FaqController@index')->name('faq.index');
        Route::get('faq/create/', 'Faq\FaqController@create')->name('faq.create');
        Route::post('faq/store', 'Faq\FaqController@store')->name('faq.store');
        Route::get('faq/edit/{id}', 'Faq\FaqController@edit')->name('faq.edit');
        Route::get('faq/destroy/{id}', 'Faq\FaqController@destroy')->name('faq.destroy');
        // });


        Route::namespace('Orderpurchase')->group(function () {
            Route::resource('orderpurchase', 'OrderpurchaseController');
            Route::post('removeProductattribute','OrderpurchaseController@removeProductattribute')->name('orderpurchase.removeProductattribute');
            Route::get('getProducts', 'OrderpurchaseController@getProducts')->name('orderpurchase.getProducts');
            Route::post('getProductsattribute', 'OrderpurchaseController@getProductsattribute')->name('orderpurchase.getProductsattribute');
            Route::post('getProductsdetail', 'OrderpurchaseController@getProductsdetail')->name('orderpurchase.getProductsdetail');
            Route::post('orderpurchase/update/{id}', 'OrderpurchaseController@update')->name('orderpurchase.update');
            Route::get('orderpurchase/{id}/invoice', 'OrderpurchaseController@generateInvoice')->name('orderpurchase.invoice.generate');
        });
    });
    Route::get('translate', 'Admin\TranslateController@translate')->name('translate');
});

/**
 * Frontend routes
 */
Auth::routes();
Route::namespace('Auth')->group(function () {
    Route::get('cart/login', 'CartLoginController@showLoginForm')->name('cart.login');
    Route::post('cart/login', 'CartLoginController@login')->name('cart.login');
    Route::get('logout', 'LoginController@destroy');
    Route::get('user/login', 'LoginController@loginform')->name('userLogin');
    Route::post('/user/login', 'LoginController@loginstore')->name('loginstore');
    Route::post('/user/register/login', 'LoginController@registerloginstore')->name('registerloginstore');
    Route::post('/register/{no}', 'LoginController@register')->name('userRegistration');
    Route::post('register', 'LoginController@store')->name('userStorRegistration');
    Route::post('resendotp', 'LoginController@resendOtp')->name('resendotp');

});

Route::namespace('Front')->group(function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::group(['middleware' => ['auth', 'web']], function () {

        Route::namespace('Payments')->group(function () {
            Route::get('bank-transfer', 'BankTransferController@index')->name('bank-transfer.index');
            Route::post('bank-transfer', 'BankTransferController@store')->name('bank-transfer.store');
        });

        Route::namespace('Addresses')->group(function () {
            Route::resource('country.state', 'CountryStateController');
            Route::resource('state.city', 'StateCityController');
        });

        Route::get('accounts', 'AccountsController@index')->name('accounts');
        Route::get('checkout', 'CheckoutController@index')->name('checkout.index');
        Route::post('checkout', 'CheckoutController@store')->name('checkout.store');
        Route::get('checkout/execute', 'CheckoutController@executePayPalPayment')->name('checkout.execute');
        Route::post('checkout/execute', 'CheckoutController@charge')->name('checkout.execute');
        Route::get('checkout/cancel', 'CheckoutController@cancel')->name('checkout.cancel');
        Route::get('checkout/success', 'CheckoutController@success')->name('checkout.success');
        Route::resource('customer.address', 'CustomerAddressController');
    });
    Route::resource('cart', 'CartController');
    Route::get("category/{slug}", 'CategoryController@getCategory')->name('front.category.slug');
    // Route::get("search", 'ProductController@search')->name('search.product');
    // Route::get("{product}", 'ProductController@show')->name('front.get.product');

    Route::get("productCategory", 'ProductController@productCategory')->name('productCategory.product');
    Route::get("productSubCategory/{id}", 'ProductController@productSubCategory')->name('productSubCategory.product');
    Route::get("productDetail/{id}", 'ProductController@productDetail')->name('productDetail.product');
    Route::get("productListing", 'ProductController@productListing')->name('productListing.product');
    Route::get("product/{id}", 'ProductController@productCategoryListing')->name('productCategoryListing.product');
    Route::get("productCategory/{cat}", 'ProductController@productCategoryListingCat')->name('productCategoryListingCat.product');
    Route::get("productSubSubCategory/{id}", 'ProductController@productSubSubCategory')->name('productSubSubCategory.name');
    
    Route::get("checkout", 'ProductController@checkout')->name('checkout.product');
    //Route::get("productDetail/productCheckcart", 'ProductController@productCheckcart')->name('productDetail.productCheckcart');
    Route::get("myorder", 'ProductController@myorder')->name('myorder');

    Route::get("profile",'ProfileController@profile')->name('profile');
    Route::get("contact-us",'ProfileController@contactus')->name('contactus');
    Route::post("send-email",'ProfileController@sendEmail');
    Route::get("tearm-condition",'ProfileController@tearmcondition')->name('tearmcondition');
    Route::get("privacy-policy",'ProfileController@privacypolicy')->name('privacypolicy');
    Route::get("faq",'ProfileController@faq')->name('faq');
    Route::get("customer/orderhistory",'ProfileController@orders')->name('orders');
    Route::get("customer/orderhistory/{id}",'ProfileController@show')->name('show');
    Route::get("about-us",'ProfileController@aboutus')->name('aboutus');
    Route::post("editProfile/{id}",'ProfileController@editProfile')->name('editProfile');
    Route::post("editAddress/{id}",'ProfileController@editAddress')->name('editAddress');
    Route::get("productCheckcart", 'HomeController@productCheckcart')->name('productCheckcart');
    Route::get("SelectUnitPrice/{id}", 'HomeController@SelectUnitPrice')->name('SelectUnitPrice');
    Route::post("getProductSearch", 'ProductController@getProductSearch')->name('getProductSearch');
    Route::post("updatecart", 'ProductController@updatecart')->name('updatecart');
    Route::get("SelectUnitPrice/{id}", 'HomeController@SelectUnitPrice')->name('SelectUnitPrice');
    
    Route::get("defaultAddres", 'ProductController@defaultAddres')->name('defaultAddres');
    Route::get("DeleteCart", 'ProductController@deleteCart')->name('DeleteCart');
    
    Route::post("contact/mails", 'ContactController@mailsend')->name('contact.mails');
    Route::post("contactMail", 'ContactController@mailSend')->name('contactMail');

});
