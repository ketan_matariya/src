<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Shop\Employees\Employee;
use Illuminate\Http\UploadedFile;

$factory->define(Employee::class, function (Faker\Generator $faker) {
    static $password;
    $file = UploadedFile::fake()->image('company.png', 600, 600);
    return [
        'name' => $faker->firstName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'logo' => $file->store('companies', ['disk' => 'public']),
        'remember_token' => str_random(10),
        'status' => 1
    ];
});
