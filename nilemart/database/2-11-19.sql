
Register:

ALTER TABLE `customers` ADD `address` TEXT NULL AFTER `updated_at`, ADD `language` VARCHAR(200) NULL AFTER `address`;

ALTER TABLE `customers` CHANGE `email` `email` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;

ALTER TABLE `customers` CHANGE `password` `password` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;

ALTER TABLE `customers` ADD `image` TEXT NULL DEFAULT NULL AFTER `language`;

ALTER TABLE `customers` ADD `otp` INT(11) NULL DEFAULT NULL AFTER `image`;


Cart 
create cart table  
ALTER TABLE `cart` ADD `product_id` INT(11) NOT NULL AFTER `customer_id`, ADD `quantity` INT(11) NOT NULL AFTER `product_id`, ADD `created_at` INT(11) NULL AFTER `quantity`;


Crop
ALTER TABLE `crop` ADD `deleted_at` TIMESTAMP NULL AFTER `updated_at`;