<?php

namespace App\Helper;

use Illuminate\Http\Request;
use Auth;
use Stichoza\GoogleTranslate\GoogleTranslate;
use Log;


class Translate 
{
    public static function translate($request="")
    {
    	try {
    	   // dd($request);
    		$alldata = $request;
    		$result = array();
    		foreach ($alldata as $key => $value) {
	    		$mainString = trim($value);
	    		$tr = new GoogleTranslate();
				$tr->setTarget('gu');
				$data['gu'] = $tr->translate($mainString);
				$tr->setTarget('hi');
				$data['hi'] = $tr->translate($mainString);
    			$result[$key] = $data;
    		}
			return json_encode($result);
    	} catch (Exception $e) {
    		Log::error($e);
    	}
    }
}