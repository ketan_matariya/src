<?php

namespace App\Helper;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Hash;

class NotificationHelper extends Controller
{
	public static function notification($message, $server_api_key, $token, $title) 
	{
	    $server_api_key = "AAAAIy2XKRg:APA91bGxox_lBNewVJZE_oXolcUfhyACsGlXBSnOrAuNMbzgqWEYrrPlGA5K5cLftikt6FW8SGB4SPXoXvPfB4aJtKFfS0XpUoTe1qfJmAv3stP0uzZxdHN2CYo0tNfn_8uw-4KNA9jk";
	    $msg = array
	           (
					'body' 	=> $message,
					'title'	=> $title,
		         	'icon'	=> 'myicon',/*Default Icon*/
		          	'sound' => 'mySound'/*Default sound*/
	           );
		$fields = array
				(
					'to'		=> $token,
					'notification'	=> $msg,
				);
		$headers = array
				(
					'Authorization: key='.$server_api_key,
					'Content-Type: application/json'
				);

		#Send Reponse To FireBase Server	
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
		return $result;
	}
	public static function notificationDriver($message, $server_api_key, $token, $title) 
	{
	    $server_api_key = "AAAAp7uXP-8:APA91bG9DHCjviwTsX7f7L8wVVD5hRD_ZcHoq0ZMzNRI9QVX7kMh6nmJX_APcHBMK-lLGGLOU_BG6eHACbviFfUvf84oRS2c6zThllij8nw-NEpQMa2HODwK3hKHrfxAD980th9Bml0q";
	    $msg = array
	           (
					'body' 	=> $message,
					'title'	=> $title,
		         	'icon'	=> 'myicon',/*Default Icon*/
		          	'sound' => 'mySound'/*Default sound*/
	           );
		$fields = array
				(
					'to'		=> $token,
					'notification'	=> $msg,
				);
		$headers = array
				(
					'Authorization: key='.$server_api_key,
					'Content-Type: application/json'
				);

		#Send Reponse To FireBase Server	
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
		return $result;
	}
	
}