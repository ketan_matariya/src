<?php

namespace App\Helper;

use Illuminate\Http\Request;
use Mail;

class MailHelper 
{
    public static function sendOTP($data="") 
	{
	    // dd($product);
	    try {
			if($data=="") {
				return "false";
			} else{
				    $sendMail = Mail::send('emails.company.sendOTP',['data'=>$data] , function($message) use ($data) {
			        $message->to($data['email'], $data['name'])->subject('NileMart OTP details');
			        $message->from('testerdeveloper195@gmail.com','NileMart');
			    });
		        if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}

	public static function productNotification($data="") 
	{
	    // dd($product);
	    try {
			if($data=="") {
				return "false";
			} else{
				$sendMail = Mail::send('emails.company.productNotification',['data'=>$data] , function($message) use ($data) {
			        $message->to($data['email'], $data['name'])->subject('NileMart product details');
			        $message->from('testerdeveloper195@gmail.com','NileMart');
			    });
		        if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}

	public static function invoiceNotification($data="") 
	{
		try {
			if($data=="") {
				return "false";
			} else{
				$sendMail = Mail::send('emails.customer.invoice',['data'=>$data] , function($message) use ($data) {
			        $message->to($data['email'], $data['name'])->subject('NileMart order invoice');
			        $message->from('testerdeveloper195@gmail.com','NileMart');
			        $message->attach(public_path('invoices/'.$data['order']->id.'.pdf'));
			    });
		        if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}
	
	public static function customerOrder($data="") 
	{
		try {
			if($data=="") {
				return "false";
			} else{
				$sendMail = Mail::send('emails.admin.costomerOrderDetails',['data'=>$data] , function($message) use ($data) {
			          $message->to(config('mail.username'))->subject('NileMart Order');
			        $message->from('testerdeveloper195@gmail.com','NileMart');
			    });
		        if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}
	
	public static function customerOrderToAdmin($data="") 
	{
		try {
			if($data=="") {
				return "false";
			} else{
				$sendMail = Mail::send('emails.admin.costomerOrderDetails',['data'=>$data] , function($message) use ($data) {
			       
			      $message->to(config('mail.username'))->subject('NileMart');
			        $message->cc($data['email'], $data['customer']['name'])->subject('NileMart');
			        $message->from('testerdeveloper195@gmail.com','Nilemart');
			    });
		        if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}
    
    public static function errorMail($data="") 
	{
	    try {
			if($data=="") {
				return "false";
			} else{
				$sendMail = Mail::send('emails.company.error',['data'=>$data] , function($message) use ($data) {
			        $message->to('testerdeveloper195@gmail.com', 'NileMart')->subject('NileMart');
			        $message->from('testerdeveloper195@gmail.com','NileMart');
			    });
		        if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}
	
	public static function sendMailCustomer($data="") 
	{
	    try {
			if($data=="") {
				return "false";
			} else{
				$sendMail = Mail::send('emails.customer.contactUs',['data'=>$data] , function($message) use ($data) {
			        $message->to($data['email'])->subject('NileMart');
			      	$message->cc(config('mail.username'))->subject('NileMart');
			        $message->from('testerdeveloper195@gmail.com','NileMart');
			    });
		        if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}
	
}