<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Offers\Offer;
use App\Shop\Products\Product;
use App\Shop\Categories\Category;
use App\Shop\Addresses\Address_1;
use App\Shop\Orders\Order;
use App\Shop\OrderProducts\OrderProduct;
use App\Shop\Products\Product_attributes;
use App\Shop\Offers\OfferProduct;
use App\Shop\Orders\Repositories\Interfaces\OrderRepositoryInterface;
use App\Shop\Orders\Repositories\OrderRepository;
use App\Shop\Couriers\Repositories\CourierRepository;
use App\Shop\Couriers\Repositories\Interfaces\CourierRepositoryInterface;
use App\Shop\Drivers\Driver;
use App\Shop\Countries\Country;
use App\Shop\Cities\City;
use App\Shop\Customers\Customer;
use App\Shop\Addresses\Address;
use App\Shop\Addresses\OrderAddressDriver;
use App\Shop\Addresses\Repositories\Interfaces\AddressRepositoryInterface;
use App\Shop\Addresses\Transformations\AddressTransformable;
use App\Shop\OrderStatuses\Repositories\Interfaces\OrderStatusRepositoryInterface;
use App\Shop\OrderStatuses\Repositories\OrderStatusRepository;
use App\Shop\CMS\TearmCondtion;
use App\Shop\CMS\PrivacyPolicy;
use App\Shop\CMS\Faq;
use Auth;
use Mail;

class ProfileController extends Controller
{
    use AddressTransformable;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepo;

    /**
     * @var CourierRepositoryInterface
     */
    private $courierRepo;

    /**
     * @var AddressRepositoryInterface
     */
    private $addressRepo;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepo;

    /**
     * @var OrderStatusRepositoryInterface
     */
    private $orderStatusRepo;

    public function __construct(
        OrderRepositoryInterface $orderRepository,
        CourierRepositoryInterface $courierRepository,
        AddressRepositoryInterface $addressRepository,
        OrderStatusRepositoryInterface $orderStatusRepository
        
    ) {
        $this->orderRepo = $orderRepository;
        $this->courierRepo = $courierRepository;
        $this->addressRepo = $addressRepository;
        $this->orderStatusRepo = $orderStatusRepository;
    }
    public function profile()
    {
    	$data['offers'] = Offer::get();
        $data['products'] = Product::join('employees','employees.id','products.company_id')
                            ->join('categories','categories.id','products.category_id')
                            ->leftjoin('product_attribute','product_attribute.product_id','products.id')
                            ->select('products.*', 'employees.name as company', 'categories.name as category','product_attribute.price as proPrice','product_attribute.sale_price as proSaleprice')
                            ->orderBy('id', 'DESC')
                            ->where('employees.status',1)
                            ->get();
        // dd($data['products']);
       
        $data['categories'] = Category::join('employees', 'employees.id', 'categories.company_id')->Select('categories.id', 'categories.name','categories.status', 'categories.cover')
                            ->where('employees.status',1)
                            ->where('parent_id', null)
                            ->get();
        $data['address'] = Address_1::where(['user_id'=>auth()->user()->id])->first();

    	return view('front.profile')->with($data);
    }

    public function contactus()
    {
        
    	$data['offers'] = Offer::get();
        $data['products'] = Product::leftjoin('product_attribute','product_attribute.product_id','products.id')->get();
        // dd($data['products']);
       
        $data['categories'] = Category::join('employees', 'employees.id', 'categories.company_id')->Select('categories.id', 'categories.name','categories.status', 'categories.cover')
                            ->where('employees.status',1)
                            ->where('parent_id', null)
                            ->get();
        return view('front.contactus')->with($data);
    }





    public function aboutus()
    {
        $data['offers'] = Offer::get();
        $data['products'] = Product::leftjoin('product_attribute','product_attribute.product_id','products.id')->get();
        // dd($data['products']);
       
        $data['categories'] = Category::join('employees', 'employees.id', 'categories.company_id')->Select('categories.id', 'categories.name','categories.status', 'categories.cover')
                            ->where('employees.status',1)
                            ->where('parent_id', null)
                            ->get();
        return view('front.aboutus')->with($data);
    }

    public function orders()
    {
        $data['categories'] = Category::join('employees', 'employees.id', 'categories.company_id')->Select('categories.id', 'categories.name','categories.status', 'categories.cover')
        ->where('employees.status',1)
        ->where('parent_id', null)
        ->get();

        $data['orders'] = Order::join('customers', 'customers.id', '=', 'orders.customer_id')
            ->join('order_statuses','order_statuses.id', '=','orders.order_status_id' )
            ->select('customers.name as customer_name','customers.mobile', 'order_statuses.name as status', 'order_statuses.color as color', 'orders.*')
            ->where(['customer_id'=>auth()->user()->id])
            ->get();
        
        $data['address'] = Address_1::where(['user_id'=>auth()->user()->id,'default'=>1])->first(); 
        return view('front.myorder')->with($data); 
    }
    public function show($orderId)
    {
        $data['categories'] = Category::join('employees', 'employees.id', 'categories.company_id')->Select('categories.id', 'categories.name','categories.status', 'categories.cover')
                       ->where('employees.status',1)
                       ->where('parent_id', null)
                       ->get();

        $order = $this->orderRepo->findOrderById($orderId);
                       // dd($order);
                       if(isset($order->driver_id)){
                           $driver = Driver::find($order->driver_id);
                           $order->driver = $driver->name;
                       }
                       $order->courier = $this->courierRepo->findCourierById($order->courier_id);
                       $order->address = $this->addressRepo->findAddressById($order->address_id);
                       $order->order_address = Address_1::join('order_address_driver','order_address_driver.address_id','address.id')
                                                   ->where('order_id',$orderId)
                                                   ->first();
                       date_default_timezone_set('Asia/Kolkata');
                       $current_date = date('Y-m-d');
                       $orderRepo = new OrderRepository($order);

        $current_date = date('Y-m-d');             
        $items = OrderProduct::where('order_id', $orderId)->get();
        foreach($items as $item){
            $attr = Product_attributes::find($item->attribute_id);
            $hsn = Product::find($item->product_id);
            if($attr){
                $item->product_price = $attr->sale_price;
                $item->unit = $attr->value." ".$attr->key;
            }
            if($item->offer_id!=0){
                $offer = OfferProduct::join('offers','offers.id','offer_product.offer_id')
		    							->where('product_id',$item->product_id)
		    							->where('expired_date','>=',$current_date)
		    							->where('offer_id',$item->offer_id)
		    							->where('status',1)
		    							->first();	
    		    $offer_status=1;		
                $item->offer_product = $hsn->offer_product;
            }
        }
        $data['currentStatus'] = $this->orderStatusRepo->findOrderStatusById($order->order_status_id);
        $data['items'] =  $items;
        $data['order'] =   $order;

        // dd($data['order']);
        return view('front.showmyorder')->with($data); 
    }

    public function editProfile(Request $request,$id)
    {
        
        $userData = Customer::find($id);
        $userData->name  = $request->fullname;
        $userData->email  = $request->useremail;
        $userData->save();

        $data['categories'] = Category::join('employees', 'employees.id', 'categories.company_id')->Select('categories.id', 'categories.name','categories.status', 'categories.cover')
                            ->where('employees.status',1)
                            ->where('parent_id', null)
                            ->get();

        $data['offers'] = Offer::get();
        $data['products'] = Product::join('employees','employees.id','products.company_id')
                            ->join('categories','categories.id','products.category_id')
                            ->leftjoin('product_attribute','product_attribute.product_id','products.id')
                            ->select('products.*', 'employees.name as company', 'categories.name as category','product_attribute.price as proPrice','product_attribute.sale_price as proSaleprice')
                            ->orderBy('id', 'DESC')
                            ->where('employees.status',1)
                            ->get();
        $data['address'] = Address_1::where(['user_id'=>auth()->user()->id])->first();
    
        return view('front.profile')->with($data);
    }

    public function tearmcondition()
    {
        
        $data['offers'] = Offer::get();
        $data['products'] = Product::leftjoin('product_attribute','product_attribute.product_id','products.id')->get();
        // dd($data['products']);
       
        $data['categories'] = Category::join('employees', 'employees.id', 'categories.company_id')->Select('categories.id', 'categories.name','categories.status', 'categories.cover')
                            ->where('employees.status',1)
                            ->where('parent_id', null)
                            ->get();

        $tearmcondition = TearmCondtion::get();
        
        foreach ($tearmcondition as $key => $value) {
            $data['tearmcondition'] = $value;
        }
        return view('front.tearmcondtion')->with($data);
    }  

    public function privacypolicy()
    {
        
        $data['offers'] = Offer::get();
        $data['products'] = Product::leftjoin('product_attribute','product_attribute.product_id','products.id')->get();
        // dd($data['products']);
       
        $data['categories'] = Category::join('employees', 'employees.id', 'categories.company_id')->Select('categories.id', 'categories.name','categories.status', 'categories.cover')
                            ->where('employees.status',1)
                            ->where('parent_id', null)
                            ->get();

        $privacypolicy = PrivacyPolicy::get();
        
        foreach ($privacypolicy as $key => $value) {
            $data['privacypolicy'] = $value;
        }
        return view('front.privacypolicy')->with($data);
    }

    public function faq()
    {
        
        $data['offers'] = Offer::get();
        $data['products'] = Product::leftjoin('product_attribute','product_attribute.product_id','products.id')->get();
        // dd($data['products']);
       
        $data['categories'] = Category::join('employees', 'employees.id', 'categories.company_id')->Select('categories.id', 'categories.name','categories.status', 'categories.cover')
                            ->where('employees.status',1)
                            ->where('parent_id', null)
                            ->get();

        $data['faq'] = Faq::get();

        return view('front.faq')->with($data);
    }    
}
