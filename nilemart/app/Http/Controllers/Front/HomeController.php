<?php

namespace App\Http\Controllers\Front;

use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Shop\Offers\OfferProduct;
use App\Shop\Offers\Offer;
use App\Shop\Products\Product;
use App\Shop\AdditionalInfos\AdditionalInfo;
use App\Shop\Categories\Category;
use App\Shop\Products\Product_attributes;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\Shop\Carts\Cart;

class HomeController
{
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepo;

    /**
     * HomeController constructor.
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepo = $categoryRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data['offers'] = Offer::get();
        
        $data['login'] =  (Auth::check()) ? 'true':'false';
        $data['NewProducts'] = Product::select('products.*','employees.name as company','product_attribute.sale_price','product_attribute.available_quantity','product_attribute.id as attribute_id','product_attribute.max_quantities')
                          ->join('product_attribute','product_attribute.product_id','products.id')
                          ->join('employees','employees.id','products.company_id')
                          ->orderBy('product_attribute.id','DESC')
                          ->where(['employees.status'=>1,'products.status'=>1])
                          ->get();
                          $data['products']=[];
                          foreach($data['NewProducts'] as $product)
                          {
                              if($data['login']=='true')
                              {
                                  $attributes=Product_attributes::where('product_id',$product->id)->first();
                                  $cartItem = Cart::where(['customer_id'=> auth()->user()->id,'product_id'=>$product->id,'attribute_id'=>$attributes->id])->orderBy('attribute_id','DESC')->first();
                                  if(!empty($cartItem))
                                  {
                                    $product['quantity']= $cartItem->quantity;
                                  }
                                }
                                $data['products'][$product->id]=$product;
                          }
        
        $data['offerProduct'] = OfferProduct::join('products','products.id','offer_product.product_id')->join('product_attribute', 'product_attribute.id','offer_product.attribute_id')->select('products.id','products.name as name','product_attribute.price','product_attribute.sale_price','products.cover','product_attribute.discount')->get();

        $data['categories'] = Category::with('children')->whereNull('parent_id')->where('status',1)->get();
         
        return view('front.index')->with($data);
        

    }
    public function productCheckcart(Request $request){
       
        $customer_id = $request->get('customer_id');
        
       $product_id = $request->get('product_id');
        
        $product_attr = $request->get('attribute_id');

       $quantity = $request->get('quantity');
      
        $getCart = Cart::where(['customer_id'=> $customer_id,'product_id'=>$product_id,'attribute_id'=>$product_attr])->count();
        if($getCart>0){
            $getCart =  Cart::where(['customer_id'=> $customer_id,'product_id'=>$product_id,'attribute_id'=>$product_attr])->first();
            return  $getCart;    
        }
        else{

            return 1;
        }

    }
    public function SelectUnitPrice($id)
    {
       
        $SelectUnitPrice = Product_attributes::where('id',$id)->select('sale_price','price','key','value','discount','max_quantities','available_quantity')->get();
      
        return response()->json($SelectUnitPrice);
    }

   

}
