<?php

namespace App\Http\Controllers\Front;

use App\Shop\Products\Product;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Shop\Products\Transformations\ProductTransformable;
use App\Shop\Carts\Repositories\Interfaces\CartRepositoryInterface;
use App\Shop\Offers\OfferProduct;
use App\Shop\Offers\Offer;
use App\Shop\AdditionalInfos\AdditionalInfo;
use App\Shop\Categories\Category;
use App\Shop\Products\Product_attributes;
use Illuminate\Http\Request;
use App\Shop\Addresses\Address_1;
use App\Shop\Carts\Cart;
use Auth;
use DB;
class ProductController extends Controller
{
    use ProductTransformable;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepo;
    public function __construct(
        CartRepositoryInterface $cartRepository
        // CourierRepositoryInterface $courierRepository,
        // AddressRepositoryInterface $addressRepository,
        // CustomerRepositoryInterface $customerRepository,
        // ProductRepositoryInterface $productRepository,
        // OrderRepositoryInterface $orderRepository,
        // ShippingInterface $shipping
    ) {
        $this->cartRepo = $cartRepository;
        // $this->courierRepo = $courierRepository;
        // $this->addressRepo = $addressRepository;
        // $this->customerRepo = $customerRepository;
        // $this->productRepo = $productRepository;
        // $this->orderRepo = $orderRepository;
        // $this->payPal = new PayPalExpressCheckoutRepository;
        // $this->shippingRepo = $shipping;
    }
    /**
     * ProductController constructor.
     * @param ProductRepositoryInterface $productRepository
     */
    // public function __construct(ProductRepositoryInterface $productRepository)
    // {
    //     $this->productRepo = $productRepository;
    // }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    // public function search()
    // {
    //     if (request()->has('q') && request()->input('q') != '') {
    //         $list = $this->productRepo->searchProduct(request()->input('q'));
    //     } else {
    //         $list = $this->productRepo->listProducts();
    //     }

    //     $products = $list->where('status', 1)->map(function (Product $item) {
    //         return $this->transformProduct($item);
    //     });

    //     return view('front.products.product-search', [
    //         'products' => $this->productRepo->paginateArrayResults($products->all(), 10)
    //     ]);
    // }

    /**
     * Get the product
     *
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
   

    public function productCategory()
    {
        $data['offers'] = Offer::get();
        $data['products'] = Product::leftjoin('product_attribute','product_attribute.product_id','products.id')->get();
       
        $data['categories'] = Category::join('employees', 'employees.id', 'categories.company_id')->Select('categories.id', 'categories.name','categories.status', 'categories.cover')
                            ->where('employees.status',1)
                            ->where('parent_id', null)
                            ->get();
        return view('front.products.productCategory')->with($data);
    }

    public function productSubCategory($id)
    {
        $data['offers'] = Offer::get();
        $data['products'] = Product::leftjoin('product_attribute','product_attribute.product_id','products.id')->get();

        $data['categories'] = Category::join('employees', 'employees.id', 'categories.company_id')->Select('categories.id', 'categories.name','categories.status', 'categories.cover')
                            ->where('employees.status',1)
                            ->where('parent_id', null)
                            ->get();
       
        $data['subCategories'] = Category::join('employees', 'employees.id', 'categories.company_id')
                            ->Select('categories.id', 'categories.name','categories.status', 'categories.cover')
                            ->where('employees.status',1)
                            ->where('categories.parent_id',$id)
                            ->get();

          $data['getproducts'] = Product::leftjoin('product_attribute','product_attribute.product_id','products.id')->where('products.subcategory_id',$id)->first();

        return view('front.products.productSubCategory')->with($data);
    }

    public function productSubSubCategory($id)
    {

       $data['offers'] = Offer::get();
        $data['products'] = Product::leftjoin('product_attribute','product_attribute.product_id','products.id')->get();

       $data['categories'] = Category::with('children')->whereNull('parent_id')->where('status',1)->get();


        $data['getproducts'] = Product::leftjoin('product_attribute','product_attribute.product_id','products.id')
                                    ->where('products.subcategory_id',$id)->first();

        $data['subsubcategories'] = Category::select('id','name','cover')->where('categories.parent_id',$id)->get();

//        dd($data['subsubcategories'] );
        $data['cat']=$id;
        return view('front.products.productSubSubCategory')->with($data);

        return $data;
    }

    public function productDetail($id)
    {
        $data['cartItems'] = $this->cartRepo->getCartItemsTransformed();
        $cart = Cart::where('product_id',$id)->first();
        if(!empty($cart)) 
        {
            $data['cart'] = $cart;
        }
        $data['offers'] = Offer::get();
        $data['products'] = Product::leftjoin('product_attribute','product_attribute.product_id','products.id')->where('product_attribute.product_id',$id)->first();
       $attributes=Product_attributes::where('product_id',$id)->first();
       $data['product'] = Product_attributes::where('product_id',$id)->get();
       $data['login'] =  (Auth::check()) ? 'true':'false';
       if($data['login']=='true')
       {
           $cartItem = Cart::where(['customer_id'=> auth()->user()->id,'product_id'=>$id,'attribute_id'=>$attributes->id])->first();
           if(!empty($cartItem))
           {
               $data['quantity']= $cartItem->quantity;
            }
        }
       
        $data['categories'] = Category::join('employees', 'employees.id', 'categories.company_id')->Select('categories.id', 'categories.name','categories.status', 'categories.cover')
                            ->where('employees.status',1)
                            ->where('categories.status',1)
                            ->where('parent_id', null)
                            ->get();

        $data['getproducts'] = Product::leftjoin('product_attribute','product_attribute.product_id','products.id')->where('products.subcategory_id',$id)->first();
        return view('front.products.productDetail')->with($data);
    }
   
    public function productListing()
    {
        $data['offers'] = Offer::get();
        $categories = Category::join('employees', 'employees.id', 'categories.company_id')
        ->Select('categories.id', 'categories.name','categories.status', 'categories.cover','categories.parent_id')
        ->where('employees.status',1)
        ->where('categories.parent_id','!=',null);
        $getSubcategory= ($categories->count()>0) ?   $categories->get() :null;    
        $data['subcategory']=$getSubcategory;  
        $data['products'] = Product::leftjoin('product_attribute','product_attribute.product_id','products.id')->groupBy('products.id')->get();
        $getMaincategry= Product::leftjoin('product_attribute','product_attribute.product_id','products.id')->where('products.subcategory_id','!=',null)->count();
        if($getMaincategry>0){
            $data['products'] = Product::leftjoin('product_attribute','product_attribute.product_id','products.id')->where('products.subcategory_id','!=',null)->get();
            $data['getproducts'] = Product::leftjoin('product_attribute','product_attribute.product_id','products.id')->where('products.subcategory_id','!=',null)->first();
         }
         else{
            $data['products'] = Product::leftjoin('product_attribute','product_attribute.product_id','products.id')->where('products.category_id','!=',null)->get();
            $data['getproducts'] = null;
        }
        $data['categories'] = Category::join('employees', 'employees.id', 'categories.company_id')->Select('categories.id', 'categories.name','categories.status', 'categories.cover')
                            ->where('employees.status',1)
                            ->where('parent_id', null)
                            ->get();
        return view('front.products.productListing')->with($data);
    }

    public function productCategoryListing($id)
    {
             
        $data['categories'] = Category::join('employees', 'employees.id', 'categories.company_id')->Select('categories.id', 'categories.name','categories.status', 'categories.cover','categories.parent_id')
                            ->where('employees.status',1)
                            ->where('parent_id', null)
                            ->get();
                            $categories = Category::join('employees', 'employees.id', 'categories.company_id')
                            ->Select('categories.id', 'categories.name','categories.status', 'categories.cover','categories.parent_id')
                            ->where('employees.status',1)
                            ->where('categories.parent_id',$id);

         $getSubcategory= ($categories->count()>0) ?   $categories->get() :null;    
         $data['subcategory']=$getSubcategory;    
        $data['products'] = Product::leftjoin('product_attribute','product_attribute.product_id','products.id')->where('products.subcategory_id',$id)->get();
        $data['cat']=$id;
        
        return view('front.products.productListing')->with($data);   
    }

    public function productCategoryListingCat($cat)
    {
        $data['categories'] = Category::join('employees', 'employees.id', 'categories.company_id')->select('categories.id', 'categories.name','categories.status', 'categories.cover','categories.parent_id')
                            ->where('employees.status',1)
                            ->where('categories.status',1)
                            ->where('parent_id', null)
                            ->get();
                     
         $categories = Category::join('employees', 'employees.id', 'categories.company_id')
                            ->select('categories.id', 'categories.name','categories.status', 'categories.cover','categories.parent_id')
                            ->where('employees.status',1)
                            ->where('categories.status',1)
                            ->where('categories.parent_id',$cat);
                           
         $getSubcategory = ($categories->count()>0) ?   $categories->get() :null;        
                  
         $data['login'] =  (Auth::check()) ? 'true':'false';
      
         $data['subcategory']=$getSubcategory;  
           
         $getMaincategry= Product::leftjoin('product_attribute','product_attribute.product_id','products.id')->where('products.subcategory_id',$cat)->count();
          
        // dd($getMaincategry);
         if($getMaincategry>0){
             
            $data['NewProducts'] = Product::select('products.*','employees.name as company','product_attribute.sale_price','product_attribute.available_quantity','product_attribute.id as attribute_id','product_attribute.max_quantities')
                          ->join('product_attribute','product_attribute.product_id','products.id')
                          ->join('employees','employees.id','products.company_id')
                          ->orderBy('product_attribute.id','DESC')
                          ->where(['employees.status'=>1,'products.status'=>1,'products.subcategory_id'=>$cat])
                          ->orwhere('subsubcategory_id',$cat)
                          ->get();
                          $data['products']=[];
                          foreach($data['NewProducts'] as $product)
                          {
                              if($data['login']=='true')
                              {
                                  $attributes=Product_attributes::where('product_id',$product->id)->first();
                                  $cartItem = Cart::where(['customer_id'=> auth()->user()->id,'product_id'=>$product->id,'attribute_id'=>$attributes->id])->orderBy('attribute_id','DESC')->first();
                                  //$cartItem = Cart::where('customer_id',auth()->user()->id)->orderBy('id','DESC')->get();
                                  if(!empty($cartItem))
                                  {
                                    $product['quantity']= $cartItem->quantity;
                                  }
                                }
                                $data['products'][$product->id]=$product;
                          }


           // $data['products'] = Product::leftjoin('product_attribute','product_attribute.product_id','products.id')
           // ->where('products.subcategory_id',$cat)->get();

            $data['getproducts'] = Product::leftjoin('product_attribute','product_attribute.product_id','products.id')->where('products.subcategory_id',$cat)->first();

            $data['getsubcategories'] = Category::select('id','name')->where('categories.parent_id',$cat)->get();

         }
         else{

            $data['NewProducts'] = Product::select('products.*','employees.name as company','product_attribute.sale_price','product_attribute.available_quantity','product_attribute.id as attribute_id','product_attribute.max_quantities')
            ->join('product_attribute','product_attribute.product_id','products.id')
            ->join('employees','employees.id','products.company_id')
            ->orderBy('product_attribute.id','DESC')
            ->where(['employees.status'=>1,'products.status'=>1,'products.category_id'=>$cat])
            ->orwhere('subsubcategory_id',$cat)
            ->get();
            // dd( $data['NewProducts']);
            $data['products']=[];
            foreach($data['NewProducts'] as $product)
            {
                if($data['login']=='true')
                {
                    $attributes=Product_attributes::where('product_id',$product->id)->first();
                    $cartItem = Cart::where(['customer_id'=> auth()->user()->id,'product_id'=>$product->id,'attribute_id'=>$attributes->id])->orderBy('attribute_id','DESC')->first();
                    //$cartItem = Cart::where('customer_id',auth()->user()->id)->orderBy('id','DESC')->get();
                    if(!empty($cartItem))
                    {
                      $product['quantity']= $cartItem->quantity;
                    }
                  }
                  $data['products'][$product->id]=$product;
            }

            // $data['products'] = Product::leftjoin('product_attribute','product_attribute.product_id','products.id')
            // ->where('products.category_id',$cat)->get();

            $data['getproducts'] = null;

        }
        
        $data['categori'] = Category::with('children')->whereNull('parent_id')->where('status',1)->first();


        $data['cat']=$cat;
        $data['parantid']=$cat;
        
        // dd($data);
        
        return view('front.products.productListing')->with($data);
    }

    public function checkout(){
        
        $data['offers'] = Offer::get();
        $data['products'] = Product::leftjoin('product_attribute','product_attribute.product_id','products.id')->get();
        if(Auth::check()){

          
            $data['cartitems'] = Cart::with(['products','productsattribute'])->where(['customer_id'=>Auth::user()->id]);
           
            if($data['cartitems']->count()>0){
                
                $data['totalamount'] = Cart::select(DB::raw('sum(total_price) as total'))->where(['customer_id'=>Auth::user()->id])->first();
                $data['totalprice'] = Cart::where(['customer_id'=>Auth::user()->id])->first();
                $data['newtotalamount'] =  $data['totalamount'];
                $data['totalshipamount'] = Cart::select(DB::raw('sum(total_price * quantity + shipping_price) as totalship'))->where(['customer_id'=>Auth::user()->id])->first();
                $data['totalshiping'] = Cart::select(DB::raw('sum(shipping_price) as totalshipping'))->where(['customer_id'=>Auth::user()->id])->first();
                $data['address']=Address_1::where(['user_id'=>Auth::user()->id]);
               
                 foreach($data['address']->get() as $result)
                 {
                     if($result->default==1)
                     {
                        $data['activAddress'] = Address_1::where(['user_id'=>Auth::user()->id,'default'=>$result->default])->first();
                     }
                 }
                
                $data['getcntaddress']=$data['address']->count();
                $data['latsaddress']=$data['address'];
                
            }
            else{
                $data['totalamount']=0.00;
                $data['totalshiping'] = 0.00;
                $data['totalshipamount'] =0.00;
                $data['address']= "";
                $data['getcntaddress']=0;
            }
        }
        else{
            
            $data['cartitems']=0;
            $data['totalamount']=0.00;
            $data['totalshiping'] = 0.00;
            $data['totalshipamount'] =0.00;
        }
   
    //    echo $data['address'];
    //    exit;
     
        $data['categories'] = Category::join('employees', 'employees.id', 'categories.company_id')->Select('categories.id', 'categories.name','categories.status', 'categories.cover')
                            ->where('employees.status',1)
                            ->where('parent_id', null)
                            ->get();

        if(auth()->check() == "true"){
            return view('front.checkout')->with($data);
        }
        else{
          return view('auth.login')->with($data);
        }
    }
    
    public function updatecart(Request $request){
        try{
            echo $request->input('cartid');
        }
        catch(Exception $e){

        }
    }
    public function myorder()
    {
        $data['offers'] = Offer::get();
        $data['products'] = Product::leftjoin('product_attribute','product_attribute.product_id','products.id')->get();
        // dd($data['products']);
       
        $data['categories'] = Category::join('employees', 'employees.id', 'categories.company_id')->Select('categories.id', 'categories.name','categories.status', 'categories.cover')
                            ->where('employees.status',1)
                            ->where('parent_id', null)
                            ->get();
        return view('front.myorder')->with($data);
    }
    function getProductSearch(Request $request)
    {
        if($request->get('query'))
        {
          $query = $request->get('query');
          $data = Product::where('name', 'LIKE', "%{$query}%")->orderBy('name', 'asc')->get();
          $output = '<ul class="dropdown-menu" style="display:block; position:relative; width:100%;margin-bottom: -1100px; overflow:scroll;">';
          foreach($data as $row)
          {
           $output .= '<li><a style="font-size: 15px;" href="'.route("productDetail.product",$row->id).'">'.$row->name.'</a></li>';
          }
          $output .= '</ul>';
          echo $output;
        }
        
    }

    public function defaultAddres(Request $request){
        
        $user_id = $request->get('user_id');
        $address_id = $request->get('address_id');
        $activAddress = Address_1::where(['user_id'=>$user_id,'id'=>$address_id])->first();
        return $activAddress;
    }

    public function deleteCart(Request $request)
    {
        $user_id = $request->get('user_id');
        $attributeid = $request->get('attributeid');
        $product_id = $request->get('product_id');
        $result = Cart::where(['customer_id'=>$user_id,'product_id'=>$product_id,'attribute_id'=>$product_id])->delete();
        
    }

   
}
