<?php

namespace App\Http\Controllers\Admin\Drivers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Drivers\Driver;
use App\Shop\Vehicles\Vehicle;
use App\Helper\MailHelper;
use App\Helper\RandomHelper;
use App\Helper\Permission;
use Validator;
use Log;
use Hash;

class DriverController extends Controller
{
	public function show()
	{
	    try 
		{
            $permission = Permission::permission('driver');
            if($permission->view==1) {
                // $data['drivers'] = Driver::select('drivers.*',
                //     'vehicle.vehicle_model_name as vehiclename'
                // )->leftjoin('vehicle','vehicle.id','drivers.vehicle_type')->get();
                $data['drivers'] = Driver::get();
                $data['permission'] = $permission;
                return view('admin.drivers.list')->with($data);
            } else {
                return view('layouts.errors.403');
            }
        }
		catch (Exception $e)
		{
			Log::error($e);
		}
		
    }
      
    public function create(Request $request)
    {
        try
        {
            $permission = Permission::permission('driver');
            // dd($permission);
            if($permission->add==1) {
                // $data['vehicles'] = Vehicle::select('vehicle_model_name','id')->where('status',1)->get();
                $data['vehicles'] = [];
                $data['drivers'] = Driver::all();
                return view('admin.drivers.create')->with($data);
            } else {
                return view('layouts.errors.403');
            }
        }
        catch (Exception $e) 
    	{
    		Log::error($e);
    	}
    }
    public function store(Request $request){
        try{
            $permission = Permission::permission('driver');
            if($permission->add==1 || $permission->edit==1) {
                $rules = [
                    // 'vehicletype' => 'required',
                    'name' => 'required',
                    'email' => 'required',
                    'contact' => 'required',
                    'status' => 'required',
                    // 'vehicle_reg_no' => 'required',
                    'licence_no' => 'required',
                    // 'bank_account_no' => 'required',
                    // 'bank_name' => 'required',
                    // 'account_hold_name' => 'required',
                    // 'postcode' => 'required',
                    'address'=>'required',
                    'country_code' => 'required'
                ];
                $customMsg = [
                    'vehicletype.required' => 'Please enter vehicle type',
                    'name.required' => 'Please enter name',
                    'email.required' => 'Please enter email',
                    'contact.required' => 'Please enter contact',
                    'status.required' => 'Please select status',
                    'vehicle_reg_no.required' => 'Please enter  vehicle registration ',
                    'licence_no.required' => 'Please enter vehicle type',
                    'country_code.required' => 'Please enter country code',
                ];
                if (isset($request->id)) {
                    $drivers = Driver::find($request->id);
                    $oldMobile = $drivers->contact;
                    $newMobile = $request->contact;
                    if ($oldMobile!=$newMobile) {
                        $rules['contact'] = 'required|unique:drivers,contact';
                    }
                    if ($drivers->email!=$request->email) {
                        $rules['email'] = 'required|unique:drivers,email';
                    }
                } else{
                    $rules['contact'] = 'required|unique:drivers,contact';
                    $rules['email'] = 'required|unique:drivers,email';
                }
                $validator = Validator::make($request->all(), $rules ,$customMsg);
                if ($validator->fails()) {
                    return back()->withErrors($validator->errors())->withInput();
                }else{
                    if ($request->id) {
                        $id=$request->id;
                        $drivers = Driver::find($id);
                    }else{
                        $drivers = new Driver();
                    }
                    // dd('enter');
                    // dd($request->all());
                    // $drivers->vehicle_type = request('vehicletype');
                    $drivers->name = request('name');
                    $drivers->name_hi = request('name_hi');
                    $drivers->name_gu = request('name_gu');
                    $drivers->email = request('email');
                    $drivers->contact = request('contact');
                    $drivers->status = request('status');
                    // $drivers->vehicle_reg_no = request('vehicle_reg_no');
                    $drivers->licence_no = request('licence_no');
                    // $drivers->bank_acc_no = request('bank_account_no');
                    // $drivers->bank_name = request('bank_name');
                    // $drivers->acc_holder_name = request('account_hold_name');
                    // $drivers->postalcode = request('postcode');
                    $drivers->address = trim(strip_tags($request->address));
                    $drivers->address_hi = trim(strip_tags($request->address_hi));
                    $drivers->address_gu = trim(strip_tags($request->address_gu));
                    $drivers->country_code = trim(strip_tags($request->country_code));
                    // if ($request->id=="") {
                    //     $password = RandomHelper::driverPassword();
                    //     $drivers->password = $password;
                    //     $drivers->token = md5(time());
                    //     MailHelper::DriverRegister($drivers);
                    //     $drivers->password = Hash::make($password);
                    // }
                    
                    if($drivers->save()){
                        if ($request->has('profile')) {
                            $file = $request->profile;
                            $extension = $file->getClientOriginalExtension();                
                            $time = rand(00,100000);
                            $filename = $time.".".$extension;
                            $file->move(public_path(env('IMAGE_URL').'drivers'), $filename);
                            $drivers = Driver::find($drivers->id);
                            $drivers->profile = $filename;
                            $drivers->save(); 
                        }
                        // if ($request->has('police_certificate')) {
                        //     $file = $request->police_certificate;
                        //     $extension = $file->getClientOriginalExtension();                
                        //     $time = rand(00,100000);
                        //     $filename = $time.".".$extension;
                        //     $file->move(public_path(env('IMAGE_URL').'drivers/police-certificate'), $filename);
                        //     $drivers = Driver::find($drivers->id);
                        //     $drivers->police_certificate = $filename;
                        //     $drivers->save(); 
                        // }
                    }
    
                    if($request->id){
                        $request->session()->flash('message', 'Driver updated successfully');    
                    }else{
                        $request->session()->flash('message', 'Driver created successfully!');
                    }
                    return redirect('admin/driver/index');   
                } 
            } else {
                return view('layouts.errors.403');
            }    
        }catch (Exception $e){
            Log::error($e); 
        }
    }

    public function edit($id){
        try 
        {
            $permission = Permission::permission('driver');
            if($permission->edit==1) {
                // $data['vehicles'] = Vehicle::select('vehicle_model_name','id')->where('status',1)->get();
                $data['drivers'] = Driver::find($id);
                $data['vehicles'] = [];
                return view('admin.drivers.create')->with($data);
            } else {
                return view('layouts.errors.403');
            } 
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    public function destroy($id){
        try {
            $drivers = Driver::find($id);
            $drivers->delete();
            return redirect('admin/driver/index')->with('message', 'Delete Succesfully...');
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    public function usershow($id){
        try{
            $data['drivers'] = Driver::select('drivers.*','vehicle.vehicle_model_name as vehicle_name')->leftjoin('vehicle','drivers.vehicle_type','vehicle.id')->where('drivers.id',$id)->first();
            return view('admin.drivers.usershow')->with($data);
        }catch (Exception $e){
            Log::error($e);
        }
    }
}
