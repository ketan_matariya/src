<?php

namespace App\Http\Controllers\Admin\Postcodes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Postcodes\Postcode;
use log;
use App\Helper\Permission;
use Validator;
use App\Helper\ResponseMessage;

class PostcodeController extends Controller
{
    public function index(){
    	try {
    	    $permission = Permission::permission('postcode');
            if($permission->view==1) {
        		$data['postcodes'] = Postcode::Select('id','postcode')->get();
        		$data['permission'] = $permission;
        		return view('admin.postcodes.index')->with($data);
            } else {
                return view('layouts.errors.403');
            }
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }
    public function create(){
    	try {
    	    $permission = Permission::permission('postcode');
            if($permission->add==1) {
        		return view('admin.postcodes.create');
            } else {
                return view('layouts.errors.403');
            }
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }
    public function edit($id){
    	try {
    	    $permission = Permission::permission('postcode');
            if($permission->edit==1) {
        		$data['postcode'] = Postcode::find($id);
        		return view('admin.postcodes.create')->with($data);
            } else {
                return view('layouts.errors.403');
            }
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }
    public function store(Request $request){
    	try {
    	    $permission = Permission::permission('postcode');
            if($permission->edit==1||$permission->add==1) {
                $rules = [];
                $customeMessage = [
                    'postcode.required' => 'Please enter postcode',
                    'postcode.unique' => 'Postcode is already exists',
                ];
    
                if (isset($request->id) && !empty($request->id)) {
                    if (strcasecmp(Postcode::find($request->id)->postcode, $request->postcode)) {
                        $rules['postcode'] = 'required|unique:postcodes,postcode';
                    }
                } else{
                    $rules['postcode'] = 'required|unique:postcodes,postcode';
                }
                $validator = Validator::make($request->all(),$rules, $customeMessage);
    
                if( $validator->fails() ) {
                    return back()->withInput()->withErrors($validator->errors());
                } else {
            		if(isset($request->id)){
            			$postcode = Postcode::find($request->id);
            		}else{
            			$postcode = new Postcode();
            		}
            		$postcode->postcode = $request->postcode;
            		$postcode->save();
            		if($request->id){
                        return redirect()->route('admin.postcode.index')->with('message', 'Postcode updated successfully');
                    } else {
            	        return redirect()->route('admin.postcode.index')->with('message', 'Postcode added successfully');
                    }
                }
            } else {
                return view('layouts.errors.403');
            }
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }
    public function destroy($id){
		try {
		    $permission = Permission::permission('postcode');
            if($permission->delete==1) {
    			$postcode = Postcode::find($id);
                $postcode->delete();
                return redirect()->route('admin.postcode.index')->with('message', 'Postcode delete successfully');
            } else {
                return view('layouts.errors.403');
            }
		} catch (Exception $e) {
			log:error($e);
		}
    }
}
