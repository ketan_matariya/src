<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;;
use App\Shop\CMS\PrivacyPolicy;
use App\Helper\ResponseMessage;

class PrivacyPolicyController extends Controller
{
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $policy = PrivacyPolicy::get();
        
        foreach ($policy as $key => $value) {
            $data = $value;
        }
        return view('admin.privacy_policy.edit',compact('data'));
    }

    public function update(Request $request)
    {
       // dd($request);
        $id = $request->id;

        $policy = PrivacyPolicy::find($id);
        $policy->title = $request->title;
        $policy->description = $request->description;
        $policy->status = $request->status;
        $policy->updated_at = date('Y-m-d H:i:s');
        $policy->save();

        return redirect()->back()->with('data',$policy)->with([
                        'message' => 'Privacy Policy updated successfully']);
    }
}