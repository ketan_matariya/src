<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;;
use App\Shop\CMS\TearmCondtion;

class TearmCondtionController extends Controller
{
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permission = TearmCondtion::get();
        
        foreach ($permission as $key => $value) {
            $data = $value;
        }
        
        return view('admin.tearm_condition.edit',compact('data'));
    }

    public function update(Request $request)
    {
       // dd($request);
        $id = $request->id;

        $tearm = TearmCondtion::find($id);
        $tearm->title = $request->title;
        $tearm->description = $request->description;
        $tearm->status = $request->status;
        $tearm->updated_at = date('Y-m-d H:i:s');
        $tearm->save();

        return redirect()->back()->with('data',$tearm)->with([
                        'message' => 'Term & Condition updated successfully']);
    }
}