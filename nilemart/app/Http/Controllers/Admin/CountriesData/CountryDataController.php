<?php

namespace App\Http\Controllers\Admin\CountriesData;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\CountriesData\CountryData;
use App\Shop\Customers\Customer;
use App\Shop\Addresses\Address;
use App\Helper\Permission;

class CountryDataController extends Controller
{
    public function index()
    {
        $permission = Permission::permission('offer');
        if($permission->view==1) {
            $list = CountryData::select('STCode', 'DTName as name')->where('DTCode',000)->get();
            // dd($list);
            return view('admin.country_data.list', [
                'list' => $list,
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }
    
    public function create(){
        $permission = Permission::permission('country_data');
        if($permission->add==1) {
            $states = CountryData::select('STCode', 'DTName as name')->where('DTCode',000)->get();
            return view('admin.country_data.add', [
                'list' => $states,
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }
    
    public function store(Request $request){
        $state_id = $request->state_id;
        $name = $request->name;
        
        $data = new CountryData();
        if(isset($request->city_id)){
            $city_id = $request->city_id;
            $data->STCode = $state_id;
            $data->DTCode = $city_id;
            $getData = CountryData::where('STCode', $state_id)->where('DTCode', $city_id)->first();
            $data->DTName = $getData->DTName;
            if(isset($request->tehsil_id)){
                $last = CountryData::orderBy('TVCode','DESC')->first();
                $data->SDTCode = $request->tehsil_id;
                $data->SDTName = $getData->SDTName;
                $TVCode = $last->TVCode+1;
                $data->TVCode = $TVCode;
                $data->Name = $name;
                if($data->save()){
                    return redirect()->route('admin.country_data.index')->with('message', 'Village Added Successfully');
                }
            } else {
                $last = CountryData::orderBy('SDTCode','DESC')->first();
                $data->SDTName = $name;
                $SDTCode = $last->SDTCode+1;
                $data->SDTCode = $SDTCode;
                $data->TVCode = 000000;
                $data->Name = $name;
                if($data->save()){
                    return redirect()->route('admin.country_data.index')->with('message', 'Tehsil Added Successfully');
                }
            }
        }
        else {
            $last = CountryData::orderBy('DTCode','DESC')->first();
            $data->STCode = $state_id;
            $DTCode = $last->DTCode+1;
            $data->DTCode = $DTCode;
            $data->DTName = $name;
            $data->SDTCode = 00000;
            $data->SDTName = $name;
            $data->TVCode = 000000;
            $data->Name = $name;
            if($data->save()){
                return redirect()->route('admin.country_data.index')->with('message', 'District Added Successfully');
            }
        }
        
    }
    
    public function createVillage(){
        $permission = Permission::permission('country_data');
        if($permission->add==1) {
            $states = CountryData::select('STCode', 'DTName as name')->where('DTCode',000)->get();
            return view('admin.country_data.create', [
                'list' => $states,
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }
    
    public function createCity(){
        $permission = Permission::permission('country_data');
        if($permission->add==1) {
            $states = CountryData::select('STCode', 'DTName as name')->where('DTCode',000)->get();
            return view('admin.country_data.createCity', [
                'list' => $states,
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }
    
    public function show($id){
        $permission = Permission::permission('country_data');
        if($permission->view==1) {
            $cities = CountryData::select('DTCode', 'DTName as name')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', $id)->get();
            // foreach($cities as $city){
            //     $tehsils = countrydata::select('SDTCode', 'SDTName as name')->where('DTCode', 'like', '%' .$city->DTCode)->where('SDTCode', '!=',00000)->where('TVCode',000000)->get();
            //     // dd($tehsils);
            //     foreach($tehsils as $tehsil){
            //         $villages = countrydata::select('TVCode', 'Name')->where('SDTCode','like', '%' .$tehsil->SDTCode)->where('TVCode', '!=',000000)->get();
            //         // dd($villages);
            //         foreach($villages as $village){
            //             $vcount =countrydata::where('SDTCode','like', '%' .$tehsil->SDTCode)->where('TVCode', 'like', '%' .$village->TVCode)->count();
            //             if($vcount>1){
            //                 dd($vcount);
            //                 $vil = countrydata::where('SDTCode','like', '%' .$tehsil->SDTCode)->where('TVCode', 'like', '%' .$village->TVCode)->first();
            //                 $vil->delete();
            //             }
            //         }
            //     }
            // }
            return view('admin.country_data.show', [
                'list' => $cities,
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }
    public function showTehsil($id)
    {
        $permission = Permission::permission('country_data');
        if($permission->view==1) {
            $tehsils = countrydata::select('SDTCode', 'SDTName as name')->where('DTCode',$id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->get();
            return view('admin.country_data.showTehsil', [
                'list' => $tehsils,
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }
    public function showVillage($id)
    {
        $permission = Permission::permission('country_data');
        if($permission->view==1) {
            $villages = countrydata::select('TVCode', 'Name')->where('SDTCode',$id)->where('TVCode', '!=',000000)->get();
            foreach($villages as $village){
                $vilage =countrydata::where('SDTCode',$id)->where('TVCode', 'like', '%' .$village->TVCode)->count();
                if($vilage>1){
                    $village1s = countrydata::where('SDTCode',$id)->where('TVCode', 'like', '%' .$village->TVCode)->first();
                    $village1s->delete();
                }
            }
            // dd($villages);
            return view('admin.country_data.showVillage', [
                'list' => $villages,
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }
    public function destroy($id)
    {
        $permission = Permission::permission('offer');
        if($permission->view==1) {
            // $villages = countrydata::select('TVCode', 'Name')->where('SDTCode',$id)->where('TVCode', '!=',000000)->get();
            if(!(Customer::where('village_id',$id)->exists() || Address::where('village_id',$id)->exists())){
                $village = countrydata::where('TVCode', $id)->first();
                if($village->delete()){
                    return redirect()->back()->with('message', 'Village Deleted Successfully');
                }
            } else {
                return redirect()->route('admin.country_data.index')->with('message', 'Village can not delete because user available!!');
            }
            
        } else {
            return view('layouts.errors.403');
        }
    }
    
    function getCity(Request $request){
        $cities = CountryData::select('DTCode', 'DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', $request->id)->get();
        $output = [];
        foreach( $cities as $city )
        {
            $output[$city->DTCode] = $city->DTName;
        }
        return $output;
    }
    
    function getTehsil(Request $request){
        $tehsils = countrydata::select('SDTCode', 'SDTName as name')->where('DTCode',$request->id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->get();
        $output = [];
        foreach( $tehsils as $tehsil)
        {
            $output[$tehsil->SDTCode] = $tehsil->name;
        }
        return $output;
    }
}
