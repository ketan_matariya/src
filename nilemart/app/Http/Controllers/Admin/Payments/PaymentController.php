<?php

namespace App\Http\Controllers\Admin\Payments;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Addresses\OrderAddressDriver;
use App\Shop\Payments\DriverPayment;
use App\Shop\Drivers\Driver;
use App\Helper\SMSHelper;
use App\Helper\MailHelper;
use App\Helper\Permission;
use App\Shop\Orders\Order;
use App\Helper\RandomHelper;
use App\Helper\NotificationHelper;
use Log;

class PaymentController extends Controller
{
    public function index(){
        try{
            $permission = Permission::permission('payment');
            if($permission->view==1) {
                $data['drivers'] = Driver::where('status',1)->get();
                foreach($data['drivers'] as $driver){
                    $query = Order::join('customers', 'customers.id', '=', 'orders.customer_id')
                                    ->join('order_statuses','order_statuses.id', '=','orders.order_status_id' )
                                    ->select('orders.total', 'orders.total_shipping');
                    $query->orderBy('orders.created_at', 'DESC');
                    $orders = $query->where('payment','COD')->where('driver_payment',0)->where('driver_id',$driver->id)->where('order_status_id',1)->get();
                    $grandtotal=0;
                    foreach($orders as $order){
                        $grandtotal = $grandtotal+$order->total+$order->total_shipping;
                    }
                    $driver->total = str_replace(",","",number_format($grandtotal,2));
                }
                // dd($data);
                $data['permission'] = $permission;
                return view('admin.payments.index')->with($data);
            } else {
                return view('layouts.errors.403');
            }
        }catch (Exception $e) {
    		log::error($e);
    		$data['error']=$e;
    		$error_mail = MailHelper::errorMail($data);
        }
    }
    
    public function show($id){
        try{
            $permission = Permission::permission('payment');
            if($permission->view==1) {
                if(Driver::where('status',1)->where('id',$id)->exists()){
                    $query = Order::join('customers', 'customers.id', '=', 'orders.customer_id')
                                    ->join('order_statuses','order_statuses.id', '=','orders.order_status_id' )
                                    ->select('customers.name as customer_name','customers.mobile', 'order_statuses.name as status', 'order_statuses.color as color', 'orders.*');
                    $query->orderBy('orders.created_at', 'DESC');
                    $data['orders'] = $query->where('payment','COD')->where('driver_payment',0)->where('driver_id',$id)->where('order_status_id','!=',4)->get();
                    return view('admin.payments.show')->with($data);
                }
            } else {
                return view('layouts.errors.403');
            }
        }catch (Exception $e) {
    		log::error($e);
    		$data['error']=$e;
    		$error_mail = MailHelper::errorMail($data);
        }
    }
    
    public function driverNotification($id,$total){
        try{
            if(Driver::where('status',1)->where('id',$id)->exists()){
                if($total!=0.00){
                    $driver = Driver::where('status',1)->where('id',$id)->first();
                  
                    $country_code = 91;
        			$SMSresponse = SMSHelper::sendOTP($country_code, $driver->contact);
        			$contact_no = trim(strip_tags($SMSresponse['phone']));
                    $otp = $SMSresponse['otp'];    
                    
        			if($SMSresponse){
    	        		if($driver){
    		                $driver->otp_payment = $otp;
    			        	$driver->save();
    	        		}
        			}
                    return back()->with('message', 'Otp send successfully to '.$driver->name);
                }else{
                    return redirect()->route('admin.payment.index')->with('message', 'Payment should be greater than 0.00');
                }
            }else{
                return redirect()->route('admin.payment.show', $id)->with('message', 'Driver not found');
            }
        }catch (Exception $e) {
    		log::error($e);
    		$data['error']=$e;
    		$error_mail = MailHelper::errorMail($data);
        }
    }
    
    function driverNotifications(Request $request){
        try{
            if(Driver::where('status',1)->where('id',$request->id)->exists()){
                $driver = Driver::where('status',1)->where('id',$request->id)->first();
                if($driver->token!=""){
                    $server_api_key = 'AAAAIy2XKRg:APA91bGxox_lBNewVJZE_oXolcUfhyACsGlXBSnOrAuNMbzgqWEYrrPlGA5K5cLftikt6FW8SGB4SPXoXvPfB4aJtKFfS0XpUoTe1qfJmAv3stP0uzZxdHN2CYo0tNfn_8uw-4KNA9jk';
                    $title = 'Notification ';
                    $token = $driver->token;
                    $otp = RandomHelper::randomOTP();
                    $message = $otp. " is the one time password (OTP) for Goyo Mart. This is usable once time and PLEASE DO NOT SHARE WITH ANYONE.";
                    $result = NotificationHelper::notificationDriver($message, $server_api_key, $token, $title);
                    $SMSresponse = json_decode($result);
                }else{
                    $country_code = 91;
                    $SMSresponse = SMSHelper::sendOTP($country_code, $driver->contact);
                    $contact_no = trim(strip_tags($SMSresponse['phone']));
                    $otp = $SMSresponse['otp'];    
                }
                
    			if($SMSresponse){
	        		if($driver){
		                $driver->payment_otp = $otp;
			        	$driver->save();
	        		}
    			}
    			return "success";
            }else{
                return "false";
            }
        }catch (Exception $e) {
    		log::error($e);
    		$data['error']=$e;
    		$error_mail = MailHelper::errorMail($data);
        }
    }
    
    public function paymentSubmit(Request $request){
        try{
            // dd($request->all());
            if(Driver::where('status',1)->where('id',$request->id)->exists()){
                $driver = Driver::where('status',1)->where('id',$request->id)->first();
                if($driver->payment_otp==$request->otp){
                    $query = Order::join('customers', 'customers.id', '=', 'orders.customer_id')
                                    ->join('order_statuses','order_statuses.id', '=','orders.order_status_id' )
                                    ->select('orders.id');
                    $query->orderBy('orders.created_at', 'DESC');
                    $orders = $query->where('payment','COD')->where('driver_id',$driver->id)->where('order_status_id',1)->get();
                    foreach($orders as $order){
                        $order_com = Order::find($order->id);
                        $order_com->driver_payment = 1;
                        $order_com->save();
                    }
                    $payment = new DriverPayment();
                    $payment->driver_id = $driver->id;
                    $payment->payment = $request->total;
                    $payment->save();
                    return redirect()->route('admin.payment.index')->with('message', $driver->name.' submit payment successfully');
                }else{
                    return back()->with('message', "Otp does not match");
                }
            } else {
                return back()->with('message', ' Driver not found');
            }
        } catch (Exception $e) {
    		log::error($e);
    		$data['error']=$e;
    		$error_mail = MailHelper::errorMail($data);
        }
    }
}
