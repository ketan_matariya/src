<?php

namespace App\Http\Controllers\Admin\Offers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Offers\Repositories\OfferRepository;
use App\Shop\Offers\Repositories\Interfaces\OfferRepositoryInterface;
use App\Shop\Offers\Offer;
use App\Shop\Categories\Category;
use App\Shop\Employees\Repositories\Interfaces\EmployeeRepositoryInterface;
use CreateOffersTable;
use App\Shop\Offers\Requests\CreateOfferRequest;
use App\Shop\Offers\Requests\UpdateOfferRequest;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Shop\Offers\Transformations\OffersTransformable;
use App\Helper\Permission;
use Auth;
use Validator;
use App\Helper\ResponseMessage;
use App\Shop\Products\Product_attributes;
use App\Shop\Offers\OfferProduct;
use App\Shop\Products\Product;

class OffersController extends Controller
{
    private $offerRepo;
    use OffersTransformable;

    public function __construct(CategoryRepositoryInterface $categoryRepository,OfferRepository $offerRepo,EmployeeRepositoryInterface $employeeRepository)
    {
        $this->offerRepo = $offerRepo;
        $this->companiesRepo = $employeeRepository;
        $this->categoryRepo = $categoryRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permission = Permission::permission('offer');
        if($permission->view==1) {
            $list = $this->offerRepo->listOffers('created_at', 'desc');

            $offers = $list->map(function (Offer $offer) {
                return $this->transformOffer($offer);
            })->all();
            return view('admin.offers.list', [
                'offers' => $this->offerRepo->paginateArrayResults($offers),
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $offers = Offer::where('parent_id',null)->get();
        $permission = Permission::permission('offer');
        $products = Product::join('employees','employees.id','products.company_id')
                            ->join('categories','categories.id','products.category_id')
                            ->select('products.*', 'employees.name as company', 'categories.name as category')
                            ->orderBy('id', 'DESC')
                            ->where('employees.status',1)
                            ->where('products.status',1)
                            ->get();
        foreach($products as $product){
            $items = Product_attributes::select('sale_price','id','key')
                                        ->where('product_id', $product->id)
                                        ->get();
            $product->items = $items;
        }
        if($permission->add==1) {
            return view('admin.offers.create', [
                'offer' => new Offer,
                'offers'=> $offers,
                'products' => $products,
                'companies' => $this->companiesRepo->listEmployees('name', 'asc')
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }

   /**
     * Store a newly created resource in storage.
     *
     * @param  CreateProductRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateOfferRequest $request)
    {

        $permission = Permission::permission('offer');
        if($permission->add==1) {
            $rules = [
                'product' => 'required',
            ];
            $customeMessage = [
                'product.required' => 'Please select atleast one product',
            ];
            $validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
                 return back()->withInput()->withErrors($validator->errors());
            } else {
                $offer = new Offer();
                $offer->offer = $request->offer;
                $offer->offer_hi = $request->offer_hi;
                $offer->offer_gu = $request->offer_gu;
                $offer->offer_type = $request->offer_type;
                $offer->status = $request->status;
                $date = date("Y-m-d", strtotime($request->expired_date));
                $offer->expired_date = $date;
                $offer->expired_time = $request->expired_time;
                $offer->description = $request->description;
                $offer->description_hi = $request->description_hi;
                $offer->description_gu = $request->description_gu;
                $url = env('APP_URL');
                if($offer->save()) {
                    $product = $request->product;
                    $item_key = $request->item_key;
                    for($i=0; $i<count($product); $i++) {
                        $offer_product = new OfferProduct();
                        $offer_product->offer_id = $offer->id;
                        $offer_product->product_id = $product[$i];
                        $offer_product->attribute_id = $item_key[$i];
                        $offer_product->save();
                    }
                    if ($request->has('cover')) {
                        $file    = $request->cover;
                        $time    = md5(time());
                        $profile = $file->getClientOriginalExtension();
                        $name = $file->getClientOriginalName();
                        $path    = "images/offers/";
                        $file->move(public_path(env('IMAGE_URL').$path), $time.'.'.$profile);
                        $profile_update          = Offer::find($offer->id);
                        $profile_update->cover = $url."/".$path.$time.'.'.$profile;
                        $profile_update->save();
                    }
                }
                return redirect()->route('admin.offers.index')->with('message', 'Created offer successfully');
            }
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permission = Permission::permission('offer');
        $offer = $this->offerRepo->findOfferById($id);
        $offers = Offer::where('parent_id',$id)->get();
        return view('admin.offers.show', [
            'offer' => $offer,
            'offers'=> $offers,
            'permission' => $permission,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::permission('offer');
        if($permission->edit==1) {
            $offer = $this->offerRepo->findOfferById($id);
            $offers = Offer::where('parent_id',null)->get();
            $categories = Category::where('company_id', $offer->company_id)->where('parent_id',null)->get();
            $offer_products = OfferProduct::where('offer_id',$id)->get(); 
            $products = Product::join('employees','employees.id','products.company_id')
                            ->join('categories','categories.id','products.category_id')
                            ->select('products.*', 'employees.name as company', 'categories.name as category')
                            ->orderBy('id', 'DESC')
                            ->where('employees.status',1)
                            ->where('products.status',1)
                            ->get();
            foreach($products as $product){
                $items = Product_attributes::select('sale_price','id','key')
                                            ->where('product_id', $product->id)
                                            ->get();
                $product->items = $items;
            }
            return view('admin.offers.edit', [
                'offer' => $offer,
                'offers' => $offers,
                'products' => $products,
                'offer_products'=>$offer_products,
                'companies' => $this->companiesRepo->listEmployees('name', 'asc'),
                'categories' => $categories,
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOfferRequest $request, $id)
    {
        $permission = Permission::permission('offer');
        if($permission->edit==1) {
            $rules = [
                'product' => 'required',
            ];
            $customeMessage = [
                'product.required' => 'Please select atleast one product',
            ];
            $validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
                 return back()->withInput()->withErrors($validator->errors());
            } else {
                $offer = $this->offerRepo->findOfferById($id);
                $update = new OfferRepository($offer);
                $data = $request->except('_token', '_method');
                $des = $data['description'];
                $des = str_replace('<p>', "", $des);
                $des = str_replace('</p>', "", $des);
                $data['description']=$des;
                $expired_date = $data['expired_date'];
                $date = date("Y-m-d", strtotime($expired_date));
                $data['expired_date'] = $date;
                // dd($data);
                $url = env('APP_URL');
                $update->updateOffer($data);
                $offer_product=OfferProduct::where('offer_id',$id)->get();
                if(count($offer_product)>0){
                    foreach($offer_product as $op){
                        $offer_prod = OfferProduct::find($op->id);
                        $offer_prod->delete();
                    }
                }
                $product = $request->product;
                $item_key = $request->item_key;
                for($i=0; $i<count($product); $i++) {
                    $offer_product = new OfferProduct();
                    $offer_product->offer_id = $offer->id;
                    $offer_product->product_id = $product[$i];
                    $offer_product->attribute_id = $item_key[$i];
                    $offer_product->save();
                }
                if ($request->has('cover')) {
                    $file    = $request->cover;
                    $time    = md5(time());
                    $profile = $file->getClientOriginalExtension();
                    $name = $file->getClientOriginalName();
                    $path    = "images/offers/";
                    $file->move(public_path(env('IMAGE_URL').$path), $time.'.'.$profile);
                    $profile_update          = Offer::find($offer->id);
                    $profile_update->cover = $url."/".$path.$time.'.'.$profile;
                    $profile_update->save();
                }
                $request->session()->flash('message', 'Offer update successfully');
                return redirect()->route('admin.offers.index');
            }
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission = Permission::permission('offer');
        if($permission->delete==1) {
            $offer = $this->offerRepo->findOfferById($id);
            $offer->delete();
            request()->session()->flash('message', 'Offer delete successfully');
            return redirect()->route('admin.offers.index');
        } else {
            return view('layouts.errors.403');
        }
    }

    /* @param Request $request
    * @return \Illuminate\Http\RedirectResponse
    */
   public function removeImage(Request $request)
   {
       $this->offerRepo->deleteFile($request->only('offers'));
       request()->session()->flash('message', 'Image delete successful');
       return redirect()->route('admin.offers.edit', $request->input('offers'));
   }
   
   public function getProduct(Request $request){
        $item = Product_attributes::find($request->id);
        return $item;
    }
}
