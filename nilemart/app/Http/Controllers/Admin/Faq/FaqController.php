<?php

namespace App\Http\Controllers\Admin\Faq;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\CMS\Faq;
use log;
use App\Helper\Permission;
use Validator;
use App\Helper\ResponseMessage;

class FaqController extends Controller
{
    public function index(){
    	try {
    	    $permission = Permission::permission('faq');
            if($permission->view==1) {
        		$data['faq'] = Faq::Select('id','question','answer')->get();
        		$data['permission'] = $permission;
        		return view('admin.faq.index')->with($data);
            } else {
                return view('layouts.errors.403');
            }
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }
    public function create(){
    	try {
    	    $permission = Permission::permission('faq');
            if($permission->add==1) {
        		return view('admin.faq.create');
            } else {
                return view('layouts.errors.403');
            }
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }
    public function edit($id){
    	try {
    	    $permission = Permission::permission('faq');
            if($permission->edit==1) {
        		$data['faq'] = Faq::find($id);
        		return view('admin.faq.create')->with($data);
            } else {
                return view('layouts.errors.403');
            }
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }
    public function store(Request $request){
    	try {
    	    $permission = Permission::permission('faq');
            if($permission->edit==1||$permission->add==1) {
                $rules = [];
                $customeMessage = [
                    'question.required' => 'Please enter question',
                    'answer.required' => 'Please enter answer',
                ];
    
                if (isset($request->id) && !empty($request->id)) {
                    if (strcasecmp(Faq::find($request->id)->question, $request->answer)) {
                        $rules['question'] = 'required';
                        $rules['answer'] = 'required';
                    }
                } else{
                    $rules['question'] = 'required';
                    $rules['answer'] = 'required';
                }
                $validator = Validator::make($request->all(),$rules, $customeMessage);
    
                if( $validator->fails() ) {
                    return back()->withInput()->withErrors($validator->errors());
                } else {
            		if(isset($request->id)){
            			$faq = Faq::find($request->id);
            		}else{
            			$faq = new Faq();
            		}
            		$faq->question = $request->question;
                    $faq->answer = $request->answer;
            		$faq->save();
            		if($request->id){
                        return redirect()->route('admin.faq.index')->with('message', 'Faq updated successfully');
                    } else {
            	        return redirect()->route('admin.faq.index')->with('message', 'Faq added successfully');
                    }
                }
            } else {
                return view('layouts.errors.403');
            }
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }
    public function destroy($id){
		try {
		    $permission = Permission::permission('faq');
            if($permission->delete==1) {
    			$faq = Faq::find($id);
                $faq->delete();
                return redirect()->route('admin.faq.index')->with('message', 'Faq deleted successfully');
            } else {
                return view('layouts.errors.403');
            }
		} catch (Exception $e) {
			log:error($e);
		}
    }
}
