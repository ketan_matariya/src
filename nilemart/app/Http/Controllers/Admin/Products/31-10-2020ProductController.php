<?php

namespace App\Http\Controllers\Admin\Products;

use App\Shop\Attributes\Repositories\AttributeRepositoryInterface;
use App\Shop\AttributeValues\Repositories\AttributeValueRepositoryInterface;
use App\Shop\Brands\Repositories\BrandRepositoryInterface;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Shop\ProductAttributes\ProductAttribute;
use App\Shop\Products\Exceptions\ProductInvalidArgumentException;
use App\Shop\Products\Exceptions\ProductNotFoundException;
use App\Shop\Products\Product;
use App\Shop\Crops\Crop;
use App\Shop\TechnicalCategories\Technical;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Shop\Products\Repositories\ProductRepository;
use App\Shop\Products\Requests\CreateProductRequest;
use App\Shop\Products\Requests\UpdateProductRequest;
use App\Http\Controllers\Controller;
use App\Shop\Categories\Category;
use App\Shop\Products\Transformations\ProductTransformable;
use App\Shop\Tools\UploadableTrait;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Shop\Employees\Repositories\Interfaces\EmployeeRepositoryInterface;
use App\Shop\Employees\Repositories\EmployeeRepository;
use App\Helper\Permission;
use App\Shop\AdditionalInfos\AdditionalInfo;
use App\Shop\Products\Product_attributes;
use App\Shop\Employees\Employee;

class ProductController extends Controller
{
    use ProductTransformable, UploadableTrait;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepo;
    private $companiesRepo;

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepo;

    /**
     * @var AttributeRepositoryInterface
     */
    private $attributeRepo;

    /**
     * @var AttributeValueRepositoryInterface
     */
    private $attributeValueRepository;

    /**
     * @var ProductAttribute
     */
    private $productAttribute;

    /**
     * @var BrandRepositoryInterface
     */
    private $brandRepo;

    /**
     * ProductController constructor.
     *
     * @param ProductRepositoryInterface $productRepository
     * @param CategoryRepositoryInterface $categoryRepository
     * @param AttributeRepositoryInterface $attributeRepository
     * @param AttributeValueRepositoryInterface $attributeValueRepository
     * @param ProductAttribute $productAttribute
     * @param BrandRepositoryInterface $brandRepository
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        CategoryRepositoryInterface $categoryRepository,
        AttributeRepositoryInterface $attributeRepository,
        AttributeValueRepositoryInterface $attributeValueRepository,
        ProductAttribute $productAttribute,
        BrandRepositoryInterface $brandRepository,
        EmployeeRepositoryInterface $employeeRepository
    ) {
        $this->productRepo = $productRepository;
        $this->categoryRepo = $categoryRepository;
        $this->attributeRepo = $attributeRepository;
        $this->attributeValueRepository = $attributeValueRepository;
        $this->productAttribute = $productAttribute;
        $this->brandRepo = $brandRepository;
        $this->companiesRepo = $employeeRepository;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $permission = Permission::permission('product');
        if($permission->view==1) {   
            $products = Product::join('employees','employees.id','products.company_id')
                            ->join('categories','categories.id','products.category_id')
                            ->select('products.*', 'employees.name as company', 'categories.name as category')
                            ->orderBy('id', 'DESC')
                            ->where('employees.status',1)
                            ->get();
            return view('admin.products.list', [
                'products' => $products,
                'permission' => $permission
            ]);

        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = Permission::permission('product');
        if($permission->add==1) { 
            
            $crops = Crop::where('crop_category', null)->get(); 
            $categories = $this->categoryRepo->listCategories('name', 'asc')->where('parent_id', null)->where('status',1);
            $technicals = Technical::get();
            $infos = AdditionalInfo::select('name')->get(); 

            return view('admin.products.create', [
                'brands' => $this->brandRepo->listBrands(['*'], 'name', 'asc'),
                'default_weight' => env('SHOP_WEIGHT'),
                'weight_units' => Product::MASS_UNIT,
                'product' => new Product,
                'companies' => $this->companiesRepo->listEmployees('name', 'asc')->where('status',1),
                'crops' => $crops,
                'categories' => $categories,
                'technicals' => $technicals,
                'infos' => $infos
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateProductRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductRequest $request)
    {
        $permission = Permission::permission('product');
        if($permission->add==1) { 
            $data = $request->except('_token', '_method');
            $data['shipping_price']=number_format($request->shipping_price,2);
            $data['slug'] = str_slug($request->input('name'));
            $unit = [];
            if($request->input('unitkey') && $request->input('unitoption'))
            {
                $arrayunitkey = $request->input('unitkey');
                $arrayData = $request->input('unitoption');
                foreach ($arrayData as $key => $value) {
                    $unit[$key] =  array(
                        'key' => $value,
                        'value' => $arrayunitkey[$key]
                    );
                }
            }
            $additional_info = [];
            $additional_info_hi = [];
            $additional_info_gu = [];
            if($request->input('additional_info') && $request->input('additional_info_value'))
            {
                $array1 = $request->input('additional_info');
                $array2 = $request->input('additional_info_value');
                foreach ($array2 as $key => $value) {
                    $name = $array1[$key];
                    if(!(AdditionalInfo::where('name', $name)->exists())){
                        $info = new  AdditionalInfo();
                        $info->name = $name;
                        $info->save();
                    }
                    $additional_info[$key] =  array(
                        'key' => $value,
                        'value' => $array1[$key]
                    );
                }

                $array3 = $request->input('additional_info_hi');
                $array4 = $request->input('additional_info_value_hi');
                foreach ($array4 as $key => $value) {
                    $name = $array3[$key];
                    if(!(AdditionalInfo::where('name', $name)->exists())){
                        $info = new  AdditionalInfo();
                        $info->name = $name;
                        $info->save();
                    }
                    $additional_info_hi[$key] =  array(
                        'key' => $value,
                        'value' => $array3[$key]
                    );
                }

                $array5 = $request->input('additional_info_gu');
                $array6 = $request->input('additional_info_value_gu');
                foreach ($array6 as $key => $value) {
                    $name = $array5[$key];
                    if(!(AdditionalInfo::where('name', $name)->exists())){
                        $info = new  AdditionalInfo();
                        $info->name = $name;
                        $info->save();
                    }
                    $additional_info_gu[$key] =  array(
                        'key' => $value,
                        'value' => $array5[$key]
                    );
                }
            }
            
            $data['additional_info'] = json_encode($additional_info);
            $data['additional_info_hi'] = json_encode($additional_info_hi);
            $data['additional_info_gu'] = json_encode($additional_info_gu);

            if ($request->hasFile('cover') && $request->file('cover') instanceof UploadedFile) {
                // $data['cover'] = $this->productRepo->saveCoverImage($request->file('cover'));
            }
            $product = $this->productRepo->createProduct($data);

            $productRepo = new ProductRepository($product);

            if ($request->hasFile('image')) {
                $productRepo->saveProductImages(collect($request->file('image')));
            }

            $productRepo = new ProductRepository($product);
            if ($request->hasFile('image')) {
                $productRepo->saveProductImages(collect($request->file('image')));
            }

            if ($request->has('categories')) {
                $productRepo->syncCategories($request->input('categories'));
            } else {
                $productRepo->detachCategories();
            }
            if ($request->has('cover')) {
			    $file    = $request->cover;
                $time    = md5(time());
                $profile = $file->getClientOriginalExtension();
                $name = $file->getClientOriginalName();
                $path    = "images/products/" . $product->id . "/file";
                $file->move(public_path(env('IMAGE_URL').$path), $time.'.'.$profile);
                $profile_update          = Product::find($product->id);
                $profile_update->cover = env('APP_URL')."/".$path . "/" .$time.'.'.$profile;
                $profile_update->save();
                $data['cover']=$profile_update->cover;
            }
            
            if ($request->has('offer_product_image')) {
			    $file    = $request->offer_product_image;
                $time    = md5(time());
                $profile = $file->getClientOriginalExtension();
                $name = $file->getClientOriginalName();
                $path    = "images/offer_products/" . $product->id . "/file";
                $file->move(public_path(env('IMAGE_URL').$path), $time.'.'.$profile);
                $profile_update          = Product::find($product->id);
                $profile_update->offer_product_image = env('APP_URL')."/".$path . "/" .$time.'.'.$profile;
                $profile_update->save();
                $data['offer_product_image']=$profile_update->offer_product_image;
            }

            $arrayunitkey = $request->input('unitkey');
            $arrayData = $request->input('unitoption');
            $price = $request->input('prices');
            $sale_price = $request->input('sale_prices');
            $quantity = $request->input('quantities');
            $discount = $request->input('discount');
            $max_quantities = $request->input('max_quantities');
            // dd($max_quantities);
            // $gst = $request->input('gst');
            if (Product_attributes::where('product_id',$product->id)->exists()){
                Product_attributes::where('product_id',$product->id)->delete();
            }
            for ($i=0; $i < count($quantity); $i++) { 
                $product_attribute = New Product_attributes;
                $product_attribute->quantity = $quantity[$i];
                $product_attribute->value = $arrayunitkey[$i];
                $product_attribute->key = $arrayData[$i];
                $product_attribute->price = number_format($price[$i],2);
                $product_attribute->sale_price = number_format($sale_price[$i],2);
                $product_attribute->available_quantity = $quantity[$i];
                $product_attribute->max_quantities = $max_quantities[$i];
                $product_attribute->discount = $discount[$i];
                $product_attribute->gst_price = 0;
                $product_attribute->product_id = $product->id;
                $product_attribute->save();
                // dd($product_attribute);
            }

            return redirect()->route('admin.products.index')->with('message', 'Product added successfully');
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $product = $this->productRepo->findProductById($id);
        return view('admin.products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $permission = Permission::permission('product');
        if($permission->edit==1) { 
            $product = Product::find($id);
            $technicals = Technical::get();
            $productAttributes = $product->attributes()->get();
            $product['additional_info'] = json_decode($product->additional_info);
            $product['additional_info_hi'] = json_decode($product->additional_info_hi);
            $product['additional_info_gu'] = json_decode($product->additional_info_gu);
            $infos = AdditionalInfo::select('name')->get();  
            $unit = Product_attributes::where('product_id',$id)->get();
            $product['unit'] = $unit;

            if (request()->has('delete') && request()->has('pa')) {
                $pa = $productAttributes->where('id', request()->input('pa'))->first();
                $pa->attributesValues()->detach();
                $pa->delete();

                request()->session()->flash('message', 'Delete successful');
                return redirect()->route('admin.products.edit', [$product->id, 'combination' => 1]);
            }
            $categories = $this->categoryRepo->listCategories('name', 'asc')->where('parent_id', null)->where('status',1)
                ->where('company_id', $product->company_id);
            $subcategories = $this->categoryRepo->listCategories('name', 'asc')->where('parent_id', $product->category_id)->where('status',1);
             $subsubcategories = $this->categoryRepo->listCategories('name', 'asc')->where('parent_id', $product->subcategory_id);
            return view('admin.products.edit', [
                'product' => $product,
                'images' => $product->images()->get(['src']),
                'categories' => $categories,
                'selectedIds' => $product->categories()->pluck('category_id')->all(),
                'attributes' => $this->attributeRepo->listAttributes(),
                'productAttributes' => $productAttributes,
                'brands' => $this->brandRepo->listBrands(['*'], 'name', 'asc'),
                'weight' => $product->weight,
                'default_weight' => $product->mass_unit,
                'weight_units' => Product::MASS_UNIT,
                'companies' => $this->companiesRepo->listEmployees('name', 'asc')->where('status',1),
                'crops' => Crop::where('crop_category', null)->get(),
                'subcategories' => $subcategories,
                'subsubcategories'=>$subsubcategories,
                'cropscategories' => Crop::where('crop_category', $product->crop_category)->get(),
                'technicals' => $technicals,
                'infos' => $infos
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateProductRequest $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \App\Shop\Products\Exceptions\ProductUpdateErrorException
     */
    public function update(UpdateProductRequest $request, int $id)
    {
        $permission = Permission::permission('product');
        if($permission->edit==1) { 
            $product = $this->productRepo->findProductById($id);
            $productRepo = new ProductRepository($product);
            // dd($request->all());

            if ($request->has('attributeValue')) {
                $this->saveProductCombinations($request, $product);
                return redirect()->route('admin.products.edit', [$id, 'combination' => 1])
                    ->with('message', 'Attribute combination created successful');
            }

            $data = $request->except(
                'categories',
                '_token',
                '_method',
                'default',
                'image',
                'productAttributeQuantity',
                'productAttributePrice',
                'attributeValue',
                'combination',
                'prices',
                'sale_prices',
                'quantities',
                'discount',
                'gst',
                'max_quantities'
            );
            $data['shipping_price']=number_format($request->shipping_price,2);

            // $data['available_quantity']=$request->quantity;
            $data['slug'] = str_slug($request->input('name'));
            $unit = [];
            if($request->input('unitkey') && $request->input('unitoption'))
            {
                $arrayunitkey = $request->input('unitkey');
                $arrayData = $request->input('unitoption');
                foreach ($arrayData as $key => $value) {
                    $unit[$key] =  array(
                        'key' => $value,
                        'value' => $arrayunitkey[$key]
                    );
                }
            }
            $additional_info = [];
            $additional_info_hi = [];
            $additional_info_gu = [];
            if($request->input('additional_info') && $request->input('additional_info_value'))
            {
                $array1 = $request->input('additional_info');
                $array2 = $request->input('additional_info_value');
                foreach ($array2 as $key => $value) {
                    $name = $array1[$key];
                    if(!(AdditionalInfo::where('name', $name)->exists())){
                        $info = new  AdditionalInfo();
                        $info->name = $name;
                        $info->save();
                    }
                    $additional_info[$key] =  array(
                        'key' => $value,
                        'value' => $array1[$key]
                    );
                }
                $array3 = $request->input('additional_info_hi');
                $array4 = $request->input('additional_info_value_hi');
                foreach ($array4 as $key => $value) {
                    $name = $array3[$key];
                    if(!(AdditionalInfo::where('name', $name)->exists())){
                        $info = new  AdditionalInfo();
                        $info->name = $name;
                        $info->save();
                    }
                    $additional_info_hi[$key] =  array(
                        'key' => $value,
                        'value' => $array3[$key]
                    );
                }

                $array5 = $request->input('additional_info_gu');
                $array6 = $request->input('additional_info_value_gu');
                foreach ($array6 as $key => $value) {
                    $name = $array5[$key];
                    if(!(AdditionalInfo::where('name', $name)->exists())){
                        $info = new  AdditionalInfo();
                        $info->name = $name;
                        $info->save();
                    }
                    $additional_info_gu[$key] =  array(
                        'key' => $value,
                        'value' => $array5[$key]
                    );
                }
            }
            
            // $data['unit'] = json_encode($unit);
            $data['additional_info'] = json_encode($additional_info);
            $data['additional_info_hi'] = json_encode($additional_info_hi);
            $data['additional_info_gu'] = json_encode($additional_info_gu);

            unset($data['unitkey'],$data['unitoption'],$data['additional_info_value'],$data['additional_info_value_hi'],$data['additional_info_value_gu']);

            if ($request->has('cover')) {
			    $file    = $request->cover;
                $time    = md5(time());
                $profile = $file->getClientOriginalExtension();
                $name = $file->getClientOriginalName();
                $path    = "images/products/" . $product->id . "/file";
                $file->move(public_path(env('IMAGE_URL').$path), $time.'.'.$profile);
                $profile_update          = Product::find($product->id);
                $profile_update->cover = env('APP_URL')."/".$path . "/" .$time.'.'.$profile;
                $profile_update->save();
                $data['cover']=$profile_update->cover;
            }
            
            if ($request->has('offer_product_image')) {
			    $file    = $request->offer_product_image;
                $time    = md5(time());
                $profile = $file->getClientOriginalExtension();
                $name = $file->getClientOriginalName();
                $path    = "images/offer_products/" . $product->id . "/file";
                $file->move(public_path(env('IMAGE_URL').$path), $time.'.'.$profile);
                $profile_update          = Product::find($product->id);
                $profile_update->offer_product_image = env('APP_URL')."/".$path . "/" .$time.'.'.$profile;
                $profile_update->save();
                $data['offer_product_image']=$profile_update->offer_product_image;
            } 
            If(!isset($request->offer_title)){
                $data['offer_product_image']=null;
            }

            if ($request->hasFile('image')) {
                $productRepo->saveProductImages(collect($request->file('image')));
            }

            if ($request->has('categories')) {
                $productRepo->syncCategories($request->input('categories'));
            } else {
                $productRepo->detachCategories();
            }
            // dd($data);
            $productRepo->updateProduct($data);
            
            $arrayunitkey = $request->input('unitkey');
            $arrayData = $request->input('unitoption');
            $price = $request->input('prices');
            $sale_price = $request->input('sale_prices');
            $quantity = $request->input('quantities');
            $discount = $request->input('discount');
            $max_quantities = $request->input('max_quantities');
             $unitmainid = $request->input('unitmainid');
            // $gst = $request->input('gst');
            date_default_timezone_set('Asia/Kolkata');
            if (Product_attributes::where('product_id',$id)->exists()){
                $pro_attr = Product_attributes::where('product_id',$id)->get();
                foreach($pro_attr as $atr) {
                    $del_att = Product_attributes::find($atr->id);
                    $del_att->deleted_at = date("Y-m-d H:i:s");
                    $del_att->save();
                }
            }
           /* for ($i=0; $i < count($quantity); $i++) { 
                
                
                if(Product_attributes::where('value',$arrayunitkey[$i])->where('key',$arrayData[$i])->where('product_id',$id)->exists()){
                    $product_attribute = Product_attributes::where('value',$arrayunitkey[$i])->where('key',$arrayData[$i])->where('product_id',$id)->first();
                }else{
                    $product_attribute = New Product_attributes;
                }
                
                $product_attribute->quantity = $quantity[$i];
                $product_attribute->value = $arrayunitkey[$i];
                $product_attribute->key = $arrayData[$i];
                $product_attribute->price = number_format($price[$i],2);
                $product_attribute->sale_price = number_format($sale_price[$i],2);
                $product_attribute->available_quantity = $quantity[$i];
                $product_attribute->discount = $discount[$i];
                $product_attribute->max_quantities = $max_quantities[$i];
                $product_attribute->gst_price = 0;
                $product_attribute->product_id = $id;
                $product_attribute->deleted_at = null;
                $product_attribute->save();
            }
            */
            
             for ($i=0; $i < count($quantity); $i++) { 
                
               // echo $unitmainid[$i];
                /*if(Product_attributes::where('value',$arrayunitkey[$i])->where('key',$arrayData[$i])->where('product_id',$id)->exists()){
                    $product_attribute = Product_attributes::where('value',$arrayunitkey[$i])->where('key',$arrayData[$i])->where('product_id',$id)->first();
                }else{
                    $product_attribute = New Product_attributes;
                }*/
                  if($unitmainid[$i]!=''){
                      $getCnt=Product_attributes::where('id',$unitmainid[$i])->count();
                      if($getCnt>0){
                         $product_attribute = Product_attributes::find($unitmainid[$i]);
                      }
                      else{
                          $product_attribute = New Product_attributes;
                      }
                  }
                  else{
                         $product_attribute = New Product_attributes;
                        
                  }
                        $product_attribute->quantity = $quantity[$i];
                        $product_attribute->value = $arrayunitkey[$i];
                        $product_attribute->key = $arrayData[$i];
                        $product_attribute->price = number_format($price[$i],2);
                        $product_attribute->sale_price = number_format($sale_price[$i],2);
                        $product_attribute->available_quantity = $quantity[$i];
                        $product_attribute->discount = $discount[$i];
                        $product_attribute->max_quantities = $max_quantities[$i];
                        $product_attribute->gst_price = 0;
                        $product_attribute->product_id = $id;
                        $product_attribute->deleted_at = null;
                        $product_attribute->save();
               
            }
            return redirect()->route('admin.products.index')
                ->with('message', 'Product updated successfully');
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $permission = Permission::permission('product');
        if($permission->delete==1) { 
            $product = $this->productRepo->findProductById($id);
            $product->categories()->sync([]);
            $productAttr = $product->attributes();
            
            if (Product_attributes::where('product_id',$id)->exists()){
                Product_attributes::where('product_id',$id)->delete();
            }
            
            $productAttr->each(function ($pa) {
                DB::table('attribute_value_product_attribute')->where('product_attribute_id', $pa->id)->delete();
            });

            $productAttr->where('product_id', $product->id)->delete();

            $productRepo = new ProductRepository($product);
            $productRepo->removeProduct();

            return redirect()->route('admin.products.index')->with('message', 'Product deleted successfully');
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeImage(Request $request)
    {
        $this->productRepo->deleteFile($request->only('product', 'image'), 'uploads');
        return redirect()->back()->with('message', 'Image deleted successfully');
    }
     public function removeProductattribute(Request $request){
        
          $product_attribute = Product_attributes::find($request->id)->delete();
          if($product_attribute){
              echo "true";
          }
          else{
               echo "false";
          }
    }
    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeThumbnail(Request $request)
    {
        $this->productRepo->deleteThumb($request->input('src'));
        return redirect()->back()->with('message', 'Image deleted successfully');
    }

    /**
     * @param Request $request
     * @param Product $product
     * @return boolean
     */
    private function saveProductCombinations(Request $request, Product $product): bool
    {
        $fields = $request->only(
            'productAttributeQuantity',
            'productAttributePrice',
            'sale_price',
            'default'
        );

        if ($errors = $this->validateFields($fields)) {
            return redirect()->route('admin.products.edit', [$product->id, 'combination' => 1])
                ->withErrors($errors);
        }

        $quantity = $fields['productAttributeQuantity'];
        $price = $fields['productAttributePrice'];

        $sale_price = null;
        if (isset($fields['sale_price'])) {
            $sale_price = $fields['sale_price'];
        }

        $attributeValues = $request->input('attributeValue');
        $productRepo = new ProductRepository($product);

        $hasDefault = $productRepo->listProductAttributes()->where('default', 1)->count();

        $default = 0;
        if ($request->has('default')) {
            $default = $fields['default'];
        }

        if ($default == 1 && $hasDefault > 0) {
            $default = 0;
        }

        $productAttribute = $productRepo->saveProductAttributes(
            new ProductAttribute(compact('quantity', 'price', 'sale_price', 'default'))
        );

        // save the combinations
        return collect($attributeValues)->each(function ($attributeValueId) use ($productRepo, $productAttribute) {
            $attribute = $this->attributeValueRepository->find($attributeValueId);
            return $productRepo->saveCombination($productAttribute, $attribute);
        })->count();
    }

    /**
     * @param array $data
     *
     * @return
     */
    private function validateFields(array $data)
    {
        $validator = Validator::make($data, [
            'productAttributeQuantity' => 'required'
        ]);

        if ($validator->fails()) {
            return $validator;
        }
    }

    //This function use for get the category
    function getCategories( Request $request )
    {
       $categories = $this->categoryRepo->listCategories('name', 'asc')->where('parent_id', null)->where('company_id', $request->get('id'));
        $output = [];
        foreach( $categories as $category )
        {
            $output[$category->id] = $category->name;
        }
        return $output;
    }
    function getSubSubCategories( Request $request )
    {
       $categories = $this->categoryRepo->listCategories('name', 'asc')->where('id', $request->get('parentid'))->where('company_id', $request->get('id'));
        $output = [];
        foreach( $categories as $category )
        {
            $output[$category->id] = $category->name;
        }
        return $output;
    }
    //This function use for get the category
    function getCropCategories( Request $request )
    {
       // $categories = $this->attributeRepo->listAttributes()->where('crop_category', $request->get('id'));
        $categories = Crop::where('crop_category', $request->id)->get();
        $output = [];
        foreach( $categories as $category )
        {
            $output[$category->id] = $category->name;
        }
        return $output;
    }

    //This funcion use for get the sub category
    function getSubCategories( Request $request )
    {
       $categories = $this->categoryRepo->listCategories('name', 'asc')->where('parent_id', $request->get('id'));
        $output = [];
        foreach( $categories as $category )
        {
            $output[$category->id] = $category->name;
        }
        return $output;
    }

    function checkHsnCode(Request $request)
    {
        $hsncode = trim(strip_tags($request->hsncode));
        $product = Product::where('hsn_code',$hsncode)->count();
        if($product==0){
            echo "false";
        } else{
            echo "true";
        }
    }
}
