<?php

namespace App\Http\Controllers\RestAPIs\v1\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\ResponseMessage;
use App\Shop\Postcodes\Postcode;
use Validator;
use App\Shop\States\State;
use App\Shop\Cities\City;
use Log;
use App\Shop\CountriesData\CountryData;
use App\Shop\Addresses\Address_1;
use App\Shop\Addresses\OrderAddressDriver;
use App\Helper\Translate;
use DB;


class AddressController extends Controller
{
    public function addAddress(Request $request){
        try {
            $rules = [
                'user_id' => 'required',
                'address' => 'required',
                'type' => 'required',
                'postcode' => 'required',
                'name'=>'required',
                'mobile'=>'required',
                'email'=>'required'
            ];
            $customeMessage = [
                'user_id.required' => 'Please enter user id.',
                'address.required' => 'Please enter address.',
                'type.required' => 'Please enter address type.',
                'postcode.required' => 'Please enter postcode.',
                'landmark.required' => 'Please enter landmark.',
                 'name.required' => 'Please enter name.',
                 'mobile.required' => 'Please enter mobile.',
                 'email.required' => 'Please enter email.'
            ];
            $validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            } else {
                $user_id = trim(strip_tags($request->user_id));
                $add = trim(strip_tags($request->address));
                $type = trim(strip_tags($request->type));
                $postcode = trim(strip_tags($request->postcode));
                $name = trim(strip_tags($request->name));
                $email = trim(strip_tags($request->email));
                $mobile = trim(strip_tags($request->mobile));
                $isedit =  trim(strip_tags($request->isedit));
                // echo Address_1::where('user_id',$user_id)->count();
                // exit;
                if(Postcode::where('postcode',$postcode)->exists()){
                    if(isset($request->id)){
                        $id = trim(strip_tags($request->id));
                        if(Address_1::where('id',$id)->exists()){
                            $address = Address_1::find($id);
                        }else {
                            ResponseMessage::error("Address not found");
                        }
                    }else{
                        $address = new Address_1();
                    }
                    $data['address']=$add;
                    if(isset($request->landmark)){
                        $data['landmark']=$request->landmark;
                    }
                    if(isset($request->area)){
                        $data['area']=$request->area;
                    }
                    if(isset($request->city)){
                        $data['city']=$request->city;
                    }
                    $translate = Translate::translate($data);
                    $decode = json_decode($translate);
                    // dd($decode->address->hi);
                    $address->address = $add;
                    $address->name = $name;
                    $address->email = $email;
                    $address->mobile = $mobile;
                    $address->address_hi = $decode->address->hi;
                    $address->address_gu = $decode->address->gu;
                    $address->user_id = $user_id;
                    $address->type = $type;
                    $address->postcode = $postcode;
                    if($isedit==0){
                         $address->default = (Address_1::where('user_id',$user_id)->count()==0) ? 1:0;
                    }
                   
                    if(isset($request->landmark)){
                        $address->landmark = trim(strip_tags($request->landmark));
                        $address->landmark_hi = $decode->landmark->hi;
                        $address->landmark_gu = $decode->landmark->gu;
                    }else{
                        $address->landmark = NULL;
                    }
                    if(isset($request->area)){
                        $address->area = trim(strip_tags($request->area));
                        $address->area_hi = $decode->area->hi;
                        $address->area_gu = $decode->area->gu;
                    }
                    if(isset($request->city)){
                        $address->city = trim(strip_tags($request->city));
                        $address->city_hi = $decode->city->hi;
                        $address->city_gu = $decode->city->gu;
                    }
                    /*  if(Address_1::where('user_id',$user_id)->count()==0){
                         $address->default=1;
                    }
                  else{
                         if(isset($request->default)){
                            $address->default = trim(strip_tags($request->default));
                        }else{
                            $address->default = 0;
                        }
                    }*/
                    if(isset($request->lat)){
                        $address->lat = trim(strip_tags($request->lat));
                    }else{
                        $address->lat = NULL;
                    }
                    if(isset($request->long)){
                        $address->long = trim(strip_tags($request->long));
                    }else{
                        $address->long = NULL;
                    }
                    $address->save();
                    ResponseMessage::success('Success', $address);
                }else{
                    ResponseMessage::error("Postcode not found");
                }
            }
        } catch (Exception $e) {
            log::error($e);
        }
    }
    public function userAddressList(Request $request) {
        try {
            $rules = [
                'user_id' => 'required',
                'type' => 'required'
            ];
            $customeMessage = [
                'user_id.required' => 'Please enter user id.',
                'type.required' => 'Please enter type.'
            ];
            $validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            } else {
                $user_id = trim(strip_tags($request->user_id));
                $type = trim(strip_tags($request->type));
                $all=trim(strip_tags($request->all));
                if( $all==1){
                     $getCntAddrss=Address_1::where('user_id',$user_id)->count();
                     if($getCntAddrss>0){
                          if($type==1) {
                            $address = Address_1::where(['user_id'=>$user_id,'default'=>1])->first();
                        } else {
                            $address_id = OrderAddressDriver::join('orders','orders.id','order_address_driver.order_id')->select('order_address_driver.address_id')->where('customer_id',$user_id)->orderBy('order_address_driver.id','desc')->first();
                            if($address_id){
                                $address = DB::select(DB::raw("select * from `address` WHERE `user_id`= $user_id,`default`= '1', ORDER BY FIELD(`id`,$address_id->address_id) DESC,`id`"))->first();
                            }else{
                                $address = Address_1::where(['user_id'=>$user_id,'default'=>1])->first();
                            }  
                        }
                        
                       $getaddress = [];
                       
                       //$getaddress[] =$address;
                        // foreach($addresses as $addr){
                            
                        // }
                        // if(Postcode::where('postcode',$addresses->postcode)->exists()){
                        //         array_push($address,$addr);
                        //     }
                        //array_push($address,$getaddress);
                     }
                    
                }
                else{
                     if($type==1) {
                    $addresses = Address_1::where('user_id',$user_id)->get();
                } else {
                    $address_id = OrderAddressDriver::join('orders','orders.id','order_address_driver.order_id')->select('order_address_driver.address_id')->where('customer_id',$user_id)->orderBy('order_address_driver.id','desc')->first();
                    if($address_id){
                        $addresses = DB::select(DB::raw("select * from `address` WHERE `user_id`= $user_id ORDER BY FIELD(`id`,$address_id->address_id) DESC,`id`"));
                    }else{
                        $addresses = Address_1::where('user_id',$user_id)->get();
                    }  
                }
                $address = [];
                foreach($addresses as $addr){
                    if(Postcode::where('postcode',$addr->postcode)->exists()){
                        array_push($address,$addr);
                    }
                }
                }
               /*if($all==1){
                   ResponseMessage::newsuccess('Success', $address);
               }
               else{
                   ResponseMessage::success('Success', $address);
               }*/
                   ResponseMessage::success('Success', $address);
            }
        } catch (Exception $e) {
            log::error($e);
        }
    }
    public function userDefaultAddress(Request $request) {
        try {
            $rules = [
                'user_id' => 'required',
                'address_id' => 'required'
            ];
            $customeMessage = [
                'user_id.required' => 'Please enter user id.',
                'address_id.required' => 'Please enter address id.'
            ];
            $validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            } else {
                $user_id = trim(strip_tags($request->user_id));
                $address_id = trim(strip_tags($request->address_id));
                if(Address_1::where('user_id',$user_id)->where('id',$address_id)->exists()){
                    $addresses = Address_1::where('user_id',$user_id)->get();
                    foreach($addresses as $address){
                        $address = Address_1::where('id',$address->id)->first();
                        $address->default = 0;
                        $address->save();
                    }
                    $add = Address_1::find($address_id);
                    $add->default = 1;
                    $add->save();
                    ResponseMessage::success('Success', "Success");
                }else{
                    ResponseMessage::error("Address not found.");
                }
            }
        } catch (Exception $e) {
            log::error($e);
        }
    }
    public function checkPostcode(Request $request){
        try{
            $rules = [
                'postcode' => 'required'
            ];
            $customeMessage = [
                'postcode.required' => 'Please enter postcode.'
            ];
            $validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            } else {
                $postcode = trim(strip_tags($request->postcode));
                if(Postcode::where('postcode',$postcode)->exists()){
                    ResponseMessage::success('Success', "Postcode is available");
                }else{
                    ResponseMessage::error("Postcode not found");
                }
            }
        } catch (Exception $e) {
    		log::error($e);
    	}
    }
    public function deleteAddress(Request $request){
        try {
            $rules = [
                // 'user_id' => 'required',
                'address_id' => 'required'
            ];
            $customeMessage = [
                // 'user_id.required' => 'Please enter user id.',
                'address_id.required' => 'Please enter address id.',
            ];
            $validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            } else {
                $address_id = trim(strip_tags($request->address_id));
                if(Address_1::where('id',$address_id)->exists()){
                    $address = Address_1::find($address_id);
                    $address->delete();
                    ResponseMessage::success('Success', "Address delete successfully.");
                }else{
                    ResponseMessage::error("Address not found");
                }
            }
    	} catch (Exception $e) {
    		log::error($e);
    	}    
    }
    public function state(Request $request) {
    	try {
    	    $states=[];
    	    if($request->code!=""){
    	        if(CountryData::where('STCode', 'like', '%' .$request->code)->where('DTCode',000)->exists()){
                    $states = DB::select(DB::raw("select `STCode`,`DTName` from `countryData` WHERE `DTCode`= 000 ORDER BY FIELD(`STCode`,$request->code) DESC, `DTName`"));
    	        } else {
    	            $states = CountryData::select('STCode', 'DTName')->where('DTCode',000)->orderBy('DTName', 'ASC')->get();
    	        }
    	    }
    	    else{
                $states = CountryData::select('STCode', 'DTName')->where('DTCode',000)->orderBy('DTName', 'ASC')->get();
    	    }
    		ResponseMessage::success('Success', $states);
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }

    public function city(Request $request) {
    	try {
    		$id = trim(strip_tags($request->state_id));
            $cities=[];
    	    if($request->code!=""){
    	        if(CountryData::where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' .$id)->exists()){
                    $cities = DB::select(DB::raw("select `DTCode`,`DTName` from `countryData` WHERE `SDTCode`= 00000 AND `DTCode`!= 000 AND `STCode`= $id ORDER BY FIELD(`DTCode`,$request->code) DESC,`DTName`"));
    	        } else {
    	            $cities = CountryData::select('DTCode', 'DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' . $id)->orderBy('DTName', 'ASC')->get();
    	        }
    	    }
    	    else{
                $cities = CountryData::select('DTCode', 'DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' . $id)->orderBy('DTName', 'ASC')->get();
    	    }
    		ResponseMessage::success('Success', $cities);
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }
    public function tehsil(Request $request) {
        try {
            $tehsils=[];
            $id = trim(strip_tags($request->city_id));
            if($request->code!=""){
    	        if(CountryData::where('DTCode', 'like', '%' .$id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->exists()){
                    $tehsils = DB::select(DB::raw("select `SDTCode`,`SDTName` from `countryData` WHERE `TVCode`= 000000 AND `SDTCode`!= 00000 AND `DTCode`= $id ORDER BY FIELD(`SDTCode`,$request->code) DESC,`SDTName`"));
    	        } else {
    	            $tehsils = countrydata::select('SDTCode', 'SDTName')->where('DTCode', 'like', '%' .$id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->orderBy('SDTName', 'ASC')->get();
    	        }
    	    }
    	    else{
                $tehsils = countrydata::select('SDTCode', 'SDTName')->where('DTCode', 'like', '%' .$id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->orderBy('SDTName', 'ASC')->get();
    	    }
            ResponseMessage::success('Success', $tehsils);
        } catch (Exception $e) {
            log::error($e);
        }
    }
    public function village(Request $request) {
        try {
            $id = trim(strip_tags($request->tehsil_id));
            if($request->code!=""){
    	        if(CountryData::where('SDTCode', 'like', '%' .$id)->where('TVCode', '!=',000000)->exists()){
                    $villages = DB::select(DB::raw("select `TVCode`,`Name` from `countryData` WHERE `TVCode`!= 000000 AND `SDTCode`= $id ORDER BY FIELD(`TVCode`,$request->code) DESC,`Name`"));
    	        } else {
    	            $villages = countrydata::select('TVCode', 'Name')->where('SDTCode', 'like', '%' .$id)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->get();
    	        }
    	    }
    	    else{
                $villages = countrydata::select('TVCode', 'Name')->where('SDTCode', 'like', '%' .$id)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->get();
    	    }
            
            ResponseMessage::success('Success', $villages);
        } catch (Exception $e) {
            log::error($e);
        }
    }
}
