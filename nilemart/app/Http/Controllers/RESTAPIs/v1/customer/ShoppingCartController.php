<?php

namespace App\Http\Controllers\RestAPIs\v1\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\OrderProducts\OrderProduct;
use App\Helper\ResponseMessage;
use App\Shop\Customers\Customer;
use App\Shop\Addresses\Address;
use App\Shop\Addresses\OrderAddressDriver;
use App\Shop\Products\Product;
use App\Shop\Payments\Payment;
use App\Shop\States\State;
use App\Shop\Orders\Order;
use App\Shop\Carts\Cart;
use \stdClass;
use Validator;
use Log;
use App\Shop\Products\Product_attributes;
use App\Shop\Notifications\Notification;
use App\Shop\Customers\Wallet;
use App\Shop\Offers\OfferProduct;
use App\Helper\MailHelper;
use App\Shop\OrderStatuses\OrderStatus;

class ShoppingCartController extends Controller
{
    public function payment(Request $request) 
    {
		try {
			$rules = [
    			'device' => 'required',
    			'amount' => 'required',
    			'transaction_id' => 'required',
    			'payment_type' => 'required',
    			'userid' => 'required',
    			'status' => 'required',
    		];
    		$customeMessage = [
    			'device.required' => 'Please enter device.',
    			'amount.required' => 'Please enter amount.',
    			'payment_type.required' => 'Please enter payment type.',            
    			'transaction_id.required' => 'Please enter transaction id.',
    			'userid.required' => 'Please enter user id.',
    			'status.required' => 'Please enter status.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$device = trim(strip_tags($request->device));
	        	$amount = trim(strip_tags($request->amount));
	        	$transaction_id = trim(strip_tags($request->transaction_id));
	        	$payment_type = trim(strip_tags($request->payment_type));
	        	$userid = trim(strip_tags($request->userid));
	        	$status = trim(strip_tags($request->status));

	        	$payment = new Payment();
	        	$payment->device = $device;
	        	$payment->amount = $amount;
	        	$payment->transaction_id = $transaction_id;
	        	$payment->payment_type = $payment_type;
	        	$payment->userid = $userid;
	        	$payment->status = $status;
	        	if($payment->save()){
	        		ResponseMessage::success('Success', $payment);
		        } else {
		        	ResponseMessage::error('Somethig was wrong please try again!!');
		        }
	        }
		} catch (Exception $e) {
			log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
		}
	}

	public function order(Request $request) {
		try {
			$rules = [
    			'alias' => 'required',
    			'mobile' => 'required|numeric',
    			'zip' => 'required|numeric',
    			'address_id' => 'required',
    			'street' => 'required',
    // 			'city' => 'required',
    // 			'state'=>'required',
    //             'tid' => 'required',
    //             'vid' => 'required',
    			'payment' => 'required',
    			'customer_id' =>'required',
    		];
    		$customeMessage = [
    			'alias.required' => 'Please enter alias.',
    			'address_id.required' => 'Please enter address id.',
    			'mobile.required' => 'Please enter mobile number.',
    			'mobile.numeric' => 'Please enter number only.',
    			'zip.required' => 'Please enter pin code.',
    			'zip.numeric' => 'Please enter number only.',
    			'street.required' => 'Please enter socity/street.',
    			'city.required' => 'Please enter city',
                'tid.required' => 'Please enter tehsil',
                'vid.required' => 'Please enter village',
    			'state.required' => 'Please enter state',
    			'full_address.required' => 'Please enter full_address.',
    			'payment.required' => 'Please select payment method.',
    			'customer_id.required' => 'Please enter customer id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$alias = trim(strip_tags($request->alias));
	        	$mobile = trim(strip_tags($request->mobile));
	        	$addr_id = trim(strip_tags($request->address_id));
	        	$zip = trim(strip_tags($request->zip));
	        	$street = trim(strip_tags($request->street));
	       // 	$city = trim(strip_tags($request->city));
	       // 	$state = trim(strip_tags($request->state));
        //         $tid = trim(strip_tags($request->tid));
        //         $vid = trim(strip_tags($request->vid));
	        	$full_address = trim(strip_tags($request->full_address));
	        	$payment = trim(strip_tags($request->payment));
	        	$customer_id = trim(strip_tags($request->customer_id));
	       // 	$wallet_point = trim(strip_tags($request->wallet_point));
	        	$total_gst = 0;
	        	$email = null;
	        	if(isset($request->mail)){
	        	    $email = trim(strip_tags($request->mail));
	        	}
	        	if(Cart::where('customer_id', $customer_id)->where('status',1)->exists()){
		        	$address = new Address();
		        	$address->alias = $alias;
		        	$address->address_1 = $street;
		        	$address->address_2 = $full_address;
		        	$address->zip = $zip;
		      //  	$address->city = $city;
		      //  	$address->state_code = $state;
		      //  	$address->tehsil_id = $tid;
		      //  	$address->village_id = $vid;
		        	$address->phone = $mobile;
		        	$address->country_id = 99;
		        	$address->customer_id = $customer_id;
		        	$address->save();
	           // dd($address);
	           
		        	$address_id = Address::where('customer_id', $customer_id)->orderBy('id', 'DESC')->first();
		        	$total_price=0;
		        	$shipping_price=0;
		        	$discount = 0;
		        	$total_shipping_price = array();
		        	$products = Cart::where('customer_id', $customer_id)
			        					->where('status',1)
			        					->get();
		        	foreach($products as $prod) {
			        	$product = Product_attributes::find($prod->attribute_id);
			        	if($prod->quantity > $product->available_quantity){
			        		$productname = Product::find($prod->product_id);
			        		ResponseMessage::error('Currently '.$productname->name.' product quantity not available.');
			        	}
			        	if ($product) {
			        		$discount = $discount+($product->discount*$prod->quantity);	
			        	}
			        	$sale_price = $product->sale_price;
			        	$sale_price = str_replace(",","",$sale_price);
			        	$total_price = $total_price+($sale_price*$prod->quantity);
			        }
		        	$reference = Order::max('id')+1;
		        	$order = new Order();
		        	$order->reference = $reference;
		        	$order->customer_id = $customer_id;
		        	if(isset($request->mail)){
		        	    $order->email = $email;
		        	}
		        	$order->address_id = $address_id->id;
		        	$order->payment = $payment;
		        	$order->order_status_id = 3;
		        	$order->total = $total_price;
		        	$order->discounts = $discount;
		        	$order->courier_id = 1;
		        	$order->total_products = $total_price;
		      //  	$order->wallet_point = $wallet_point;
		        	$order->mail_status=1;
		        	$order->tax = $total_gst;
		        	date_default_timezone_set('Asia/Kolkata');
                    $order->created_at = date("Y-m-d H:i:s");
		        	$order->save();
		        	if ($order->save()) {
		        		// if ($order->wallet_point!=0 && $order->wallet_point!="") 
		        		// {
		        		// 	$wallet = new Wallet();
		        		// 	$wallet->customer_id = $order->customer_id;
		        		// 	$wallet->order_id = $order->id;
		        		// 	$wallet->credit_or_debit = "debit";
		        		// 	$wallet->points = $order->wallet_point;
		        		// 	$wallet->amount = $order->wallet_point*env('DISCOUNT_POINT');
		        		// 	$wallet->save();	
		        		// }
		        	}
		        	
		        	
	                $orderAddress = new OrderAddressDriver();
	                $orderAddress->order_id = $order->id;
	                $orderAddress->address_id = $addr_id;
                    $orderAddress->save();

			        $order_id = Order::where('customer_id', $customer_id)->orderBy('id', 'DESC')->first();
	    						
		        	foreach($products as $prod) {
			        	$product = Product::find($prod->product_id);
			        	$attribute = Product_attributes::find($prod->attribute_id);
			        	$orderproduct = new OrderProduct();
			        	$orderproduct->order_id = $order_id->id;
			        	$orderproduct->product_id = $product->id;
			        	$orderproduct->quantity = $prod->quantity;
			        	$orderproduct->product_name = $product->name;
			        	$orderproduct->product_sku = $product->sku;
			        	$orderproduct->product_description = $product->description;
			        	$orderproduct->product_price = $attribute->sale_price*$prod->quantity;
			        	$orderproduct->attribute_id = $prod->attribute_id;
			        	$orderproduct->offer_id = $prod->offer_id;
			        	
			        	if($product->shipping_price){
			        	    $orderproduct->shipping_price = $product->shipping_price;
			        	    $shipping_price=$shipping_price+$product->shipping_price;
			        	}
			        	if($orderproduct->save()) {
			        		$attribute = Product_attributes::find($prod->attribute_id);
			        		$available_quantity = $attribute->available_quantity-$prod->quantity;
			        		$attribute->available_quantity =$available_quantity;
			        		$attribute->save();
			        		
			        		$notification = new Notification();
							$notification->customer_id=$customer_id;
							$notification->notify_detail="Product ordered Successfully";
							date_default_timezone_set('Asia/Kolkata');
							$notification->time=date("Y-m-d H:i:s");
							$notification->save();
							
                            $current_date=date("Y-m-d H:i:s");
                            $customer = Customer::find($customer_id);
                            $customer->mobile = $mobile;
							$status = OrderStatus::find($order->order_status_id);
        					$statuses = $status->name;
					        foreach($products as $product){
					            $attr = Product_attributes::find($product->attribute_id);
					            $hsn = Product::find($product->product_id);
					            $product->product_name = $hsn->name;
					           // $product->hsn_code = $hsn->hsn_code;
					            $offer_status=0;
					            if($attr){
					                $product->product_price = $attr->sale_price;
					                $product->unit = $attr->value." ".$attr->key;
					               // $product->gst_price = $attr->gst_price;
					            }
					            if($product->offer_id!=0){
					                $offer = OfferProduct::join('offers','offers.id','offer_product.offer_id')
							    							->where('product_id',$product->product_id)
							    							->where('expired_date','>=',$current_date)
							    							->where('offer_id',$product->offer_id)
							    							->where('status',1)
							    							->first();
							    	$offer_status=1;
					                $product->offer_product = $hsn->offer_product;
					            }
					        }
					        $data = [
					            'order' => $order,
					            'products' => $products,
					            'statuses' => $statuses,
					            'offer_status' => $offer_status,
					            'customer' => $customer,
					            'email' => $email
					           
					        ];
					       // log::error($data);
					       // dd($prod->attribute_id);
    			        		$carts = Cart::where('customer_id', $customer_id)
    			        					// ->where('product_id',$product->id)
    			        					->where('attribute_id',$prod->attribute_id)
    			        					->where('status',1)
    			        					->get();
    			        		foreach($carts as $cart){
    			        		    $cart->delete();
    			        		}
			        	}

		        	}
		        	$order_id->total_shipping = $shipping_price;
		        	$order_id->save();
		        	
			        
					       // dd('e');
				// 	Log::error($request->mail);       
			        if(isset($request->mail)){
					    $customer_mail = MailHelper::customerOrderToAdmin($data);
			        }else{
			            $admin_mail = MailHelper::customerOrder($data);
			        } 
		        	ResponseMessage::success('Success', "You have placed order successfully");
		        } else {
		        	ResponseMessage::error('Cart is empty!!!!');
		        }
	        }
		} catch (Exception $e) {
			log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
		}
	}

	public function getOrder(Request $request){
		try {
			$rules = [
    			'user_id' => 'required',
    			'flag' => 'required',
    		];
    		$customeMessage = [
    			'user_id.required' => 'Please enter user id',
    			'flag.required' => 'Please enter flag',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$admin_id = trim(strip_tags($request->user_id));
	        	$flag = trim(strip_tags($request->flag));
	        	$order=[];
		    	if(Order::where('customer_id', $admin_id)->exists()) {
		    		$query = Order::where('customer_id', $admin_id);
		    		if($flag==0){
		    			$query = $query->where('order_status_id', 3)
			    					->orWhere('order_status_id', 2);
		    		}
		    		if($flag==1){
		    			$query = $query->where('order_status_id', 1);
		    		}
		    		if($flag==2){
		    			$query = $query->where('order_status_id', 4);
		    		}
		    		$query = $query->orderBy('id','DESC')->get();
		    		foreach($query as $data){
    		    		$orders['id'] = $data['id'];
    		    		// if($data['wallet_point']!=0){
    		    		//     $total = number_format($data['total']+$data['total_shipping']+$data['tax']-$data['wallet_point']*10, 2);
    		    		// }else{
    		    		    $total = number_format($data['total']+$data['total_shipping']+$data['tax'], 2);
    		    		// }
    		    		$orders['total'] = str_replace(",","",$total);
    		    		$orders['created_at'] = $data['created_at'];
    		    		array_push($order, $orders);
		    		}
		    		ResponseMessage::success('Success', $order);
		    	} else {
		    		ResponseMessage::error('Customer order not found');
		    	}
	        }
		} catch (Exception $e) {
			log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
		}
	}

    public function currentOrder(Request $request) {
    	try {
    		$rules = [
    			'order_id' =>'required',
    		];
    		$customeMessage = [
    			'order_id.required' => 'Please enter order id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$data = array();
		    	$order_id = trim(strip_tags($request->order_id));
		    	if(Order::where('id', $order_id)->exists()) {
			    	$orders = Order::where('id', $order_id)->get();
			    	if($orders) {
			    		foreach ($orders as $order) {
			    			$query = Product::join('order_product', 'order_product.product_id', 'products.id')
			    								->join('orders', 'orders.id', 'order_product.order_id')
			    								->join('product_attribute','product_attribute.id','order_product.attribute_id')
			    								->join('employees', 'employees.id', 'products.company_id');
			    			if($request->get('userdetail')){
                                if($request->get('userdetail')->language=="hi"){
		    						$query->Select('orders.id as order_id','products.name_hi as product_name', 'product_attribute.sale_price', 'order_product.quantity', 'product_attribute.price', 'products.shipping_price', 'product_attribute.value', 'product_attribute.key', 'product_attribute.discount', 'employees.name_hi as Dealer', 'orders.created_at', 'products.cover','product_attribute.gst_price');
		    					} elseif($request->get('userdetail')->language=="gu"){
		    					    $query->Select('orders.id as order_id','products.name_gu as product_name', 'product_attribute.sale_price', 'order_product.quantity', 'product_attribute.price', 'products.shipping_price', 'product_attribute.value', 'product_attribute.key', 'product_attribute.discount', 'employees.name_gu as Dealer', 'orders.created_at', 'products.cover','product_attribute.gst_price');
		    					} else{
		    					    $query->Select('orders.id as order_id','products.name as product_name', 'product_attribute.sale_price', 'order_product.quantity', 'product_attribute.price', 'products.shipping_price', 'product_attribute.value', 'product_attribute.key', 'product_attribute.discount', 'employees.name as Dealer', 'orders.created_at', 'products.cover','product_attribute.gst_price');
		    					}
			    			}
			    			$products = $query->where('order_product.order_id', $order->id)->get();
		    				foreach($products as $product) {
		    				    $i=0;
		    				    $product->sale_price=str_replace(",","",$product->sale_price);
		    				    $gst = ($product->quantity*$product->gst_price*$product->sale_price)/100;
		    				    $product->price=str_replace(",","",$product->price);
		    				    $product->sale_price=str_replace(",","",$product->sale_price);
		    				    $total = ($product->quantity*$product->sale_price)+$product->shipping_price+$gst;
		    				    $total = number_format($total,2);
		    				    $product->total = str_replace(",","",$total);
		    				    $gst =  number_format($gst,2);
		    				    $product->gst_price=str_replace(",","",$gst);
		    					array_push($data, $product);
		    					
		    				} 
			    		}
		    			ResponseMessage::success('Success', $data);
			    	} else {
			    		ResponseMessage::error('Order not found.');
			    	}
			    } else {
			    	ResponseMessage::error('Order not found.');
			    }
		    }
    	} catch (Exception $e) {
    		log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
    	}
    }

    public function completeOrder(Request $request) {
    	try {
    		$rules = [
    			'order_id' => 'required',
    		];
    		$customeMessage = [
    			'order_id.required' => 'Please enter order id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$data = array();
		    	$order_id = trim(strip_tags($request->order_id));
		    	if(Order::where('id', $order_id)->exists()) {
			    	$orders = Order::where('id', $order_id)->get();
			    	if($orders) {
			    		foreach($orders as $order) {
			    			$query = Product::join('order_product', 'order_product.product_id', 'products.id')
			    				->join('orders', 'orders.id', 'order_product.order_id')
			    				->join('product_attribute','product_attribute.id','order_product.attribute_id')
			    				->join('employees', 'employees.id', 'products.company_id');
			    			if($request->get('userdetail')){
                                if($request->get('userdetail')->language=="hi"){
		    						$query->Select('orders.id as order_id','products.name_hi as product_name', 'product_attribute.sale_price', 'order_product.quantity', 'product_attribute.price', 'products.shipping_price', 'product_attribute.value', 'product_attribute.key', 'product_attribute.discount', 'employees.name_hi as Dealer', 'orders.created_at', 'products.cover','product_attribute.gst_price');
		    					} elseif($request->get('userdetail')->language=="gu"){
		    					    $query->Select('orders.id as order_id','products.name_gu as product_name', 'product_attribute.sale_price', 'order_product.quantity', 'product_attribute.price', 'products.shipping_price', 'product_attribute.value', 'product_attribute.key', 'product_attribute.discount', 'employees.name_gu as Dealer', 'orders.created_at', 'products.cover','product_attribute.gst_price');
		    					} else{
		    					    $query->Select('orders.id as order_id','products.name as product_name', 'product_attribute.sale_price', 'order_product.quantity', 'product_attribute.price', 'products.shipping_price', 'product_attribute.value', 'product_attribute.key', 'product_attribute.discount', 'employees.name as Dealer', 'orders.created_at', 'products.cover','product_attribute.gst_price');
		    					}
			    			}	
			    	        $products = $query->where('order_product.order_id', $order->id)->get();
			    			foreach($products as $product) {
			    			    $i=0;
			    			    $product->sale_price=str_replace(",","",$product->sale_price);
			    			    $gst = ($product->quantity*$product->gst_price*$product->sale_price)/100;
		    				    $product->price=str_replace(",","",$product->price);
		    				    $product->sale_price=str_replace(",","",$product->sale_price);
		    				    $total = ($product->quantity*$product->sale_price)+$product->shipping_price+$gst;
		    				    $total = number_format($total,2);
		    				    $product->total = str_replace(",","",$total);
		    				    $gst =  number_format($gst,2);
		    				    $product->gst_price=str_replace(",","",$gst);
		    					array_push($data, $product);
		    					$i++;
		    				}
			    		}
			    		ResponseMessage::success('Success', $data);
			    	} else {
			    		ResponseMessage::error('Order not found.');
			    	}
			    } else {
			    	ResponseMessage::error('Order not found.');
			    }
		    }
    	} catch (Exception $e) {
    		log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
    	}
    }

    public function showCancelOrder(Request $request) {
    	try {
    		$rules = [
    			'order_id' => 'required',
    		];
    		$customeMessage = [
    			'order_id.required' => 'Please enter order id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$data = array();
		    	$order_id = trim(strip_tags($request->order_id));
		    	if(Order::where('id', $order_id)->exists()) {
			    	$orders = Order::where('id', $order_id)->get();

			    	if($orders) {
			    		foreach($orders as $order) {
			    			$query = Product::join('order_product', 'order_product.product_id', 'products.id')
			    			->join('orders', 'orders.id', 'order_product.order_id')
			    			->join('product_attribute','product_attribute.id','order_product.attribute_id')
			    			->join('employees', 'employees.id', 'products.company_id');
			    			if($request->get('userdetail')){
                                if($request->get('userdetail')->language=="hi"){
		    						$query->Select('orders.id as order_id','products.name_hi as product_name', 'product_attribute.sale_price', 'order_product.quantity', 'product_attribute.price', 'products.shipping_price', 'product_attribute.value', 'product_attribute.key', 'product_attribute.discount', 'employees.name_hi as Dealer', 'orders.created_at', 'products.cover','product_attribute.gst_price');
		    					} elseif($request->get('userdetail')->language=="gu"){
		    					    $query->Select('orders.id as order_id','products.name_gu as product_name', 'product_attribute.sale_price', 'order_product.quantity', 'product_attribute.price', 'products.shipping_price', 'product_attribute.value', 'product_attribute.key', 'product_attribute.discount', 'employees.name_gu as Dealer', 'orders.created_at', 'products.cover','product_attribute.gst_price');
		    					} else{
		    					    $query->Select('orders.id as order_id','products.name as product_name', 'product_attribute.sale_price', 'order_product.quantity', 'product_attribute.price', 'products.shipping_price', 'product_attribute.value', 'product_attribute.key', 'product_attribute.discount', 'employees.name as Dealer', 'orders.created_at', 'products.cover','product_attribute.gst_price');
		    					}
			    			}
			    	        $products = $query->where('order_product.order_id', $order->id)->get();
			    			foreach($products as $product) {
			    			    $i=0; 
			    			    $product->sale_price=str_replace(",","",$product->sale_price);
			    			    $gst = ($product->quantity*$product->gst_price*$product->sale_price)/100;
		    				    $product->price=str_replace(",","",$product->price);
		    				    $product->sale_price=str_replace(",","",$product->sale_price);
		    				    $total = ($product->quantity*$product->sale_price)+$product->shipping_price+$gst;
		    				    $total = number_format($total,2);
		    				    $product->total = str_replace(",","",$total);
		    				    $gst =  number_format($gst,2);
		    				    $product->gst_price=str_replace(",","",$gst);
		    					array_push($data, $product);
		    					$i++;
		    				}
			    		}
			    		ResponseMessage::success('Success', $data);
			    	} else {
			    		ResponseMessage::error('Order not found.');
			    	}
			    } else {
			    	ResponseMessage::error('Order not found.');
			    }
		    }
    	} catch (Exception $e) {
    		log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
    	}
    }

    public function cancelOrder(Request $request) {
    	try {
    		$rules = [
    			'order_id' => 'required',
    		];
    		$customeMessage = [
    			'order_id.required' => 'Please enter order id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
		    	$id = trim(strip_tags($request->order_id));
		    	if(Order::where('id', $id)->exists()) {
			    	$order = Order::where('id', $id)	
			    					->where('order_status_id', '!=', 1)
			    					->where('order_status_id', '!=', 4)
			    					->first();
			    	if($order) {
			    		$order->order_status_id = 4;
			    		if($order->save()){
							
				    		$product = OrderProduct::where('order_id', $id)->get();
				    		
				    		foreach ($product as $prod) {
				    			$attribute = Product_attributes::find($prod->attribute_id);
				    			$quantity = $attribute->available_quantity + $prod->quantity;
				    			$attribute->available_quantity = $quantity;
				    			$attribute->save();
				    // 			$wallet = new Wallet();
			     //               $wallet->customer_id = $order->customer_id;
			     //               $wallet->order_id = $id;
			     //               $wallet->points = $order->wallet_point;
			     //               $wallet->amount = $order->wallet_point*env('DISCOUNT_POINT');
			     //               $wallet->credit_or_debit = "credit";
			     //               $wallet->save();
				    		}
			    			$response = Product::join('order_product', 'order_product.product_id', 'products.id')
			    				->join('orders', 'orders.id', 'order_product.order_id')
			    				->join('product_attribute','product_attribute.id','order_product.attribute_id')
			    				->join('employees', 'employees.id', 'products.company_id')
			    				->Select('orders.id as order_id', 'order_product.product_name', 'product_attribute.price', 'product_attribute.sale_price', 'products.shipping_price', 'order_product.quantity', 'products.shipping_price', 'product_attribute.value', 'product_attribute.key', 'employees.name as Dealer','employees.id as customer_id', 'orders.created_at', 'products.cover', 'product_attribute.gst_price')
			    				->where('order_product.order_id', $order->id)
			    				->get();
		    				for($i=0;$i<count($response);$i++){
		    				  //  dd($response[$i]);
		    				    $response[$i]->sale_price = str_replace(",","",$response[$i]->sale_price);
		    				    $gst = ($response[$i]->quantity*$response[$i]->gst_price*$response[$i]->sale_price)/100;
		    				    $total = ($response[$i]->quantity*$response[$i]->sale_price)+$response[$i]->shipping_price+$gst;
		    				    $gst = number_format($gst,2);
		    				    $total = number_format($total,2);
		    				    $response[$i]->gst_price=str_replace(",","",$gst);
		    				    $response[$i]->price=str_replace(",","",$response[$i]->price);
		    				    $response[$i]->sale_price=str_replace(",","",$response[$i]->sale_price);
		    				    $response[$i]->total=str_replace(",","",$total);
		    				}
			    			ResponseMessage::success('Success', $response);
			        	} else{
			        		ResponseMessage::error("Something went wrong please try again.");	
			        	}
			    	} else {
			    		ResponseMessage::error('Order not found.');
			    	}
			    } else {
			    	ResponseMessage::error('Order not found.');
			    }
		    }
    	} catch (Exception $e) {
    		log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
    	}
    }

    public function deleteProductCart(Request $request) {
    	try {
    		$rules = [
    			'product_id' => 'required',
    			'customer_id' => 'required',
    			'attribute_id' => 'required',

    		];
    		$customeMessage = [
    			'product_id.required' => 'Please enter product id.',
    			'customer_id.required' => 'Please enter customer id.',
    			'attribute_id.required' => 'Please enter attribute id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	    		$product_id = trim(strip_tags($request->product_id));
	    		$customer_id = trim(strip_tags($request->customer_id));
	    		$attribute_id = trim(strip_tags($request->attribute_id));
	    		$total_price = trim(strip_tags($request->total_price));
	    		$delete_price = trim(strip_tags($request->delete_price));
	    		
	    		if(Cart::where('product_id', $product_id)->where('customer_id', $customer_id)->where('attribute_id', $attribute_id)->where('status', 1)->exists()) {
		    		$product = Cart::where('product_id', $product_id)
		    						->where('customer_id', $customer_id)
		    						->where('attribute_id', $attribute_id)
		    						->where('status', 1)
		    						->first();
		    	    
                    $shipping_price = $product->shipping_price;
                    $total_price = $product->total_price;
		    		if($product->delete()){
		        		$notification = new Notification();
						$notification->customer_id=$customer_id;
						$notification->notify_detail="Product deleted successfully from cart";
						date_default_timezone_set('Asia/Kolkata');
						$notification->time=date("Y-m-d H:i:s");
						$notification->save();
						$count = Cart::where('customer_id', $customer_id)->count();
			    		$cart_delete['count'] = $count;
			    		//$carts = Cart::where('customer_id',$customer_id)->get();
			    		 $carts = Cart::where('customer_id', $customer_id)
            				//	->where('id','!=', $cart_id)
            					->where('status', 1)
            					->where('attribute_id','!=',$attribute_id)
            				    ->where('product_id','!=',$product_id)
            					->get();
			    		 $saving_price=array();
                	foreach($carts as $cart){
                            			 
                        $attribute = Product_attributes::where('id',$cart->attribute_id)->where('product_id',$cart->product_id)->first();
                       if($attribute){
                    		    			   $cart->price = str_replace( ',', '', $attribute->price);
                    		    			   $cart->sale_price = str_replace( ',', '', $attribute->sale_price);
                    		    			   $total = $cart->sale_price*$cart->quantity;
                    		    			   $totalprice = $cart->price*$cart->quantity;
                    		    			   $totpc= number_format($totalprice - $total,2);
                    		    			   array_push($saving_price,$totpc);
                       }
                
                	}
			    		if ($request->total_price!="") {
			    		//	$total_order_price = $request->total_price - $request->total_shipping_price - $total_price;
			    			 $total_order_price = number_format($request->total_price - $delete_price , 2);
				    		$cart_delete['total_order_price'] = $total_order_price;
                            $cart_delete['shipping_price'] = number_format($request->total_shipping_price - $shipping_price, 2);
                            $cart_delete['saving_price']=number_format(array_sum($saving_price),2);
                            $cart_delete['total'] = number_format($request->total_price - $delete_price , 2);
			    		}
		    			ResponseMessage::success("Product delete successfully.",$cart_delete);	
		    		} else {
		    			ResponseMessage::error("Something went wrong please try again.");
		    		}
		    	} else {
		    		ResponseMessage::error("Product does not found!!!.");
		    	}
	    	}
    	} catch (Exception $e) {
    		log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
    	}
    }

    public function addCart(Request $request) {
    	try {
    		$rules = [
    			'customer_id' => 'required',
    			'product_id' => 'required',
    			'attribute_id' => 'required',
    			'quantity' => 'required|numeric',
    			'total_price' => 'required',
    		];
    		$customeMessage = [
    			'customer_id.required' => 'Please enter user id.',
    			'product_id.required' => 'Please enter product id.',
    			'attribute_id.required' => 'Please enter attribute id.',
    			'quantity.required' => 'Please enter quantity.',
    			'quantity.numeric' => 'Please enter number.',
    			'total_price.required' => 'Please enter total price.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
		    	$customer_id = trim(strip_tags($request->customer_id));
		    	$product_id = trim(strip_tags($request->product_id));
		    	$attribute_id = trim(strip_tags($request->attribute_id));
		    	$quantity = trim(strip_tags($request->quantity));
		    	$total_price = trim(strip_tags($request->total_price));
		    	if(Customer::where('id',$customer_id)->exists()) {
		    		if(Product::where('id',$product_id)->exists()) {
		    		    $prod = Product::where('id',$product_id)->first();
		    		    // dd($prod);
		    			if(Product_attributes::where('id',$attribute_id)->where('product_id',$product_id)->exists()){
		    			    date_default_timezone_set('Asia/Kolkata');
    						$current_date = date('Y-m-d');
		    				$offer=OfferProduct::join('offers','offers.id','offer_product.offer_id')
		    							->where('product_id',$product_id)
		    							->where('expired_date','>=',$current_date)
		    							->first();
		    			    $Product_attributes = Product_attributes::where('id',$attribute_id)->where('product_id',$product_id)->first();
				    		if(Cart::where('customer_id',$customer_id)->where('product_id', $product_id)->where('attribute_id',$attribute_id)->exists()) {
				    			$cart = Cart::where('customer_id',$customer_id)->where('product_id', $product_id)->where('attribute_id',$attribute_id)->first();
				    		    if($quantity=='0'){
		    			             Cart::where('customer_id',$customer_id)->where('product_id', $product_id)->where('attribute_id',$attribute_id)->delete();
		    			       }
				    		} else {
				    			$cart = new Cart();
				    		}
				    		$cart->customer_id = $customer_id;
				    		$cart->product_id = $product_id; 
				    		$cart->quantity = $quantity;
				    		$cart->total_price=$total_price;
				    		$cart->status = 1;
				    		$cart->attribute_id = $attribute_id; 
				    		$cart->gst = 0;
				    		$shipping_price=str_replace(",","",$prod->shipping_price);
				    		$cart->shipping_price = $shipping_price;
				    		$sale_price=$Product_attributes->sale_price;
				    		$sale_price=str_replace(",","",$sale_price);
				    		$gst = $sale_price*$Product_attributes->gst_price/100;
				    		$cart->gst_price = 0;
				    		if(isset($offer)){
				    			$cart->offer_id = $offer->offer_id;
				    		}
				    		if($cart->save()) {
    			        		$notification = new Notification();
    							$notification->customer_id=$customer_id;
    							$notification->notify_detail="Product added to cart successfully";
    							date_default_timezone_set('Asia/Kolkata');
    							$notification->time=date("Y-m-d H:i:s");
    							$notification->save();
								$count = Cart::where('customer_id', $customer_id)->count();
        		               ResponseMessage::success('Product added successfully in the cart', $count);
				    		} else {
				    			ResponseMessage::error('Somethig was wrong please try again');
				    		}
				    	} else {
				    		ResponseMessage::error('Product attribute not found');
				    	}
			    	} else {
			    		ResponseMessage::error('Product not found');
			    	}
		    	} else {
		    		ResponseMessage::error('Customer not found');
		    	}
		    }
    	} catch (Exception $e) {
    		log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
    	}
    }
    
    public function showCart(Request $request) {
    	try {
    		$rules = [
    			'customer_id' => 'required',
    		];
    		$customeMessage = [
    			'customer_id.required' => 'Please enter user id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
		    	$customer_id = trim(strip_tags($request->customer_id));
		    	if(Customer::where('id',$customer_id)->exists()) {
		    	    if(Customer::where('id',$customer_id)->where('language','hi')->exists()){
    		    		$carts = Cart::join('products', 'products.id', 'cart.product_id')
    		    						->join('employees', 'employees.id', 'products.company_id')
    		    						->join('product_attribute', 'product_attribute.id', 'cart.attribute_id')
    		    						->Select('products.id','cart.id as cart_id', 'products.name_hi as name','product_attribute.price as single_price','product_attribute.available_quantity','product_attribute.max_quantities' ,'products.price','products.','product_attribute.value','product_attribute.key','cart.quantity', 'cart.product_size', 'cart.shipping_price', 'cart.gst','cart.gst_price' ,'cart.attribute_id', 'products.sale_price', 'cart.total_price', 'products.cover', 'employees.name_hi as dealer_name')
    		    					    ->where('cart.status', 1)
    		    					    ->where('cart.customer_id', $customer_id)
    		    					   ->get();
		    	    } elseif( Customer::where('id',$customer_id)->where('language','gu')->exists() ){
		    	        $carts = Cart::join('products', 'products.id', 'cart.product_id')
    		    						->join('employees', 'employees.id', 'products.company_id')
    		    						->join('product_attribute', 'product_attribute.id', 'cart.attribute_id')
    		    						->Select('products.id', 'cart.id as cart_id','products.name_gu as name','product_attribute.price as single_price','product_attribute.available_quantity','product_attribute.max_quantities', 'products.price','product_attribute.value','product_attribute.key' , 'cart.quantity', 'cart.product_size', 'cart.shipping_price', 'cart.gst','cart.gst_price' ,'cart.attribute_id', 'products.sale_price', 'cart.total_price', 'products.cover', 'employees.name_gu as dealer_name')
    		    					    ->where('cart.status', 1)
    		    					    ->where('cart.customer_id', $customer_id)
    		    					    ->get();
		    	        
		    	    } else{
		    	        $carts = Cart::join('products', 'products.id', 'cart.product_id')
    		    						->join('employees', 'employees.id', 'products.company_id')
    		    						->join('product_attribute', 'product_attribute.id', 'cart.attribute_id')
    		    						->Select('products.id','cart.id as cart_id', 'products.name','product_attribute.price as single_price', 'cart.quantity', 'cart.product_size', 'cart.shipping_price','product_attribute.available_quantity','product_attribute.max_quantities','product_attribute.value','product_attribute.key' ,'cart.gst','cart.gst_price' ,'cart.attribute_id', 'products.sale_price', 'cart.total_price', 'products.cover', 'employees.name as dealer_name')
    		    					    ->where('cart.status', 1)
    		    					    ->where('cart.customer_id', $customer_id)
    		    					    ->get();
		    	    }		 	   
		    		$total =0;
		    		$total_array = array();
		    		$total_order_price = array();
		    		$gst_price = array();
		    		$shipping_price = array();
		    	    $saving_price = array();
		    		foreach ($carts as $cart) {
		    			$attribute = Product_attributes::where('id',$cart->attribute_id)
		    											->where('product_id',$cart->id)
		    											->first();
		    			if($attribute){
    		    			$cart->price = str_replace( ',', '', $attribute->price);
    		    			$cart->sale_price = str_replace( ',', '', $attribute->sale_price);
    		    			$cart->single_price = $cart->sale_price;
    		    			$total_price = $cart->sale_price * $cart->quantity  + $cart->shipping_price;
    		    			$total = $cart->sale_price*$cart->quantity;
    		    			
    		    			$total = $cart->sale_price*$cart->quantity;
    		    			$totalprice = $cart->price*$cart->quantity;
    		    			$totpc= number_format($totalprice - $total,2);
    		    			
    		    		
    		    			$cart->sale_price = number_format($cart->sale_price * $cart->quantity,2);
    		    			$cart->total_price = number_format($total_price,2);
    		    			$cart->total_price = str_replace( ',', '', $cart->total_price);
    		    			array_push($saving_price,$totpc);
    		    			array_push($gst_price, $cart->gst_price);
    		    			array_push($total_array, $total_price);
    		    			array_push($total_order_price, $total);
    		    			array_push($shipping_price, $cart->shipping_price);
		    			}else {
		    			    ResponseMessage::error('Product attribute not found.');
		    			}
		    		}
		    		$product['total'] = number_format(array_sum($total_array) ,2);
		    		$product['total_order_price'] = number_format(array_sum($total_order_price) ,2);
		    		$product['shipping_price'] = number_format(array_sum($shipping_price) ,2);
		    		$product['saving_price']=number_format(array_sum($saving_price),2);
		    		$product['gst_price'] = number_format(array_sum($gst_price) ,2);
		    		$product['carts']=$carts;
		    	
		    		ResponseMessage::success('Success', $product);
		    	}  else {
		    		ResponseMessage::error('Customer not found.');
		    	}
		    }
    	} catch (Exception $e) {
    		log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
    	}
    }
    
    public function updateQuntity(Request $request) {
		try {
			$rules = [
    			'customer_id' => 'required',
    			'product_id' => 'required',
    			'quantity' => 'required|numeric',
    		];
    		$customeMessage = [
    			'customer_id.required' => 'Please enter user id.',
    			'product_id.required' => 'Please enter product id.',
    			'quantity.required' => 'Please enter quantity.',
    			'quantity.numeric' => 'Please enter only number.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$customer_id = trim(strip_tags($request->customer_id));
	        	$product_id = trim(strip_tags($request->product_id));
	        	$quantity = trim(strip_tags($request->quantity));
	        	if(Cart::where('customer_id', $customer_id)->where('product_id', $product_id)->exists()) {
	        		$product = Cart::where('customer_id', $customer_id)
	        					->where('product_id', $product_id)
	        					->where('status', 1)
	        					->first();
		        	$product->quantity = $quantity;
		        	if($product->save()) {
		        		ResponseMessage::success('Success', 'Product quantity update successfully.');
		        	} else {
		        		ResponseMessage::error('Somethig was wrong please try again!!');
		        	}
		        } else {
		        	ResponseMessage::error('Customer not found!!');
		        }
	        }
		} catch (Exception $e) {
			log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
		}
	}
	
	public function cartProductCount(Request $request){
		try {
			$rules = [
    			'customer_id' => 'required',
    		];
    		$customeMessage = [
    			'customer_id.required' => 'Please enter customer id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$customer_id = trim(strip_tags($request->customer_id));
	        	if(Cart::where('customer_id', $customer_id)->exists()) {
	        		$count = Cart::where('customer_id', $customer_id)->count();
	        		ResponseMessage::success('Success', $count);
	        	} else {
	        	    $count=0;
	        		ResponseMessage::success('Success', $count);
	        	}
	        }
		} catch (Exception $e) {
			log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
		}
	}
	
	public function updateCart(Request $request){
    	try {
			$rules = [
    			'customer_id' => 'required',
    			'cart_id' => 'required',
    			'plus_minus' => 'required',
    		];
    		$customeMessage = [
    			'customer_id.required' => 'Please enter customer id.',
    			'cart_id.required' => 'Please enter cart id.',
    			'plus_minus.required' => 'Please enter plus minus.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
        		$customer_id = trim(strip_tags($request->customer_id));
            	$cart_id = trim(strip_tags($request->cart_id));
            	$quantity = trim(strip_tags($request->quantity));
            	$plus_minus = $request->plus_minus;
            	$total_all = trim(strip_tags($request->total));
            	if(Cart::where('customer_id', $customer_id)->where('id', $cart_id)->exists()) {
            		$cart_item = Cart::where('customer_id', $customer_id)
            					->where('id', $cart_id)
            					->where('status', 1)
            					->first();
            						if($quantity=='0'){
		    			          Cart::where('customer_id',$customer_id)->where('product_id', $cart_item->product_id)->where('attribute_id',$cart_item->attribute_id)->delete();
		    	                	$cart_data = array();
		    	                	 $carts = Cart::where('customer_id', $customer_id)
            				//	->where('id','!=', $cart_id)
            					->where('status', 1)
            					->get();
            			$cart_array = array();	
            			$saving_price = array();
            		    	$total =0;
            		    	$totalprice=0;
            			foreach($carts as $cart){
            			 
            			    $attribute = Product_attributes::where('id',$cart->attribute_id)->where('product_id',$cart->product_id)->first();
            			   
            			    if($attribute){
    		    			   $cart->price = str_replace( ',', '', $attribute->price);
    		    			   $cart->sale_price = str_replace( ',', '', $attribute->sale_price);
    		    			   $total = $cart->sale_price*$cart->quantity;
    		    			   
    		    			   
    		    			   $totalprice = $cart->price*$cart->quantity;
    		    		
    		    			 
    		    			   $totpc= number_format($totalprice - $total,2);
    		    	
    		    			   array_push($saving_price,$totpc);
            			    }
            			    array_push($cart_array , $cart->total_price);
            			    

            			    
            			}
          
            			if($plus_minus=="+"){
            			    $total_order_price =  $total_all + $product_attribute->sale_price;  
            		
            			}else{
            			    $total_order_price =  $total_all - $product_attribute->sale_price; 
            
            			}
       
			    		$cart_data['total_order_price'] = number_format($total_order_price, 2);
                        $cart_data['shipping_price'] = number_format($request->total_shipping_price , 2);
                        $cart_data['saving_price'] =number_format(array_sum($saving_price),2);
                        $cart_data['total'] = number_format($total_order_price + $request->total_shipping_price  , 2);
                        ResponseMessage::success('Success',$cart_data);
		    		}
		    		else{
		    		    $cart_item->quantity = $quantity;
    	        	$cart_data = array();
    	        	$product_attribute = Product_attributes::where('product_id',$cart_item->product_id)->first();
	        	    $product = Product::where('id',$cart_item->product_id)->first();
	        	    $total =  $quantity * $product_attribute->sale_price ;
	        	    $cart_item->total_price = $total;
	        	    
    	        	if($cart_item->save()) {
    	        	    
    	        	    $carts = Cart::where('customer_id', $customer_id)
            				//	->where('id','!=', $cart_id)
            					->where('status', 1)
            					->get();
            			$cart_array = array();		
            				$saving_price = array();
            		    	$total =0;
            		    	$totalprice=0;
            			foreach($carts as $cart){
            			     $attribute = Product_attributes::where('id',$cart->attribute_id)->where('product_id',$cart->product_id)->first();
            			      if($attribute){
            			           $cart->price = str_replace( ',', '', $attribute->price);
    		    			   $cart->sale_price = str_replace( ',', '', $attribute->sale_price);
    		    			   $total = $cart->sale_price*$cart->quantity;
    		    			   
    		    			   
    		    			   $totalprice = $cart->price*$cart->quantity;
    		    		
    		    			 
    		    			   $totpc= number_format($totalprice - $total,2);
    		    	
    		    			   array_push($saving_price,$totpc);
            			      }
            			    array_push($cart_array , $cart->total_price);
            			}
            			
            			if($plus_minus=="+"){
            			    $total_order_price =  $total_all + $product_attribute->sale_price;    
            			}else{
            			    $total_order_price =  $total_all - $product_attribute->sale_price; 
            			     
            			}
			    		$cart_data['total_order_price'] = number_format($total_order_price, 2);
                        $cart_data['shipping_price'] = number_format($request->total_shipping_price , 2);
                        $cart_data['saving_price'] =number_format(array_sum($saving_price),2);
                        $cart_data['total'] = number_format($total_order_price + $request->total_shipping_price  , 2);
    	        		ResponseMessage::success('Success',$cart_data);
    	        	} else {
    	        		ResponseMessage::error('Somethig was wrong please try again!!');
    	        	}
		    		    
		    		}
    	        	
    	        } else {
    	        	ResponseMessage::error('Customer not found!!');
    	        }
	        }
    	} catch (Exception $e) {
    		log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
    	}	
	}
	
	public function buyNow(Request $request) {
		try {
			$rules = [
    			'customer_id' => 'required',
    			'product_id' => 'required',
    			'quantity' => 'required',
    			'attribute_id' => 'required',
    			'total_price'=>'required',
    		];
    		$customeMessage = [
    			'customer_id.required' => 'Please enter user id',
    			'product_id.required' => 'Please enter product id',
    			'quantity.required' => 'Please enter product quantity',
    			'attribute_id.required' => 'Please enter attribute id',
    			'total_price' => 'Please enter total price',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
		    	$customer_id = $request->customer_id;
		    	$product_id = trim(strip_tags($request->product_id));
		    	$quantity = trim(strip_tags($request->quantity));
		    	$attribute_id = trim(strip_tags($request->attribute_id));
		    	$total_price = trim(strip_tags($request->total_price));
		    	if(Customer::where('id',$customer_id)->first()) {
		    		if(Product::where('id',$product_id)->exists()) {
		    		    $prod = Product::where('id',$product_id)->first();
		    			if(Product_attributes::where('id',$attribute_id)->exists()){
		    			    date_default_timezone_set('Asia/Kolkata');
    						$current_date = date('Y-m-d');
		    				$Product_attributes = Product_attributes::where('id',$attribute_id)->where('product_id',$product_id)->first();
                            $offer=OfferProduct::join('offers','offers.id','offer_product.offer_id')
		    							->where('product_id',$product_id)
		    							->where('expired_date','>=',$current_date)
		    							->first();
	    					if(Cart::where('customer_id',$customer_id)->where('product_id', $product_id)->where('attribute_id',$attribute_id)->exists()) {
				    			$cart = Cart::where('customer_id',$customer_id)->where('product_id', $product_id)->where('attribute_id',$attribute_id)->first();
				    		} else {
				    			$cart = new Cart();
				    		}
				    		$Product_attributes->sale_price = str_replace(",","",$Product_attributes->sale_price);
				    		$cart->customer_id = $customer_id;
				    		$cart->product_id = $product_id; 
				    		$cart->quantity = $quantity;
				    		$cart->total_price=$total_price;
				    		$cart->shipping_price=$prod->shipping_price;
				    		$cart->status = 1;
				    		$cart->attribute_id = $attribute_id; 
				    		$cart->gst = $Product_attributes->gst_price;
				    		$gst = $Product_attributes->sale_price*$Product_attributes->gst_price/100;
				    		$cart->gst_price = $gst*$quantity;
				    		if(isset($offer)){
				    			$cart->offer_id = $offer->offer_id;
				    		}
				    		$cart->save();

				    		$carts = Cart::join('products', 'products.id', 'cart.product_id')
			    						->join('employees', 'employees.id', 'products.company_id')
			    						->Select('products.id', 'products.name', 'products.price', 'cart.quantity', 'cart.shipping_price', 'cart.product_size', 'cart.gst','cart.gst_price' ,'cart.attribute_id', 'products.sale_price', 'cart.total_price', 'products.cover', 'employees.name as dealer_name')
			    					   ->where('cart.status', 1)
			    					   ->where('cart.customer_id', $customer_id)
			    					   ->get();
				    		$total =0;
				    		$total_order_price = array();
				    		$total_shipping_price = array();
				    		$gst_price = array();
				    		foreach ($carts as $cart) {
				    			$attribute = Product_attributes::where('id',$cart->attribute_id)
				    											->where('product_id',$cart->id)
				    											->first();
				    			if($attribute){
		    		    			$cart->price = str_replace(",","",$attribute->price);
		    		    			$cart->sale_price = str_replace(",","",$attribute->sale_price);
		    		    			$total = $total+($cart->sale_price*$cart->quantity);
		    		    			array_push($gst_price, $cart->gst_price);
		    		    			array_push($total_shipping_price, $cart->shipping_price);
		    		    			array_push($total_order_price, $cart->sale_price);
				    			} else {
				    			    ResponseMessage::error('Product attribute not found.');
				    			}
				    		}
				    		$total_order_price = number_format(array_sum($total_order_price), 2);
		    				$total_order_price = str_replace( ',', '', $total_order_price );
				    		$product['total_order_price'] = $total_order_price;
				    		$total_shipping_price = number_format(array_sum($total_shipping_price), 2);
				    		$product['shipping_price'] = str_replace( ',', '', $total_shipping_price);
				    		$product['total'] = $total_order_price+$total_shipping_price;
				    		$product['total'] = str_replace( ',', '', number_format($product['total'], 2));
				    		ResponseMessage::success('Success', $product);
		    				
		    			} else {
		    				ResponseMessage::error('Product attribute not found!!!!');
		    			}
		    		} else {
		    			ResponseMessage::error('Product not found!!!!');
		    		}
		    	} else {
		    		ResponseMessage::error('Customer not found!!!!');
		    	}
		    }
		} catch (Exception $e) {
			log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
		}
	}
}
