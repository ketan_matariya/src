<?php

namespace App\Http\Controllers\RESTAPIs\v1\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Helper\ResponseMessage;
use App\Shop\Orders\Order;
use App\Shop\Customers\Wallet;

class WalletController extends Controller
{
    public function getWallet(Request $request) {
    	try {
    		$rules = [
    			'customer_id' => 'required'
    		];
    		$customeMessage = [
    			'customer_id.required' => 'Please enter customer id.'
    		];
    		$validator = Validator::make($request->all(),$rules,$customeMessage);
    		if ($validator->fails()) {
    			$errors = $validator->errors();
    			ResponseMessage::error($errors->first());
    		} else{
    			$customer_id = $request->customer_id;
    			$orders = Wallet::where('customer_id',$customer_id)->get();
    			$orders_array = array();
    			$totalcreditamount = array();
    			$totalcreditpoint = array();
    			$totaldebitamount = array();
    			$totaldebitpoint = array();
    			foreach ($orders as $order) {
    				if ($order->credit_or_debit == "credit") {
    					$order['title'] = "Added to wallet";
    					array_push($totalcreditamount, $order->amount);
    					array_push($totalcreditpoint, $order->points);
    				} else{
    					array_push($totaldebitamount, $order->amount);
    					array_push($totaldebitpoint, $order->points);
    					$order['title'] = "Paid from wallet";
    				}
    				array_push($orders_array, $order);
    			}

    			$totalpoint = array_sum($totalcreditpoint) - array_sum($totaldebitpoint);
    			$totalamount = array_sum($totalcreditamount) - array_sum($totaldebitamount);
    			$totalamount = number_format($totalamount, 2); 
    			ResponseMessage::successWallet('Success', $orders_array ,$totalpoint ,$totalamount);
    		}
    		
    	} catch (Exception $e) {
    		Log::error($e);
    	}
	}
}
