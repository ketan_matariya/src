<?php

namespace App\Http\Controllers\RestAPIs\v1\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Categories\Category;
use App\Shop\Products\Product;
use App\Shop\Employees\Employee;
use Log;
use Validator;
use App\Helper\MailHelper;
use App\Helper\ResponseMessage;
use \stdClass;
use App\Shop\Products\Product_attributes;

class FilterController extends Controller
{
    public function filter(Request $request) 
    {
        try {
            $data = array();
            $prod=[];
            $technicals = $request->technicals;
            $companies = $request->companies;
            $solution_id = ($request->solution_id)?$request->solution_id:array();
            if($companies && $technicals) {
                foreach ($technicals as $technical) {
                    $technical = $technical;
                    foreach($companies as $company){
                        $id = $company;
                        $productquery = Product::query();
                        $productquery->join('employees', 'employees.id', 'products.company_id')
                                     ->join('categories', 'categories.id', 'products.subcategory_id')
                                     ->leftjoin('technicals',function($join){
                                        $join->on('technicals.name','=','products.technical_name');
                                    });
                        if($request->get('userdetail')){
                            if($request->get('userdetail')->language=="hi"){
                                $productquery->select('products.id', 'products.name_hi as name', 'employees.name_hi as dealer_name', 'employees.mobile', 'technicals.name_hi as technical_name', 'products.price', 'products.sale_price', 'products.additional_info_hi as additional_info', 'products.cover', 'categories.name_hi as category_name')->where('technicals.name_hi', $technical);
                            } elseif($request->get('userdetail')->language=="gu"){
                                $productquery->select('products.id', 'products.name', 'employees.name as dealer_name', 'employees.mobile', 'technicals.name_gu as technical_name', 'products.price', 'products.sale_price', 'products.additional_info_gu as additional_info', 'products.cover', 'categories.name_gu as category_name')->where('technicals.name_gu', $technical);
                            } else{
                                $productquery->select('products.id', 'products.name', 'employees.name as dealer_name', 'employees.mobile', 'products.technical_name', 'products.price', 'products.sale_price', 'products.additional_info', 'products.cover', 'categories.name as category_name')->where('products.technical_name', $technical);
                            }
                        } else{
                            $productquery->select('products.id', 'products.name', 'employees.name as dealer_name', 'employees.mobile', 'products.technical_name', 'products.price', 'products.sale_price', 'products.additional_info', 'products.cover', 'categories.name as category_name')->where('products.technical_name', $technical);
                        }

                        $technical_prod =  $productquery->where('employees.id', $id)->where('products.status',1)->where('employees.status',1)->where('categories.status',1)->whereIn('products.crop_id',$solution_id)->get();

                        foreach ($technical_prod as $com) {
                            $attr = Product_attributes::where('product_id',$com->id)->first();
                            if($attr){
                                $price= str_replace(",","",$attr->price);
                                $sale_price= str_replace(",","",$attr->sale_price);
                                $com->price = $price;
                                $com->sale_price = $sale_price;
                            }
                            array_push($prod, $com);
                        }
                    }
                }
                for($j=0; $j<count($technicals);$j++) {
                    for($i=0; $i<count($prod);$i++) {
                        if($technicals[$j]==$prod[$i]->technical_name){
                            array_push($data, $prod[$i]);
                        }
                    }  
                }
            } else {
                if($companies) {
                    foreach($companies as $company){
                        $id = $company;

                        $employeequery = Employee::query();
                        $employeequery->join('products', 'products.company_id', 'employees.id')
                                        ->join('categories', 'categories.id', 'products.subcategory_id')
                                        ->leftjoin('technicals',function($join){
                                            $join->on('technicals.name','=','products.technical_name');
                                        });
                        if($request->get('userdetail')){
                            if($request->get('userdetail')->language=="hi"){
                                $employeequery->select('products.id', 'products.name_hi as name', 'employees.name_hi as dealer_name', 'employees.mobile', 'technicals.name_hi as technical_name', 'products.price', 'products.sale_price', 'products.additional_info_hi', 'products.cover', 'categories.name_hi as category_name');
                            } elseif($request->get('userdetail')->language=="gu"){
                                $employeequery->select('products.id', 'products.name_gu as name', 'employees.name_gu as dealer_name', 'employees.mobile', 'technicals.name_gu as technical_name', 'products.price', 'products.sale_price', 'products.additional_info_hi', 'products.cover', 'categories.name_gu as category_name');
                            } else{
                                $employeequery->select('products.id', 'products.name', 'employees.name as dealer_name', 'employees.mobile', 'technicals.name as technical_name', 'products.price', 'products.sale_price', 'products.additional_info_hi', 'products.cover', 'categories.name as category_name');
                            }
                        } else{
                            $employeequery->select('products.id', 'products.name', 'employees.name as dealer_name', 'employees.mobile', 'technicals.name as technical_name', 'products.price', 'products.sale_price', 'products.additional_info_hi', 'products.cover', 'categories.name as category_name');
                        }
                        $company_prod = $employeequery->where('employees.id', $id)
                                        ->where('products.status',1)
                                        ->where('employees.status',1)
                                        ->where('categories.status',1)
                                        ->get();

                        foreach ($company_prod as $com) {
                            $attr = Product_attributes::where('product_id',$com->id)->first();
                            if($attr){
                                $price= str_replace(",","",$attr->price);
                                $sale_price= str_replace(",","",$attr->sale_price);
                                $com->price = $price;
                                $com->sale_price = $sale_price;
                            }
                            array_push($data, $com);
                        }
                    }
                } else {
                    foreach($technicals as $technical){
                        $technical = $technical;

                        $productquery = Product::query();
                        $productquery->join('employees', 'employees.id', 'products.company_id')
                                     ->join('categories', 'categories.id', 'products.subcategory_id')
                                     ->leftjoin('technicals',function($join){
                                        $join->on('technicals.name','=','products.technical_name');
                                    });
                        if($request->get('userdetail')){
                            if($request->get('userdetail')->language=="hi"){
                                $productquery->select('products.id', 'products.name_hi as name', 'employees.name_hi as dealer_name', 'employees.mobile', 'technicals.name_hi as technical_name', 'products.price', 'products.sale_price', 'products.additional_info_hi as additional_info', 'products.cover', 'categories.name_hi as category_name')->where('technicals.name_hi', $technical);
                            }else if($request->get('userdetail')->language=="gu"){
                                $productquery->select('products.id', 'products.name_gu as name', 'employees.name_gu as dealer_name', 'employees.mobile', 'technicals.name_gu as technical_name', 'products.price', 'products.sale_price', 'products.additional_info_hi as additional_info', 'products.cover', 'categories.name_gu as category_name')->where('technicals.name_gu', $technical);
                            }else{
                                $productquery->select('products.id', 'products.name ', 'employees.name as dealer_name', 'employees.mobile', 'technicals.name as technical_name', 'products.price', 'products.sale_price', 'products.additional_info as additional_info', 'products.cover', 'categories.name as category_name')->where('products.technical_name', $technical);
                            }
                        } else{
                            $productquery->select('products.id', 'products.name ', 'employees.name as dealer_name', 'employees.mobile', 'technicals.name as technical_name', 'products.price', 'products.sale_price', 'products.additional_info as additional_info', 'products.cover', 'categories.name as category_name')->where('products.technical_name', $technical);
                        }

                        $technical_prod = $productquery->where('products.status',1)
                                        ->where('employees.status',1)
                                        ->where('categories.status',1)
                                        ->whereIn('crop_id',$solution_id)
                                        ->get();
                        $price=null; 
                        $sale_price =null;             
                        foreach ($technical_prod as $com) {
                            $attr = Product_attributes::where('product_id',$com->id)->first();
                            if($attr){
                                $price= str_replace(",","",$price);
                                $sale_price= str_replace(",","",$sale_price);
                                $com->price = $price;
                                $com->sale_price = $sale_price;
                            }
                            array_push($data, $com);
                        }
                    }
                }
            }
            ResponseMessage::success('Success', $data);
        } catch (Exception $e) {
            log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
        }
    }

    public function search(Request $request){
        try {
            $rules = [
                'value' => 'required',
            ];
            $customeMessage = [
                'value.required' => 'Please enter search value.',
            ];$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            } else {
                $value = trim(strip_tags($request->value));
                $productquery = Product::query();
                $productquery->join('categories', 'categories.id', 'products.subcategory_id')
                                    ->join('employees', 'employees.id', 'products.company_id');
                                    // ->leftjoin('technicals',function($join){
                                    //     $join->on('technicals.name','=','products.technical_name');
                                    // });
                if($request->get('userdetail')){
                    if($request->get('userdetail')->language=="hi"){
                        $productquery->select('products.id', 'products.name_hi as name', 'products.price', 'products.sale_price', 'products.cover', 'employees.name_hi as dealer_name');
                    } elseif($request->get('userdetail')->language=="gu"){
                        $productquery->select('products.id', 'products.name_gu as name', 'products.price', 'products.sale_price', 'products.cover', 'employees.name_gu as dealer_name');
                    } else{
                        $productquery->select('products.id', 'products.name', 'products.price', 'products.sale_price', 'products.cover', 'employees.name as dealer_name');
                    }
                } else{
                    $productquery->select('products.id', 'products.name', 'products.price', 'products.sale_price', 'products.cover', 'employees.name as dealer_name');
                }
                $products = $productquery->where('products.name', 'like','%'.$value.'%')
                                    ->orWhere('products.technical_name', 'like','%'.$value.'%')
                                    ->orWhere('employees.name', 'like','%'.$value.'%')
                                    ->where('employees.status',1)->where('products.status',1)->get()
                                    ;

                if(count($products)>0){
                    foreach($products as $solution) {
                        $attr = Product_attributes::where('product_id',$solution->id)->first();
                        if($attr){
                            $price= str_replace(",","",$attr->price);
                            $sale_price= str_replace(",","",$attr->sale_price);
                            $solution->price = number_format($price,2);
                            $solution->sale_price = $sale_price;
                        }
                    }
                    ResponseMessage::success('Success', $products);
                } else {
                    ResponseMessage::error('Product not found');
                }
            }
        } catch (Exception $e) {
            log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
        }
    }
}
