<?php

namespace App\Http\Controllers\RestAPIs\v1\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Offers\Offer;
use App\Shop\Offers\OfferProduct;
use App\Shop\Products\Product;
use Validator;
use App\Helper\MailHelper;
use App\Helper\ResponseMessage;

class OfferController extends Controller
{
    public function offer(Request $request) 
    {
    	try {
    		$current_date =  date('Y-m-d');
    		date_default_timezone_set('Asia/Kolkata');
    		$current_time = date( 'H:i:s');

            $offerquery = Offer::query();
            if($request->get('userdetail')){
                if($request->get('userdetail')->language=="hi"){
                    $offerquery->select('id', 'offer_hi as offer', 'cover', 'description_hi as description');
                } else if($request->get('userdetail')->language=="gu"){
                    $offerquery->select('id', 'offer_gu as offer', 'cover', 'description_gu      as description');
                } else{
                    $offerquery->select('id', 'offer', 'cover', 'description');
                }
            } else{
                $offerquery->select('id', 'offer', 'cover', 'description');
            }
            $offers = $offerquery->where('expired_date', '>=', $current_date)
                            ->where('status', 1)
                            ->get();
			if(count($offers)==0) {
				$offers=[];
			}
			ResponseMessage::success("Success", $offers);
    	} catch (Exception $e) {
    		log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
    	}
    }
    
    public function getOffer(Request $request) 
    {
        try {
            $rules = [
                'offer_id' => 'required',
            ];
            $customeMessage = [
                'offer_id.required' => 'Please enter offer id.',
            ];
            $validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            } else {
                $offer_id = trim(strip_tags($request->offer_id));
                if(Offer::where('id',$offer_id)->where('status',1)->exists()) {
                    if(OfferProduct::where('offer_id', $offer_id)->exists()) {

                        $offerquery = OfferProduct::query();
                        $offerquery->join('products','products.id','offer_product.product_id')
                                   ->join('product_attribute', 'product_attribute.id','offer_product.attribute_id');
                        if($request->get('userdetail')){
                            if($request->get('userdetail')->language=="hi"){
                                $offerquery->select('products.id','products.name_hi as name','product_attribute.price','product_attribute.sale_price','products.cover');
                            } else if($request->get('userdetail')->language=="gu"){
                                $offerquery->select('products.id','products.name_gu as name','product_attribute.price','product_attribute.sale_price','products.cover');
                            } else{
                                $offerquery->select('products.id','products.name','product_attribute.price','product_attribute.sale_price','products.cover');
                            }
                        } else{
                            $offerquery->select('products.id','products.name','product_attribute.price','product_attribute.sale_price','products.cover');
                        }   
                        $products = $offerquery->where('offer_id', $offer_id)
                                                ->where('products.status',1)
                                                ->get();
                        ResponseMessage::success('Success', $products);
                    } else {
                        ResponseMessage::error('Offer product not found!!');
                    }
                } else {
                    ResponseMessage::error('Offer not found!!');
                }
            }
        } catch (Exception $e) {
            log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
        }
    }
}
