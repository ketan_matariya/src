<?php

namespace App\Http\Controllers\RESTAPIs\v1\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\ResponseMessage;
use App\Shop\Drivers\Driver;
use App\Shop\Orders\Order;
use App\Shop\Payments\DriverPayment;
use App\Shop\OrderProducts\OrderProduct;
use App\Shop\Addresses\OrderAddressDriver;
use App\Helper\MailHelper;
use Validator;
use Log;

class DriverController extends Controller
{
    public function driverProfile(Request $request){
        try {
			$rules = [
    			'driver_id' => 'required',
    		];
    		$customeMessage = [
    			'driver_id.required' => 'Please enter driver id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$driver_id = trim(strip_tags($request->driver_id));
		    	if(Driver::where('id', $driver_id)->exists()) {
		    	    $driver =  Driver::select('name','email','contact','address','licence_no','profile')->where('id', $driver_id)->first();
		    	    ResponseMessage::success('Success', $driver);
		    	}  else {
		    		ResponseMessage::error('Driver not found');
		    	}
	        }
		} catch (Exception $e) {
			log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
		}
    }
    
    public function driverOrderList(Request $request){
		try {
			$rules = [
    			'driver_id' => 'required',
    		];
    		$customeMessage = [
    			'driver_id.required' => 'Please enter driver id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$driver_id = trim(strip_tags($request->driver_id));
		    	if(Driver::where('id', $driver_id)->exists()) {
		    	 //   $orders_query = OrderAddressDriver::join('orders','orders.driver_id','order_address_driver.driver_id')
		    	    $orders_query = Order::join('order_address_driver','order_address_driver.order_id','orders.id')    
		    	                                        ->join('address','address.id','order_address_driver.address_id');                               
		    	    if($request->get('driverdetail')){  
		    	         if($request->get('driverdetail')->language=="hi"){
		    	             $orders_query->Select('orders.id','orders.payment','orders.total','orders.total_shipping','address.address_hi as address','address.landmark_hi as landmark','address.area_hi as area','address.city_hi as city','address.lat','address.long','orders.created_at');
		    	         } elseif($request->get('driverdetail')->language=="gu"){
		    	             $orders_query->Select('orders.id','orders.payment','orders.total','orders.total_shipping','address.address_gu as address','address.landmark_gu as landmark','address.area_gu as area','address.city_gu as city','address.lat','address.long','orders.created_at');
		    	         } else{
		    	            $orders_query->Select('orders.id','orders.payment','orders.total','orders.total_shipping','address.address','address.landmark','address.area','address.city','address.lat','address.long','orders.created_at');
		    	         }
		    	    } else{
		    	         $orders_query->Select('orders.id','orders.payment','orders.total','orders.total_shipping','address.address','address.landmark','address.area','address.city','address.lat','address.long','orders.created_at');
		    	    }
		    	    $orders = $orders_query->where('orders.driver_id',$driver_id)
		    	                            ->where('orders.order_status_id',2)
		    	                            ->orWhere('orders.order_status_id',3)
		    	                            ->whereNotNull('orders.driver_id')
		    	                            ->orderBy('order_address_driver.id','desc')
		    	                            ->get();
		    	    foreach($orders as $order){
		    	        $grand_total = $order->total+$order->total_shipping;
		    	        $order['grand_total']=str_replace(",","",number_format($grand_total,2));
		    	        $query = OrderProduct::join('products','products.id','order_product.product_id');
		    	        if($request->get('driverdetail')){
                            if($request->get('driverdetail')->language=="hi"){
                                $query->Select('products.name_hi as name','order_product.quantity','order_product.product_price');
                            } elseif($request->get('driverdetail')->language=="gu"){
                                $query->Select('products.name_gu as name','order_product.quantity','order_product.product_price');
                            } else{
                                $query->Select('products.name','order_product.quantity','order_product.product_price');
                            }
                        } else{
                            $query->Select('products.name','order_product.quantity','order_product.product_price');
                        }
		    	         //  $product = OrderProduct::Select('product_name','quantity','product_price')->where('order_id',$order->id)->get();
	    	            $product = $query->where('order_id',$order->id)->get();
	    	            $order['product'] = $product;
		    	    }
		    		ResponseMessage::success('Success', $orders);
		    	} else {
		    		ResponseMessage::error('Driver not found');
		    	}
	        }
		} catch (Exception $e) {
			log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
		}
	}
	
	public function completeOrder(Request $request){
	    try{
	        $rules = [
    			'order_id' => 'required',
    		];
    		$customeMessage = [
    			'order_id.required' => 'Please enter order id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$order_id = trim(strip_tags($request->order_id));
		    	if(Order::where('id', $order_id)->exists()) {
		    	    $order = Order::find($order_id);
		    	    $order->order_status_id = 1;
		    	    $order->save(); 
		    	    $order_addr_driver = OrderAddressDriver::where('order_id',$order_id)->first();
		    	    date_default_timezone_set('Asia/Kolkata');
                    $order_addr_driver->order_complete_date = date("Y-m-d H:i:s");
                    $order_addr_driver->save();
		    	    ResponseMessage::success('Success', "Order deliver successfully");
		    	} else {
		    	    ResponseMessage::error("Order not found");
		    	}
	        }
	        
	    } catch (Exception $e) {
			log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
		}
	}
    
    public function langChange(Request $request){
        try {
            $rules = [
                'driver_id' => 'required',
                'lang' => 'required',
            ];
            $customeMessage = [
                'driver_id.required' => 'Please enter driver id',
                'lang.required' => 'Please enter language',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()) {
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            } else {
                if(Driver::find($request->driver_id)->exists()){
                    $driver = Driver::find($request->driver_id);
                    $driver->language = $request->lang;
                    if($driver->save()){
                        ResponseMessage::success("Success");
                    }else{
                        ResponseMessage::error("Something went wrong!!");
                    }
                } else {
                    ResponseMessage::error("Driver not found");
                }
            }
        } catch (Exception $e) {
            log:: error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
        }
    }
    
    public function driverCompleteOrderList(Request $request){
        try {
			$rules = [
    			'driver_id' => 'required',
    		];
    		$customeMessage = [
    			'driver_id.required' => 'Please enter driver id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$driver_id = trim(strip_tags($request->driver_id));
		    	if(Driver::where('id', $driver_id)->exists()) {
		    	    $orders = OrderAddressDriver::join('orders','orders.id','order_address_driver.order_id')
		    	                                ->join('address','address.id','order_address_driver.address_id')
		    	                                ->Select('orders.id','orders.payment','orders.total','orders.total_shipping','order_address_driver.order_complete_date')
		    	                                ->where('order_address_driver.driver_id',$driver_id)
		    	                                ->where('orders.order_status_id',1)
		    	                                ->orderBy('order_address_driver.id','desc')
		    	                                ->get();
		    	    foreach($orders as $order){
		    	        $grand_total = $order->total+$order->total_shipping;
		    	        $order['grand_total']=str_replace(",","",number_format($grand_total,2));
		    	        
		    	    }
		    		ResponseMessage::success('Success', $orders);
		    	} else {
		    		ResponseMessage::error('Driver not found');
		    	}
	        }
		} catch (Exception $e) {
			log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
		}
    }
		
	public function driverWeeklyOrder(Request $request){
	    try{
	        $rules = [
    			'driver_id' => 'required',
    		];
    		$customeMessage = [
    			'driver_id.required' => 'Please enter driver id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$driver_id = trim(strip_tags($request->driver_id));
		    	if(Driver::where('id', $driver_id)->exists()) {
		    	    date_default_timezone_set('Asia/Kolkata');
                    $current_date = date("Y-m-d");
                    $today_date = date("Y-m-d");
                    $yesterday_date = date('Y-m-d',strtotime('-1 day',strtotime($current_date)));
                    $startdate = date("Y-m-d", strtotime("-1 week"));
                    $startdate = date('Y-m-d',strtotime('+1 day',strtotime($startdate)));
                    $data=[];
                    $order['today']=[];
                    $order['yesterday']=[];
                    $order['record']=[];
                    for($i=0;$i<7;$i++){
                        if(isset($current_date) && strtotime($current_date) >= strtotime($startdate)) {
                            $total=0;
                            $COD=0;
                            $net_banking=0;
                            $orders = OrderAddressDriver::join('orders','orders.id','order_address_driver.order_id')
                                                        ->select('orders.id','orders.payment','orders.total','orders.total_shipping')
                                                        ->where('order_complete_date', 'like', $current_date . "%")
                                                        ->where('orders.driver_id',$driver_id)
                                                        ->get();
                            foreach($orders as $order_detail){
                                $total=$total+$order_detail->total+$order_detail->total_shipping;
                                if($order_detail->payment=="COD"){
                                    $COD=$COD+$order_detail->total+$order_detail->total_shipping;
                                }else{
                                    $net_banking=$net_banking+$order_detail->total+$order_detail->total_shipping;
                                }
                            }
                            $total = str_replace(",","",number_format($total,2));
                            $COD = str_replace(",","",number_format($COD,2));
                            $net_banking = str_replace(",","",number_format($net_banking,2));
                            $today['date']=$current_date; 
                            $today['total']=$total;
                            $today['COD']= $COD;
                            $today['Net_payment']=$net_banking;
                            $today['Order']=$orders;
                            if(strtotime($current_date) == strtotime($today_date)){
                                array_push($order['today'],$today);
                            }else if(strtotime($current_date) == strtotime($yesterday_date)){
                                array_push($order['yesterday'],$today);
                            }else{
                                array_push($order['record'],$today);
                            }
                        }
                        $current_date = date('Y-m-d',strtotime('-1 day',strtotime($current_date)));
                    }
                    ResponseMessage::success("Success",$order);
		    	} else {
		    		ResponseMessage::error('Driver not found');
		    	}
	        }
	    } catch (Exception $e) {
    		log::error($e);
    		$data['error']=$e;
    		$error_mail = MailHelper::errorMail($data);
    	}
	}
	
	public function todayOrder(Request $request){
	    try{
	        $rules = [
    			'driver_id' => 'required',
    		];
    		$customeMessage = [
    			'driver_id.required' => 'Please enter driver id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$driver_id = trim(strip_tags($request->driver_id));
		    	if(Driver::where('id', $driver_id)->exists()) {
		    	    date_default_timezone_set('Asia/Kolkata');
                    $current_date = date("Y-m-d");
                    if(isset($current_date)) {
                        $total=0;
                        $COD=0;
                        $net_banking=0;
                        $orders = OrderAddressDriver::join('orders','orders.id','order_address_driver.order_id')
		    	                                ->join('address','address.id','order_address_driver.address_id')
		    	                                ->Select('orders.id','orders.payment','orders.total','orders.total_shipping','order_address_driver.order_complete_date')
		    	                                ->where('order_address_driver.driver_id',$driver_id)
		    	                                ->where('orders.order_status_id',1)
		    	                                ->where('order_complete_date', 'like', $current_date . "%")
		    	                                ->orderBy('order_address_driver.id','desc')
		    	                                ->get();
                        // $orders = OrderAddressDriver::join('orders','orders.id','order_address_driver.order_id')
                        //                             ->select('orders.id','orders.payment','orders.total','orders.total_shipping')
                        //                             ->where('order_complete_date', 'like', $current_date . "%")
                        //                             ->get();
                        foreach($orders as $order_detail){
                            $total=$total+$order_detail->total+$order_detail->total_shipping;
                            if($order_detail->payment=="COD"){
                                $COD=$COD+$order_detail->total+$order_detail->total_shipping;
                            }else{
                                $net_banking=$net_banking+$order_detail->total+$order_detail->total_shipping;
                            }
                        }
                        $total = str_replace(",","",number_format($total,2));
                        $COD = str_replace(",","",number_format($COD,2));
                        $net_banking = str_replace(",","",number_format($net_banking,2));
                        $order['date']=$current_date; 
                        $order['total']=$total;
                        $order['COD']= $COD;
                        $order['Net_payment']=$net_banking;
                        $order['Order']=$orders;
                    }
                    ResponseMessage::success("Success",$order);
		    	} else {
		    		ResponseMessage::error('Driver not found');
		    	}
	        }
	    } catch (Exception $e) {
    		log::error($e);
    		$data['error']=$e;
    		$error_mail = MailHelper::errorMail($data);
    	}
	}
	
	public function driverCODPayment(Request $request){
	    try{
	        $rules = [
    			'driver_id' => 'required',
    		];
    		$customeMessage = [
    			'driver_id.required' => 'Please enter driver id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$driver_id = trim(strip_tags($request->driver_id));
		    	if(Driver::where('id', $driver_id)->exists()) {
		    	    $payment = DriverPayment::select('id','payment','created_at')->where('driver_id', $driver_id)->orderBy('created_at','desc')->get();
		    	    ResponseMessage::success("Success",$payment);
		    	}else {
		    	    ResponseMessage::error("Driver not found!!");
		    	}
	        }
	    } catch (Exception $e) {
    		log::error($e);
    		$data['error']=$e;
    		$error_mail = MailHelper::errorMail($data);
    	}
	}
	public function login_logout(Request $request) {
        try {
            $rules = [
                // 'token' => 'required',
                'driver_id' => 'required',
            ];
            $customeMessage = [
                // 'token.required' => 'Please enter status',
                'driver_id.required' => 'Please enter driver id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()) {
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            } else {
                $token = trim(strip_tags($request->token));
                $driver_id = trim(strip_tags($request->driver_id));
                if(Driver::find($driver_id)->exists()){
                    $driver = Driver::find($driver_id);
                    if($token) {
                        $driver->token = $token;
                    } else {
                        $driver->token = null;
                    }
                    if($request->lang){
                        $driver->language = $request->lang;
                    }
                    $driver->save();
                    ResponseMessage::success("Success");
                } else {
                    ResponseMessage::error("Customer not found");
                }
            }
        } catch (Exception $e) {
            log:: error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
        }
    }
	
}
