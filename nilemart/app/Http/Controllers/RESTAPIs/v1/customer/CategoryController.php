<?php

namespace App\Http\Controllers\RESTAPIs\v1\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Log;
use Validator;
use App\Helper\ResponseMessage;
use App\Shop\Categories\Category;
use App\Shop\Products\Product;
use App\Shop\Employees\Employee;
use App\Shop\Priorities\Priority;
use \stdClass;
use App\Helper\MailHelper;
use App\Shop\Carts\Cart;
use App\Shop\Priorities\TrendingProduct;
use App\Shop\Products\Product_attributes;
use App\Shop\TechnicalCategories\Technical;

class CategoryController extends Controller
{
	public function productCategories(Request $request) {
		try {
			$data = array();
			$comp = array();
			$cate = array();
			$sub_cat = array();
			if(Category::where('parent_id', null)->exists()) {

				$categories = Category::Select('id', 'name', 'name_hi', 'name_gu', 'cover')->where('parent_id', null)->where('status',1)->get();
				foreach ($categories as $category) {
					$cate_add = new stdClass();
					$cate_add->id=$category->id;
					if($request->get('userdetail')){
                       	if($request->get('userdetail')->language=="hi"){
                       		$cate_add->name=$category->name_hi;
                       	} else if($request->get('userdetail')->language=="gu"){
                       		$cate_add->name=$category->name_gu;
                       	} else{
                       		$cate_add->name=$category->name;
                       	}
                   	} else{
						$cate_add->name=$category->name;
                   	}
					$cate_add->cover=$category->cover;
					$cate_add->price=null;
					$cate_add->sale_price=null;
					$cate_add->company_name=null;
					array_push($cate, $cate_add);
				}
				$companies = Employee::Select('id', 'name','name_hi','name_gu', 'logo as cover')->where('status',1)->get();
				foreach ($companies as $company) {
					$company_add = new stdClass();
					$company_add->id=$company->id;
					if($request->get('userdetail')){
                       	if($request->get('userdetail')->language=="hi"){
                       		$company_add->name=$company->name_hi;
                       	} else if($request->get('userdetail')->language=="gu"){
                       		$company_add->name=$company->name_gu;
                       	} else{
                       		$company_add->name=$company->name;
                       	}
                   	} else{
						$company_add->name=$company->name;
                   	}
					$company_add->cover=$company->cover;
					$company_add->price=null;
					$company_add->sale_price=null;
					$company_add->company_name=null;
					array_push($comp, $company_add);
				}

				$category = new stdClass();
				$category->type = 'Product Categories';
				$category->data = $cate;
				$priority = Priority::where('name', $category->type)->first();
				$category->priority = $priority->priority;
				array_push($data, $category);

				// $company = new stdClass();
				// $company->type = 'Company List';
				// $company->data = $comp;
				// $priority = Priority::where('name', $company->type)->first();
				// $company->priority = $priority->priority;
				// array_push($data, $company);

				// $product = new stdClass();
				// $product->type = 'Trending Products';
				// $priority = Priority::where('name', $product->type)->first();
				// $subcategories = TrendingProduct::select('priority','product_id')->get();
				// foreach ($subcategories as $subcategory) {
				// 	$productquery = Product::query();
				// 	$productquery->join('employees', 'employees.id', 'products.company_id')
				// 				->join('product_attribute','product_attribute.product_id','products.id');
				// 	if($request->get('userdetail')){
    //                     if($request->get('userdetail')->language=="hi"){
    //                     	$productquery->Select('products.id', 'products.name_hi as name', 'products.cover', 'product_attribute.price', 'product_attribute.sale_price', 'employees.name_hi as company_name');
    //                     } elseif($request->get('userdetail')->language=="gu"){
    //                     	$productquery->Select('products.id', 'products.name_gu as name', 'products.cover', 'product_attribute.price', 'product_attribute.sale_price', 'employees.name_gu as company_name');
    //                     } else{
    //                     	$productquery->Select('products.id', 'products.name', 'products.cover', 'product_attribute.price', 'product_attribute.sale_price', 'employees.name as company_name');
    //                     }
    //                 } else{
    //                 	$productquery->Select('products.id', 'products.name', 'products.cover', 'product_attribute.price', 'product_attribute.sale_price', 'employees.name as company_name');
    //                 }
				// 	$sub_category_data = $productquery->where('products.status',1)
				// 					->where('employees.status',1)
				// 					->where('products.id', $subcategory->product_id)
				// 					->first();
									
				// 	if($sub_category_data){
				//         $sub_category_data->price = str_replace(",","",$sub_category_data->price);
				//         $sub_category_data->sale_price = str_replace(",","",$sub_category_data->sale_price);
				// 		array_push($sub_cat, $sub_category_data);
				// 	}
				// }
				// $product->data = $sub_cat;
				// $product->priority = $priority->priority;
				// array_push($data, $product);
				ResponseMessage::success('Success', $data);
			} else {
				ResponseMessage::error('Category not found.');
			}
		} catch (Exception $e) {
			log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
		}
	}

/*	public function selectedCategory(Request $request) {
		try {
			$rules = [
    			'id' => 'required',
    		];
    		$customeMessage = [
    			'id.required' => 'Please enter sub-category id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$id = trim(strip_tags($request->id));
	        	$data=[];
				if(Category::where('id', $id)->where('status',1)->exists()) {
	        		$category = array();
	                $about_product = array();
	                $i=1;
	                $technicals = Technical::get();
	                if(Product::where('subcategory_id', $id)->orwhere('category_id',$id)->where('status',1)->exists()){
	                    $technicalquery = Technical::query();
	                    $productquery = Product::query();
	                    $productquery->join('categories',function($query){
				                    	$query->on('categories.id','=','products.category_id')->where('subcategory_id',null);
				                    	$query->oron('categories.id','=','products.subcategory_id')->where('subcategory_id','!=',null);
				                    })
	                                ->join('employees', 'employees.id', 'products.company_id')
	                                ->leftjoin('technicals',function($join){
	                                    $join->on('technicals.name','=','products.technical_name');
	                                });
	                        if($request->get('userdetail')){
	                            if($request->get('userdetail')->language=="hi"){
	                                $technicalquery->select('*','name_hi as name');
	                                $productquery->select('products.id', 'products.name_hi as name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info_hi as additional_info', 'employees.name_hi as dealer_name', 'employees.mobile', 'categories.name_hi as category_name','technicals.name_hi as technical_name');
	                            } elseif($request->get('userdetail')->language=="gu"){
	                                $technicalquery->select('*','name_gu as name');
	                                $productquery->select('products.id', 'products.name_gu as name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info_gu as additional_info', 'employees.name_gu as dealer_name', 'employees.mobile', 'categories.name_gu as category_name','technicals.name_gu as technical_name');
	                            } else{
	                                $productquery->select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name');
	                            }
	                        } else{
	                            $productquery->select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name');
	                        }
	                    $solutions = $productquery->where('products.subcategory_id', $id)
	                                ->orwhere('products.category_id', $id)
	                                ->where('products.status',1)->where('employees.status',1)->where('categories.status',1)->get();
	                    $technicals = $technicalquery->get();
                        $technical = array();
                        $testarr=array();
                          $user_quantity=0;
                        if(count($solutions)>0) {
                            foreach($solutions as $solution) {
                            	$attr = Product_attributes::where('product_id',$solution->id)->first();
                            	 $attrnew= Product_attributes::where('product_id',$solution->id)->get();
    	                        	foreach($attrnew as $key=>$new){
    	                        	 	 if(Cart::where('customer_id', $request->user_id)->where('product_id',$solution->id)->count()>0){
    	                        	           $cartcnt=Cart::where('customer_id',  $request->user_id)
                                                ->where('product_id', $solution->id)->count();
                                                
                                                 $carts = Cart::where('customer_id',  $request->user_id)
                                                ->where('product_id', $solution->id)
                                                ->get();
                                                foreach ($carts as $newk=>$cart) {
                                                    
                                                   if($attr->id == $cart->attribute_id){
                                                        $user_quantity = $cart->quantity;
                                                        
                                                    } else {
                                                        //$user_quantity = 0;
                                                      
                                                    }
                                                   
                                                  //  echo "<br>===>>>>".$user_quantity;	    
                                                }
                                              
                                            } else {
                                                $user_quantity = 0;
                                            }
                                            $testarr[$key]=['id'=>$new->id,'product_id'=>$new->product_id,'key'=>$new->key,'value'=>$new->value,'price'=>str_replace(",","",$new->price),'sale_price'=>str_replace(",","",$new->sale_price),'quantity'=>$new->quantity,'available_quantity'=>$new->available_quantity,'max_quantities'=>$new->max_quantities,'user_quantity'=>$user_quantity];
    	                        	 }
    	                        	 	  $solution->unit=$testarr;
                                if($attr){
                                    $price= str_replace(",","",$attr->price);
                                    $sale_price= str_replace(",","",$attr->sale_price);
                                    $solution->price = $price;
                                    $solution->sale_price = $sale_price;
                                }
                                if($solution->additional_info) {
                                    $decode = json_decode($solution->additional_info);
                                    $solution->additional_info = $decode;
                                }else {
                                    $solution->additional_info = [];
                                }
                                // array_push($technical, $solution);
                            }
    		                for($j=0; $j<count($technicals);$j++) {
    		                    for($i=0; $i<count($solutions);$i++) {
    		                        if($technicals[$j]->name==$solutions[$i]->technical_name){
    		                            array_push($technical, $solutions[$i]);
    		                        }
    		                    }  
    		                }
	                        $data = $technical;
                        }
		        		ResponseMessage::success('Success', $data);
		        	}else {
						ResponseMessage::error('Product not found!!');
					}
				} else {
					ResponseMessage::error('Category id not found!!');
				}
	        }
		} catch (Exception $e) {
			log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
		}
	}
*/
public function selectedCategory(Request $request) {
		try {
			$rules = [
    			'id' => 'required',
    		];
    		$customeMessage = [
    			'id.required' => 'Please enter sub-category id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	            	$id = trim(strip_tags($request->id));
			if(Category::where('id', $id)->exists()) {
					$categoryquery = Category::query();
					if($request->get('userdetail'))
					{
                    	if($request->get('userdetail')->language=="hi"){
                    		$categoryquery->select('id','name_hi as name','cover');
                    	}else if($request->get('userdetail')->language=="gu"){
                    		$categoryquery->select('id','name_gu as name','cover');
                    	} else{
                    		$categoryquery->select('id','name','cover');
                    	}
                    } else{
                    	$categoryquery->select('id','name','cover');
                    }
		        	$sub_categories = $categoryquery->where('parent_id', $id)->where('status',1)->get();

		        	$extra=[];
                    $data=[];
                    $extra['subcategory_status']=0;
		        	if(count($sub_categories)>0){
		        		$extra['subcategory_status']=0;
		        		$data=$sub_categories;
		        	} else {
		        		$category = array();
		                $about_product = array();
		                $i=1;
                    	// $technicals = Technical::get();
		                if(Product::where('subcategory_id', $id)->where('status',1)->exists()){
		             //if(Product::where('category_id', $id)->where('status',1)->exists()){
		        			$extra['subcategory_status']=1;
		        			$technicalquery = Technical::query();
		        			$productquery = Product::query();
		                    $productquery->join('categories', 'categories.id', 'products.category_id')
	                                ->join('employees', 'employees.id', 'products.company_id')
	                                ->leftjoin('technicals',function($join){
	                                    $join->on('technicals.name','=','products.technical_name');
	                                });
	                        if($request->get('userdetail')){
	                            if($request->get('userdetail')->language=="hi"){
	                                $technicalquery->select('*','name_hi as name');
	                                $productquery->select('products.id', 'products.name_hi as name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info_hi as additional_info', 'employees.name_hi as dealer_name', 'employees.mobile', 'categories.name_hi as category_name','technicals.name_hi as technical_name');
	                            } elseif($request->get('userdetail')->language=="gu"){
	                                $technicalquery->select('*','name_gu as name');
	                                $productquery->select('products.id', 'products.name_gu as name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info_gu as additional_info', 'employees.name_gu as dealer_name', 'employees.mobile', 'categories.name_gu as category_name','technicals.name_gu as technical_name');
	                            } else{
	                                $productquery->select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name');
	                            }
	                        } else{
	                            $productquery->select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name');
	                        }
	                        if(	$extra['subcategory_status']==1){
	                             $solutions = $productquery->where('products.subcategory_id', $id)->where('products.status',1)->get();
	                        }
	                        else{
	                             $solutions = $productquery->where('products.category_id', $id)->where('products.status',1)->get();
	                        }
	                   
	                    $technicals = $technicalquery->get();
						
	                        $technical = array();
	                         $testarr=array();
	                           $user_quantity=0;
	                        if(count($solutions)>0) {
    	                        foreach($solutions as $solution) {
    	                        	$attr = Product_attributes::where('product_id',$solution->id)->first();
    	                        	 $attrnew= Product_attributes::where('product_id',$solution->id)->get();
    	                        	foreach($attrnew as $key=>$new){
    	                        	 	 if(Cart::where('customer_id', $request->user_id)->where('product_id',$solution->id)->count()>0){
    	                        	           $cartcnt=Cart::where('customer_id',  $request->user_id)
                                                ->where('product_id', $solution->id)->count();
                                                
                                                 $carts = Cart::where('customer_id',  $request->user_id)
                                                ->where('product_id', $solution->id)
                                                ->get();
                                                foreach ($carts as $newk=>$cart) {
                                                    
                                                   if($attr->id == $cart->attribute_id){
                                                        $user_quantity = $cart->quantity;
                                                        
                                                    } else {
                                                        //$user_quantity = 0;
                                                      
                                                    }
                                                   
                                                  //  echo "<br>===>>>>".$user_quantity;	    
                                                }
                                              
                                            } else {
                                                $user_quantity = 0;
                                            }
                                            if($new->product_id==$solution->id)
                              {
                                 $getcnt=Cart::where('customer_id',  $request->user_id)
                                              ->where('product_id', $solution->id)
                                              ->where('attribute_id',$new->id)->count();
                                      if($getcnt>0)
                                      {
                                             $getcarts = Cart::where('customer_id',  $request->user_id)
                                      ->where('product_id', $solution->id)
                                      ->where('attribute_id',$new->id)
                                        ->first()->quantity;
                                      }
                                      else{
                                          $getcarts=0;
                                      }
                                  $testarr[$solution->id][]=['id'=>$new->id,'product_id'=>$new->product_id,'key'=>$new->key,'value'=>$new->value,'price'=>str_replace(",","",$new->price),'sale_price'=>str_replace(",","",$new->sale_price),'quantity'=>$new->quantity,'available_quantity'=>$new->available_quantity,'max_quantities'=>$new->max_quantities,'user_quantity'=>$getcarts];
                            
                                  
                              }
                                           // $testarr[$key]=['id'=>$new->id,'product_id'=>$new->product_id,'key'=>$new->key,'value'=>$new->value,'price'=>str_replace(",","",$new->price),'sale_price'=>str_replace(",","",$new->sale_price),'quantity'=>$new->quantity,'available_quantity'=>$new->available_quantity,'max_quantities'=>$new->max_quantities,'user_quantity'=>$user_quantity];
    	                        	 	}
    	                        	 	 $solution->unit = $testarr[$solution->id];
    	                            if($attr){
                                        $price= str_replace(",","",$attr->price);
                                        $sale_price= str_replace(",","",$attr->sale_price);
    	                                $solution->price = $price;
    	                                $solution->sale_price = $sale_price;
    	                            }
    	                            if($solution->additional_info) {
    	                                $decode = json_decode($solution->additional_info);
    	                                $solution->additional_info = $decode;
    	                            }else {
    	                                $solution->additional_info = [];
    	                            }
    	                        }
    			                for($j=0; $j<count($technicals);$j++) {
    			                    for($i=0; $i<count($solutions);$i++) {
    			                        if($technicals[$j]->name==$solutions[$i]->technical_name){
    			                            array_push($technical, $solutions[$i]);
    			                        }
    			                    }  
    			                }
    			                $data = $technical;
	                        }
			        	}
		        	}
		      //  	print_r($data);
		      //  	exit;
		        	ResponseMessage::successor('Success', $data, $extra);
		        //	ResponseMessage::success('Success', $data);
				} else {
					ResponseMessage::error('Category id not found!!');
				}
			}
	        /*	$id = trim(strip_tags($request->id));
	        	$data=[];
				if(Category::where('id', $id)->where('status',1)->exists()) {
	        		$category = array();
					$about_product = array();
					$extra=array();
	                $i=1;
	                $technicals = Technical::get();
	                	$extra=[];
                    $data=[];
	                if(Product::where('subcategory_id', $id)->orwhere('category_id',$id)->where('status',1)->exists()){
	                    $technicalquery = Technical::query();
	                    $productquery = Product::query();
	                    $productquery->join('categories',function($query){
				                    	$query->on('categories.id','=','products.category_id')->where('subcategory_id',null);
				                    	$query->oron('categories.id','=','products.subcategory_id')->where('subcategory_id','!=',null);
				                    })
	                                ->join('employees', 'employees.id', 'products.company_id')
	                                ->leftjoin('technicals',function($join){
	                                    $join->on('technicals.name','=','products.technical_name');
	                                });
	                                
	                        if($request->get('userdetail')){
	                            if($request->get('userdetail')->language=="hi"){
	                                $technicalquery->select('*','name_hi as name');
	                                $productquery->select('products.id', 'products.name_hi as name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info_hi as additional_info', 'employees.name_hi as dealer_name', 'employees.mobile', 'categories.name_hi as category_name','technicals.name_hi as technical_name');
	                            } elseif($request->get('userdetail')->language=="gu"){
	                                $technicalquery->select('*','name_gu as name');
	                                $productquery->select('products.id', 'products.name_gu as name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info_gu as additional_info', 'employees.name_gu as dealer_name', 'employees.mobile', 'categories.name_gu as category_name','technicals.name_gu as technical_name');
	                            } else{
	                                $productquery->select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name');
	                            }
	                        } else{
	                            $productquery->select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name');
							}
							
							if(Category::where('id', $id)->exists()) {
									$categoryquery = Category::query();
									if($request->get('userdetail'))
									{
										if($request->get('userdetail')->language=="hi"){
											$categoryquery->select('id','name_hi as name','cover');
										}else if($request->get('userdetail')->language=="gu"){
											$categoryquery->select('id','name_gu as name','cover');
										} else{
											$categoryquery->select('id','name','cover');
										}
									} else{
										$categoryquery->select('id','name','cover');
									}
								$sub_categories = Category::where('parent_id', $id)->where('status',1)->get();
								// print_r($sub_categories);
								// exit;
							}
							
							$extra['subcategory_status']=0;
							if(count($sub_categories)>0){
								$extra['subcategory_status']=0;
								$data=$sub_categories;
							} else {
								$extra['subcategory_status']=1;

							}
	                    $solutions = $productquery->where('products.subcategory_id', $id)
	                                ->orwhere('products.category_id', $id)
	                                ->where('products.status',1)->where('employees.status',1)->where('categories.status',1)->get();
	                    $technicals = $technicalquery->get();
                        $technical = array();
                        if(count($solutions)>0) {
                            foreach($solutions as $solution) {
                            	$attr = Product_attributes::where('product_id',$solution->id)->first();
                            	 $attrnew= Product_attributes::where('product_id',$solution->id)->get();
                            	 foreach($attrnew as $key=>$new){
                            	      if(Cart::where('customer_id', $request->user_id)->where('product_id',$solution->id)->count()>0){
    	                        	           $cartcnt=Cart::where('customer_id',  $request->user_id)
                                                ->where('product_id', $solution->id)->count();
                                                
                                                 $carts = Cart::where('customer_id',  $request->user_id)
                                                ->where('product_id', $solution->id)
                                                ->get();
                                                foreach ($carts as $newk=>$cart) {
                                                    
                                                   if($attr->id == $cart->attribute_id){
                                                        $user_quantity = $cart->quantity;
                                                        
                                                    } else {
                                                        //$user_quantity = 0;
                                                      
                                                    }
                                                   
                                                  //  echo "<br>===>>>>".$user_quantity;	    
                                                }
                                              
                                            } else {
                                                $user_quantity = 0;
                                            }
                                            $testarr[$key]=['id'=>$new->id,'product_id'=>$new->product_id,'key'=>$new->key,'value'=>$new->value,'price'=>str_replace(",","",$new->price),'sale_price'=>str_replace(",","",$new->sale_price),'quantity'=>$new->quantity,'available_quantity'=>$new->available_quantity,'max_quantities'=>$new->max_quantities,'user_quantity'=>$user_quantity];
                            	 }
                            	  $solution->unit=$testarr;
                                if($attr){
                                    $price= str_replace(",","",$attr->price);
                                    $sale_price= str_replace(",","",$attr->sale_price);
                                    $solution->price = $price;
                                    $solution->sale_price = $sale_price;
                                }
                                if($solution->additional_info) {
                                    $decode = json_decode($solution->additional_info);
                                    $solution->additional_info = $decode;
                                }else {
                                    $solution->additional_info = [];
                                }
                                // array_push($technical, $solution);
                            }
    		                for($j=0; $j<count($technicals);$j++) {
    		                    for($i=0; $i<count($solutions);$i++) {
    		                        if($technicals[$j]->name==$solutions[$i]->technical_name){
    		                            array_push($technical, $solutions[$i]);
    		                        }
    		                    }  
    		                }
	                        $data = $technical;
                        }
                        
                        // print_r($extra);
                        // exit;
		        		ResponseMessage::successor('Success', $data,$extra);
		        	}else {
						ResponseMessage::error('Product not found!!');
					}
				} else {
					ResponseMessage::error('Category id not found!!');
				}*/
	        //}
		} catch (Exception $e) {
			log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
		}
	}
	public function get_subCategory(Request $request) {
		try {
			$rules = [
    			'id' => 'required',
    		];
    		$customeMessage = [
    			'id.required' => 'Please enter category id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$id = trim(strip_tags($request->id));
				if(Category::where('id', $id)->exists()) {
					$categoryquery = Category::query();
					if($request->get('userdetail'))
					{
                    	if($request->get('userdetail')->language=="hi"){
                    		$categoryquery->select('id','name_hi as name','cover');
                    	}else if($request->get('userdetail')->language=="gu"){
                    		$categoryquery->select('id','name_gu as name','cover');
                    	} else{
                    		$categoryquery->select('id','name','cover');
                    	}
                    } else{
                    	$categoryquery->select('id','name','cover');
                    }
		        	$sub_categories = $categoryquery->where('parent_id', $id)->where('status',1)->get();

		        	$extra=[];
                    $data=[];
                    $extra['subcategory_status']=0;
		        	if(count($sub_categories)>0){
		        		$extra['subcategory_status']=0;
		        		$data=$sub_categories;
		        	} else {
		        		$category = array();
		                $about_product = array();
		                $i=1;
                    	// $technicals = Technical::get();
		                if(Product::where('category_id', $id)->where('status',1)->exists()){
		        			$extra['subcategory_status']=1;
		        			$technicalquery = Technical::query();
		        			$productquery = Product::query();
		                    $productquery->join('categories', 'categories.id', 'products.category_id')
	                                ->join('employees', 'employees.id', 'products.company_id')
	                                ->leftjoin('technicals',function($join){
	                                    $join->on('technicals.name','=','products.technical_name');
	                                });
	                        if($request->get('userdetail')){
	                            if($request->get('userdetail')->language=="hi"){
	                                $technicalquery->select('*','name_hi as name');
	                                $productquery->select('products.id', 'products.name_hi as name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info_hi as additional_info', 'employees.name_hi as dealer_name', 'employees.mobile', 'categories.name_hi as category_name','technicals.name_hi as technical_name');
	                            } elseif($request->get('userdetail')->language=="gu"){
	                                $technicalquery->select('*','name_gu as name');
	                                $productquery->select('products.id', 'products.name_gu as name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info_gu as additional_info', 'employees.name_gu as dealer_name', 'employees.mobile', 'categories.name_gu as category_name','technicals.name_gu as technical_name');
	                            } else{
	                                $productquery->select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name');
	                            }
	                        } else{
	                            $productquery->select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name');
	                        }
	                    $solutions = $productquery->where('products.category_id', $id)->where('products.status',1)->get();
	                    $technicals = $technicalquery->get();
						
	                        $technical = array();
	                         $testarr=array();
	                           $user_quantity=0;
	                        if(count($solutions)>0) {
    	                        foreach($solutions as $solution) {
    	                        	$attr = Product_attributes::where('product_id',$solution->id)->first();
    	                        	 $attrnew= Product_attributes::where('product_id',$solution->id)->get();
    	                        	foreach($attrnew as $key=>$new){
    	                        	 	 if(Cart::where('customer_id', $request->user_id)->where('product_id',$solution->id)->count()>0){
    	                        	           $cartcnt=Cart::where('customer_id',  $request->user_id)
                                                ->where('product_id', $solution->id)->count();
                                                
                                                 $carts = Cart::where('customer_id',  $request->user_id)
                                                ->where('product_id', $solution->id)
                                                ->get();
                                                foreach ($carts as $newk=>$cart) {
                                                    
                                                   if($attr->id == $cart->attribute_id){
                                                        $user_quantity = $cart->quantity;
                                                        
                                                    } else {
                                                        //$user_quantity = 0;
                                                      
                                                    }
                                                   
                                                  //  echo "<br>===>>>>".$user_quantity;	    
                                                }
                                              
                                            } else {
                                                $user_quantity = 0;
                                            }
                                            $testarr[$key]=['id'=>$new->id,'product_id'=>$new->product_id,'key'=>$new->key,'value'=>$new->value,'price'=>str_replace(",","",$new->price),'sale_price'=>str_replace(",","",$new->sale_price),'quantity'=>$new->quantity,'available_quantity'=>$new->available_quantity,'max_quantities'=>$new->max_quantities,'user_quantity'=>$user_quantity];
    	                        	 	}
    	                        	 	  $solution->unit=$testarr;
    	                            if($attr){
                                        $price= str_replace(",","",$attr->price);
                                        $sale_price= str_replace(",","",$attr->sale_price);
    	                                $solution->price = $price;
    	                                $solution->sale_price = $sale_price;
    	                            }
    	                            if($solution->additional_info) {
    	                                $decode = json_decode($solution->additional_info);
    	                                $solution->additional_info = $decode;
    	                            }else {
    	                                $solution->additional_info = [];
    	                            }
    	                        }
    			                for($j=0; $j<count($technicals);$j++) {
    			                    for($i=0; $i<count($solutions);$i++) {
    			                        if($technicals[$j]->name==$solutions[$i]->technical_name){
    			                            array_push($technical, $solutions[$i]);
    			                        }
    			                    }  
    			                }
    			                $data = $technical;
	                        }
			        	}
		        	}
		        	ResponseMessage::successor('Success', $data, $extra);
				} else {
					ResponseMessage::error('Category id not found!!');
				}
			}
		} catch (Exception $e) {
			log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
		}
	}

	public function get_subCategory_product(Request $request) {
		try {
			$rules = [
    			'id' => 'required',
    		];
    		$customeMessage = [
    			'id.required' => 'Please enter category id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$id = trim(strip_tags($request->id));
				if(Category::where('id', $id)->where('status',1)->exists()) {
	        		$category = array();
	                $about_product = array();
	                $i=1;
	                $data=[];
                    
	                if(Product::where('subcategory_id', $id)->where('status',1)->exists()){

                		$technicalquery = Technical::query();
	        			$productquery = Product::query();
	                    $productquery->join('categories', 'categories.id', 'products.subcategory_id')
                                ->join('employees', 'employees.id', 'products.company_id')
                                ->leftjoin('technicals',function($join){
                                    $join->on('technicals.name','=','products.technical_name');
                                });
                        if($request->get('userdetail')){
                            if($request->get('userdetail')->language=="hi"){
                                $technicalquery->select('*','name_hi as name');
                                $productquery->select('products.id', 'products.name_hi as name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info_hi as additional_info', 'employees.name_hi as dealer_name', 'employees.mobile', 'categories.name_hi as category_name','technicals.name_hi as technical_name');
                            } elseif($request->get('userdetail')->language=="gu"){
                                $technicalquery->select('*','name_gu as name');
                                $productquery->select('products.id', 'products.name_gu as name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info_gu as additional_info', 'employees.name_gu as dealer_name', 'employees.mobile', 'categories.name_gu as category_name','technicals.name_gu as technical_name');
                            } else{
                                $productquery->select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name');
                            }
                        } else{
                            $productquery->select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name');
                        }
                    $solutions = $productquery->where('products.subcategory_id', $id)->where('products.status',1)->where('employees.status',1)->where('categories.status',1)->get();
                    $technicals = $technicalquery->get();
						

	                    /*$solutions = Product::join('categories', 'categories.id', 'products.subcategory_id')
	                                        ->join('employees', 'employees.id', 'products.company_id')
	                                        ->Select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name')
	                                        ->where('products.subcategory_id', $id)
	                                        ->where('products.status',1)
	                                        ->where('employees.status',1)
	                                        ->where('categories.status',1)
	                                        ->get();*/
                        $technical = array();
                        if(count($solutions)>0){
                            foreach($solutions as $solution) {
                            	$attr = Product_attributes::where('product_id',$solution->id)->first();
                            	 $attrnew= Product_attributes::where('product_id',$solution->id)->get();
    	                        	foreach($attrnew as $key=>$new){
    	                        	 	 if(Cart::where('customer_id', $request->user_id)->where('product_id',$solution->id)->count()>0){
    	                        	           $cartcnt=Cart::where('customer_id',  $request->user_id)
                                                ->where('product_id', $solution->id)->count();
                                                
                                                 $carts = Cart::where('customer_id',  $request->user_id)
                                                ->where('product_id', $solution->id)
                                                ->get();
                                                foreach ($carts as $newk=>$cart) {
                                                    
                                                   if($attr->id == $cart->attribute_id){
                                                        $user_quantity = $cart->quantity;
                                                        
                                                    } else {
                                                        //$user_quantity = 0;
                                                      
                                                    }
                                                   
                                                  //  echo "<br>===>>>>".$user_quantity;	    
                                                }
                                              
                                            } else {
                                                $user_quantity = 0;
                                            }
                                            if($new->product_id==$solution->id)
                              {
                                 $getcnt=Cart::where('customer_id',  $request->user_id)
                                              ->where('product_id', $solution->id)
                                              ->where('attribute_id',$new->id)->count();
                                      if($getcnt>0)
                                      {
                                             $getcarts = Cart::where('customer_id',  $request->user_id)
                                      ->where('product_id', $solution->id)
                                      ->where('attribute_id',$new->id)
                                        ->first()->quantity;
                                      }
                                      else{
                                          $getcarts=0;
                                      }
                                  $testarr[$solution->id][]=['id'=>$new->id,'product_id'=>$new->product_id,'key'=>$new->key,'value'=>$new->value,'price'=>str_replace(",","",$new->price),'sale_price'=>str_replace(",","",$new->sale_price),'quantity'=>$new->quantity,'available_quantity'=>$new->available_quantity,'max_quantities'=>$new->max_quantities,'user_quantity'=>$getcarts];
                            
                                  
                              }
                                           // $testarr[$key]=['id'=>$new->id,'product_id'=>$new->product_id,'key'=>$new->key,'value'=>$new->value,'price'=>str_replace(",","",$new->price),'sale_price'=>str_replace(",","",$new->sale_price),'quantity'=>$new->quantity,'available_quantity'=>$new->available_quantity,'max_quantities'=>$new->max_quantities,'user_quantity'=>$user_quantity];
    	                        	 	}
    	                        	 	 $solution->unit = $testarr[$solution->id];
                                if($attr){
                                    $price= str_replace(",","",$attr->price);
                                    $sale_price= str_replace(",","",$attr->sale_price);
                                    $solution->price = $price;
                                    $solution->sale_price = $sale_price;
                                }
                                if($solution->additional_info) {
                                    $decode = json_decode($solution->additional_info);
                                    $solution->additional_info = $decode;
                                }else {
                                    $solution->additional_info = [];
                                }
                            }
    		                for($j=0; $j<count($technicals);$j++) {
    		                    for($i=0; $i<count($solutions);$i++) {
    		                        if($technicals[$j]->name==$solutions[$i]->technical_name){
    		                            array_push($technical, $solutions[$i]);
    		                        }
    		                    }  
    		                }
    	                    $data = $technical;
	                    }
		        	}
		        	ResponseMessage::success('Success', $data);
				} else {
					ResponseMessage::error('Category id not found!!');
				}
			}
		} catch (Exception $e) {
			log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
		}
	}
	public function get_subsubCategory_product(Request $request) {
		try {
			$rules = [
    			'id' => 'required',
    		];
    		$customeMessage = [
    			'id.required' => 'Please enter category id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$id = trim(strip_tags($request->id));
				if(Category::where('id', $id)->where('status',1)->exists()) {
	        		$category = array();
	                $about_product = array();
	                $i=1;
	                $data=[];
                    
	                if(Product::where('subsubcategory_id', $id)->where('status',1)->exists()){

                		$technicalquery = Technical::query();
	        			$productquery = Product::query();
	                    $productquery->join('categories', 'categories.id', 'products.subsubcategory_id')
                                ->join('employees', 'employees.id', 'products.company_id')
                                ->leftjoin('technicals',function($join){
                                    $join->on('technicals.name','=','products.technical_name');
                                });
                        if($request->get('userdetail')){
                            if($request->get('userdetail')->language=="hi"){
                                $technicalquery->select('*','name_hi as name');
                                $productquery->select('products.id', 'products.name_hi as name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info_hi as additional_info', 'employees.name_hi as dealer_name', 'employees.mobile', 'categories.name_hi as category_name','technicals.name_hi as technical_name');
                            } elseif($request->get('userdetail')->language=="gu"){
                                $technicalquery->select('*','name_gu as name');
                                $productquery->select('products.id', 'products.name_gu as name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info_gu as additional_info', 'employees.name_gu as dealer_name', 'employees.mobile', 'categories.name_gu as category_name','technicals.name_gu as technical_name');
                            } else{
                                $productquery->select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name');
                            }
                        } else{
                            $productquery->select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name');
                        }
                    $solutions = $productquery->where('products.subsubcategory_id', $id)->where('products.status',1)->where('employees.status',1)->where('categories.status',1)->get();
                    $technicals = $technicalquery->get();
						

	                    /*$solutions = Product::join('categories', 'categories.id', 'products.subcategory_id')
	                                        ->join('employees', 'employees.id', 'products.company_id')
	                                        ->Select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name')
	                                        ->where('products.subcategory_id', $id)
	                                        ->where('products.status',1)
	                                        ->where('employees.status',1)
	                                        ->where('categories.status',1)
	                                        ->get();*/
	                                        
                        $technical = array();
                        if(count($solutions)>0){
                            foreach($solutions as $solution) {
                            	$attr = Product_attributes::where('product_id',$solution->id)->first();
                            	 $attrnew= Product_attributes::where('product_id',$solution->id)->get();
    	                        	foreach($attrnew as $key=>$new){
    	                        	 	 if(Cart::where('customer_id', $request->user_id)->where('product_id',$solution->id)->count()>0){
    	                        	           $cartcnt=Cart::where('customer_id',  $request->user_id)
                                                ->where('product_id', $solution->id)->count();
                                                
                                                 $carts = Cart::where('customer_id',  $request->user_id)
                                                ->where('product_id', $solution->id)
                                                ->get();
                                                foreach ($carts as $newk=>$cart) {
                                                    
                                                   if($attr->id == $cart->attribute_id){
                                                        $user_quantity = $cart->quantity;
                                                        
                                                    } else {
                                                        //$user_quantity = 0;
                                                      
                                                    }
                                                   
                                                  //  echo "<br>===>>>>".$user_quantity;	    
                                                }
                                              
                                            } else {
                                                $user_quantity = 0;
                                            }
                                            if($new->product_id==$solution->id)
                              {
                                 $getcnt=Cart::where('customer_id',  $request->user_id)
                                              ->where('product_id', $solution->id)
                                              ->where('attribute_id',$new->id)->count();
                                      if($getcnt>0)
                                      {
                                             $getcarts = Cart::where('customer_id',  $request->user_id)
                                      ->where('product_id', $solution->id)
                                      ->where('attribute_id',$new->id)
                                        ->first()->quantity;
                                      }
                                      else{
                                          $getcarts=0;
                                      }
                                  $testarr[$solution->id][]=['id'=>$new->id,'product_id'=>$new->product_id,'key'=>$new->key,'value'=>$new->value,'price'=>str_replace(",","",$new->price),'sale_price'=>str_replace(",","",$new->sale_price),'quantity'=>$new->quantity,'available_quantity'=>$new->available_quantity,'max_quantities'=>$new->max_quantities,'user_quantity'=>$getcarts];
                            
                                  
                              }
                                           // $testarr[$key]=['id'=>$new->id,'product_id'=>$new->product_id,'key'=>$new->key,'value'=>$new->value,'price'=>str_replace(",","",$new->price),'sale_price'=>str_replace(",","",$new->sale_price),'quantity'=>$new->quantity,'available_quantity'=>$new->available_quantity,'max_quantities'=>$new->max_quantities,'user_quantity'=>$user_quantity];
    	                        	 	}
    	                        	 	 $solution->unit = $testarr[$solution->id];
                                if($attr){
                                    $price= str_replace(",","",$attr->price);
                                    $sale_price= str_replace(",","",$attr->sale_price);
                                    $solution->price = $price;
                                    $solution->sale_price = $sale_price;
                                }
                                if($solution->additional_info) {
                                    $decode = json_decode($solution->additional_info);
                                    $solution->additional_info = $decode;
                                }else {
                                    $solution->additional_info = [];
                                }
                            }
    		                for($j=0; $j<count($technicals);$j++) {
    		                    for($i=0; $i<count($solutions);$i++) {
    		                        if($technicals[$j]->name==$solutions[$i]->technical_name){
    		                            array_push($technical, $solutions[$i]);
    		                        }
    		                    }  
    		                }
    	                    $data = $technical;
	                    }
		        	}
		        	ResponseMessage::success('Success', $data);
				} else {
					ResponseMessage::error('Category id not found!!');
				}
			}
		} catch (Exception $e) {
			log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
		}
	}
}