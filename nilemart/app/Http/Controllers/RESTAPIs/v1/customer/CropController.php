<?php

namespace App\Http\Controllers\RestAPIs\v1\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\RestAPIs\v1\customer\Controllers;
use Log;
use Validator;
use App\Helper\MailHelper;
use App\Shop\Crops\Crop;
use App\Shop\Products\Product;
use App\Shop\CategoryProducts\CategoryProduct;
use App\Helper\ResponseMessage;
use App\Shop\Products\Product_attributes;
use App\Shop\Customers\Customer;
use App\Shop\TechnicalCategories\Technical;
use Auth;

class CropController extends Controller
{
    /**
     * @OA\Get(
     *      path="/crop",
     *      operationId="crop",
     *      tags={"Post"},
     *      summary="Get list of crop",

     * @OA\Response(
     *          response=200,
     *          description="successful crop"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     *       security={
     *           {"crop": {}}
     *       }
     *     )
     */
    public function selectCrop(Request $request) 
    {
    	try {
        	if(Crop::where('crop_category', null)->exists()){
                $query = Crop::query(); 
                if($request->get('userdetail')){
                    if($request->get('userdetail')->language=="hi"){
                        $query->select('id', 'name_hi as name', 'image');
                    } elseif($request->get('userdetail')->language=="gu"){
                        $query->select('id', 'name_gu as name', 'image');
                    } else{
                        $query->select('id', 'name', 'image');
                    }
                } else{
                    $query->select('id', 'name', 'image');
                }
        		$crops = $query->where('crop_category', null)->get();
        		ResponseMessage::success("Success", $crops);
        	} else {
        		ResponseMessage::error("Crop not found.");
        	}
    	} catch (Exception $e) {
    		log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
    	}
    }
    public function selectCropSolution(Request $request) {
    	try {
    		$rules = [
    			'id' => 'required',
    		];
    		$customeMessage = [
    			'id.required' => 'Please Select crop.',
    		];
    		$validator = Validator::make($request->all(),$rules,$customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	    		$id = trim(strip_tags($request->id));
	    		if(Crop::where('crop_category', $id)->exists()) {
                    $query = Crop::query();
                    if($request->get('userdetail')){
                        if($request->get('userdetail')->language=="hi"){
                            $query->select('id', 'name_hi as name', 'image');
                        } elseif($request->get('userdetail')->language=="gu"){
                            $query->select('id', 'name_gu as name', 'image');
                        } else{
                            $query->select('id', 'name', 'image');
                        }
                    } else{
                        $query->select('id', 'name', 'image');
                    }
                    $cropSolutions = $query->where('crop_category', $id)->get();
	    			if(count($cropSolutions)==0){
	    			    $cropSolutions=[];
	    			}
	    			ResponseMessage::success('Success', $cropSolutions);
	    		} else {
	    			ResponseMessage::error('Crop does not exist.');
	    		}
	    	}
    	} catch (Exception $e) {
    		log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
    	}
    }

    public function selectSolution(Request $request) {
    	try {
    		$rules = [
    			'id' => 'required',
    		];
    		$customeMessage = [
    			'id.required' => 'Please Select crop.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$id = trim(strip_tags($request->id));
                $category = array();
                $about_product = array();
                $data=[];
                $i=1;
	        	if(Product::where('crop_id', $id)->where('status',1)->exists()){

                    $technicalquery = Technical::query();
                    $productquery = Product::query();
                    $productquery->join('categories', 'categories.id', 'products.category_id')
                                ->join('employees', 'employees.id', 'products.company_id')
                                ->leftjoin('technicals',function($join){
                                    $join->on('technicals.name','=','products.technical_name');
                                });
                        
                        if($request->get('userdetail')){
                            if($request->get('userdetail')->language=="hi"){
                                $technicalquery->select('*','name_hi as name');
                                $productquery->select('products.id', 'products.name_hi as name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info_hi as additional_info', 'employees.name_hi as dealer_name', 'employees.mobile', 'categories.name_hi as category_name','technicals.name_hi as technical_name');
                            } elseif($request->get('userdetail')->language=="gu"){
                                $technicalquery->select('*','name_gu as name');
                                $productquery->select('products.id', 'products.name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info_gu as additional_info', 'employees.name_gu as dealer_name', 'employees.mobile', 'categories.name_gu as category_name','technicals.name_gu as technical_name');
                            } else{
                                $productquery->select('products.id', 'products.name_gu as name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name');
                            }
                        } else{
                            $productquery->select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name');
                        }
                    $solutions = $productquery->where('products.crop_id', $id)->where('products.status',1)->where('employees.status',1)->where('categories.status',1)->get();
                    $technicals = $technicalquery->get();
                    $technical = array();
                    if(count($solutions)>0){
                        foreach($solutions as $solution) {
                            $attr = Product_attributes::where('product_id',$solution->id)->first();
                            if($attr){
                                $price= str_replace(",","",$attr->price);
                                $sale_price= str_replace(",","",$attr->sale_price);
                                $solution->price = $price;
                                $solution->sale_price = $sale_price;
                            }
                            if($solution->additional_info) {
            	        		$decode = json_decode($solution->additional_info);
            	        		$solution->additional_info = $decode;
            	        		foreach($solution->additional_info as $info){
                	        		if(!$info->key){
                	        		    $solution->additional_info = [];
                	        		}
            	        		}
                            }else {
                                $solution->additional_info = [];
                            }
                        }
                        for($j=0; $j<count($technicals);$j++) {
                            for($i=0; $i<count($solutions);$i++) {
                                if($technicals[$j]->name==$solutions[$i]->technical_name){
                                    array_push($technical, $solutions[$i]);
                                }
                            }  
                        }
                        $data = $technical;
                    }
	        		ResponseMessage::success('Success', $data);
	        	} else {
	        		ResponseMessage::error('Solution does not exist.');
	        	}
	        }
    	} catch (Exception $e) {
    		log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
    	}
    }
}
