<?php

namespace App\Http\Controllers\Auth;

use App\Shop\Admins\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Shop\Offers\Offer;
use App\Shop\Products\Product;
use App\Shop\Categories\Category;
use Illuminate\Http\Request;
use App\Shop\Customers\Customer;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Helper\SMSHelper;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/accounts?tab=profile';

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * Login the admin
     *
     * @param LoginRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(LoginRequest $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $details = $request->only('email', 'password');
        $details['status'] = 1;
        if (auth()->attempt($details)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function loginform()
    {
        $data['offers'] = Offer::get();
        $data['products'] = Product::leftjoin('product_attribute','product_attribute.product_id','products.id')->get();
        // dd($data['products']);
       
        $data['categories'] = Category::join('employees', 'employees.id', 'categories.company_id')->Select('categories.id', 'categories.name','categories.status', 'categories.cover')
                            ->where('employees.status',1)
                            ->where('parent_id', null)
                            ->get();
        return view('auth.login')->with('front-login')->with($data);
    }
    
    public function store(Request $request)
    {
        $rules = [
                'mobile' => 'required|numeric|digits_between:8,15',
            ];
            $customeMessage = [
                'mobile.required' => 'Please enter mobile',
                'mobile.numeric' => 'Please enter number',
                'mobile.digits_between' => 'Please enter only digits and maximum 15 digits.',
            ];
            $validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
                 return back()->withInput()->withErrors($validator->errors());
            } else {
            $mobile = trim(strip_tags($request->mobile));
            $country_code = 91;
            $SMSresponse = SMSHelper::sendOTP($country_code, $mobile);
            $otp = $SMSresponse['otp'];
            $otpSave = new Customer();
            $otpSave->otp = $otp;
            $otpSave->mobile = $mobile;
            $otpSave->save();
            
            return redirect()->back()->with('result',$mobile);
        }
    }

    public function register()
    {
        return view('auth.register')->with('front-register');
    }
    
    public function loginstore(Request $request)
    {
        $rules = [
            'otp' => 'required',
            ];
            $customeMessage = [
                'otp.required' => 'Please enter OTP',
            ];
            $validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
                 return back()->withInput()->withErrors($validator->errors());
            } else {
                $otp = trim(strip_tags($request->otp));
                $customer = Customer::where('otp', $otp)->first();
                if($customer){
                    if($otp==$customer->otp) {
                            auth()->login($customer);
                        return redirect()->to('/');
                        } else {
                            return back()->with([
                        'message' => 'OTP not match, please try again'
                    ]);
                        }
                }else{
                    return back()->with([
                        'message' => 'OTP not match, please try again'
                    ]);
                }
            }
    }
    
    public function destroy()
    {
        auth()->logout();
        
        return redirect()->to('/');
    }
}
