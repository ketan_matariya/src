<?php

namespace App\Shop\Addresses;

use Illuminate\Database\Eloquent\Model;

class OrderAddressDriver extends Model
{
    public $table = "order_address_driver";
	public $timestamps = false;
    public $fillable = [
        'address_id',
        'driver_id',
        'order_id'
    ];
}
