<?php

namespace App\Shop\Addresses;

use Illuminate\Database\Eloquent\Model;

class Address_1 extends Model
{
	public $table = "address";
	public $timestamps = false;
    public $fillable = [
        'user_id',
        'address',
        'email',
        'mobile',
        'name',
        'type',
        'landmark',
        'default'
    ];
}
