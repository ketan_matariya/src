<?php

namespace App\Shop\OrderPurchaseDetail;

use App\Shop\AttributeValues\AttributeValue;
use App\Shop\Products\Product;
use Illuminate\Database\Eloquent\Model;

class OrderPurchaseDetail extends Model
{
    protected $fillable = [
        'order_purchase_id',
        'category_id',
        'subcategory_id',
        'subsubcategory_id',
        'product_id',
        'available_quantity',
        'quantity',
        'unit_key',
        'unit_val',
        'price',
        'sale_price',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function attributesValues()
    {
        return $this->belongsToMany(AttributeValue::class);
    }
}
