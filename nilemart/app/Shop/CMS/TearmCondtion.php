<?php

namespace App\Shop\CMS;

use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;

class TearmCondtion extends Model
{
    use NodeTrait;

    protected $table = "tearm_condition";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'status'
    ];
}
