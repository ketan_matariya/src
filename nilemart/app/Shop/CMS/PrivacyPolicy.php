<?php

namespace App\Shop\CMS;

use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;

class PrivacyPolicy extends Model
{
    use NodeTrait;

    protected $table = "privacy_policy";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'status'
    ];
}
