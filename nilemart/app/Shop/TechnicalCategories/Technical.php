<?php

namespace App\Shop\TechnicalCategories;

use Illuminate\Database\Eloquent\Model;

class Technical extends Model
{
    protected $fillable = [
        'name'
    ];
}
