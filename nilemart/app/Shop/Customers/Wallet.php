<?php

namespace App\Shop\Customers;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    public $table = "wallet";
    public $timestamps = false;
}
