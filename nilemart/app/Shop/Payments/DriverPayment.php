<?php

namespace App\Shop\Payments;

use Illuminate\Database\Eloquent\Model;

class DriverPayment extends Model
{
    public $table = 'driver_payment';
   	protected $fillable = [ 
	   	'driver_id',
	   	'payment'
   	];
}
