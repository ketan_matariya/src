<?php

namespace App\Shop\OrderPurchase;

use App\Shop\AttributeValues\AttributeValue;
use App\Shop\Products\Product;
use Illuminate\Database\Eloquent\Model;

class OrderPurchase extends Model
{
    protected $fillable = [
        'order_no',
        'product_oldnew',
        'email',
        'mobile',
        'address',
        'gstno',
        'seller_name',
        'invoice_date',
        'invoice_no',
     //   'category_id',
      //  'subcategory_id',
      //  'subsubcategory_id',
      //  'product_name',
      //  'quantity',
       // 'price',
       // 'sale_price',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function attributesValues()
    {
        return $this->belongsToMany(AttributeValue::class);
    }
}
