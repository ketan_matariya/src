<?php

namespace App\Shop\CountriesData;

use Illuminate\Database\Eloquent\Model;

class CountryData extends Model
{
	public $table = 'countryData';
    public $fillable = [
        'STCode',
        'DTCode',
        'DTName',
        'SDTCode',
        'SDTName',
        'TVCode',
        'Name'
    ];

    public $timestamps = false;
}
