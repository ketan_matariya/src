<?php

namespace App\Shop\Carts;

use Illuminate\Database\Eloquent\Model;

use App\Shop\Products\Product;
use App\Shop\Products\Productattribute;
class Cart extends Model
{
    public $table = "cart";
    public $primaryKey = "id";
    public $timestamps = false;
    protected $fillable = [
        'product_id',
        'customer_id',
        'quantity',
        'product_size',
        'total_price',
        'shipping_price',
        'offer_id'
    ];
    public function products(){
        return $this->hasOne(Product::class,'id', 'product_id');
    }
    public function productsattribute(){
        return $this->hasOne(Productattribute::class,'id', 'attribute_id');
    }
}
