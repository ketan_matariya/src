<?php

namespace App\Shop\Offers;

use Illuminate\Database\Eloquent\Model;

class OfferProduct extends Model
{
    public $timestamps = false;
    public $table = 'offer_product';
    protected $fillable = [
    	'offer_id',
    	'product_id',
    	'attribute_id',
    ];
}
