<?php

namespace App\Shop\Products\Requests;

use App\Shop\Base\BaseFormRequest;

class CreateProductRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'sku' => ['required'],
            // 'name' => ['required', 'unique:products'],
            // 'quantity' => ['required', 'numeric'],
            'category_id' => ['required'],
            // 'price' => ['required', 'numeric'],
            // 'hsn_code'=>['required','unique:products'],
            'cover' => ['required', 'file', 'image:png,jpeg,jpg,gif']
        ];
    }
}
