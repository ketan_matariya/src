<?php

namespace App\Shop\Postcodes;

use Illuminate\Database\Eloquent\Model;

class Postcode extends Model
{
    public $table = "postcodes";
	public $timestamps = false;
    protected $fillable = [
        'postcode'
    ];
}
