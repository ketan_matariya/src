<?php

namespace App\Shop\Crops\Exceptions;

class UpdateCropInvalidArgumentException extends \Exception
{
}
